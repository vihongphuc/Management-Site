﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    public enum TransferType
    {
        Deposit = 0,
        DepositTo = 1,
        Withdraw = 2,
        WithdrawFrom = 3
    }
}
