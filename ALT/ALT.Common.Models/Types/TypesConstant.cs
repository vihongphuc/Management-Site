﻿using System.Linq;

namespace ALT.Common.Models
{
	public static class AuditTypeArray
	{
        public static AuditType[] Values = System.Enum.GetValues(typeof(AuditType))
													  .Cast<AuditType>()
													  .OrderBy(c => c)
													  .ToArray();
        public static string[] Strings = System.Enum.GetValues(typeof(AuditType))
													 .Cast<AuditType>()
													 .OrderBy(c => c)
													 .Select(c => c.ToString())
													 .ToArray();
	}

	public static class AuditTypeString
	{
	    public static System.Collections.Generic.IDictionary<string, AuditType> Values = System.Enum.GetValues(typeof(AuditType))
													                                              .Cast<AuditType>()
													                                              .ToDictionary(d => d.ToString(), d => d);

		public const string None = "None";
		public const string SignIn = "SignIn";
		public const string SignInFail = "SignInFail";
		public const string SignOut = "SignOut";
		public const string Token = "Token";
		public const string Retrieval = "Retrieval";
		public const string Add = "Add";
		public const string Change = "Change";
		public const string Balance = "Balance";
		public const string Transaction = "Transaction";
		public const string Deposit = "Deposit";
		public const string DepositTo = "DepositTo";
		public const string Withdraw = "Withdraw";
		public const string WithdrawFrom = "WithdrawFrom";
	}
	public static class BetSettingTypeArray
	{
        public static BetSettingType[] Values = System.Enum.GetValues(typeof(BetSettingType))
													  .Cast<BetSettingType>()
													  .OrderBy(c => c)
													  .ToArray();
        public static string[] Strings = System.Enum.GetValues(typeof(BetSettingType))
													 .Cast<BetSettingType>()
													 .OrderBy(c => c)
													 .Select(c => c.ToString())
													 .ToArray();
	}

	public static class BetSettingTypeString
	{
	    public static System.Collections.Generic.IDictionary<string, BetSettingType> Values = System.Enum.GetValues(typeof(BetSettingType))
													                                              .Cast<BetSettingType>()
													                                              .ToDictionary(d => d.ToString(), d => d);

		public const string BetLimit = "BetLimit";
		public const string MaxWinLimit = "MaxWinLimit";
		public const string MinLossLimit = "MinLossLimit";
		public const string DailyAutoReset = "DailyAutoReset";
	}
	public static class GameBudgetTypeArray
	{
        public static GameBudgetType[] Values = System.Enum.GetValues(typeof(GameBudgetType))
													  .Cast<GameBudgetType>()
													  .OrderBy(c => c)
													  .ToArray();
        public static string[] Strings = System.Enum.GetValues(typeof(GameBudgetType))
													 .Cast<GameBudgetType>()
													 .OrderBy(c => c)
													 .Select(c => c.ToString())
													 .ToArray();
	}

	public static class GameBudgetTypeString
	{
	    public static System.Collections.Generic.IDictionary<string, GameBudgetType> Values = System.Enum.GetValues(typeof(GameBudgetType))
													                                              .Cast<GameBudgetType>()
													                                              .ToDictionary(d => d.ToString(), d => d);

		public const string ProfitPoint = "ProfitPoint";
		public const string WinPoint = "WinPoint";
		public const string Budget = "Budget";
	}
	public static class PersonStatusTypeArray
	{
        public static PersonStatusType[] Values = System.Enum.GetValues(typeof(PersonStatusType))
													  .Cast<PersonStatusType>()
													  .OrderBy(c => c)
													  .ToArray();
        public static string[] Strings = System.Enum.GetValues(typeof(PersonStatusType))
													 .Cast<PersonStatusType>()
													 .OrderBy(c => c)
													 .Select(c => c.ToString())
													 .ToArray();
	}

	public static class PersonStatusTypeString
	{
	    public static System.Collections.Generic.IDictionary<string, PersonStatusType> Values = System.Enum.GetValues(typeof(PersonStatusType))
													                                              .Cast<PersonStatusType>()
													                                              .ToDictionary(d => d.ToString(), d => d);

		public const string Active = "Active";
		public const string Suspended = "Suspended";
		public const string Disabled = "Disabled";
	}
	public static class PersonTypeArray
	{
        public static PersonType[] Values = System.Enum.GetValues(typeof(PersonType))
													  .Cast<PersonType>()
													  .OrderBy(c => c)
													  .ToArray();
        public static string[] Strings = System.Enum.GetValues(typeof(PersonType))
													 .Cast<PersonType>()
													 .OrderBy(c => c)
													 .Select(c => c.ToString())
													 .ToArray();
	}

	public static class PersonTypeString
	{
	    public static System.Collections.Generic.IDictionary<string, PersonType> Values = System.Enum.GetValues(typeof(PersonType))
													                                              .Cast<PersonType>()
													                                              .ToDictionary(d => d.ToString(), d => d);

		public const string Admin = "Admin";
		public const string SuperShareHolder = "SuperShareHolder";
		public const string ShareHolder = "ShareHolder";
		public const string SuperSenior = "SuperSenior";
		public const string Senior = "Senior";
		public const string SuperMaster = "SuperMaster";
		public const string Master = "Master";
		public const string SuperAgent = "SuperAgent";
		public const string Agent = "Agent";
		public const string Member = "Member";
		public const string SubAccount = "SubAccount";
		public const string Robot = "Robot";
	}
	public static class PostitionTakingTypeArray
	{
        public static PostitionTakingType[] Values = System.Enum.GetValues(typeof(PostitionTakingType))
													  .Cast<PostitionTakingType>()
													  .OrderBy(c => c)
													  .ToArray();
        public static string[] Strings = System.Enum.GetValues(typeof(PostitionTakingType))
													 .Cast<PostitionTakingType>()
													 .OrderBy(c => c)
													 .Select(c => c.ToString())
													 .ToArray();
	}

	public static class PostitionTakingTypeString
	{
	    public static System.Collections.Generic.IDictionary<string, PostitionTakingType> Values = System.Enum.GetValues(typeof(PostitionTakingType))
													                                              .Cast<PostitionTakingType>()
													                                              .ToDictionary(d => d.ToString(), d => d);

		public const string Upline = "Upline";
		public const string CurrentMax = "CurrentMax";
	}
	public static class TransactionStatusTypeArray
	{
        public static TransactionStatusType[] Values = System.Enum.GetValues(typeof(TransactionStatusType))
													  .Cast<TransactionStatusType>()
													  .OrderBy(c => c)
													  .ToArray();
        public static string[] Strings = System.Enum.GetValues(typeof(TransactionStatusType))
													 .Cast<TransactionStatusType>()
													 .OrderBy(c => c)
													 .Select(c => c.ToString())
													 .ToArray();
	}

	public static class TransactionStatusTypeString
	{
	    public static System.Collections.Generic.IDictionary<string, TransactionStatusType> Values = System.Enum.GetValues(typeof(TransactionStatusType))
													                                              .Cast<TransactionStatusType>()
													                                              .ToDictionary(d => d.ToString(), d => d);

		public const string Active = "Active";
		public const string Disable = "Disable";
	}
	public static class TransactionTypeArray
	{
        public static TransactionType[] Values = System.Enum.GetValues(typeof(TransactionType))
													  .Cast<TransactionType>()
													  .OrderBy(c => c)
													  .ToArray();
        public static string[] Strings = System.Enum.GetValues(typeof(TransactionType))
													 .Cast<TransactionType>()
													 .OrderBy(c => c)
													 .Select(c => c.ToString())
													 .ToArray();
	}

	public static class TransactionTypeString
	{
	    public static System.Collections.Generic.IDictionary<string, TransactionType> Values = System.Enum.GetValues(typeof(TransactionType))
													                                              .Cast<TransactionType>()
													                                              .ToDictionary(d => d.ToString(), d => d);

		public const string Game = "Game";
		public const string LuckyDraw = "LuckyDraw";
		public const string Prize = "Prize";
	}
	public static class TransferTypeArray
	{
        public static TransferType[] Values = System.Enum.GetValues(typeof(TransferType))
													  .Cast<TransferType>()
													  .OrderBy(c => c)
													  .ToArray();
        public static string[] Strings = System.Enum.GetValues(typeof(TransferType))
													 .Cast<TransferType>()
													 .OrderBy(c => c)
													 .Select(c => c.ToString())
													 .ToArray();
	}

	public static class TransferTypeString
	{
	    public static System.Collections.Generic.IDictionary<string, TransferType> Values = System.Enum.GetValues(typeof(TransferType))
													                                              .Cast<TransferType>()
													                                              .ToDictionary(d => d.ToString(), d => d);

		public const string Deposit = "Deposit";
		public const string DepositTo = "DepositTo";
		public const string Withdraw = "Withdraw";
		public const string WithdrawFrom = "WithdrawFrom";
	}
}