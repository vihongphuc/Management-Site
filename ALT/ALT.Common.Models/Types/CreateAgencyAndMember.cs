﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    public enum CreateAgencyAndMember
    {
        CreateAgency = 0,
        CreateMember = 1,
    }
}
