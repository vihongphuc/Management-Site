﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    public enum PersonType
    {
        Admin = 0,
        SuperShareHolder = 1,
        ShareHolder = 2,
        SuperSenior = 3,
        Senior = 4,
        SuperMaster = 5,
        Master = 6,
        SuperAgent = 7,
        Agent = 8,
        Member = 9,

        SubAccount = 10,
        Robot = 11
    }
}
