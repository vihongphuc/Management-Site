﻿﻿using System.Linq;

<#@ template debug="false" hostspecific="true" language="C#" #>
<#@ assembly name="System.Core" #>
<#@ import namespace="System.Linq" #>
<#@ import namespace="System.Text" #>
<#@ import namespace="System.IO" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ output extension=".cs" #>
namespace ALT.Common.Models
{
<#
	var files = Directory.GetFiles(Host.ResolvePath(""), "*.cs").Where(c => c.EndsWith("Type.cs") || c.EndsWith("Key.cs"));
	foreach (var f in files)
	{
		var lines = File.ReadAllLines(f);
		bool stateOpen = false;
		string enType = null;
		bool nextFlag = false;
		foreach (var line in lines)
		{
			if (line.Trim().StartsWith("[Flags]"))
			{
				nextFlag = true;
			}
			else if (line.Trim().StartsWith("public enum "))
			{
				stateOpen = true;
				var nLine = line.Trim().Substring("public enum ".Length);
				enType = nLine;

				if (nextFlag)
				{
					nextFlag = false;
#>
	public static class <#= nLine #>Helper
	{
		public static <#= nLine #>[] GetFlagged(this <#= nLine #> value)
		{
			return <#= nLine #>Array.Values
							.Where(d => d != <#= nLine #>.All &&
									   (d & value) > 0)
							.ToArray();
		}
		public static string[] GetFlaggedString(this <#= nLine #> value)
		{
			return <#= nLine #>Array.Values
							.Where(d => d != <#= nLine #>.All &&
									   (d & value) > 0)
                            .Select(d => d.ToString())
							.ToArray();
		}
	}
<#
				}
#>
	public static class <#= nLine #>Array
	{
        public static <#= nLine #>[] Values = System.Enum.GetValues(typeof(<#= nLine #>))
													  .Cast<<#= nLine #>>()
													  .OrderBy(c => c)
													  .ToArray();
        public static string[] Strings = System.Enum.GetValues(typeof(<#= nLine #>))
													 .Cast<<#= nLine #>>()
													 .OrderBy(c => c)
													 .Select(c => c.ToString())
													 .ToArray();
	}

	public static class <#= nLine #>String
	{
	    public static System.Collections.Generic.IDictionary<string, <#= nLine #>> Values = System.Enum.GetValues(typeof(<#= nLine #>))
													                                              .Cast<<#= nLine #>>()
													                                              .ToDictionary(d => d.ToString(), d => d);

<#
			}
			else if (line.Trim() == "}" && stateOpen)
			{
				stateOpen = false;
#>
	}
<#
			}
			else if (line.Trim().Length > 1 && stateOpen)
			{
#>
		public const string <#= line.Trim().Split(' ')[0].Replace(",", "") #> = "<#= line.Trim().Split(' ')[0].Replace(",", "") #>";
<#
			}
		}
	}
#>
}