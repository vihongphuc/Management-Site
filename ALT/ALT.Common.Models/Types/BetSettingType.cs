﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    public enum BetSettingType
    {
        BetLimit = 0,
        MaxWinLimit = 1,
        MinLossLimit = 2,
        DailyAutoReset = 3
    }
}
