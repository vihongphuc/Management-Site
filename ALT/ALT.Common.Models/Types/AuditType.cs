﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    public enum AuditType
    {
        None,
        SignIn,
        SignInFail,
        SignOut,

        Token,
        Retrieval,
        Add,
        Change,
        Balance,
        Transaction,

        Deposit,
        DepositTo,
        Withdraw,
        WithdrawFrom
    }
}
