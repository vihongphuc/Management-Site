﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    public enum PersonStatusType
    {
        Active = 0,
        Suspended = 1,
        Disabled = 2
    }
}
