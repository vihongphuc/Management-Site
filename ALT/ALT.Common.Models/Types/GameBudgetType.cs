﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    public enum GameBudgetType
    {
        ProfitPoint = 0,
        WinPoint = 1,
        Budget = 2,
    }
}
