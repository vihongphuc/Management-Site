﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum TransactionType
    {
        Game,        
        LuckyDraw,
        Prize
    }
}
