﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    public class TransactionDetailResp
    {
        public string PersonOCode { get; set; }
        public string GameOCode { get; set; }
        public string GameCode { get; set; }
        public string GameCategory { get; set; }
        public string GameName { get; set; }
        public string Username { get; set; }
        public string CurrencyCode { get; set; }
        public string GameTransactionOCode { get; set; }
        public string Type { get; set; }
        public decimal Amount { get; set; }
        public decimal Result { get; set; }
        public decimal StartBalance { get; set; }
        public decimal EndBalance { get; set; }
        public decimal PointRate { get; set; }
        public string Details { get; set; }
        public DateTime Time { get; set; }
    }
}
