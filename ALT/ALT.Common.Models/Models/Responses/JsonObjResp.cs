﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models.Models
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    public class JsonObjResp
    {
        public int Status { get; set; } // will be HttpRespone.Status
        public string Message { get; set; }
        public string ResultObj { get; set; }
    }
}
