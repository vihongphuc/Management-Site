﻿using ALT.Common.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    //using Newtonsoft.Json;
    //[JsonObject]
    [Serializable]
    public class AuthenticationResp
    {
        public string Token { get; set; }
        public string Username { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Displayname { get; set; }
        public string PersonCode { get; set; }
        public PersonType Type { get; set; }
        public string UplineCode { get; set; }
        public PersonType? UplineType { get; set; }
        public Dictionary<string, decimal> Availables { get; set; }
        public bool IsSubAccount { get; set; }
        public bool FixedCurrency { get; set; }
        public bool CreateAgency { get; set; }
        public bool CreateMember { get; set; }
        public PersonStatusType PersonStatus { get; set; }
        public List<CurrencyResp> Currencies { get; set; }
    }
}
