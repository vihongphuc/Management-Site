﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    //using Newtonsoft.Json;
    //[JsonObject]
    //[Serializable]
    public class GameAccessResp
    {
        public string SessionCode { get; set; }
        public string Username { get; set; }
        public decimal PointRate { get; set; }
        public long Points { get; set; }
        public string TokenMobile { get; set; }
        public string Secret { get; set; }
        public string IPHashKey { get; set; }
        public string CurrencyCode { get; set; }
    }
}
