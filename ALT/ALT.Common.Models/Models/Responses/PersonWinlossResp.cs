﻿using ALT.Common.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    using Newtonsoft.Json;
    [JsonObject]
    [Serializable]
    public class PersonWinlossResp
    {
        public string OCode { get; set; }
        public string Username { get; set; }
        public string CurrencyCode { get; set; }

        public string Displayname { get; set; }
        public PersonType Type { get; set; }
        public decimal Amount { get; set; }
        public decimal Result { get; set; }
        public decimal GrossComission { get; set; }

        public Dictionary<PersonType, decimal> PositionTakings { get; set; }
        public Dictionary<PersonType, decimal> Commissions { get; set; }
    }
}
