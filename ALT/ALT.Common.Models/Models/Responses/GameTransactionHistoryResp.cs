﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    public class GameHistoryResp
    {
        public string GameOCode { get; set; }
        public string GameCode { get; set; }
        public string GameName { get; set; }

        public string GameTransactionOCode { get; set; }
        public string Type { get; set; }
        public decimal Amount { get; set; }
        public decimal Result { get; set; }
        public decimal StartBalance { get; set; }
        public decimal EndBalance { get; set; }

        public DateTime Time { get; set; }
        public long Ticks { get; set; }
        public decimal PointRate { get; set; }

        public string Description { get; set; }
        public string Details { get; set; }
    }
}
