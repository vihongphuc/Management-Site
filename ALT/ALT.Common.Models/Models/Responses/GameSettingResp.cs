﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    public class GameSettingResp
    {
        public string GameCode { get; set; }
        public string Publisher { get; set; }
        public string Platform { get; set; }
        public string DowloadURL { get; set; }
        public string GameHash { get; set; }
        public string GameKey { get; set; }
        public string GameHeader { get; set; }
        public bool Enable { get; set; }
        public bool IsHot { get; set; }
        public bool IsNew { get; set; }
        public string Rate { get; set; }
        public string Type { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public int Port { get; set; }
        public string Domain { get; set; }
    }
}
