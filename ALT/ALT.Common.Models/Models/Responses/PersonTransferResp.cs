﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models.Models
{
    public class PersonTransferResp : HttpRespone
    {
        public string Username { get; set; }
        public decimal CurrentAmount { get; set; }
        public TransferType TransferType { get; set; }
        public decimal UplineAmount { get; set; }
        public string CurrencyCode { get; set; }
    }
    public class HttpRespone
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public string ResultObj { get; set; }
    }
}
