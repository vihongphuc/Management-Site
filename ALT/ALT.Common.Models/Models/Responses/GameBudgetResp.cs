﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    using Newtonsoft.Json;
    [JsonObject]
    [Serializable]
    public class GameBudgetResp : GameBudget
    {
        public string GameName { get; set; }
    }
}
