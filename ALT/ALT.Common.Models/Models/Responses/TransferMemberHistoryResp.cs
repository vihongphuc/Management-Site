﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models.Models
{
    public class TransferMemberHistoryResp
    {
        public DateTime Date { get; set; }
        public string Transaction { get; set; }
        public string Type { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyCode{ get; set; }
    }
}
