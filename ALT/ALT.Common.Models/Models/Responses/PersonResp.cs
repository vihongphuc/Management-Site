﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models.Models
{
    public class PersonResp
    {
        public string OCode { get; set; }
        public string UplineOCode { get; set; }
        public string Username { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string DisplayName { get; set; }
        public DateTime? DOB { get; set; }
        public string Password { get; set; }
        public PersonStatusType Status { get; set; }
        public PersonType Type { get; set; }
        public string[] Currencies { get; set; }
        public string LastLoginIP { get; set; }
        public DateTime LastLogin { get; set; }
        public string Email { get; set; }
        public string LimitIP { get; set; }
        public decimal Available { get; set; }
        public decimal MaxCredit { get; set; }

        public Dictionary<string, decimal> Availables { get; set; }
        public Dictionary<string, decimal> MaxCredits { get; set; }

        public decimal TotalDownline { get; set; }

        public decimal DepositAmount { get; set; }
        public decimal WithdrawAmount { get; set; }
    }


}
