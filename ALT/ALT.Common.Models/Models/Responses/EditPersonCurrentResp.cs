﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models.Models
{
    public class EditPersonCurrentResp
    {
        public string OCode { get; set; }
        public string Username { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string DisplayName { get; set; }
        public string Password { get; set; }
        public DateTime? DOB { get; set; }
        public string Address { get; set; }
        public PersonType Type { get; set; }
        public PersonStatusType Status { get; set; }
        public string CountryCode { get; set; }
        public string[] Currencies { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool External { get; set; }
        public bool AutoTransfer { get; set; }
        public string LimitIP { get; set; }
        public int GroupID { get; set; }
        public string UplineOcode { get; set; }
        public bool FixedCurrency { get; set; }
        public bool CreateAgency { get; set; }
        public bool CreateMember { get; set; }

        public IList<PersonMaxCreditReq> PersonMaxCredits { get; set; }
        public IList<PersonBalanceReq> PersonBalances { get; set; }
        public IList<PersonBetSettingReq> BetSettings { get; set; }
        public IList<PositionTakingReq> PositionTakings { get; set; }

    }
}
