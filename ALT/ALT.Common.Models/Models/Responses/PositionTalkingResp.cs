﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    public class PositionTalkingResp
    {
        public string PersonOCode { get; set; }
        public string Currency { get; set; }
        public string Type { get; set; }
        public decimal Value { get; set; }
    }
}
