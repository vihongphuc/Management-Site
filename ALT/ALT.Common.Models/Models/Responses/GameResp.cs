﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    public class GameResp
    {
        public string OCode { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string GroupOCode { get; set; }
        public string GroupName { get; set; }
        public string Type { get; set; }
        public bool Enable { get; set; }

        public List<GameSettingResp> Settings { get; set; }
    }
}
