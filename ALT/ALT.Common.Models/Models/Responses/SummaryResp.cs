﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models.Models
{
    public class SummaryResp
    {
        public string PersonOCode { get; set; }
        public string CurrentNamePerson { get; set; }
        public string Type { get; set; }
        public string Username { get; set; }
        public DateTime Time { get; set; }
        public string GameName { get; set; }
        public string GameType { get; set; }
        public string GameSites { get; set; }
        public string CurrencyCode { get; set; }

        public decimal BetAmount { get; set; }
        public decimal WinAmount { get; set; }
        public decimal TotalWinLossAmount { get; set; }

        public decimal BetAmountRateCurrencyCode { get; set; }
        public decimal WinAmountRateCurrencyCode { get; set; }
        public decimal TotalWinLossAmountRateCurrencyCode { get; set; }

    }
}
