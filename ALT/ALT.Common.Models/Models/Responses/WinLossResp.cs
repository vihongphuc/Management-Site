﻿using System;

namespace ALT.Common.Models.Models
{
    public class WinLossResp
    {
        public string PersonOCode { get; set; }
        public string Username { get; set; }
        public DateTime Time { get; set; }
        public string CurrencyCode { get; set; }
        public int TotalUsername { get; set; }
        public int TotalNewUserBet { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TotalResult { get; set; }
        public decimal TotalWinLoss{ get; set; }

    }
}
