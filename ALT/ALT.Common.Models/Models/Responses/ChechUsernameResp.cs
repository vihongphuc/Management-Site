﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models.Models
{
    public class ChechUsernameResp
    {
        public bool IsCheck { get; set; }
        public string Username { get; set; }
    }
}
