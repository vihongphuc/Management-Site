﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Queue
{
    public class QueueDataPacket
    {
        public string ExchangeName { get; set; }
        public string QueueName { get; set; }
        public IDictionary<string, object> Headers { get; set; }
        public object Data { get; set; }
    }
}
