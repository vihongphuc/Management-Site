﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Queue
{
    using RabbitMQ.Client;
    public interface IQueueConnection : IDisposable
    {
        IQueuePublisher<T> CreatePublisher<T>(string queueName, string exchangeName = null);
        IQueuePublisher<T> CreateFanoutPublisher<T>(string exchangeName);
        IQueuePublisher<T> CreateHeaderPublisher<T>(string exchangeName);
        IQueuePublisher<T> CreateTopicPublisher<T>(string exchangeName, string pattern);
        IDisposable Subscribe<T>(string queueName, QueueProcessor<T> processor);
        IConnection QueueConnection();
    }

    public delegate Task<QueueProcessResult> QueueProcessor<T>(T data, IDictionary<string, object> headers);
}
