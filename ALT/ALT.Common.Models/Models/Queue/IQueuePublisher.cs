﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Queue
{
    public interface IQueuePublisher<T> : IDisposable
    {
        bool IsOpen { get; }
        Task Publish(T data, IDictionary<string, object> headers);
    }
}
