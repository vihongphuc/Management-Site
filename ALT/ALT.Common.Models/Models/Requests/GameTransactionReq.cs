﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    using Newtonsoft.Json;
    [JsonObject]
    [Serializable]
    public class GameTransactionReq
    {
        public string SessionOCode { get; set; }
        public bool OnlyUpdateAck { get; set; }

        public string GameTransactionOCode { get; set; }
        public string GameCode { get; set; }

        public DateTime Time { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string Details { get; set; }

        public string GroupCode { get; set; }

        public decimal StartPointBalance { get; set; }
        public decimal EndPointBalance { get; set; }
        public decimal PointStake { get; set; }
        public decimal PointResult { get; set; }
        
        public int RetryCounter { get; set; }
    }
}
