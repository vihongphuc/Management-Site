﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    using Newtonsoft.Json;
    using System.ComponentModel.DataAnnotations;

    [JsonObject]
    [Serializable]
    public class GameTransactionHistoryReq
    {
        [Required]
        public string Username { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public string GameOCode { get; set; }
        public int PageIndex { get; set; }
        public int ItemPerPage { get; set; }
    }
}
