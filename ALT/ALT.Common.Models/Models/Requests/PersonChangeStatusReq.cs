﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    public class PersonChangeStatusReq
    {
        public PersonStatusType Status { get; set; }
    }
}
