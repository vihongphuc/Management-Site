﻿using System;
namespace ALT.Common.Models
{
    using Newtonsoft.Json;
    using System.ComponentModel.DataAnnotations;

    [JsonObject]
    [Serializable]
    public class TransactionDetailReq
    {
        [Required]
        public string PersonOCode { get; set; }
        public string[] BetTypeItemOCode { get; set; }
        public string GameOCode { get; set; }

        public string CurrencyCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public int PageIndex { get; set; }
        public int ItemPerPage { get; set; }

    }
}
