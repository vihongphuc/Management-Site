﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    using Newtonsoft.Json;
    [JsonObject]
    [Serializable]
    public class PersonBalanceReq
    {
        [Required]
        public string PersonOCode { get; set; }
        public string CurrencyCode { get; set; }
        public decimal? Available { get; set; }
    }
}
