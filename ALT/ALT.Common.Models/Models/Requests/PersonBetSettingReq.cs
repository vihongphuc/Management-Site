﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    using Newtonsoft.Json;
    [JsonObject]
    [Serializable]
    public class PersonBetSettingReq
    {
        [Required]
        public string PersonOCode { get; set; }
        public string CurrencyCode { get; set; }
        public string BetTypeItemOcode { get; set; }
        public BetSettingType SettingType { get; set; }
        public decimal? Value { get; set; }
        public string ValueString { get; set; }
    }
}
