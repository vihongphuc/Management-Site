﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    using Newtonsoft.Json;
    [JsonObject]
    [Serializable]
    public class PersonTransferReq
    {
        public string CurrencyCode { get; set; }
        public TransferType Type { get; set; }
        public decimal Amount { get; set; }
    }
}
