﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    using Newtonsoft.Json;
    [JsonObject]
    [Serializable]
    public class PersonBalanceUpdateRequest
    {
        public string PersonOCode { get; set; }
        public string CurrencyCode { get; set; }
        public string SessionOCode { get; set; }
        
        public decimal? Outstanding { get; set; }
        public decimal? EGameBalance { get; set; }
    }
}
