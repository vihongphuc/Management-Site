﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    using Newtonsoft.Json;
    [JsonObject]
    [Serializable]
    public class AuthenticationReq
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public bool NotExpired { get; set; }
        public string  Platform { get; set; }
        public string IP { get; set; }
        public string Publisher { get; set; }
        public string Data { get; set; }
    }
}
