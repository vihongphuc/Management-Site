﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models.Models
{
    public class UpdatedPointMessageReq
    {
        public string Token { get; set; }
        public long AddedPointed { get; set; }
        public bool ShowPopup { get; set; }
    }
}
