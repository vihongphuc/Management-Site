﻿using System;

namespace ALT.Common.Models
{
    using Newtonsoft.Json;
    using System.ComponentModel.DataAnnotations;

    [JsonObject]
    [Serializable]
    public class WinLossReq
    {
        [Required]
        public string PersonOCode { get; set; }
        public string Username { get; set; }
        public string[] BetTypeItemOCodes { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
