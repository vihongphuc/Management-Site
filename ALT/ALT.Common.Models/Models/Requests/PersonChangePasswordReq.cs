﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    public class PersonChangePasswordReq
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
