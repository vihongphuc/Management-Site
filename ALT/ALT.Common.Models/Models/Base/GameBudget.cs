﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Models
{
    using Newtonsoft.Json;
    using System.ComponentModel.DataAnnotations;

    [JsonObject]
    [Serializable]
    public class GameBudget
    {
        [Required]
        public string GameCode { get; set; }
        [Required]
        public GameBudgetType Type { get; set; }
        [Required]
        public long Points { get; set; }
    }
}
