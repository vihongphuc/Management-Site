local currPoint = tonumber(redis.call('HGET', 'Server:Points', @Token))
if (currPoint == nil) then
	return {0,0,0,0,0}
else
	local storedWin = 0
	local vals = redis.call('HGETALL', @StoredWinKey)
	if (vals ~= nil) then
		for ix=1,#vals,2 do
			storedWin = storedWin + tonumber(vals[ix + 1])
		end
	end

	local storedBet = 0
	local bets = redis.call('HGETALL', @StoredBetKey)
	if (bets ~= nil) then
		for ix=1,#bets,2 do
			storedBet = storedBet + tonumber(bets[ix + 1])
		end
	end

	redis.call('DEL', @StoredWinKey)
	redis.call('DEL', @StoredBetKey)
	redis.call('HDEL', 'Server:Points', @Token)

	return {1, currPoint + storedWin + storedBet, currPoint, storedWin, storedBet}
end