﻿local currPoint = tonumber(redis.call('HGET', 'Server:Points', @Token))
if (currPoint == nil) then
	return nil
else
	local storedWin = 0
	if (@StoredWinKey ~= '') then
		local vals = redis.call('HGETALL', @StoredWinKey)
		if (vals ~= nil) then
			for ix=1,#vals,2 do
				storedWin = storedWin + tonumber(vals[ix + 1])
			end
		end
	end

	return currPoint + storedWin
end