﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATL.Common.Redis
{
    using StackExchange.Redis;

    public class Connection
    {
        private string ConnectionString;
        private int ConnectionTimeOut;
        private int SystemTimeOut;

        public ConnectionMultiplexer rediConnection { get; set; }

        public Connection(string connectionString, int connectionTimeOut = 0, int systemTimeOut = 0)
        {
            this.ConnectionString = connectionString;
            this.ConnectionTimeOut = connectionTimeOut;
            this.SystemTimeOut = systemTimeOut;
            InitializeConnection();
        }

        public void InitializeConnection()
        {
            if (false == string.IsNullOrEmpty(ConnectionString))
            {
                var configurationOptions = new ConfigurationOptions
                {
                    EndPoints = { this.ConnectionString },
                    AbortOnConnectFail = false
                };

                if (ConnectionTimeOut > 0)
                    configurationOptions.ConnectTimeout = this.ConnectionTimeOut;
                if (this.SystemTimeOut > 0)
                    configurationOptions.SyncTimeout = this.SystemTimeOut;

                rediConnection = ConnectionMultiplexer.Connect(configurationOptions);
            }
        }
        
        public IDatabase getDataBase()
        {
            return rediConnection.GetDatabase();
        }

        public IServer getDataServer(string server)
        {
            var endPoint = rediConnection.GetEndPoints(true).First();
            return rediConnection.GetServer(endPoint);
        }
    }
}
