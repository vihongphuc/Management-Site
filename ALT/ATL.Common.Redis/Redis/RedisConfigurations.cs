﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATL.Common.Redis
{
    public static class RedisConfigurations
    {
        #region Connection info
        public static string Group = "Redis";

        public static string ConnectionString = "ConnectionString";
        public static string ConnectionTimeOut = "ConnectionTimeOut";
        public static string SystemTimeOut = "SystemTimeOut";

        #endregion

        public const string PlayerServer = "Player";
        public const string PlayerDataBase = "PlayerDataBase";

        public const string TokenServer = "Token";
        public const string TokenDataBase = "TokenDataBase";
    }
}
