﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATL.Common.Redis
{
    public struct PointRemoveResult
    {
        public bool Success { get; set; }
        public long Returned { get; set; }
        public long Point { get; set; }
        public long StoredWin { get; set; }
        public long StoredBet { get; set; }
    }
}
