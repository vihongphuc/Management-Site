﻿using ATL.Common.Redis.Properties;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATL.Common.Redis
{
    public static class RedisScripts
    {
        public static class GameState
        {
            public static async Task<RedisResult> GetCurrentPoint(string token, string storedWinKey)
            {
                return await luaGetPoint.Value.EvaluateAsync(RedisManager.GetDataBase(RedisConfigurations.PlayerServer), new
                {
                    Token = token,
                    StoredWinKey = storedWinKey ?? String.Empty,
                }).ConfigureAwait(false);
            }
            public static async Task<PointModifyResult> ModifyPoint(string token, long modify)
            {
                var result = (long[])await luaModifyPoint.Value.EvaluateAsync(RedisManager.GetDataBase(RedisConfigurations.PlayerServer), new
                {
                    Token = token,
                    Modify = modify
                }).ConfigureAwait(false);

                return new PointModifyResult
                {
                    Success = result[0] == 1,
                    CurrentPoint = result[1]
                };
            }

            public static async Task<PointRemoveResult> RemoveTokenPoint(string token, string storedWinKey, string storedBetKey)
            {
                var result = (long[])await luaRemovePoint.Value.EvaluateAsync(RedisManager.GetDataBase(RedisConfigurations.PlayerServer), new
                {
                    Token = token,
                    StoredWinKey = storedWinKey,
                    StoredBetKey = storedBetKey
                }).ConfigureAwait(false);

                return new PointRemoveResult
                {
                    Success = result[0] == 1,
                    Returned = result[1],
                    Point = result[2],
                    StoredWin = result[3],
                    StoredBet = result[4]
                };
            }
            

            private static readonly Lazy<LoadedLuaScript> luaModifyPoint;
            private static readonly Lazy<LoadedLuaScript> luaRemovePoint;
            private static readonly Lazy<LoadedLuaScript> luaGetPoint;
            static GameState()
            {
                luaModifyPoint = new Lazy<LoadedLuaScript>(() =>
                {
                    var luaModifyPoint = LuaScript.Prepare(Resources.lua_modify_point);
                    return luaModifyPoint.Load(RedisManager.GetServer(RedisConfigurations.PlayerServer));
                }, true);

                luaRemovePoint = new Lazy<LoadedLuaScript>(() =>
                {
                    var luaRemovePoint = LuaScript.Prepare(Resources.lua_remove_point);
                    return luaRemovePoint.Load(RedisManager.GetServer(RedisConfigurations.PlayerServer));
                }, true);

                luaGetPoint = new Lazy<LoadedLuaScript>(() =>
                {
                    var luaGetPoint = LuaScript.Prepare(Resources.lua_get_point);
                    return luaGetPoint.Load(RedisManager.GetServer(RedisConfigurations.PlayerServer));
                }, true);
            }
        }
    }
}
