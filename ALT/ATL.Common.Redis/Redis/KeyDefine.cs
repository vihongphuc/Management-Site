﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATL.Common.Redis
{
    public static class KeyDefine
    {
        #region Servers
        public static string ServerPointKey
        {
            get
            {
                return $"{Server}:{Point}";
            }
        }

        public static string GetServerTokenKey(string requestToken)
        {
            return $"{Server}:{Token}:{requestToken}";
        }

        public static string GetBudgetKey(string type)
        {
            return string.Format(GameBudget, type);
        }

        public static string GetBudgetHashKey(string haskKey)
        {
            return haskKey;
        }

        #endregion

        #region Players

        public static string GetPlayerKey(string activeSessionOCode)
        {
            return $"{Player}:{State}:{activeSessionOCode}";
        }

        public static string GetPersonLoginKey(string tokenKey)
        {
            return $"{PersonLoginKey}:{tokenKey}";
        }

        #endregion

        #region params

        public const string Server = "Server";
        public const string Player = "Player";
        public const string State = "State";
        public const string PersonLoginKey = "Authentication:Token";
        public const string Point = "Points";
        public const string Token = "Token";
        public const string IsSubAccount = "IsSubAccount";
        public const string SubAccountOCode = "SubAccountOCode";
        public const string LoginTime = "LoginTime";

        public const string PersonOCode = "PersonOCode";
        public const string UplineOCode = "UplineOCode";
        public const string Username = "Username";
        public const string PersonType = "PersonType";
        public const string SessionOCode = "SessionOCode ";
        public const string Points = "Points";
        public const string Currency = "Currency";
        public const string IPHashKey = "IPHashKey";
        public const string Publisher = "Publisher";
        public const string Platform = "Platform";
        public const string Secret = "Secret";
        public const string TokenMobile = "TokenMobile";
        public const string PointRate = "PointRate";
        public const string GameKey = "GameKey";
        public const string GameHash = "GameHash";



        public const string GameBudget = "Server:ScrollerServer:{0}";

        #endregion
    }

    // Get Member online: Game:Points => list active sessionocode
    //                    Player:Active:sessionOCode => Game Name.
    public static class Player
    {

    }

    public static class Server
    {

    }
}
