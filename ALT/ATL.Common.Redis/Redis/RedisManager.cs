﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATL.Common.Redis
{
    using ATL.Common;
    using StackExchange.Redis;

    public static class RedisManager
    {
        public static IDatabase GetDataBase(string redisConfigName)
        {
            //var conectionString = ALT.Common.Configuration.ConfigurationManager.Get<string>(RedisConfigurations.Group, redisConfigName, string.Empty);
            //var redis = new Connection(conectionString);
            //return redis.getDataBase();
            ConnectionMultiplexer cnm;
            if (dtConnections.TryGetValue(redisConfigName, out cnm))
            {
                return cnm.GetDatabase();
            }
            throw new ArgumentException("Cannot find the redis server registration", "server");
        }

        public static IServer GetServer(string redisConfigName)
        {
            //var conectionString = ALT.Common.Configuration.ConfigurationManager.Get<string>(RedisConfigurations.Group, redisConfigName, string.Empty);
            //var redis = new Connection(conectionString);
            //return redis.getDataServer(RedisConfigurations.ServerDataBase);

            ConnectionMultiplexer cm;
            if (dtConnections.TryGetValue(redisConfigName, out cm))
            {
                var endPoint = cm.GetEndPoints(true).First();
                return cm.GetServer(endPoint);
            }

            throw new ArgumentException("Cannot find the redis server registration", "server");
        }



        private static Dictionary<string, ConnectionMultiplexer> dtConnections = new Dictionary<string, ConnectionMultiplexer>();
        public static async Task<ConnectionMultiplexer> ConnectServer(string server)
        {
            lock (typeof(RedisManager))
            {
                ConnectionMultiplexer cnm;
                if (dtConnections.TryGetValue(server, out cnm))
                {
                    return cnm;
                }
            }

            var connectionString = ALT.Common.Configuration.ConfigurationManager.Get<string>(RedisConfigurations.Group, server, string.Empty);
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new Exception($"Cannot find Redis connection for server: {server}");
            }

            var connection = await ConnectionMultiplexer.ConnectAsync(connectionString);

            lock (typeof(RedisManager))
            {
                var newConnection = new Dictionary<string, ConnectionMultiplexer>(dtConnections);
                newConnection.Add(server, connection);
                dtConnections = newConnection;
            }
            return connection;
        }

        public static ConnectionMultiplexer GetConnection(string server)
        {
            ConnectionMultiplexer cnm;
            if (dtConnections.TryGetValue(server, out cnm))
            {
                return cnm;
            }
            throw new ArgumentException($"Cannot find the redis server registration.", "server");
        }
    }
}
