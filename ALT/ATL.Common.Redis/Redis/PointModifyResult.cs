﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATL.Common.Redis
{
    public struct PointModifyResult
    {
        public bool Success { get; set; }
        public long CurrentPoint { get; set; }
    }
}
