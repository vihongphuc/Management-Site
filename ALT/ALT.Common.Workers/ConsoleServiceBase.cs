﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Workers
{
    public class ConsoleServiceBase : ServiceBase
    {
        [DefaultValue(300)]
        public int ErrorRestartDelayMilliseconds { get; set; }
        [DefaultValue(10000)]
        public int ErrorResetDelaySeconds { get; set; }

        public ConsoleServiceBase()
        {
            ErrorRestartDelayMilliseconds = 300;
            ErrorResetDelaySeconds = 10000;
        }

        public static void Run(ConsoleServiceBase service)
        {
            service.isStopped = false;

            if (Environment.UserInteractive)
            {
                HandleConsole(service, Environment.GetCommandLineArgs().Skip(1).ToArray());
            }
            else
            {
                ServiceBase.Run(service);
            }
        }
        public static void Fail(ConsoleServiceBase service)
        {
            Environment.Exit(1);
        }
        public static void Stop(ConsoleServiceBase service)
        {
            if (!service.isStopped)
            {
                service.isStopped = true;

                if (Environment.UserInteractive)
                {
                    service.OnStop();
                }
                else
                {
                    service.Stop();
                }
            }
        }

        private static void HandleConsole(ConsoleServiceBase service, string[] args)
        {
            bool isServiceRunning = false;
            bool isServiceInstalled = false;
            if (ServiceController.GetServices()
                                 .Where(s => String.Equals(s.ServiceName, service.ServiceName, StringComparison.InvariantCultureIgnoreCase))
                                 .SingleOrDefault() != null)
            {
                isServiceInstalled = true;

                using (var controller = new ServiceController(service.ServiceName))
                {
                    Console.WriteLine("The service \"{0}\" is currently installed, its current status is: {1}", service.ServiceName, controller.Status);

                    isServiceRunning = (controller.Status == ServiceControllerStatus.Running);
                }
            }
            else
            {
                Console.WriteLine("The service \"{0}\" is NOT yet installed in this machine", service.ServiceName);
            }
            Console.WriteLine();

            var argument = args.FirstOrDefault();
            if (!String.IsNullOrEmpty(argument))
            {
                if (AutoConsole(service, isServiceInstalled, isServiceRunning, argument))
                {
                    return;
                }
            }

            HandleConsoleInput(service, isServiceInstalled, isServiceRunning);
        }
        private static bool AutoConsole(ConsoleServiceBase service, bool isServiceInstalled, bool isServiceRunning, string runType)
        {
            switch (runType.ToLowerInvariant())
            {
                case "--install":
                    if (!isServiceInstalled)
                    {
                        Console.WriteLine("Installing service...");
                        DoInstall(service);
                    }
                    else
                    {
                        Console.WriteLine("The service is currently installed");
                    }
                    return true;

                case "--uninstall":
                    if (isServiceInstalled)
                    {
                        Console.WriteLine("Uninstalling service...");
                        DoUninstall(service);
                    }
                    else
                    {
                        Console.WriteLine("The service is currently not installed");
                    }
                    return true;

                case "--start":
                    if (isServiceInstalled)
                    {
                        if (!isServiceRunning)
                        {
                            Console.WriteLine("Starting the service...");
                            using (var controller = new ServiceController(service.ServiceName))
                            {
                                controller.Start();
                            }
                        }
                        else
                        {
                            Console.WriteLine("The service is currently running");
                        }
                    }
                    else
                    {
                        Console.WriteLine("The service is currently not installed");
                    }
                    return true;

                case "--stop":
                    if (isServiceInstalled)
                    {
                        if (isServiceRunning)
                        {
                            Console.WriteLine("Stopping the service...");
                            using (var controller = new ServiceController(service.ServiceName))
                            {
                                controller.Stop();
                            }
                        }
                        else
                        {
                            Console.WriteLine("The service is currently not running");
                        }
                    }
                    else
                    {
                        Console.WriteLine("The service is currently not installed");
                    }
                    return true;
            }

            return false;
        }
        private static void HandleConsoleInput(ConsoleServiceBase service, bool isServiceInstalled, bool isServiceRunning)
        {
            Console.WriteLine("Press [Esc] to quit");
            if (isServiceInstalled)
            {
                Console.WriteLine("      [U]   to uninstall the service");
                if (isServiceRunning)
                {
                    Console.WriteLine("      [S]   to stop the service");
                }
                else
                {
                    Console.WriteLine("      [S]   to start the service");
                }
            }
            else
            {
                Console.WriteLine("      [I]   to install the service");
            }
            Console.WriteLine("      [R]   to run the service in the console");

            var iCount = 0;
            while (iCount < 5)
            {
                var key = Console.ReadKey(true).Key;
                switch (key)
                {
                    case ConsoleKey.I:
                        if (!isServiceInstalled)
                        {
                            if (!Confirm("Please confirm to INSTALL this as a service."))
                                return;

                            DoInstall(service);
                            return;
                        }
                        break;

                    case ConsoleKey.U:
                        if (isServiceInstalled)
                        {
                            if (!Confirm("Please confirm to UNINSTALL the service."))
                                return;

                            DoUninstall(service);
                            return;
                        }
                        break;

                    case ConsoleKey.S:
                        if (isServiceInstalled)
                        {
                            if (isServiceRunning)
                            {
                                if (!Confirm("Please confirm to STOP this service."))
                                    return;

                                using (var controller = new ServiceController(service.ServiceName))
                                {
                                    controller.Stop();
                                }
                            }
                            else
                            {
                                if (!Confirm("Please confirm to START this service."))
                                    return;

                                using (var controller = new ServiceController(service.ServiceName))
                                {
                                    controller.Start();
                                }
                            }
                        }
                        return;

                    case ConsoleKey.R:
                        if (isServiceRunning)
                        {
                            if (!Confirm("The service is currently running, are you sure to proceed with console run?"))
                                return;
                        }

                        RunInConsole(service);
                        return;

                    case ConsoleKey.Escape:
                        // quit
                        return;
                }

                iCount++;
            }

            Console.WriteLine("Maximum number of key press reached");
        }

        private static bool Confirm(string text)
        {
            Console.WriteLine();
            Console.WriteLine(text);
            Console.WriteLine("Press [Y] to continue...");
            return (Console.ReadKey(true).Key == ConsoleKey.Y);
        }
        private static void RunInConsole(ConsoleServiceBase service)
        {
            Console.WriteLine();
            Console.WriteLine("Starting service run");
            service.OnStart(new string[] { });
            Console.WriteLine("Service in console started! Press [ESC] to stop");
            while (Console.ReadKey(true).Key != ConsoleKey.Escape) ;

            ConsoleServiceBase.Stop(service);
            Console.WriteLine("Service stopped!");
        }
        private static void DoUninstall(ConsoleServiceBase service)
        {
            // don't want to hard link the dependency into the SharedHelper
            var asm = Assembly.Load("System.Configuration.Install, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a, Custom=null");
            var managedInstallerClass = asm.GetType("System.Configuration.Install.ManagedInstallerClass");
            var method = managedInstallerClass.GetMethod("InstallHelper");

            method.Invoke(null, new object[] { new string[] { "/u", Assembly.GetEntryAssembly().Location } });
        }
        private static void DoInstall(ConsoleServiceBase service)
        {
            // don't want to hard link the dependency into the SharedHelper
            var asm = Assembly.Load("System.Configuration.Install, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a, Custom=null");
            var managedInstallerClass = asm.GetType("System.Configuration.Install.ManagedInstallerClass");
            var method = managedInstallerClass.GetMethod("InstallHelper");

            method.Invoke(null, new object[] { new string[] { Assembly.GetEntryAssembly().Location } });

            if (service.ErrorRestartDelayMilliseconds > 0 && service.ErrorResetDelaySeconds > 0)
            {
                Process.Start("sc", String.Format("failure \"{0}\" reset= {1} actions= restart/{2}", service.ServiceName, service.ErrorResetDelaySeconds, service.ErrorRestartDelayMilliseconds))
                       .WaitForExit();
            }
        }
        public static void DoInstallService(string serviceLocation, string serviceName, int errorResetDelaySeconds, int errorRestartDelayMilliseconds)
        {
            Process.Start("sc", String.Format("create {0} displayname= \"{0}\" binpath= \"{1}\" start= auto", serviceName, serviceLocation))
                   .WaitForExit();

            if (errorResetDelaySeconds > 0 && errorRestartDelayMilliseconds > 0)
            {
                Process.Start("sc", String.Format("failure \"{0}\" reset= {1} actions= restart/{2}", serviceName, errorResetDelaySeconds, errorRestartDelayMilliseconds))
                       .WaitForExit();
            }
        }

        protected override void OnStart(string[] args)
        {
            throw new NotImplementedException();
        }
        protected override void OnStop()
        {
            throw new NotImplementedException();
        }
        protected override void OnShutdown()
        {
            base.OnShutdown();

            this.OnStop();
        }

        private volatile bool isStopped;
    }
}
