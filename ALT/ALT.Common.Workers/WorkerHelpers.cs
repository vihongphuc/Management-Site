﻿using ALT.Common.Configuration;
using ALT.Common.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Workers
{
    public static class WorkerHelpers
    {
        public static readonly string DefaultConfigFile = "configs.ini";

        private static bool scopeIsSet = false;
        private static bool idGeneratorIsSet = false;

        private static readonly ConfigurationStoreCollection configStoreCollection = new ConfigurationStoreCollection();

        public static void ReadConfigurations(string configPath, bool fromAssemblyLocation = true)
        {
            ReadConfigurations(configPath, ConfigurationStoreCollection.DefaultOrderNumber, fromAssemblyLocation);
        }
        public static void ReadConfigurations(string configPath, int sortOrder, bool fromAssemblyLocation = true)
        {
            if (fromAssemblyLocation)
            {
                configPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), configPath);
            }

            if (String.IsNullOrWhiteSpace(configPath))
            {
                throw new ArgumentException("Config file path not set", nameof(configPath));
            }

            if (!File.Exists(configPath))
            {
                throw new ArgumentException("Config file not found", nameof(configPath));
            }

            configStoreCollection.AddConfigurationStore(configPath, new FileConfigurationStore(configPath), sortOrder);
            ConfigurationManager.ApplyConfiguration(configStoreCollection);
            InitializeFromConfigurations();
        }
        public static void WatchConfiguration(string configPath, bool fromAssemblyLocation = true, Action<string, FileSystemEventArgs, Exception> onUpdated = null)
        {
            if (fromAssemblyLocation)
            {
                configPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), configPath);
            }

            if (String.IsNullOrWhiteSpace(configPath))
            {
                throw new ArgumentException("Config file path not set", nameof(configPath));
            }

            if (!File.Exists(configPath))
            {
                throw new ArgumentException("Config file not found", nameof(configPath));
            }

            var fsw = new FileSystemWatcher(Path.GetDirectoryName(configPath), Path.GetFileName(configPath));
            fsw.NotifyFilter = NotifyFilters.LastWrite;
            fsw.Changed += (s, ev) =>
            {
                lock (syncLock)
                {
                    if (updatingConfigurations.Contains(configPath))
                        return;
                    updatingConfigurations.Add(configPath);
                }

                Task.Run(async delegate
                {
                    await Task.Delay(DefaultHandleConfigurationUpdatedDelay);

                    try
                    {
                        HandleConfigChange(configPath, ev, onUpdated);
                    }
                    finally
                    {
                        lock (syncLock)
                        {
                            updatingConfigurations.Remove(configPath);
                        }
                    }
                });
            };

            fsw.EnableRaisingEvents = true;
        }

        private static void InitializeFromConfigurations()
        {
            if (!scopeIsSet)
            {
                int scope;
                if (ConfigurationManager.TryGet<int>(CommonConfigurations.Group, CommonConfigurations.IDGeneratorScope, out scope))
                {
                    IDGenerator.SetProvider(new UniqueNumberProvider(scope));
                    scopeIsSet = true;
                }
                else
                {
                    if (!idGeneratorIsSet)
                    {
                        IDGenerator.SetProvider(new UniqueNumberProvider(Environment.TickCount));
                        idGeneratorIsSet = true;
                    }
                }
            }
        }
        private static void HandleConfigChange(string configPath, FileSystemEventArgs fse, Action<string, FileSystemEventArgs, Exception> onUpdated)
        {
            Exception rEx = null;
            try
            {
                configStoreCollection.ResetConfigurationStore(configPath);
                ConfigurationManager.ResetAndApplyConfiguration(configStoreCollection);
            }
            catch (Exception ex)
            {
                rEx = ex;
            }

            if (onUpdated != null)
                onUpdated(configPath, fse, rEx);
        }

        private static readonly object syncLock = new object();
        private static readonly HashSet<string> updatingConfigurations = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

        private const int DefaultHandleConfigurationUpdatedDelay = 1000;
    }

    public static class IDGenerator
    {
        private static UniqueNumberProviderBase provider = new UniqueNumberProvider();

        public static void SetProvider(UniqueNumberProviderBase provider)
        {
            IDGenerator.provider = provider;
        }

        public static long GenerateID()
        {
            return IDGenerator.provider.GenerateID();
        }
        public static IList<long> GenerateIDs(int count)
        {
            return IDGenerator.provider.GenerateIDs(count);
        }

    }
}
