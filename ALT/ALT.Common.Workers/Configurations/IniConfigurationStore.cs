﻿using IniParser.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Workers
{
    public abstract class IniConfigurationStore : IConfigurationStore
    {
        public static readonly string GlobalSectionName = CommonConfigurations.Group;

        public IEnumerable<Configuration> GetConfigurations()
        {
            var iniData = GetIniData();
            return GetConfigurationsCore(iniData).ToArray();
        }

        protected abstract IniData GetIniData();
        protected virtual IEnumerable<Configuration> GetConfigurationsCore(IniData mergedIniData)
        {
            foreach (var globalData in mergedIniData.Global)
            {
                yield return new Configuration
                {
                    Group = GlobalSectionName,
                    Key = globalData.KeyName,
                    Value = globalData.Value
                };
            }
            foreach (var section in mergedIniData.Sections)
            {
                foreach (var keyValue in section.Keys)
                {
                    yield return new Configuration
                    {
                        Group = section.SectionName,
                        Key = keyValue.KeyName,
                        Value = keyValue.Value
                    };
                }
            }
        }

        public void Dispose()
        {
            Dispose(isDiposing: true);
            GC.SuppressFinalize(this);
        }
        ~IniConfigurationStore()
        {
            Dispose(isDiposing: false);
        }
        protected virtual void Dispose(bool isDiposing)
        {
        }
    }
}
