﻿using ALT.Web.Management.Core;
using System;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace ALT.Web.Management.Infrastructer
{
    public static class HtmlHelperExtensions
    {
        public static string ActivePage(this HtmlHelper helper, string controller, string action, string persontype)
        {
            string classValue = "";

            string currentController = helper.ViewContext.Controller.ValueProvider.GetValue("controller").RawValue.ToString();
            string currentAction = helper.ViewContext.Controller.ValueProvider.GetValue("action").RawValue.ToString();
            var checkPersonType = helper.ViewContext.Controller.ValueProvider.GetValue("personType");

            if (checkPersonType == null)
            {
                checkPersonType = helper.ViewContext.Controller.ValueProvider.GetValue("Type");
            }

            string personType;

            if (!string.IsNullOrEmpty(persontype) && checkPersonType != null)
            {
                personType = checkPersonType.AttemptedValue;

                if (currentController == controller && currentAction == action && persontype == personType)
                {
                    classValue = "active select-background-item";
                }
            }
            else
            {
                if (currentController == controller && currentAction == action)
                {
                    classValue = "active select-background-item";
                }

            }
            return classValue;
        }
        public static string ActivePageParent(this HtmlHelper helper, string controller, string action)
        {
            string classValue = "";

            string currentController = helper.ViewContext.Controller.ValueProvider.GetValue("controller").RawValue.ToString();
            string currentAction = helper.ViewContext.Controller.ValueProvider.GetValue("action").RawValue.ToString();

            if (currentController == controller && currentAction == action)
            {
                classValue = "active";
            }

            return classValue;
        }
    }
}