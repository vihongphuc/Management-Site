﻿using ALT.Web.Management.Core;
using System;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace ALT.Web.Management.Infrastructer
{
    public static class HTMLHelpersExtension
    {
        public static string Label(this HtmlHelper helper, string targer, string text)
        {
            return string.Format("<lable for='{0}'>{1}</label>",targer, text);
        }

        public static string SessionAction(this UrlHelper url, string actionName)
        {
            return url.Action(actionName, new RouteValueDictionary()
            {
                { ItemKeys.RequestSessionSelectorKey, SessionManager.GetCurrentSessionID() }
            });
        }
        public static string SessionAction(this UrlHelper url, string actionName, object routeValues)
        {
            var rvd = new RouteValueDictionary(routeValues);
            rvd[ItemKeys.RequestSessionSelectorKey] = SessionManager.GetCurrentSessionID();

            return url.Action(actionName, new RouteValueDictionary(rvd));
        }
        public static string SessionAction(this UrlHelper url, string actionName, string controllerName)
        {
            return url.Action(actionName, controllerName, new RouteValueDictionary()
            {
                { ItemKeys.RequestSessionSelectorKey, SessionManager.GetCurrentSessionID() }
            });
        }
        public static string SessionAction(this UrlHelper url, string actionName, string controllerName, object routeValues)
        {
            var rvd = new RouteValueDictionary(routeValues);
            rvd[ItemKeys.RequestSessionSelectorKey] = SessionManager.GetCurrentSessionID();

            return url.Action(actionName, controllerName, rvd);
        }

        public static MvcHtmlString HiddenFormSessionKey(this HtmlHelper html)
        {
            return html.Hidden(ItemKeys.RequestSessionSelectorKey, SessionManager.GetCurrentSessionID());
        }
        public static MvcHtmlString HiddenGlobalSessionKey(this HtmlHelper html)
        {
            return html.Hidden(ItemKeys.RequestSessionSelectorKey, SessionManager.GetCurrentSessionID(), new { @class = "__global __t" });
        }

    }
}