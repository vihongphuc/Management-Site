﻿using ALT.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ALT.Web.Management.Infrastructer
{
    public class ALTHandleErrorAttribute : HandleErrorAttribute
    {
        private static readonly bool IsDebugging = String.Equals("1", System.Configuration.ConfigurationManager.AppSettings["IsDebugging"]);

        public override void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                var serviceException = filterContext.Exception as ALTServiceException;
                if (serviceException != null)
                {
                    filterContext.ExceptionHandled = HandleALTServiceException(filterContext, serviceException);
                }
            }

#if DEBUG
            if (!System.Web.Hosting.HostingEnvironment.IsDevelopmentEnvironment &&
                !IsDebugging)
            {
#endif
                if (!filterContext.ExceptionHandled)
                {
                    //filterContext.Result = StaticRoutes.GenericError();
                    filterContext.ExceptionHandled = true;
                }
#if DEBUG
            }
#endif
        }

        protected virtual bool HandleALTServiceException(ExceptionContext filterContext, ALTServiceException serviceException)
        {
            if (serviceException.Response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                //AuthManager.RemoveAuth();

                //filterContext.Result = StaticRoutes.SignIn();
                return true;
            }
            if (serviceException.Response.StatusCode == System.Net.HttpStatusCode.Forbidden)
            {
                //filterContext.Result = StaticRoutes.HttpError(serviceException.Response.StatusCode);
                return true;
            }

            return false;
        }
    }
}