﻿var checkUsername = $("#CheckUsername").val();
if (checkUsername != "True") {
    var usernameUpper = $("#UsernameUpper").val();
    $("#Username").val(usernameUpper);
}

$("input[name='CreateAgency']").attr('checked', true);
$("input[name='CreateMember']").attr('checked', true);
var personType = $("#PersonType").val();

//Change CreateAgency and CreateMember.
$("#PersonType").change(function () {
    $(".checkAgent").css('display', '');
    var type = $(this).val();
    $(this).find('option').each(function (i, opt) {
        if (opt.value === type) {
            $(opt).attr('selected', 'selected');
            if (type == ALTMainInfo.PersonTypes.Agent) {
                $(".checkAgent").css('display', 'none')
            }
        }
        else {
            $(opt).removeAttr('selected');
        }
    });
});

if (personType == ALTMainInfo.PersonTypes.Agent) {
    $(".checkAgent").css('display', 'none');
}

$("#checkusername").click(function () {
    var username = $("#Username").val();
    ALTMainInfo.CheckUsername(username);
});
