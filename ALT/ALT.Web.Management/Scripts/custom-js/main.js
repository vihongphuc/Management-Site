﻿var ALTMainInfo = new function () {
    this.LoopTime = 10 * 1000;
    this.PersonTypes = {
        Admin: "Admin",
        SuperShareHolder: "SuperShareHolder",
        ShareHolder: "ShareHolder",
        SuperSenior: "SuperSenior",
        Senior: "Senior",
        SuperMaster: "SuperMaster",
        Master: "Master",
        SuperAgent: "SuperAgent",
        Agent: "Agent",
        Member: "Member",
        SubAccount: "SubAccount",
    };

    this.CurrentNamePersons = {
        CurrentPerson: "CurrentPerson",
        NonMember: "Non-Member",
        Member: "Member",
    }

    this.PersonStatus = {
        Active: "Active",
        Suspended: "Suspended",
        Disabled: "Disabled",
        NotAvailable: "Error:Not available"
    };

    this.TransferTypes = {
        Deposit: "Deposit",
        Withdraw: "Withdraw",
        DepositTo: "DepositTo",
        WithdrawFrom: "WithdrawFrom",
    };

    this.GameBudgetTypes = {
        Budget: "Budget",
        WinPoint: "WinPoint",
        ProfitPoint: "ProfitPoint"
    };

    this.Pagination = {
        Pagination25Record: 25,
        Pagination10Record: 10
    }
    this.CheckUsername = function (username) {
        //check username exits
        $.ajax({
            url: 'CheckUsername',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ username: username }),
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var objectNew = jQuery.parseJSON(data.ResultObj)
                if (objectNew.isCheck == true) {
                    var htmlerror = ""
                    htmlerror += "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true' style='padding-left:10px'></span>";
                    htmlerror += "<span class='sr-only'>Error:</span>";
                    htmlerror += objectNew.Message;
                    $(".checkUsername").html(htmlerror);
                }
                else {
                    var htmlerror = ""
                    htmlerror += "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true' style='padding-left:10px;color: #038fdd'></span>";
                    htmlerror += "<span class='sr-only'>Error:</span>";
                    htmlerror += "<text style='color: #038fdd;'>Available</text>";
                    $(".checkUsername").html(htmlerror);
                    $('#usernames').val(objectNew.Username);
                }
            },
            error: function (xhr) {
                toastr.error('error');
            }
        });
    }
}

$(document).ready(function () {
    $('.datetimepicker1').datetimepicker({ format: "MM/DD/YYYY" });
});

