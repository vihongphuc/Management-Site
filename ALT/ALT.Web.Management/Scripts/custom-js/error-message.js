﻿var ErrorMessage = new function () {
    //show info error create-person.js
    this.CreatePerson = {
        //Error main infor
        PasswordRequired: "Please input password.",
        PasswordMinLength: "Password must be at least 06 characters.",
        PasswordSpecialCharacter: "Password must have at least one number and one letter and one special character.",
        CheckYourEmail: "Please check your email.",

        nameTheCredit: "The credit",
        nametheMaxMembercredit: "The max member credit",
        nameTheMaxWinLimit: "The max win limit",
        nameTheMinLossLimit: "The min loss limit",

        IsNotNumber: " is not a number.",
        GreaterThanZero: " must be greater than 0.",
        LimitMaxCredit: "Upline does not enough credit : ",
        LimitMaxMemberCredit: "Limit max member credit : ",
        MaxMemberCreditLessThanOrEqualCredit: "Max member credit less than or equal credit.",
        CreditGreaterThanOrEqualCredit: "Credit must be greater than  or equal  Max member credit: ",
        UplineMaxMemberCredit: "Upline does not enough Max member credit : ",

        UplineMaxWinLimit: "Upline does not enough max win limit : ",
        UplineMinLossLimit: "Upline does not enough min loss credit : "
    };

    //show person.js 
    this.Person = {
        SusscessSataus: "Username {0} status has been changed.",
        ErrorSataus: "Username {0} no status has been changed.",
        SusscessResetPassword: "Username {0} password has been changed."
    }

    //show transfer.js 
    this.Transfer = {
        CheckDepositAndWithdraw: "Please check  input deposit and withdraw",
        DepositeErrorIsNotNumber: "The Deposit amount is not a number.",
        DepositAmountGreaterThanZero: "The Deposit amount must be greater than 0.",

        WithdrawErrorIsNotNumber: "The withdraw amount is not a number.",
        WithdrawAmountGreaterThanZero: "The withdraw amount must be greater than 0.",

        InputErrorDepositAmountGreaterThanZero: "Please input deposit amount greater than 0 for username {0}.",
        InputErrorWithdrawAmountGreaterThanZero: "Please input withdraw amount greater than 0 for username {0} .",

        WithdrawErrorCurrrentUsername: "Balance of username {0} has {1}.",
        CheckAvailiableBalanceCurrency: "Please check availiable balance {0} {1}.",

        SuccessMessageTransfer: "User {0} has been {1} {2} {3} ."
    }
}

function FormatString(fmtstr) {
    var args = Array.prototype.slice.call(arguments, 1);
    return fmtstr.replace(/\{(\d+)\}/g, function (match, index) {
        return args[index];
    });
}