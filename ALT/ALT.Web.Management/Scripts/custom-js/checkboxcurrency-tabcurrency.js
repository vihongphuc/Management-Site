﻿var selectedCurrencies = $('#selectedCurrencies').val();
var currentCurrency = $('#currency_current').val();
var onlyCurrency = $("#onlyCurrency").val();

var arraySelectedCurrencys = [];
var common = new Common();
var arrCurrentCurrency = currentCurrency.split(',');

//change currency bind in tab.
$("#Currencies").change(function () {
    var currency = $("#Currencies").val();
    arrCurrentCurrency.forEach(function (itemCurrency, inx) {
        if (itemCurrency == currency) {
            common.nav_tabs_remove_all_active();
            common.nav_tabs(currency);
            common.nav_tabs_display(currency);

            common.tab_content_remove_all_active();
            common.tab_content(currency);

            common.nav_tabs_content_display(currency);
            common.icon_check_selected_currencies(currency);

            // push currencies selected in array.
            common.PushCurrencyIntoArray(currency);
        }
        else {
            common.tab_content(itemCurrency);
            common.nav_tabs_display_none(itemCurrency);

            common.nav_tabs_content_display_none(itemCurrency);
            common.icon_uncheck_selected_currencies(itemCurrency);

            // remove currencies selected in array.
            common.RemoveCurrencyIntoArray(itemCurrency);
            //$('.btn.btn-xs.btn-default').prop("disabled", false);
        }
    });

});

$("#Currencies").find('option').each(function (i, opt) {
    if (opt.value === onlyCurrency) {
        $(opt).attr('selected', 'selected');
        // push currencies selected in array.
        common.PushCurrencyIntoArray(onlyCurrency);
    }
    else {
        $(opt).removeAttr('selected');
        common.RemoveCurrencyIntoArray(opt.value);
    }
});

//bind currency defeaut.
arrCurrentCurrency.forEach(function (itemCurrency, inx) {
    if (itemCurrency == onlyCurrency) {
        common.nav_tabs_remove_all_active();
        common.nav_tabs(onlyCurrency);
        common.nav_tabs_display(onlyCurrency);

        common.tab_content_remove_all_active();
        common.tab_content(onlyCurrency);

        common.nav_tabs_content_display(onlyCurrency);
        common.icon_check_selected_currencies(onlyCurrency);

        // push currencies selected in array.
        common.PushCurrencyIntoArray(onlyCurrency);

    }
    else {
        common.tab_content(itemCurrency);
        common.nav_tabs_display_none(itemCurrency);

        common.nav_tabs_content_display_none(itemCurrency);
        common.icon_uncheck_selected_currencies(itemCurrency);

        // remove currencies selected in array.
        common.RemoveCurrencyIntoArray(itemCurrency);

        $('.btn.btn-xs.btn-default').prop("disabled", false);
    }
});

function Common() {

    //remove class 'active' in 'background-antiquewhite'.
    this.nav_tabs_remove_all_active = function () {
        $('.nav.nav-tabs').find('.background-antiquewhite').removeClass('active');
    }

    //remove class 'active' in 'background-antiquewhite' by currencies.
    this.nav_tabs_remove_active_by_currencies = function (current_currency) {
        $('.nav.nav-tabs').find('#' + current_currency).removeClass('active');

    }

    //check ul with class "nav nav-tabs" view selected currency add class active.
    this.nav_tabs = function (current_currency) {
        $('.nav.nav-tabs').find('#' + current_currency)
                .removeClass()
                .addClass('background-antiquewhite active');
    }

    //check ul with class "nav nav-tabs" view selected currency show else display:none.
    this.nav_tabs_display_none = function (current_currency) {
        $('.nav.nav-tabs').find('#' + current_currency).css("display", "none");
    }

    //check ul with class "nav nav-tabs" view selected currency show else display:''.
    this.nav_tabs_display = function (current_currency) {
        $('.nav.nav-tabs').find('#' + current_currency).css("display", "");
    }

    //before check seleted currencies remove active currencies.
    this.tab_content_remove_all_active = function () {
        $('.tab-content').parents().find('.tab-pane.fade.in').removeClass('active');
    }

    //then add currencies selected currencies.
    this.tab_content = function (current_currency) {
        $('#tab-content_' + current_currency)
                       .removeClass()
                       .addClass('tab-pane fade in active');
    }

    //add remove active for currencies selected
    this.tab_content_active_selected_currencies = function (current_currency) {
        $('#tab-content_' + current_currency)
                       .removeClass()
                       .addClass('tab-pane fade in');
    }

    //check ul with class "tab_content" view selected currency show else display:none.
    this.nav_tabs_content_display_none = function (current_currency) {
        $('.tab-content').parents().find('#tab-content_' + current_currency)
                                                .css("display", "none");

    }
    //check ul with class "tab_content" view selected currency show else display:none.
    this.nav_tabs_content_display = function (current_currency) {
        $('.tab-content').parents().find('#tab-content_' + current_currency)
                                                .css("display", "");

    }

    //By icon checked of currencies seleted.
    this.icon_check_selected_currencies = function (current_currency) {
        $('#icon_check_' + current_currency).removeClass()
                .addClass('state-icon glyphicon glyphicon-check');
    }

    //By icon uncheck  of no currencies seleted.
    this.icon_uncheck_selected_currencies = function (current_currency) {
        $('#icon_check_' + current_currency).removeClass()
                                            .addClass('state-icon glyphicon glyphicon-unchecked');
    }


    //Push currencies into Array when click currencies
    this.PushCurrencyIntoArray = function (value) {
        if (arraySelectedCurrencys.indexOf(value) < 0) {
            arraySelectedCurrencys.push(value);
        }

        $('#Currencies').val(arraySelectedCurrencys);
    }

    //Remove currencies into Array when click currencies
    this.RemoveCurrencyIntoArray = function (value) {
        if (arraySelectedCurrencys.indexOf(value) >= 0) {
            for (var i = 0; i < arraySelectedCurrencys.length; i++) {
                if (arraySelectedCurrencys[i] === value) {
                    arraySelectedCurrencys.splice(i, 1)
                }
            }
        }
        $('#Currencies').val(arraySelectedCurrencys);
    }

}

$('#radioBtn a').on('click', function () {
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#' + tog).prop('value', sel);
    $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
})
