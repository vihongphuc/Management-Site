﻿$(function () {
    $(".loading").fadeOut(2000, function () {
        $(".content-wrapper").fadeIn(1000);
    });
});

function ShowProcessing() {    
    var strhtml = '<div class="cssload-container"><div class="cssload-double-torus"></div></div>';
    var str = $(".cssload-container").html();
    if (str == null || str == "") {
        $(".content-wrapper").append(strhtml);
    }    
    $(".cssload-container").show();
    $(".content-wrapper").css({ "opacity": "0.8" })
}

function HideProcessing() {
    setTimeout(function () {
        $(".cssload-container").hide();        
        $(".content-wrapper").css({ "opacity": "1" })
    }, 500)

}