﻿
var isEdit = $('#IsUpdate').val() == 'True';
var current_type = $('#Type').val();
var member = "Member";

$(document).ready(function () {
    $('.UplinePT').each(function (index, elementUpline) {
        var selUplinePTId = elementUpline.id;
        var selUplinePTObject = document.getElementById(selUplinePTId);

        var currencyCode = $(elementUpline).data('currency-code');
        var currentUplinePTVal = $(selUplinePTObject).data('upline-pt-val');
        var selCurrentMaxPTObject = $('#selCurrentMaxPT_' + currencyCode);

        var currentUplinePTSelected = $(selUplinePTObject).data('updated-val');
        var currentMaxPTSelected = $(selCurrentMaxPTObject).data('updated-val');


        $(selUplinePTObject).each(function () {
            if (current_type == member) {
                var intRateUpline = parseInt(Math.ceil(currentUplinePTVal * 100));

                for (var i = 1; i <= intRateUpline; i++) {
                    var el = document.createElement("option");
                    el.textContent = parseFloat(Math.round(i) / 100).toFixed(2);
                    el.value = parseFloat(Math.round(i) / 100).toFixed(2);;
                    selUplinePTObject.appendChild(el);
                }
            }
            else {

                var intRateUpline = parseInt(Math.ceil(currentUplinePTVal * 100));

                for (var i = 1; i <= intRateUpline; i++) {
                    var el = document.createElement("option");
                    el.textContent = parseFloat(Math.round(i) / 100).toFixed(2);
                    el.value = parseFloat(Math.round(i) / 100).toFixed(2);;
                    selUplinePTObject.appendChild(el);
                }

                selUplinePTObject.selectedIndex = selUplinePTObject.options.length - 1;

                //Bind Current flow value upline
                var intRateCurrent = parseInt(Math.ceil(currentUplinePTVal * 100)) - parseInt(Math.ceil(currentUplinePTSelected * 100));
                console.log(intRateCurrent);
                for (var i = 1; i <= parseInt(intRateCurrent) ; i++) {
                    selCurrentMaxPTObject
                        .append($("<option></option>")
                        .attr("value", parseFloat(Math.round(i) / 100).toFixed(2))
                        .text(parseFloat(Math.round(i) / 100).toFixed(2)));
                }
            }
        });

        //Change bind first in view
        $(selUplinePTObject).change(function () {
            var selectUpline = parseFloat(this.value);
            var intRateCurrent = parseInt(((parseFloat(currentUplinePTVal) - selectUpline) * 100).toPrecision(3));
            var _currentMaxVal = parseFloat(Math.round(intRateCurrent) / 100).toFixed(2);
            var _currentSelectedValue = parseFloat(selCurrentMaxPTObject.val()).toFixed(2);

            selCurrentMaxPTObject.empty();
            for (var i = 0; i <= intRateCurrent; i++) {
                selCurrentMaxPTObject
                    .append($("<option></option>")
                    .attr("value", parseFloat(Math.round(i) / 100).toFixed(2))
                    .text(parseFloat(Math.round(i) / 100).toFixed(2)));
            }

            selCurrentMaxPTObject.val(Math.min(_currentSelectedValue, _currentMaxVal).toFixed(2));
        });

        if (current_type == member) {
            if (isEdit) {
                $(selUplinePTObject).val(currentUplinePTSelected);
            } else {
                $(selUplinePTObject).val(currentUplinePTVal);
            }
        }
        else {
            $(selUplinePTObject).val(currentUplinePTSelected);
            $(selCurrentMaxPTObject).val(currentMaxPTSelected);
        }      
    });
});