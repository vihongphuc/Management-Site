﻿var nameFunction = {
    PersonDownlineNonMember: "PersonDownlineNonMember",
    PersonDownlineMember: "PersonDownlineMember"
}

var Person = function (index, data) {
    var self = this;
    self.index = index;
    self.UplineOCode = ko.observable(data.UplineOCode);
    self.OCode = ko.observable(data.OCode);
    self.Username = ko.observable(data.Username);
    self.Firstname = ko.observable(data.Firstname);
    self.Lastname = ko.observable(data.Lastname);
    self.Status = ko.observable(data.Status);

    switch (data.Status) {
        case 0:
            self.Status = ko.observable(ALTMainInfo.PersonStatus.Active);
            break;
        case 1:
            self.Status = ko.observable(ALTMainInfo.PersonStatus.Suspended);
            break;
        case 2:
            self.Status = ko.observable(ALTMainInfo.PersonStatus.Disabled);
            break;
    }
    switch (data.Type) {
        case 0:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.Admin);
            break;
        case 1:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.SuperShareHolder);
            break;
        case 2:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.ShareHolder);
            break;
        case 3:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.SuperSenior);
            break;
        case 4:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.Senior);
            break;
        case 5:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.SuperMaster);
            break;
        case 6:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.Master);
            break;
        case 7:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.SuperAgent);
            break;
        case 8:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.Agent);
            break;
        case 9:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.Member);
            break;
        default:
            self.Type = ko.observable(data.Type);
    }

    self.Currencies = mapDictionaryCurrecyToArray(data.Currencies);
    self.LastLoginIP = ko.observable(data.LastLoginIP);
    self.LastLogin = ko.observable(data.LastLogin);
    self.Availables = mapDictionaryAvaiableToArray(data.Availables, data.OCode, data.Username, self.Type(), self.UplineOCode());
    self.TotalDownline = ko.observable(data.TotalDownline);
    self.DepositAmount = ko.observable(data.DepositAmount);
    self.WithdrawAmount = ko.observable(data.WithdrawAmount);
    self.IsLoading = ko.observable(false);

    return self
};

var AvaiableModel = function (currency, value, ocode, username, type, uplineOCode) {
    var self = this;
    self.UplineOCode = ko.observable(uplineOCode);
    self.type = ko.observable(type);
    self.CurrencyCode = ko.observable(currency);
    self.Username = ko.observable(username);
    self.value = ko.observable(numeral(value).format('0,0.0000'));
    self.DepositAmount = ko.observable(0);
    self.WithdrawAmount = ko.observable(0);
    self.OCode = ko.observable(ocode);
    self.IsLoading = ko.observable(false);
    return self;
};

var CurrencyModel = function (currency, value) {
    var self = this;
    self.CurrencyCode = ko.observable(currency);
    self.value = ko.observable(value);
    return self;
};

var ModelAvaiableCurrent = function (currency, value) {
    var self = this;
    self.CurrencyCode = ko.observable(currency);
    self.value = ko.observable(numeral(value).format('0,0.0000'));
    return self;
};

function mapDictionaryAvaiableToArray(dictionary, OCode, Username, type, uplineOCode) {
    var result = ko.observableArray();
    for (var key in dictionary) {
        if (dictionary.hasOwnProperty(key)) {
            result.push(new AvaiableModel(key, dictionary[key], OCode, Username, type, uplineOCode));
        }
    }
    return result;
}

function mapDictionaryCurrecyToArray(dictionary) {
    var result = ko.observableArray();
    for (var key in dictionary) {
        if (dictionary.hasOwnProperty(key)) {
            result.push(new CurrencyModel(key, dictionary[key]));
        }
    }
    return result;
}

var Statement = function (index, data) {
    var self = this;
    self.index = index;
    self.Date = ko.observable(moment(data.Date).format('MM/DD/YYYY'));
    self.Username = ko.observable(data.Username);
    self.Transaction = ko.observable(data.Transaction);
    self.CurrencyCode = ko.observable(data.CurrencyCode);
    self.Type = ko.observable(data.Type);
    if (data.Type == ALTMainInfo.TransferTypes.Withdraw || data.Type == ALTMainInfo.TransferTypes.DepositTo) {
        self.AmountWithdraw = ko.observable(data.Amount);
        self.AmountDeposit = ko.observable(null);
    }
    if (data.Type == ALTMainInfo.TransferTypes.Deposit || data.Type == ALTMainInfo.TransferTypes.WithdrawFrom) {
        self.AmountDeposit = ko.observable(data.Amount);
        self.AmountWithdraw = ko.observable(null);
    }
    self.Amount = ko.observable(numeral(data.Amount).value());
    return self
};

var GetBreadcrumb = function (key, value) {
    var self = this;
    self.key = ko.observable(key);
    self.value = ko.observable(value);
    return self;
};


function TransferViewModel() {
    var self = this;
    var isTrue = false;
    var istrueSubmit = false;
    var temp_ocode = null;
    var currentPageMember = 0;


    self.DepositAmount = ko.observable(0);
    self.WithdrawAmount = ko.observable(0);

    self.transfersNonMember = ko.observableArray();
    self.transfersMember = ko.observableArray();

    self.IsShowNonMember = ko.observable(false);
    self.IsShowMember = ko.observable(false);
    self.IsShowCurrentBalance = ko.observable(false);

    self.GetBreadcrumbPerson = ko.observableArray();

    var currentType = $("#current-type").val();
    var ocode = $("#select-ocode").data('ocode');

    var available = null;
    var objectGetBalance = [];

    getPersonDownlineNonMember(ocode, 1);
    getPersonDownlineMember(ocode, 1);


    function getPersonDownlineNonMember(ocode, pageIndex) {

        GetBreadcrumbPerson(ocode);
        GetBalance(ocode);

        $.getJSON(person_downline_url, { ocode: ocode, pageIndex: pageIndex, isMember: false }, function (data) {
            self.transfersNonMember.removeAll();

            var index = (data.CurrentPage - 1) * data.TotalItemPerPage;

            ko.utils.arrayMap(data.PageList, function (i) {
                var _person = new Person(index, i);
                self.transfersNonMember.push(_person);
                index++;
            });

            self.IsShowNonMember(data.PageList.length > 0);

            if (data.PageList.length > 0) {
                Pagination(".pagination-sm", data.TotalPages, ALTMainInfo.Pagination.Pagination25Record, nameFunction.PersonDownlineNonMember);
            }
        });
    }

    function getPersonDownlineMember(ocode, pageIndex) {
        $.getJSON(person_downline_url, { ocode: ocode, pageIndex: pageIndex, isMember: true }, function (data) {
            self.transfersMember.removeAll();
            var index = (data.CurrentPage - 1) * data.TotalItemPerPage;

            ko.utils.arrayMap(data.PageList, function (i) {
                var _person = new Person(index, i);
                self.transfersMember.push(_person);
                index++;
            });
            self.IsShowMember(data.PageList.length > 0);

            if (data.PageList.length > 0) {
                Pagination("#pagination-transfer-member", data.TotalPages, ALTMainInfo.Pagination.Pagination25Record, nameFunction.PersonDownlineMember);
            }
        });
    }

    function GetBreadcrumbPerson(ocode) {
        $.getJSON(person_breadcrumb_url, { ocode: ocode }, function (breadcrumb) {
            self.GetBreadcrumbPerson.removeAll();
            for (var key in breadcrumb) {
                if (breadcrumb.hasOwnProperty(key)) {
                    self.GetBreadcrumbPerson.push(new GetBreadcrumb(key, breadcrumb[key]));
                }
            }

            self.IsShowCurrentBalance(breadcrumb[key] != ALTMainInfo.PersonTypes.Admin.toLowerCase());
        });
    }

    self.currentByCurrencies = ko.observableArray();
    function GetBalance(ocode) {
        $.getJSON(transfer_get_balance_url, { ocode: ocode }, function (avaiables) {
            self.currentByCurrencies.removeAll();
            for (var key in avaiables) {
                if (avaiables.hasOwnProperty(key)) {
                    self.currentByCurrencies.push(new ModelAvaiableCurrent(key, avaiables[key]));
                }
            }

            objectGetBalance = avaiables;
            istrueSubmit = true;
        });
    }

    self.current = ko.observableArray();
    self.personDownline = function (vm) {
        ShowProcessing();

        destroyPaging();
        self.current.push(vm);
        temp_ocode = vm.OCode();

        self.transfersNonMember.removeAll();
        self.transfersMember.removeAll();

        getPersonDownlineNonMember(vm.OCode(), 1);
        getPersonDownlineMember(vm.OCode(), 1);

        currentType = vm.Type();

        GetBalance(vm.OCode());
        HideProcessing();
    }

    self.breadcrumbclickpersondownline = function (data) {
        ShowProcessing();
        temp_ocode = data.key();
        destroyPaging();

        self.current.removeAll();
        self.transfersNonMember.removeAll();
        self.transfersMember.removeAll();

        getPersonDownlineNonMember(data.key(), 1);
        getPersonDownlineMember(data.key(), 1);
        HideProcessing();
    }

    self.TransferMember = function (data) {
        var uplineOcode = data.UplineOCode();
        var currenrtTpye = data.type();
        var ocodeDownline = data.OCode();
        var amDeposit = numeral(data.DepositAmount()).value();
        var amWithdraw = numeral(data.WithdrawAmount()).value();

        if (amDeposit == 0 && amWithdraw == 0) {
            toastr.error(ErrorMessage.Transfer.CheckDepositAndWithdraw);
            return false;
        }

        var input_value = amDeposit != 0 ? amDeposit : amWithdraw;
        var currency = data.CurrencyCode();
        var type = amDeposit != 0 ? ALTMainInfo.TransferTypes.Deposit : ALTMainInfo.TransferTypes.Withdraw;
        availiable = objectGetBalance[currency];
        var valCurrentCredit = data.value();

        var isboolValitionTransfer = ValitionTransfer(data.DepositAmount(), amDeposit, data.WithdrawAmount(), amWithdraw, data.value(), currency, data.Username(), currenrtTpye, type, availiable);
        if (isboolValitionTransfer == false) {
            return false;
        }

        data.value(null);
        data.IsLoading(true);
        $.ajax({
            url: transfer_url,
            cache: false,
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({
                ocode: ocodeDownline,
                amount: input_value,
                currency: currency,
                type: type
            }),
            success: function (return_data) {
                if (return_data.Status == 200) {
                    var res = JSON.parse(return_data.ResultObj);
                    var type = null;
                    if (return_data.TransferType == 0) {
                        type = ALTMainInfo.TransferTypes.Deposit.toLowerCase();
                    }
                    if (return_data.TransferType == 2) {
                        type = ALTMainInfo.TransferTypes.Withdraw.toLowerCase();
                    }

                    data.value(numeral(res.CurrentAmount).format('0,0.0000'));
                    toastr.error(FormatString(ErrorMessage.Transfer.SuccessMessageTransfer, res.Username, type, data.CurrencyCode, numeral(input_value).format('0,0.0000')));

                    $('.no-member-withdraw').val(0);
                    $('.no-member-deposit').val(0);
                    data.DepositAmount(0);
                    data.WithdrawAmount(0);
                    GetBalance(uplineOcode);

                    data.IsLoading(false);
                    istrueSubmit = false;
                }
                else {
                    toastr.error(return_data.Message);

                    data.value(numeral(valCurrentCredit).format('0,0.0000'));
                    data.DepositAmount(0);
                    data.WithdrawAmount(0);
                    data.IsLoading(false);
                }
            }
        });
    }
    self.TransferNoMember = function (data) {
        var uplineOcode = data.UplineOCode();
        var currenrtTpye = data.type();
        var ocodeDownline = data.OCode();
        var amDeposit = numeral(data.DepositAmount()).value();
        var amWithdraw = numeral(data.WithdrawAmount()).value();
        var input_value = amDeposit != 0 ? amDeposit : amWithdraw;

        if (amDeposit == 0 && amWithdraw == 0) {
            toastr.error(ErrorMessage.Transfer.CheckDepositAndWithdraw);
            return false;
        }
        var currency = data.CurrencyCode();
        var type = amDeposit != 0 ? ALTMainInfo.TransferTypes.Deposit : ALTMainInfo.TransferTypes.Withdraw;
        availiable = objectGetBalance[currency];
        var valCurrentCredit = data.value();
        var isboolValitionTransfer = ValitionTransfer(data.DepositAmount(), amDeposit, data.WithdrawAmount(), amWithdraw, data.value(), currency, data.Username(), currenrtTpye, type, availiable);
        if (isboolValitionTransfer == false) {
            return false;
        }

        data.value(null);
        data.IsLoading(true);
        $.ajax({
            url: transfer_url,
            cache: false,
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({
                ocode: ocodeDownline,
                amount: input_value,
                currency: currency,
                type: type
            }),
            success: function (return_data) {
                if (return_data.Status == 200) {
                    var res = JSON.parse(return_data.ResultObj);
                    var type = null;
                    if (res.TransferType == 0) {
                        type = ALTMainInfo.TransferTypes.Deposit.toLowerCase();
                    }
                    if (res.TransferType == 2) {
                        type = ALTMainInfo.TransferTypes.Withdraw.toLowerCase();
                    }

                    data.value(numeral(res.CurrentAmount).format('0,0.0000'));

                    toastr.success("User " + res.Username + " has been " + type + " " + data.CurrencyCode() + " " + numeral(input_value).format('0,0.0000') + " ");

                    $('.no-member-withdraw').val(0);
                    $('.no-member-deposit').val(0);

                    data.DepositAmount(0);
                    data.WithdrawAmount(0);
                    GetBalance(uplineOcode);

                    data.IsLoading(false);
                    istrueSubmit = false;
                }
                else {
                    toastr.error(return_data.Message);

                    data.value(numeral(valCurrentCredit).format('0,0.0000'));
                    data.DepositAmount(0);
                    data.WithdrawAmount(0);
                    data.IsLoading(false);
                }
            }
        });
    }

    self.changeDeposit = function (data) {
        $('.no-member-withdraw').val(0);
        data.WithdrawAmount(0);
    }
    self.changeWithdraw = function (data) {
        $('.no-member-deposit').val(0);
        data.DepositAmount(0);
    }

    self.clickDeposit = function (data) {
        data.DepositAmount(null);
        data.DepositAmount('');
        $('.no-member-withdraw').val(0);
        data.WithdrawAmount(0);
    }
    self.clickWithdraw = function (data) {
        data.WithdrawAmount(null);
        data.WithdrawAmount('');
        $('.no-member-deposit').val(0);
        data.DepositAmount(0);
    }

    self.keypressDeposit = function (data, event) {
        try {
            if (event.which == 13) {
                self.changeDeposit(data);
                self.TransferNoMember(data);
                return false;
            }
            return true;
        }
        catch (e) {
        }
    }
    self.keypressWithdraw = function (data, event) {
        try {
            if (event.which == 13) {
                self.changeWithdraw(data);
                self.TransferNoMember(data);
                return false;
            }
            return true;
        }
        catch (e) {
        }
    }

    self.currentUsernameNomember = ko.observable(null);
    self.statements = ko.observableArray();
    self.BindUsernameNoMemberTransactionDetail = function (vm) {
        var today = new Date();
        self.currentUsernameNomember(vm);
        var username = vm.Username();
        var personOCode = vm.OCode();

        var no_member_from_date = (moment(today).format('MM/DD/YYYY'));
        var no_member_to_date = (moment(today).format('MM/DD/YYYY'));

        $('#pagination-transfer').twbsPagination('destroy');
        $('#transfer-history-no-member').modal('show');

        GetStatement(no_member_from_date, no_member_to_date, 1);

        self.transferHistoryNoMember = function () {
            var no_member_from_date = $("#datepicker-no-member-from-date").val();
            var no_member_to_date = $("#datepicker-no-member-to-date").val();
            $('#pagination-transfer').twbsPagination('destroy');
            GetStatement(no_member_from_date, no_member_to_date, 1);
        }

        function GetStatement(no_member_from_date, no_member_to_date, page) {
            $('#statement tbody').html("");
            $.ajax({
                url: transfer_transferhistory_url,
                cache: false,
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    username: username,
                    personOCode: personOCode,
                    startDate: no_member_from_date,
                    endDate: no_member_to_date,
                    pageIndex: page
                }),
                success: function (data) {
                    if (data.Status == 200) {
                        self.statements.removeAll();
                        var res = JSON.parse(data.ResultObj);

                        var index = (res.CurrentPage - 1) * res.TotalItemPerPage;
                        ko.utils.arrayMap(res.PageList, function (i) {
                            self.statements.push(new Statement(index, i));
                            index++;
                        });

                        if (res.PageList.length > 0) {
                            var totalPages = parseInt(res.TotalPages / ALTMainInfo.Pagination.Pagination10Record);
                            if (parseInt(res.TotalPages % ALTMainInfo.Pagination.Pagination10Record) > 0) {
                                totalPages = totalPages + 1;
                            }

                            var visiblePages = res.TotalPages < ALTMainInfo.Pagination.Pagination10Record ? 1 : Math.floor(res.TotalPages / ALTMainInfo.Pagination.Pagination10Record) + 1;

                            $('#pagination-transfer').twbsPagination({
                                totalPages: totalPages,
                                visiblePages: visiblePages < 5 ? visiblePages : 5,
                                initiateStartPageClick: false,
                                onPageClick: function (event, page) {
                                    var no_member_from_date = $("#datepicker-no-member-from-date").val();
                                    var no_member_to_date = $("#datepicker-no-member-to-date").val();
                                    GetStatement(no_member_from_date, no_member_to_date, page);
                                }
                            });
                        }
                        else {
                            var str = "<tr role='row' class='text-center' style='background:#d8d5d5;'>";
                            str += "<td colspan='7'>No data</td>";
                            str += "</tr>";
                            $("#statement tbody").html(str);
                        }
                    }
                    else {
                        toastr.error(data.Message);
                    }
                }
            });
        }
    }

    setInterval(function () {
        ocode = temp_ocode == null ? ocode : temp_ocode;
        istrueSubmit = true;
        getPersonDownlineMember(ocode, currentPageMember);

    }, ALTMainInfo.LoopTime);

    function destroyPaging() {
        $('#pagination-transfer-member').twbsPagination('destroy');
        $('.pagination-sm').twbsPagination('destroy');
    }

    function Pagination(nameClassAndIdHtml, total_pages, PageSize, nameAction) {
        var totalPages = parseInt(total_pages / PageSize);
        if ((total_pages % PageSize) > 0) {
            totalPages = totalPages + 1;
        }

        totalPages = totalPages == 0 ? 1 : totalPages;
        var visiblePages = total_pages < PageSize ? 1 : Math.floor(total_pages / PageSize) + 1;

        $(nameClassAndIdHtml).twbsPagination({
            totalPages: totalPages,
            visiblePages: visiblePages < 10 ? visiblePages : 10,
            initiateStartPageClick: false,
            onPageClick: function (event, page) {
                if (nameAction == nameFunction.PersonDownlineNonMember) {
                    self.transfersNonMember.removeAll();
                    getPersonDownlineNonMember(temp_ocode, page);
                }
                else if (nameAction == nameFunction.PersonDownlineMember) {
                    self.transfersMember.removeAll();
                    getPersonDownlineMember(temp_ocode, page);
                }
            }
        });
    }

}

var viewModel = new TransferViewModel(ocode);
ko.applyBindings(viewModel, document.getElementById("content-transfer"));

function ValitionTransfer(depositAmount, amDeposit, withdrawAmount, amWithdraw, valAvailiable, currency, username, currenrtTpye, type, availiable) {

    var availiablePersonDownline = numeral(valAvailiable).value();

    if (isNaN(depositAmount)) {
        toastr.error(ErrorMessage.Transfer.DepositeErrorIsNotNumber);
        return false;
    }

    if (amDeposit < 0) {
        toastr.error(ErrorMessage.Transfer.DepositAmountGreaterThanZero);
        return false;
    }

    if (isNaN(withdrawAmount)) {
        toastr.error(ErrorMessage.Transfer.WithdrawErrorIsNotNumber);
        return false;
    }

    if (amWithdraw < 0) {
        toastr.error(ErrorMessage.Transfer.WithdrawAmountGreaterThanZero);
        return false;
    }
    if (currenrtTpye == ALTMainInfo.PersonTypes.SuperShareHolder) {
        if (amDeposit <= 0 && type == ALTMainInfo.TransferTypes.Deposit) {
            toastr.error(FormatString(ErrorMessage.Transfer.InputErrorDepositAmountGreaterThanZero, username));
            return false;
        }
        else if (amWithdraw <= 0 && type == ALTMainInfo.TransferTypes.Withdraw) {
            toastr.error(FormatString(ErrorMessage.Transfer.InputErrorWithdrawAmountGreaterThanZero, username));
            return false;
        }
        else if (availiablePersonDownline < amWithdraw && type == ALTMainInfo.TransferTypes.Withdraw) {
            toastr.error(FormatString(ErrorMessage.Transfer.WithdrawErrorCurrrentUsername, username, numeral(valAvailiable).value()));
            return false;
        }

    } else {
        if (availiable < amDeposit) {
            toastr.error(FormatString(ErrorMessage.Transfer.CheckAvailiableBalanceCurrency, currency, availiable));
            return false;
        }
        else if (availiablePersonDownline < amWithdraw && type == ALTMainInfo.TransferTypes.Withdraw) {
            toastr.error(FormatString(ErrorMessage.Transfer.WithdrawErrorCurrrentUsername, username, numeral(valAvailiable).value()));
            return false;
        }
        else if (amDeposit <= 0 && type == ALTMainInfo.TransferTypes.Deposit) {
            toastr.error(FormatString(ErrorMessage.Transfer.InputErrorDepositAmountGreaterThanZero, username));
            return false;
        }
        else if (amWithdraw <= 0 && type == ALTMainInfo.TransferTypes.Withdraw) {
            toastr.error(FormatString(ErrorMessage.Transfer.InputErrorWithdrawAmountGreaterThanZero, username));
            return false;
        }
    }

}