﻿var GameBudget = function (data) {
    var self = this;

    self.GameCode = ko.observable(data.GameCode);
    self.GameName = ko.observable(data.GameName);
    switch (data.Type) {
        case 0:
            self.Type = ko.observable(ALTMainInfo.GameBudgetTypes.ProfitPoint);
            break;
        case 1:
            self.Type = ko.observable(ALTMainInfo.GameBudgetTypes.WinPoint);
            break;
        case 2:
            self.Type = ko.observable(ALTMainInfo.GameBudgetTypes.Budget);
            break;
    }
    self.Points = ko.observable(numeral(data.Points).format('0,0.0000'));
    self.inputPoint = ko.observable(0);
    self.inputPointAdd = ko.observable(0);
    self.valueTypeSetting = ko.observable(0);
    self.valueTypeSettingAdd = ko.observable(0);
    self.IsProcessing = ko.observable(false);
    return self;
}

var pageIndex = 1;
function GameBudgetViewModel() {
    var self = this;

    self.items = ko.observableArray();
    self.IsProcessing = ko.observable(false);
    self.inputPoint = ko.observable(0);
    self.inputPointAdd = ko.observable(0);

    self.valueTypeSetting = ko.observable(null);
    self.valueTypeSettingAdd = ko.observable(null);
    self.checkgetgame = ko.observable(null);

    var gamebudgetType = $(".setting-type").val();

    getGameBudget(gamebudgetType, 1);

    function getGameBudget(gamebudgetType, pageIndex) {
        $.getJSON("GetGameBudget", { type: gamebudgetType, pageIndex: pageIndex }, function (data) {
            self.items.removeAll();
            ko.utils.arrayMap(data, function (i) {
                var _gameBudget = new GameBudget(i);
                self.items.push(_gameBudget);
            });
        });
    }

    self.selectionChanged = function (data) {
        gamebudgetType = data.valueTypeSetting();
        self.items.removeAll();

        ShowProcessing();
        getGameBudget(data.valueTypeSetting(), 1);
        HideProcessing();
    }

    self.update_gamebudget = function (isbool, data) {
        var gameOcode = data.GameCode();
        var gameName = data.GameName();
        var type = data.Type();
        var point = data.inputPoint();
        if (numeral(data.inputPoint()).format('0,0.0000') == data.Points()) {
            toastr.error("Please check value edit amount.");
            return;
        }

        data.Points(null);
        data.IsProcessing(true);

        $.ajax({
            url: 'UpdateGameBudget',
            cache: false,
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({
                add: isbool,
                type: type,
                Points: point,
                GameCode: gameOcode
            }),
            success: function (return_data, status, xhr) {
                data.Points(numeral(point).format('0,0.0000'));
                toastr.success(gameName + " Updated Points :" + numeral(point).format('0,0.0000'));
                data.IsProcessing(false);
            },
            error: function (xhr, status, error) {

            }
        });
    }
    self.keyperss_inputPoint = function (data, event) {
        try {
            if (event.which == 13) {
                self.update_gamebudget(false, data);
                return false;
            }
            return true;
        }
        catch (e) {
        }
    }
    self.clickInputPoint = function (data, event) {
        data.inputPoint(null);
        data.inputPoint('');
    }

    self.modelBudgetModal = ko.observable(null);
    self.BindModelBudgetModal = function (vm) {
        self.inputPointAdd = ko.observable(0);

        $.getJSON("CheckGetGame", { type: vm.valueTypeSetting() }, function (data) {
            vm.checkgetgame(data);
        });
        self.modelBudgetModal(vm);
        $('#budget').show();

        self.save_setting_type = function (data) {
            var point = data.inputPointAdd();
            if (vm.checkgetgame().length != 0) {
                var gameOcode = data.valueTypeSettingAdd().Code;
            }

            var type = vm.valueTypeSetting();

            if (gameOcode == null) {
                toastr.error("Invalid Game Name.");
                $('#budget').hide();
                return false;
            }

            $.ajax({
                url: 'UpdateGameBudget',
                cache: false,
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    add: true,
                    type: type,
                    Points: point,
                    GameCode: gameOcode
                }),
                success: function (return_data, status, xhr) {
                    $('#budget').hide();
                    getGameBudget(vm.valueTypeSetting(), 1);
                    toastr.success(data.valueTypeSettingAdd().Code + " is added successful");
                },
                error: function (xhr, status, error) {

                }
            });
        };
    }

    setInterval(function () {
        getGameBudget(gamebudgetType, 1);
    }, ALTMainInfo.LoopTime);

}

var viewModel = new GameBudgetViewModel();

ko.applyBindings(viewModel, document.getElementById("game-budget"));

