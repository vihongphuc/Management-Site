﻿var SummaryViewModel = function (startDate, endDate, startTime, endTime, index, data) {
    self = this;
    self.CurrentNamePerson = ko.observable(data.CurrentNamePerson);
    self.PersonOCode = ko.observable(data.PersonOCode);
    self.Username = ko.observable(data.Username);
    self.GameName = ko.observable(data.GameName);
    self.GameType = ko.observable(data.GameType);
    self.CurrencyCode = ko.observable(data.CurrencyCode);
    self.Date = ko.observable(moment(data.Time).format("MM/DD/YYYY"));
    self.Time = ko.observable(moment(data.Time).format("HH:mm:ss"));
    self.Details = ko.observable(data.Details);
    self.GameSites = ko.observable(data.GameSites);
    self.Type = ko.observable(data.Type);

    self.BetAmount = ko.observable(numeral(data.BetAmount).format('0,0.0000'));
    self.WinAmount = ko.observable(numeral(data.WinAmount).format('0,0.0000'));
    self.TotalWinLossAmount = ko.observable(data.TotalWinLossAmount);
    self.TotalWinLossHTML = ko.observable(numeral(data.TotalWinLossAmount).format('0,0.0000'));

    self.BetAmountRateCurrencyCode = ko.observable(numeral(data.BetAmountRateCurrencyCode).format('0,0.0000'));
    self.WinAmountRateCurrencyCode = ko.observable(numeral(data.WinAmountRateCurrencyCode).format('0,0.0000'));
    self.TotalWinLossAmountRateCurrencyCode = ko.observable(data.TotalWinLossAmountRateCurrencyCode);
    self.TotalWinLossAmountRateCurrencyCodeHTML = ko.observable(numeral(data.TotalWinLossAmountRateCurrencyCode).format('0,0.0000'));

    self.StartDate = ko.observable(startDate);
    self.EndDate = ko.observable(endDate);
    self.StartTime = ko.observable(startTime);
    self.EndTime = ko.observable(endTime);

    return self;
}

var GetBreadcrumb = function (key, value) {
    var self = this;
    self.key = ko.observable(key);
    self.value = ko.observable(value);
    return self;
};

function SummaryReportViewModels() {
    var self = this;
    self.mapCurrentSummaryReport = ko.observableArray();
    self.IsCurrentName = ko.observable(null);

    self.mapSummaryReportNoMember = ko.observableArray();
    self.IsNoMember = ko.observable(null);

    self.mapMember = ko.observableArray();
    self.IsMember = ko.observable(null);

    //shown total current person
    self.totalBetCurrentPersons = ko.observable(0);
    self.totalWinCurrentPersons = ko.observable(0);
    self.totalWinLossCurrentPersons = ko.observable(0);
    self.totalWinLossCurrentPersonsHTML = ko.observable(0);

    //shown total non member
    self.totalBetNoMembers = ko.observable(0);
    self.totalWinNoMembers = ko.observable(0);
    self.totalWinLossNoMembers = ko.observable(0);
    self.totalWinLossNoMembersHTML = ko.observable(0);

    //shown total member
    self.totalBetMembers = ko.observable(0);
    self.totalWinMembers = ko.observable(0);
    self.totalWinLossMembers = ko.observable(0);
    self.totalWinLossMembersHTML = ko.observable(0);

    //change currency rate member
    self.totalWinLossChangeRateCurrencyMembers = ko.observable(0);
    self.totalWinLossChangeRateCurrencyMembersHTML = ko.observable(0);


    //bind when paging if value filter.
    $("#CurrencyCode").find('option').each(function (i, opt) {
        var currency = $("#filterCurrencyOcode").val();
        if (opt.text === currency) {
            $(opt).attr('selected', 'selected');
            var valueRate = $(opt).attr('selected', 'selected').val();
            $("#Rate").val(numeral(valueRate).value());
        }
        else {
            $(opt).removeAttr('selected');
        }
    });

    var startDate = $("#StartDate").val();
    var endDate = $("#EndDate").val();
    var username = $("#Username").val();
    var currencyCode = $("#CurrencyCode option:selected").text();

    var personOCode = $("#personOCode").val();
    self.personCode = ko.observable(personOCode);

    SummaryFillterReport(1, null, startDate, endDate, "00:00:00", "23:00:00", username, currencyCode);
    function SummaryFillterReport(page, personOcode, startDate, endDate, startTime, endTime, username, currencyCode) {
        self.IsMember(null);
        self.IsNoMember(null);

        //value total current person
        var totalBetCurrentPerson = 0;
        var totalWinCurrentPerson = 0;

        //value total no-member
        var totalBetNoMember = 0;
        var totalWinNoMember = 0;

        //value total member
        var totalBetMember = 0;
        var totalWinMember = 0;
        //value change currency 
        var totalBetRateCurrencyOCodeMember = 0;
        var totalWinRateCurrencyOCodeMember = 0;

        $.getJSON(summary_report_url, {
            page: page,
            startDate: startDate,
            endDate: endDate,
            startTime: startTime,
            endTime: endTime,
            username: username,
            currencyCode: currencyCode,
            personOcode: personOcode
        }, function (data) {

            ShowProcessing();

            self.mapCurrentSummaryReport.removeAll();
            self.mapSummaryReportNoMember.removeAll();
            self.mapMember.removeAll();

            //$("#summary-member tbody").html("");
            var index = 0;
            //var index = (data.TransactionDetails.CurrentPage - 1) * data.TransactionDetails.TotalItemPerPage;
            ko.utils.arrayMap(data.SummaryResp.PageList, function (i) {
                switch (i.CurrentNamePerson) {
                    case ALTMainInfo.CurrentNamePersons.CurrentPerson:
                        self.IsCurrentName(i.Type);

                        self.mapCurrentSummaryReport.push(new SummaryViewModel(startDate, endDate, startTime, endTime, index, i));

                        totalBetCurrentPerson += i.BetAmountRateCurrencyCode;
                        totalWinCurrentPerson += i.WinAmountRateCurrencyCode;

                        self.totalBetCurrentPersons(numeral(totalBetCurrentPerson).format('0,0.0000'));
                        self.totalWinCurrentPersons(numeral(totalWinCurrentPerson).format('0,0.0000'));
                        self.totalWinLossCurrentPersons(totalWinCurrentPerson - totalBetCurrentPerson);
                        self.totalWinLossCurrentPersonsHTML(numeral(totalWinCurrentPerson - totalBetCurrentPerson).format('0,0.0000'));
                        break;
                    case ALTMainInfo.CurrentNamePersons.NonMember:
                        self.IsNoMember(i.Type);
                        self.mapSummaryReportNoMember.push(new SummaryViewModel(startDate, endDate, startTime, endTime, index, i));

                        totalBetNoMember += i.BetAmountRateCurrencyCode;
                        totalWinNoMember += i.WinAmountRateCurrencyCode;
                        self.totalBetNoMembers(numeral(totalBetNoMember).format('0,0.0000'));
                        self.totalWinNoMembers(numeral(totalWinNoMember).format('0,0.0000'));
                        self.totalWinLossNoMembers(totalWinNoMember - totalBetNoMember);
                        self.totalWinLossNoMembersHTML(numeral(totalWinNoMember - totalBetNoMember).format('0,0.0000'));
                        break;
                    case ALTMainInfo.CurrentNamePersons.Member:
                        self.IsMember(i.Type);
                        self.mapMember.push(new SummaryViewModel(startDate, endDate, startTime, endTime, index, i));

                        totalBetMember += i.BetAmount;
                        totalWinMember += i.WinAmount;

                        totalBetRateCurrencyOCodeMember += i.BetAmountRateCurrencyCode;
                        totalWinRateCurrencyOCodeMember += i.WinAmountRateCurrencyCode;

                        self.totalBetMembers(numeral(totalBetMember).format('0,0.0000'));
                        self.totalWinMembers(numeral(totalWinMember).format('0,0.0000'));
                        self.totalWinLossMembers(totalWinMember - totalBetMember);
                        self.totalWinLossMembersHTML(numeral(totalWinMember - totalBetMember).format('0,0.0000'));

                        self.totalWinLossChangeRateCurrencyMembers(totalWinRateCurrencyOCodeMember - totalBetRateCurrencyOCodeMember);
                        self.totalWinLossChangeRateCurrencyMembersHTML(numeral(totalWinRateCurrencyOCodeMember - totalBetRateCurrencyOCodeMember).format('0,0.0000'));
                        break;
                    default:
                        break;
                }

                index++;
            });

            HideProcessing();

            //if (data.TransactionDetails.PageList.length > 0) {
            //    var totalPages = parseInt(data.TransactionDetails.TotalPages);
            //    $('#pagination-transaction-detail-report').twbsPagination({
            //        totalPages: totalPages,
            //        visiblePages: 10,
            //        initiateStartPageClick: false,
            //        onPageClick: function (event, page) {
            //            ShowProcessing();
            //            var startDate = $("#StartDate").val();
            //            var endDate = $("#EndDate").val();
            //            var startTime = $("#StartTime").val();
            //            var endTime = $("#EndTime").val();
            //            var username = $("#Username").val();
            //            var currencyCode = $("#CurrencyCode option:selected").text();
            //            getTransactionReportDetail(page, startDate, endDate, startTime, endTime, username, currencyCode);
            //            HideProcessing();
            //        }
            //    });
            //}
            //else {
            //    var str = "<tr role='row' class='text-center' style='background:#d8d5d5;'>";
            //    str += "<td colspan='10'>No data</td>";
            //    str += "</tr>";
            //    $("#transaction-detail tbody").html(str);
            //}
        });
    }


    self.personDownlineOfMemer = function (data) {
        self.personCode(data.PersonOCode());

        var startDate = $("#StartDate").val();
        var endDate = $("#EndDate").val();

        var startTime = $("#StartTime").val();
        var endTime = $("#EndTime").val();

        SummaryFillterReport(1, data.PersonOCode(), startDate, endDate, startTime, endTime, null, data.CurrencyCode());
    }

    self.personDownline = function (data) {
        self.personCode(data.PersonOCode());

        var startDate = $("#StartDate").val();
        var endDate = $("#EndDate").val();

        var startTime = $("#StartTime").val();
        var endTime = $("#EndTime").val();
        var currencyCode = $("#CurrencyCode option:selected").text();

        GetBreadcrumbPerson(data.PersonOCode());
        SummaryFillterReport(1, data.PersonOCode(), startDate, endDate, startTime, endTime, null, currencyCode);
    }

    self.submit = function () {

        var startDate = $("#StartDate").val();
        var endDate = $("#EndDate").val();

        var startTime = $("#StartTime").val();
        var endTime = $("#EndTime").val();

        var username = $("#Username").val();
        var currencyCode = $("#CurrencyCode option:selected").text();

        SummaryFillterReport(1, null, startDate, endDate, startTime, endTime, username, currencyCode);
    }

    self.GetBreadcrumbPerson = ko.observableArray();
    GetBreadcrumbPerson(personOCode);

    function GetBreadcrumbPerson(personOCode) {
        $.getJSON(person_breadcrumb_url, { ocode: personOCode }, function (breadcrumb) {
            self.GetBreadcrumbPerson.removeAll();
            for (var key in breadcrumb) {
                if (breadcrumb.hasOwnProperty(key)) {
                    self.GetBreadcrumbPerson.push(new GetBreadcrumb(key, breadcrumb[key]));
                }
            }
        });
    }

    self.breadcrumbclickpersondownline = function (data) {
        var startDate = $("#StartDate").val();
        var endDate = $("#EndDate").val();

        var startTime = $("#StartTime").val();
        var endTime = $("#EndTime").val();

        var username = $("#Username").val();
        var currencyCode = $("#CurrencyCode option:selected").text();

        self.personCode(data.key());
        GetBreadcrumbPerson(data.key());

        SummaryFillterReport(1, data.key(), startDate, endDate, startTime, endTime, username, currencyCode);
    };

    self.changeCurrencyOCode = function (_personOCode) {
        var startDate = $("#StartDate").val();
        var endDate = $("#EndDate").val();

        var startTime = $("#StartTime").val();
        var endTime = $("#EndTime").val();

        var username = $("#Username").val();
        var currencyCode = $("#CurrencyCode option:selected").text();

        $(".rateCurrencyOCode").text(currencyCode);
        SummaryFillterReport(1, _personOCode, startDate, endDate, startTime, endTime, username, currencyCode);
    }
};

var SummaryReports = new SummaryReportViewModels();
ko.applyBindings(SummaryReports, document.getElementById("summary-report"));