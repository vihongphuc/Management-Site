﻿
var Person = function (index, data) {    
    var self = this;    
    self.index = index;
    self.UplineOCode = ko.observable(data.UplineOCode);
    self.OCode = ko.observable(data.OCode);
    self.Username = ko.observable(data.Username);
    self.Firstname = ko.observable(data.Firstname);
    self.Lastname = ko.observable(data.Lastname);

    switch (data.Status) {
        case 0:
            self.Status = ko.observable(ALTMainInfo.PersonStatus.Active);
            break;
        case 1:
            self.Status = ko.observable(ALTMainInfo.PersonStatus.Suspended);
            break;
        case 2:
            self.Status = ko.observable(ALTMainInfo.PersonStatus.Disabled);
            break;
    }
    switch (data.Type) {
        case 0:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.Admin);
            break;
        case 1:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.SuperShareHolder);
            break;
        case 2:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.ShareHolder);
            break;
        case 3:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.SuperSenior);
            break;
        case 4:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.Senior);
            break;
        case 5:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.SuperMaster);
            break;
        case 6:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.Master);
            break;
        case 7:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.SuperAgent);
            break;
        case 8:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.Agent);
            break;
        case 9:
            self.Type = ko.observable(ALTMainInfo.PersonTypes.Member);
            break;
        default:
            self.Type = ko.observable(data.Type);
    }

    self.Currencies = mapDictionaryCurrecyToArray(data.Currencies);
    self.LastLoginIP = ko.observable(data.LastLoginIP);
    self.LastLogin = ko.observable(moment(data.LastLogin).format('MM/DD/YYYY'));
    self.Availables = mapDictionaryAvaiableOrMaxCreditToArray(data.Availables, data.OCode);
    self.MaxCredits = mapDictionaryAvaiableOrMaxCreditToArray(data.MaxCredits);
    self.TotalDownline = ko.observable(data.TotalDownline);
    return self
};

var CurrencyModel = function (currency, value, ocode) {
    var self = this;
    self.CurrencyCode = ko.observable(currency);
    self.value = ko.observable(value);
    return self;
};

var ModelAvaiableOrMaxCreditCurrent = function (currency, value) {
    var self = this;
    self.CurrencyCode = ko.observable(currency);
    self.value = ko.observable(numeral(value).format('0,0.0000'));
    return self;
};

function mapDictionaryAvaiableOrMaxCreditToArray(dictionary, OCode) {
    var result = ko.observableArray();
    for (var key in dictionary) {
        if (dictionary.hasOwnProperty(key)) {
            result.push(new ModelAvaiableOrMaxCreditCurrent(key, dictionary[key], OCode));
        }
    }
    return result;
}

function mapDictionaryCurrecyToArray(dictionary) {
    var result = ko.observableArray();
    for (var key in dictionary) {
        if (dictionary.hasOwnProperty(key)) {
            result.push(new CurrencyModel(key, dictionary[key]));
        }
    }
    return result;
}

var GetBreadcrumb = function (key, value) {
    var self = this;
    self.key = ko.observable(key);
    self.value = ko.observable(value);
    return self;
};

function PersonViewModel() {
    var self = this;
    var isloadingGrid = true;
    var currentPage = 1;
    var ocodeDownline;

    self.persons = ko.observableArray();
    self.index = ko.observable(0);
    self.current = ko.observableArray();

    self.password = ko.observable().extend({
        required: {
            message: 'Please input your password.'
        },
        minLength: {
            params: 6,
            message: 'Password must be at least 06 characters.'
        }
        //pattern: {
        //    message: "Password must have at least one number and one letter and one special character",
        //    params: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$/
        //}
    });

    self.confirmpassword = ko.observable().extend({
        required: {
            message: 'Please input your confirm password'
        },
        minLength: {
            params: 6,
            message: 'Confirm password must be at least 06 characters.'
        },
        equal:
           {
               params: self.password,
               message: "The confirm password does not match."
           }
    });

    var ocode = $("#select-ocode").data('ocode');
    getPersonDownline(ocode, currentPage);

    function getPersonDownline(ocode, pageIndex) {
        GetBreadcrumbPerson(ocode);
        $.getJSON(person_downline_url, { ocode: ocode, isMember: null, pageIndex: pageIndex }, function (data) {
            ShowProcessing();

            var str = "";
            var index = (data.CurrentPage - 1) * data.TotalItemPerPage;
            $('#persondownlist tbody').html(str);

            ko.utils.arrayMap(data.PageList, function (i) {
                self.persons.push(new Person(index, i));
                index++;
            });

            if (data.PageList.length > 0) {
                PaginationPerson("#pagination-persons", data.TotalPages, currentPage, ALTMainInfo.Pagination.Pagination25Record, isloadingGrid);
            }
            else {
                str = "<tr role='row' class='text-center' style='background:#d8d5d5;'>";
                str += "<td colspan='13'>No data</td>";
                str += "</tr>";
                $('#persondownlist tbody').html(str);
            }

            HideProcessing();
        })
    }

    self.GetBreadcrumbPerson = ko.observableArray();

    function GetBreadcrumbPerson(ocode) {
        $.getJSON(person_breadcrumb_url, { ocode: ocode }, function (breadcrumb) {
            self.GetBreadcrumbPerson.removeAll();
            for (var key in breadcrumb) {
                if (breadcrumb.hasOwnProperty(key)) {
                    self.GetBreadcrumbPerson.push(new GetBreadcrumb(key, breadcrumb[key]));
                }
            }
        });
    }

    self.getPage = function (data) {
        $('#pagination-persons').twbsPagination('destroy');
        self.persons.removeAll();
        self.totalPersons.removeAll();
        getPersonDownline(ocode, data.itemPading._latestValue);
    }

    self.status = function (status, data) {
        var uplineOCode = data.UplineOCode();
        var ocode = data.OCode();
        if (data.Status() == status) {
            return false;
        }
        var username = data.Username();
        $.ajax({
            url: person_status_url,
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ status: status, ocode: ocode }),
            success: function (return_data) {
                if (return_data.ResultObj == "true") {
                    getPersonDownline(uplineOCode, currentPage);
                    data.Status(status);
                }
                toastr.success(FormatString(ErrorMessage.Person.SusscessSataus, username));
            },
            error: function (xhr) {
                toastr.error(FormatString(ErrorMessage.Person.ErrorSataus, error));
            }
        });
    }

    self.breadcrumbclickpersondownline = function (data) {
        $('#pagination-persons').twbsPagination('destroy');
        getPersonDownline(data.key(), 1);
        ocodeDownline = data.key();
    };

    self.personDownline = function (vm) {
        ShowProcessing();
        self.current.push(vm);
        self.persons.removeAll();
        $('#pagination-persons').twbsPagination('destroy');        

        getPersonDownline(vm.OCode, 1);
        ocode = vm.OCode;
        ocodeDownline = vm.OCode;
        isloadingGrid = true;
        currentPage = 1;
    }
    self.currentPeople = ko.observable(null);
    self.bindUsername = function (vm) {
        self.isResetValid.errors.showAllMessages(false);

        $('#reset-new-password').val('');
        $('#reset-confirm-password').val('');

        $('#myModalSelectPerson').modal('show');
        self.currentPeople(vm);
    };
    self.isResetValid = ko.validatedObservable({
        password: self.password,
        confirmpassword: self.confirmpassword
    });

    ko.validation.init({
        insertMessages: false,
        decorateElement: true,
        errorElementClass: 'has-error'
    });

    self.resetpassword = function (data) {
        var ocode = data.OCode();
        var oldPassword = "";
        var newPassword = self.password();
        if (self.isResetValid.isValid()) {
            $.ajax({
                url: person_change_password_url,
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ ocode: ocode, oldPassword: oldPassword, newPassword: newPassword }),
                success: function (return_data) {
                    if (return_data.ResultObj) {
                        $('#myModalSelectPerson').modal('hide');
                        toastr.success("Username " + data.Username() + " password has been changed.");
                    }
                    else {
                        $('#myModalSelectPerson').modal('hide');
                    }
                },
                error: function (xhr, status, error) {
                    $('#myModalSelectPerson').modal('hide');
                }
            });
        }
        else {
            self.isResetValid.errors.showAllMessages();
        }
    }

    function PaginationPerson(nameClassAndIdHtml, Total_pages, currentPage, PageSize, isloadingGrid) {
        var totalPages = parseInt(Total_pages / PageSize);
        if ((Total_pages % PageSize) > 0) {
            totalPages = totalPages + 1;
        }
        totalPages = totalPages == 0 ? 1 : totalPages;
        var visiblePages = Total_pages < PageSize ? 1 : Math.floor(Total_pages / PageSize) + 1;

        $(nameClassAndIdHtml).twbsPagination({
            totalPages: totalPages,
            visiblePages: visiblePages < 10 ? visiblePages : 10,
            onPageClick: function (event, page) {
                if (!isloadingGrid) {
                    self.persons.removeAll();
                    currentPage = page;
                    getPersonDownline(ocodeDownline, page);
                }
                isloadingGrid = false;
            }
        });
    }

}

var viewModel = new PersonViewModel();
ko.applyBindings(viewModel, document.getElementById("personDownline"));



