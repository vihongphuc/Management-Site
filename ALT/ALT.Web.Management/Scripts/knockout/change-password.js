﻿function ChangePasswordViewModel() {
    var self = this;
    self.changepassword = ko.observableArray();

    self.oldpassword = ko.observable().extend({
        required: {
            message: 'Please input your old password.'
        },
        minLength: {
            params: 6,
            message: 'Password must be at least 06 characters.'
        }
    });

    self.newpassword = ko.observable().extend({
        required: {
            message: 'Please input your new password.'
        },
        minLength: {
            params: 6,
            message: 'New password must be at least 06 characters.'
        }
    });

    self.confirmchangepassword = ko.observable().extend({
        required: {
            message: 'Please input your confirm password.'
        },
        minLength: {
            params: 6,
            message: 'New password must be at least 06 characters.'
        },
        equal:
           {
               params: self.newpassword,
               message: "The confirmation does not match the password"
           }
    });

    self.isChangePasswordValid = ko.validatedObservable({
        oldpassword: self.oldpassword,
        newpassword: self.newpassword,
        confirmchangepassword: self.confirmchangepassword
    });

    ko.validation.init({
        insertMessages: false,
        decorateElement: true,
        errorElementClass: 'has-error'
    });

    self.ShowChangePassword = function () {
        self.isChangePasswordValid.errors.showAllMessages(false);

        $('#oldpassword').val('');
        $('#newpassword').val('');
        $('#confirmchangepassword ').val('');

        $('#myModal').modal('show');
    }

    self.savechangepassword = function (data) {
        var ocode = $('#ocode').data('ocode');
        var username = $('#username').data('username');
        var oldpassword = $('#oldpassword').val();
        var newpassword = $('#newpassword').val();
        if (self.isChangePasswordValid.isValid()) {
            $.ajax({
                url: 'ChangePassword',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ ocode: ocode, oldPassword: oldpassword, newPassword: newpassword }),
                success: function (return_data) {
                    if (return_data.ResultObj) {
                        $('#myModal').modal('hide');
                        toastr.success("Username " + username + " password has been changed ");
                    }
                    else {
                        $('#myModal').modal('hide');
                    }
                },
                error: function (xhr) {
                    $('#myModal').modal('hide');
                    toastr.success("Username " + username + " password has been changed ");
                }
            });
        }
        else {
            self.isChangePasswordValid.errors.showAllMessages();
        }
    }

}
var changePasswordviewModel = new ChangePasswordViewModel();
ko.applyBindings(changePasswordviewModel, document.getElementById("content-changepassword"));
