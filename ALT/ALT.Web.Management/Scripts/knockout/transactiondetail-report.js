﻿var TransactionReportViewModel = function (index, data) {
    var self = this;
    self.index = ko.observable(index);
    self.PersonOCode = ko.observable(data.PersonOCode);
    self.GameOCode = ko.observable(data.GameOCode);
    self.GameCode = ko.observable(data.GameCode);
    self.GameName = ko.observable(data.GameName);
    self.GameCategory = ko.observable(data.GameCategory);
    self.CurrencyCode = ko.observable(data.CurrencyCode);
    self.GameTransactionOCode = ko.observable(data.GameTransactionOCode);
    self.Type = ko.observable(data.Type);
    self.Username = ko.observable(data.Username);
    self.Amount = ko.observable(data.Amount);
    self.Result = ko.observable(data.Result);
    self.StartBalance = ko.observable(numeral(data.StartBalance).format('0,0.0000'));
    self.EndBalance = ko.observable(numeral(data.EndBalance).format('0,0.0000'));
    self.EndDate = ko.observable(numeral(data.EndDate).format('0,0.0000'));
    self.PointRate = ko.observable(data.PointRate);
    self.Date = ko.observable(moment(data.Time).format("MM/DD/YYYY"));
    self.Time = ko.observable(moment(data.Time).format("HH:mm:ss"))
    self.Details = ko.observable(data.Details);

    return self;
}

function TransactionReportDetailViewModel() {
    var self = this;
    self.transactionDetails = ko.observableArray();

    var startDate = $("#StartDate").val();
    var endDate = $("#EndDate").val();

    var startTime = $("#StartTime").val();
    var endTime = $("#EndTime").val();

    var username = $("#Username").val();

    //bind when paging if value filter.
    $("#CurrencyCode").find('option').each(function (i, opt) {
        var currency = $("#filterCurrencyOcode").val();
        if (opt.text === currency) {
            $(opt).attr('selected', 'selected');
            var valueRate = $(opt).attr('selected', 'selected').val();
            $("#Rate").val(numeral(valueRate).value());
        }
        else {
            $(opt).removeAttr('selected');
        }
    });

    var currencyCode = $("#CurrencyCode option:selected").text();

    getTransactionReportDetail(1, startDate, endDate, startTime, endTime, username, currencyCode);
    function getTransactionReportDetail(page, startDate, endDate, startTime, endTime, username, currencyCode) {
        $.getJSON(transaction_detail_url, {
            page: page,
            startDate: startDate,
            endDate: endDate,
            startTime: startTime,
            endTime: endTime,
            username: username,
            currencyCode: currencyCode
        }, function (data) {
            $("#transaction-detail tbody").html("");
            var index = (data.TransactionDetails.CurrentPage - 1) * data.TransactionDetails.TotalItemPerPage;
            self.transactionDetails.removeAll();
            ko.utils.arrayMap(data.TransactionDetails.PageList, function (i) {
                self.transactionDetails.push(new TransactionReportViewModel(index, i));
                index++;
            });

            if (data.TransactionDetails.PageList.length > 0) {
                var totalPages = parseInt(data.TransactionDetails.TotalPages);
                $('#pagination-transaction-detail-report').twbsPagination({
                    totalPages: totalPages,
                    visiblePages: 10,
                    initiateStartPageClick: false,
                    onPageClick: function (event, page) {
                        ShowProcessing();
                        var startDate = $("#StartDate").val();
                        var endDate = $("#EndDate").val();
                        var startTime = $("#StartTime").val();
                        var endTime = $("#EndTime").val();
                        var username = $("#Username").val();
                        var currencyCode = $("#CurrencyCode option:selected").text();

                        getTransactionReportDetail(page, startDate, endDate, startTime, endTime, username, currencyCode);
                        HideProcessing();
                    }
                });
            }
            else {
                var str = "<tr role='row' class='text-center' style='background:#d8d5d5;'>";
                str += "<td colspan='10'>No data</td>";
                str += "</tr>";
                $("#transaction-detail tbody").html(str);
            }
        });
    }

    self.submit = function () {
        ShowProcessing();

        var startDate = $("#StartDate").val();
        var endDate = $("#EndDate").val();
        var startTime = $("#StartTime").val();
        var endTime = $("#EndTime").val();
        var username = $("#Username").val();
        var currencyCode = $("#CurrencyCode option:selected").text();
        $('#pagination-transaction-detail-report').twbsPagination('destroy');

        getTransactionReportDetail(1, startDate, endDate, startTime, endTime, username, currencyCode);
        HideProcessing();
    }
};

var TransactionReportViewModels = new TransactionReportDetailViewModel();
ko.applyBindings(TransactionReportViewModels, document.getElementById("transaction-report"));