﻿var DailyWinLossReportViewModel = function (index, data) {
    var self = this;
    self.index = ko.observable(index);
    self.PersonOCode = ko.observable(data.PersonOCode);
    self.TotalUsername = ko.observable(data.TotalUsername);
    self.Username = ko.observable(data.Username);
    self.Date = ko.observable(moment(data.Time).format("MM/DD/YYYY"));
    self.Time = ko.observable(moment(data.Time).format("HH:mm:ss"));
    self.TotalAmount = ko.observable(numeral(data.TotalAmount).format('0,0.0000'));
    self.TotalResult = ko.observable(numeral(data.TotalResult).format('0,0.0000'));    
    self.TotalNewUserBet = ko.observable(data.TotalNewUserBet);

    self.TotalWinLoss = ko.observable(data.TotalWinLoss);
    self.TotalWinLossHtml = ko.observable(numeral(data.TotalWinLoss).format('0,0.0000'));
    return self;
}


function DailyWinLossViewModel() {
    var self = this;
    self.dailyWinLoss = ko.observableArray();

    self.totalBets = ko.observable(0);
    self.totalWins = ko.observable(0);
    self.totalWinLosss = ko.observable(0);
    self.totalWinLosssHTML = ko.observable(0);

    self.totalUserBets = ko.observable(0);
    self.totalNewUserBets = ko.observable(0);

    var startDate = $("#StartDate").val();
    var endDate = $("#EndDate").val();
    var username = $("#Username").val();
    getDailyWinLossReport(1, startDate, endDate, "00:00:00", "23:00:00", username);
    function getDailyWinLossReport(page, startDate, endDate, startTime, endTime, username) {
        var totalBet = 0;
        var totalWin = 0;
        var totalWinLoss = 0;
        var totalUserBets = 0;
        var totalNewUserBet = 0;

        $.getJSON(daily_win_loss_url, {
            page: page,
            startDate: startDate,
            endDate: endDate,
            startTime: startTime,
            endTime: endTime,
            username: username
        }, function (data) {
            $("#transaction-detail tbody").html("");
            var index = (data.WinLossResp.CurrentPage - 1) * data.WinLossResp.TotalItemPerPage;
            self.dailyWinLoss.removeAll();
            ko.utils.arrayMap(data.WinLossResp.PageList, function (i) {
                self.dailyWinLoss.push(new DailyWinLossReportViewModel(index, i));
                totalBet += i.TotalAmount;
                totalWin += i.TotalResult;
                totalWinLoss += i.TotalWinLoss;
                totalUserBets += i.TotalUsername;
                totalNewUserBet += i.TotalNewUserBet;

                index++;
            });

            self.totalBets(numeral(totalBet).format('0,0.0000'));
            self.totalWins(numeral(totalWin).format('0,0.0000'));
            self.totalWinLosss(totalWinLoss);
            self.totalWinLosssHTML(numeral(totalWinLoss).format('0,0.0000'));

            self.totalUserBets(totalUserBets);
            self.totalNewUserBets(totalNewUserBet);

            if (data.WinLossResp.PageList.length > 0) {
                var totalPages = parseInt(data.WinLossResp.TotalPages);
                $('#pagination-daily-win-loss-rp').twbsPagination({
                    totalPages: totalPages,
                    visiblePages: 10,
                    initiateStartPageClick: false,
                    onPageClick: function (event, page) {
                        ShowProcessing();
                        var startDate = $("#StartDate").val();
                        var endDate = $("#EndDate").val();
                        var startTime = $("#StartTime").val();
                        var endTime = $("#EndTime").val();
                        var username = $("#Username").val();

                        getDailyWinLossReport(page, startDate, endDate, startTime, endTime, username);
                        HideProcessing();
                    }
                });
            }
            else {
                var str = "<tr role='row' class='text-center' style='background:#d8d5d5;'>";
                str += "<td colspan='10'>No data</td>";
                str += "</tr>";
                $("#transaction-detail tbody").html(str);
            }
        });
    }
    self.submit = function () {
        ShowProcessing();
        var startDate = $("#StartDate").val();
        var endDate = $("#EndDate").val();
        var startTime = $("#StartTime").val();
        var endTime = $("#EndTime").val();
        var username = $("#Username").val();
        $('#pagination-daily-win-loss-rp').twbsPagination('destroy');

        getDailyWinLossReport(1, startDate, endDate, startTime, endTime, username);
        HideProcessing();
    }
};
var dailyWinLossViewModels = new DailyWinLossViewModel();
ko.applyBindings(dailyWinLossViewModels, document.getElementById("daily-win-loss-report"));