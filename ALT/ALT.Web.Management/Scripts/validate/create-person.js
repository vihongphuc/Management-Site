﻿var isUpdate = !($('#IsUpdate').val() == 'True');
var strIconError = '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> ';
var current_type = $('#current-type').val();

$('#Person').validate({
    rules: {
        Password: {
            required: isUpdate,
            minlength: 6,
            pwcheck: false

        },
        Email: {
            emcheck: true
        },
    },
    messages: {
        Password: {
            required: ErrorMessage.CreatePerson.PasswordRequired,
            minlength: ErrorMessage.CreatePerson.PasswordMinLength,
            pwcheck: ErrorMessage.CreatePerson.PasswordSpecialCharacter
        },
        Email: {
            emcheck: ErrorMessage.CreatePerson.CheckYourEmail
        },
    },
});

$.validator.addMethod("pwcheck",
                       function (value, element) {
                           return /^(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$/.test(value);
                       });

$.validator.addMethod("emcheck",
                       function (value, element) {
                           return /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(value);
                       });

$('.input-balance').change(function () {
    var inputValue = $(this).val();
    var input_value_balance = numeral(inputValue).value();
    var currency = $(this).data('input-balance');

    ValitionInPutBalance(currency, inputValue, input_value_balance);

});

$('.input-max-credit').change(function () {
    var inputValue = $(this).val();
    var input_value_max_credit = numeral(inputValue).value();
    var currency = $(this).data('input-max-credit-currency');
    var inputValueBalance = $('#input-value-balance_' + currency).val();

    ValitionInPutMaxMemberCredit(currency, inputValue, input_value_max_credit, inputValueBalance);

});

$('.input-max-win-limit').change(function () {
    var inputValue = $(this).val();
    var input_value_max_win_limit = numeral(inputValue).value();

    ValitionInPutMaxWinLimit(inputValue, input_value_max_win_limit, $(this));
});

$('.input-min-loss-limit').change(function () {
    var inputValue = $(this).val();
    var input_value_min_loss_limit = numeral(inputValue).value();

    ValitionInPutMinLossLimit(inputValue, input_value_min_loss_limit, $(this));

});

$('#Phone').change(function () {
    //check phone no copy and paste
    var valPhone = $('#Phone').val();
    var display = $('#errorPhone').css('display', '');
    var display_none = $('#errorPhone').css('display', 'none');

    if (isNaN(valPhone)) {
        $('#errorPhone').html(strIconError + " Phone " + ErrorMessage.CreatePerson.IsNotNumber);
        $(display.show());
        return false;
    }
    if (valPhone !== "" && valPhone === undefined) {
        if (Number(valPhone).value() < 0) {
            $('#errorPhone').html(strIconError + " Phone " + ErrorMessage.CreatePerson.GreaterThanZero);
            $(display.show());
            return false;
        }
    }
    $(display_none.hide());
});

function ValitionInPutBalance(currency, inputValue, input_value_balance) {
    //note
    var personUplineBalance = numeral($('#person-balance_' + currency).val()).value();
    var personUplineMaxCredit = numeral($('#person-max-credit_' + currency).val()).value();
    var _balanceCurrentByCurrency = $('#balance_' + currency);
    var input_max_credit = numeral($('#input-value-max-credit_' + currency).val()).value();
    var display = $(_balanceCurrentByCurrency).css('display', '');
    var display_none = $(_balanceCurrentByCurrency).css('display', 'none');

    //check copy and value no true.
    var theCredit = ErrorMessage.CreatePerson.nameTheCredit;
    var isbool = ValitonIsNoMumberAndGreaterThanZero(theCredit, inputValue, input_value_balance, _balanceCurrentByCurrency, display, display_none);
    if (isbool == false) {
        return false;
    }

    if (current_type !== ALTMainInfo.PersonTypes.SuperShareHolder && personUplineBalance > 0) {
        if (current_type === ALTMainInfo.PersonTypes.Member && input_value_balance != 0) {
            if (personUplineBalance < personUplineMaxCredit) {
                if (input_value_balance > personUplineBalance && current_type == ALTMainInfo.PersonTypes.Member) {
                    $(_balanceCurrentByCurrency).html(strIconError + ErrorMessage.CreatePerson.LimitMaxCredit + personUplineBalance);
                    $(display.show());
                    return false;
                }
            }
            else {
                if (input_value_balance > personUplineMaxCredit) {
                    $(_balanceCurrentByCurrency).html(strIconError + ErrorMessage.CreatePerson.LimitMaxMemberCredit + personUplineMaxCredit);
                    $(display.show());
                    return false;
                }

                $(display_none.hide());
            }
        }
        else {
            if (input_value_balance > personUplineBalance) {
                $(_balanceCurrentByCurrency).html(strIconError + ErrorMessage.CreatePerson.LimitMaxCredit + personUplineBalance);
                $(display.show());
                return false;
            }

            if (input_value_balance < input_max_credit) {
                $(_balanceCurrentByCurrency).html(strIconError + ErrorMessage.CreatePerson.CreditGreaterThanOrEqualCredit + input_max_credit);
                $(display.show());
                return false;
            }
        }
    }
    if (current_type !== ALTMainInfo.PersonTypes.Member) {
        if (input_value_balance < input_max_credit) {
            $('#max-member-credit_' + currency).html(strIconError + ErrorMessage.CreatePerson.MaxMemberCreditLessThanOrEqualCredit);
            $('#max-member-credit_' + currency).css('display', '');
            return false;
        }
        else {
            $('#max-member-credit_' + currency).css('display', 'none');
        }
    }
}

function ValitionInPutMaxMemberCredit(currency, inputValue, input_value_max_credit, inputValueBalance) {
    //note
    var _maxMemberCreditPersonUplineByCurrency = numeral($('#person-max-credit-Upline_' + currency).val()).value();
    var _maxMemberCreditCurrentByCurrency = $('#max-member-credit_' + currency);
    var personCurrentCredit = numeral($('#input-value-balance_' + currency).val()).value();

    var display = $(_maxMemberCreditCurrentByCurrency).css('display', '');
    var display_none = $(_maxMemberCreditCurrentByCurrency).css('display', 'none');

    //check copy and value no true.
    var theMaxMembercredit = ErrorMessage.CreatePerson.nametheMaxMembercredit;
    var isbool = ValitonIsNoMumberAndGreaterThanZero(theMaxMembercredit, inputValue, input_value_max_credit, _maxMemberCreditCurrentByCurrency, display, display_none);
    if (isbool == false) {
        return false;
    }

    if (current_type !== ALTMainInfo.PersonTypes.SuperShareHolder && _maxMemberCreditPersonUplineByCurrency > 0) {
        if (input_value_max_credit > _maxMemberCreditPersonUplineByCurrency) {
            $(_maxMemberCreditCurrentByCurrency).html(strIconError + ErrorMessage.CreatePerson.UplineMaxMemberCredit + _maxMemberCreditPersonUplineByCurrency);
            $(display.show());
            return false;
        }

        if (inputValueBalance > input_value_max_credit) {
            //note balance
            var _balanceCurrentByCurrency = $('#balance_' + currency);
            var display_none_balance = $(_balanceCurrentByCurrency).css('display', 'none');
        }

        //$(display_none.hide());
    }
    if (current_type !== ALTMainInfo.PersonTypes.Member) {
        if (input_value_max_credit > personCurrentCredit) {
            $(_maxMemberCreditCurrentByCurrency).html(strIconError + ErrorMessage.CreatePerson.MaxMemberCreditLessThanOrEqualCredit);
            $(display.show());
            return false;
        }
    }
}

function ValitionInPutMaxWinLimit(inputValue, input_value_max_win_limit, thisValueMaxWinLimit) {
    //note
    var type_max_win_limit = $(thisValueMaxWinLimit).data('type-max-win-limit');
    var currency_max_win_limit = $(thisValueMaxWinLimit).data('currency-max-win-limit');
    var personUplineBetSettingofMaxWinLimit = numeral($('#person-bettsetting_' + type_max_win_limit + currency_max_win_limit).val()).value();
    var _maxWinLimitPersonCurrentByCurrency = $('#max-win-limit_' + currency_max_win_limit);
    var display = $(_maxWinLimitPersonCurrentByCurrency).css('display', '');
    var display_none = $(_maxWinLimitPersonCurrentByCurrency).css('display', 'none');

    //check copy and value no true.
    var theMaxWinLimit = ErrorMessage.CreatePerson.nameTheMaxWinLimit;
    var isbool = ValitonIsNoMumberAndGreaterThanZero(theMaxWinLimit, inputValue, input_value_max_win_limit, _maxWinLimitPersonCurrentByCurrency, display, display_none);
    if (isbool == false) {
        return false;
    }

    if (current_type !== ALTMainInfo.PersonTypes.SuperShareHolder && personUplineBetSettingofMaxWinLimit > 0) {
        if (input_value_max_win_limit > personUplineBetSettingofMaxWinLimit) {
            $(_maxWinLimitPersonCurrentByCurrency).html(strIconError + ErrorMessage.CreatePerson.UplineMaxWinLimit + personUplineBetSettingofMaxWinLimit)
            $(display.show());
            return false;
        }
        //$(display_none.hide());
    }
}

function ValitionInPutMinLossLimit(inputValue, input_value_min_loss_limit, thisValueMinLossLimit) {
    //note
    var type_min_loss_limit = $(thisValueMinLossLimit).data('type-min-loss-limit');
    var currency_min_loss_limit = $(thisValueMinLossLimit).data('currency-min-loss-limit');

    var personUplineBetSettingofMinLossLimit = numeral($('#person-bettsetting_' + type_min_loss_limit + currency_min_loss_limit).val()).value();
    var _minLossLimitPersonCurrentByCurrency = $('#min-loss-limit_' + currency_min_loss_limit);
    var display = $(_minLossLimitPersonCurrentByCurrency).css('display', '');
    var display_none = $(_minLossLimitPersonCurrentByCurrency).css('display', 'none');

    //check copy and value no true.
    var theMinLossLimit = ErrorMessage.CreatePerson.nameTheMinLossLimit;
    var isbool = ValitonIsNoMumberAndGreaterThanZero(theMinLossLimit, inputValue, input_value_min_loss_limit, _minLossLimitPersonCurrentByCurrency, display, display_none);
    if (isbool == false) {
        return false;
    }

    if (current_type !== ALTMainInfo.PersonTypes.SuperShareHolder && personUplineBetSettingofMinLossLimit > 0) {
        if (input_value_min_loss_limit > personUplineBetSettingofMinLossLimit) {
            $(_minLossLimitPersonCurrentByCurrency).html(strIconError + ErrorMessage.CreatePerson.UplineMinLossLimit + personUplineBetSettingofMinLossLimit);
            $(display.show());
            return false;
        }
        //$(display_none.hide());
    }
}

function ValitonIsNoMumberAndGreaterThanZero(nameByInput, inputValue, valInputFormatNumber, _showErrorCurrentByCurrency, display, display_none) {
    if (isNaN(inputValue)) {
        $(_showErrorCurrentByCurrency).html(strIconError + nameByInput + ErrorMessage.CreatePerson.IsNotNumber);
        $(display.show());
        return false;
    }
    if (valInputFormatNumber === "" && valInputFormatNumber === undefined) {
        if (valInputFormatNumber < 0) {
            $(_showErrorCurrentByCurrency).html(strIconError + nameByInput + ErrorMessage.CreatePerson.GreaterThanZero);
            $(display.show());
            return false;
        }
    }
}

$('form').submit(function () {
    var currencysArr = $('#Currencies').val().split(',');
    var personType = $('#PersonType').val();
    var isUpdate = $('#IsUpdate').val();
    var username = $("#Username").val();

    var checkNotAvailable = $(".checkUsername").text();
    if (checkNotAvailable == ALTMainInfo.PersonStatus.NotAvailable) {
        return false;
    }

    ALTMainInfo.CheckUsername(username);

    if (isUpdate == "False") {      
        if (username == "") {
            var htmlerror = ""
            htmlerror += "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true' style='padding-left:10px'></span>";
            htmlerror += "<span class='sr-only'>Error:</span>";
            htmlerror += "<text>Username is not null.</text>";
            $(".checkUsername").html(htmlerror);

            return false;
        }

        if (personType != ALTMainInfo.PersonTypes.Member) {
            var createAgency = $("input[name='CreateAgency']").is(":checked");
            var createMember = $("input[name='CreateMember']").is(":checked");
            if ((createAgency || createMember) == false) {
                var htmlerror = ""
                htmlerror += "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true' style='padding-left:10px'></span>";
                htmlerror += "<span class='sr-only'>Error:</span>";
                htmlerror += "<text>Please check create agency and create member</text>";
                $(".agency-member").html(htmlerror);

                return false;
            }
            else {
                $(".agency-member").html("");
            }
        }

    }

    //check phone no copy and paste
    var valPhone = $('#Phone').val();
    var display = $('#errorPhone').css('display', '');

    if (isNaN(valPhone)) {
        $('#errorPhone').html(strIconError + " Phone " + ErrorMessage.CreatePerson.IsNotNumber);
        $(display.show());
        return false;
    }
    if (valPhone === "" && valPhone === undefined) {
        if (numeral(valPhone).value() < 0) {
            $('#errorPhone').html(strIconError + " Phone " + ErrorMessage.CreatePerson.GreaterThanZero);
            $(display.show());
            return false;
        }
    }


    if (current_type !== ALTMainInfo.PersonTypes.SuperShareHolder) {
        for (var i = 0; i < currencysArr.length; i++) {
            var inputValueBalance = $('#input-value-balance_' + currencysArr[i]).val();
            var input_value_balance = numeral(inputValueBalance).value();

            var isboolBalance = ValitionInPutBalance(currencysArr[i], inputValueBalance, input_value_balance);
            if (isboolBalance == false) {
                return false;
            }

            var inputValueMaxCredit = $('#input-value-max-credit_' + currencysArr[i]).val();
            var input_value_max_credit = numeral(inputValueMaxCredit).value();

            var isboolMaxMemberCredit = ValitionInPutMaxMemberCredit(currencysArr[i], inputValueMaxCredit, input_value_max_credit);
            if (isboolMaxMemberCredit == false) {
                return false;
            }

            var thisValueMaxWinLimit = $('#input-value-max-win-limit_' + currencysArr[i]);
            var inputValueMaxWinLimit = $('#input-value-max-win-limit_' + currencysArr[i]).val();
            var input_value_max_win_limit = numeral($(thisValueMaxWinLimit).val()).value();

            var isboolMaxWinLimit = ValitionInPutMaxWinLimit(inputValueMaxWinLimit, input_value_max_win_limit, thisValueMaxWinLimit);
            if (isboolMaxWinLimit == false) {
                return false;
            }

            var thisValueMinLossLimit = $('#input-value-min-loss-limit_' + currencysArr[i]);
            var inputValueMinLossLimit = $('#input-value-min-loss-limit_' + currencysArr[i]).val();
            var input_value_min_loss_limit = numeral($(inputValueMinLossLimit).val()).value();

            var isboolMinLossLimit = ValitionInPutMinLossLimit(inputValueMinLossLimit, input_value_min_loss_limit, thisValueMinLossLimit);
            if (isboolMinLossLimit == false) {
                return false;
            }
        }
    }
});