﻿var optionDate = {
    Daily: "Daily",
    Yesterday: "Yesterday",
    LastWeek: "LastWeek",
    DateNow: moment().format("MM/DD/YYYY")
}
$(function () {
    $("#Date").change(function () {
        var date = this.value;
        switch (date) {
            case optionDate.Daily:
                $("#StartDate").val(optionDate.DateNow);
                $("#EndDate").val(optionDate.DateNow);
                break;
            case optionDate.Yesterday:
                $("#StartDate").val(getYesterdaysDate());
                $("#EndDate").val(optionDate.DateNow);
                break
            case optionDate.LastWeek:
                $("#StartDate").val(getlastWeekDate());
                $("#EndDate").val(optionDate.DateNow);
                break;
            default:
                $("#StartDate").val(optionDate.DateNow);
                $("#EndDate").val(optionDate.DateNow);
                break;
        }
    });

    function getYesterdaysDate() {
        var date = new Date();
        return moment(date.setDate(date.getDate() - 1)).format("MM/DD/YYYY");
    }

    function getlastWeekDate() {
        var date = new Date();
        return moment(date.setDate(date.getDate() - 7)).format("MM/DD/YYYY");
    }

    var startTime = new Date();
    startTime.setHours(0, 0, 0);

    var endTime = new Date();
    endTime.setHours(23, 0, 0);

    $('#StartDate').datetimepicker({ format: "MM/DD/YYYY" });
    $('#StartTime').datetimepicker({
        ignoreReadonly: true,
        format: 'HH:mm',
        defaultDate: startTime
    });

    $('#EndDate').datetimepicker({ format: "MM/DD/YYYY" });
    $('#EndTime').datetimepicker({
        ignoreReadonly: true,
        format: 'HH:mm',
        defaultDate: endTime
    });

    $('#StartDate').on("dp.change", function (e) {
        $('#EndDate').data("DateTimePicker").show();
    });

    $('#Username').typeahead({
        source: function (query, process) {
            return $.get(autocomplete_username_url, { username: query }, function (data) {
                data = JSON.parse(JSON.stringify(data));
                return process(data);
            });
        }
    });
});