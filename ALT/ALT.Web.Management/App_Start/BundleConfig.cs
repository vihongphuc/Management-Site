﻿using System.Web;
using System.Web.Optimization;

namespace ALT.Web.Management
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                                         "~/Scripts/jquery-{version}.js"
                                         ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                                         "~/Scripts/jquery.validate*",
                                         "~/Scripts/jquery-2.2.3.min.js",
                                         "~/Scripts/bootstrap.min.js",
                                         "~/Scripts/theme-js/demo.js",
                                         "~/Scripts/custom-js/loading.js",
                                         "~/Scripts/knockout-3.4.0.js",
                                         "~/Scripts/custom-js/custome.js",
                                         "~/Scripts/toastr.js",
                                         "~/Scripts/toastr.min.js",
                                         "~/Scripts/knockout.validation.js",
                                         "~/Scripts/moment.js",
                                         "~/Scripts/numeral/numeral.js",
                                         "~/Scripts/jquery.twbsPagination.js",
                                         "~/Scripts/custom-js/error-message.js",
                                         "~/Scripts/bootstrap-datetimepicker.js",
                                         "~/Scripts/theme-js/app.min.js",
                                         "~/Scripts/custom-js/main.js",
                                         "~/Scripts/bootstrap3-typeahead.min.js"
                                        ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                                         "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/custome-javascript").Include(
                                        "~/Scripts/validate/login.js"
                                        ));

            bundles.Add(new ScriptBundle("~/bundles/game-budget").Include(
                                       "~/Scripts/knockout/game-budget.js"));

            bundles.Add(new ScriptBundle("~/bundles/custome-person-javascript").Include(
                                         "~/Scripts/validate/create-person.js",                                         
                                         "~/Scripts/custom-js/position-taking.js",
                                         "~/Scripts/custom-js/checkboxcurrency-tabcurrency.js",
                                         "~/Scripts/custom-js/check-username.js",
                                         "~/Scripts/jquery.validate.js",
                                         "~/Scripts/custom-js/custome.js"
                                        ));

            bundles.Add(new ScriptBundle("~/bundles/custome-person-listdownline-javascript").Include(
                                        "~/Scripts/knockout/person.js",                                  
                                        "~/Scripts/jquery.validate.js",
                                        "~/Scripts/custom-js/custome.js"
                                       ));

            bundles.Add(new ScriptBundle("~/bundles/custome-edit-person-javascript").Include(
                                         "~/Scripts/validate/create-person.js",
                                         "~/Scripts/custom-js/checkboxcurrency-tabcurrency.js",
                                         "~/Scripts/custom-js/position-taking.js",
                                         "~/Scripts/jquery.validate.js",
                                          "~/Scripts/custom-js/custome.js"
                                        ));

            bundles.Add(new ScriptBundle("~/bundles/custome-transfer-javascript").Include(
                                         "~/Scripts/custom-js/checkboxtransfer-tabletransfer.js",
                                         "~/Scripts/knockout/transfer.js"
                                         ));

            bundles.Add(new ScriptBundle("~/bundles/transaction-detail-report").Include(
                                         "~/Scripts/validate/transactiondetail-report.js",
                                         "~/Scripts/knockout/transactiondetail-report.js"                                       
                                         ));

            bundles.Add(new ScriptBundle("~/bundles/daily-win-loss-report").Include(
                                         "~/Scripts/validate/daily-winloss-report.js",
                                         "~/Scripts/knockout/daily-winloss-report.js"
                                         ));

            bundles.Add(new ScriptBundle("~/bundles/summary-report").Include(
                                         "~/Scripts/validate/summary-report.js",
                                         "~/Scripts/knockout/summary-report.js"
                                          ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                                        "~/Content/ionicons.min.css",
                                        "~/Content/font-awesome.min.css",
                                        "~/Content/bootstrap.min.css",
                                        "~/Content/Site-less/theme-less/AdminLTE.min.css",
                                        "~/Content/Site-less/theme-less/skins/_all-skins.min.css",
                                        "~/Content/Site-less/Custom-less/StyleSheet.min.css",
                                        "~/Content/toastr.css",
                                        "~/Content/bootstrap-theme.min.css",
                                         "~/Content/bootstrap-datetimepicker.css"
                      ));
        }
    }
}
