﻿using ALT.Common.Logging;
using ALT.Web.Management.Infrastructer;
using System.Web;
using System.Web.Mvc;

namespace ALT.Web.Management
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute());
            filters.Add(new WebLogGlobalFilterAttribute() { CanTraceResponse = WebLoggers.GetMvcLogAll() });
            filters.Add(new ALTHandleErrorAttribute());
        }
    }
}
