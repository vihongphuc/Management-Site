﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ALT.Web.Management.Startup))]
namespace ALT.Web.Management
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
