﻿using ALT.Common.Helpers;
using ALT.Common.Models;
using ALT.Common.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ALT.Web.Management.Models
{
    public class DailyWinLossViewModel
    {
        public WinLossReq WinLossReq { get; set; }

        public PagedList<WinLossResp> WinLossResp { get; set; }
    }
}