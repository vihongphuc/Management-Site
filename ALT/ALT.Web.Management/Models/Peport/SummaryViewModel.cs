﻿using ALT.Common.Helpers;
using ALT.Common.Models;
using ALT.Common.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ALT.Web.Management.Models
{
    public class SummaryViewModel
    {
        public PagedList<SummaryResp> SummaryResp { get; set; }
    }
}