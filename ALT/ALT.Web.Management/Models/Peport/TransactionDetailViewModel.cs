﻿using ALT.Common.Helpers;
using ALT.Common.Models;
using ALT.Common.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ALT.Web.Management.Models
{
    public class TransactionDetailViewModel
    {
        public string PersonOCode { get; set; }
        public string[] BetTypeItemOCodes { get; set; }
        public string GameOCode { get; set; }
        public string UsernameDownline { get; set; }
        public string Date { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public PagedList<TransactionDetailResp> TransactionDetails { get; set; }
    }
}