﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ALT.Web.Management.Models.Transfer
{
    public class TransferViewModel
    {
        public string Ocode { get; set; }
        public string Username { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Type { get; set; }
        public decimal DepositAmount { get; set; }
        public decimal WithdrawAmount { get; set; }
        public decimal Balance { get; set; }
        public string Downline { get; set; }
        public string CurrencyCode { get; set; }
    }
}