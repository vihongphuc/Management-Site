﻿using ALT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ALT.Web.Management.Models
{
    public class BetSettingViewModel
    {
        public string PersonOCode { get; set; }
        public string CurrencyCode { get; set; }
        public string BetTypeItemOcode { get; set; }
        public BetSettingType SettingType { get; set; }
        public decimal? Value { get; set; }
        public string ValueString { get; set; }
    }
}