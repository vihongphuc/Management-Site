﻿using ALT.Common.Models;

namespace ALT.Web.Management.Models
{
    public class PersonBalance
    {
        public string PersonOCode { get; set; }
        public string CurrencyCode { get; set; }
        public decimal? Available { get; set; }
    }
}