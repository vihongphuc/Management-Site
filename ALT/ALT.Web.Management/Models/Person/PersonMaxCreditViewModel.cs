﻿using ALT.Common.Models;

namespace ALT.Web.Management.Models
{
    public class PersonMaxCredit
    {
        public string PersonOCode { get; set; }
        public string CurrencyCode { get; set; }
        public decimal? MaxCredit { get; set; }
        public PersonType? PersonType { get; set; }
    }
}