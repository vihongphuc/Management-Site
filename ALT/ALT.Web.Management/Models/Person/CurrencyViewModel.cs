﻿using ALT.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ALT.Web.Management.Models
{
    public class CurrencyViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Rate { get; set; }
        public bool Enable { get; set; }
    }
}