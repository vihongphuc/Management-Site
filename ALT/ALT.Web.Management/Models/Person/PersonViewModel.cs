﻿using ALT.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ALT.Web.Management.Models
{
    public class PersonViewModel
    {
        public string OCode { get; set; }
        [Required]
        public string Username { get; set; }
        public string UsernameUpper { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string DisplayName { get; set; }
        public string Password { get; set; }
        public DateTime? DOB { get; set; }
        public string Address { get; set; }
        public PersonType PersonType { get; set; }
        public PersonStatusType Status { get; set; }
        public string CountryCode { get; set; }
        public string[] Currencies { get; set; }
        public string[] SelectedCurrencies { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool External { get; set; }
        public bool AutoTransfer { get; set; }
        public string LimitIP { get; set; }
        public int GroupID { get; set; }
        public string UplineOCode { get; set; }
        public bool IsAdd { get; set; }
        public bool IsUpdate { get; set; }
        public bool FixedCurrency { get; set; }
        public bool CreateAgency { get; set; }
        public bool CreateMember { get; set; }
        public IDictionary<string, PersonMaxCredit> PersonMaxCredits { get; set; }
        public IDictionary<string, PersonBalance> PersonBalances { get; set; }
        public IDictionary<string, List<BetSettingViewModel>> BetSettings { get; set; }
        public IDictionary<string, List<PositionTakingViewModel>> PositionTakings { get; set; }
    }
}