﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ALT.Web.Management.Models.Login
{
    public class LoginViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}