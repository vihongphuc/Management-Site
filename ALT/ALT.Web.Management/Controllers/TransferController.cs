﻿using ALT.Common.Helpers;
using ALT.Common.Models;
using ALT.Common.Models.Models;
using ALT.Web.Management.Core;
using ALT.Web.Management.Infrastructer;
using ALT.Web.Management.Models.Transfer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ALT.Web.Management.Controllers
{
    public class TransferController : BaseController
    {
        // GET: Transfer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Transfer()
        {
            ViewBag.CountData = 0;
            return View();
        }

        public ActionResult MenuTransfer()
        {
            TempData["ocode"] = AuthManager.AuthIdentity.PersonCode;
            return RedirectToAction("Transfer", "Transfer");
        }

        [HttpPost]
        [AjaxOnlyAttribute]
        public async Task<JsonResult> Transfer(string ocode, decimal amount, string currency, string type)
        {
            PersonTransferReq personTransfer = new PersonTransferReq();
            PersonTransferResp resTransferAmount = new PersonTransferResp();
            var result = new JsonObjResp();

            personTransfer.Amount = amount;
            personTransfer.Type = EnumHelper<TransferType>.Parse(type);
            personTransfer.CurrencyCode = currency;
            try
            {
                resTransferAmount = await this.person.Transfer(AuthManager.AuthIdentity.Token, ocode, personTransfer);
                result.Status = (int)HttpStatusCode.OK;
                result.Message = HttpStatusCode.OK.ToString();
                result.ResultObj = JsonConvert.SerializeObject(resTransferAmount);
            }
            catch (Exceptions.ALTServiceException ex)
            {
                result.Status = (int)ex.Response.StatusCode;
                result.Message = ex.Message;
                result.ResultObj = JsonConvert.SerializeObject(resTransferAmount);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AjaxOnlyAttribute]
        public async Task<JsonResult> GetBalance(string ocode)
        {
            var currentOCode = string.IsNullOrEmpty(ocode) ? AuthManager.AuthIdentity.PersonCode : ocode;
            var avaiable = await this.person.GetPersonBalanceByOcode(AuthManager.AuthIdentity.Token, currentOCode);

            return Json(avaiable, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AjaxOnlyAttribute]
        public async Task<JsonResult> TransferHistory(string username, string personOcode, DateTime startDate, DateTime endDate, int pageIndex)
        {
            PagedList<TransferMemberHistoryResp> transferStatement = new PagedList<TransferMemberHistoryResp>();
            var result = new JsonObjResp();

            try
            {
                transferStatement = await this.person.TransferHistory(AuthManager.AuthIdentity.Token, personOcode, startDate, endDate, pageIndex, TotalItemPerPageStatement);
                result.Status = (int)HttpStatusCode.OK;
                result.Message = HttpStatusCode.OK.ToString();
                result.ResultObj = JsonConvert.SerializeObject(transferStatement);
            }
            catch (Exceptions.ALTServiceException ex)
            {
                result.Status = (int)ex.Response.StatusCode;
                result.Message = ex.Message;
                result.ResultObj = JsonConvert.SerializeObject(transferStatement);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public TransferController(IPerson person)
        {
            this.person = person;
        }
        private readonly IPerson person;

        private readonly static int TotalItemPerPages = 25;
        private readonly static int TotalItemPerPageStatement = 10;
    }
}