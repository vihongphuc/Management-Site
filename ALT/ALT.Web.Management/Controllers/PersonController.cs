﻿using ALT.Common.Helpers;
using ALT.Common.Models;
using ALT.Common.Models.Models;
using ALT.Web.Management.Core;
using ALT.Web.Management.Infrastructer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ALT.Web.Management.Controllers
{
    using ALT.Web.Management.Core;
    using Common;
    using Common.Configuration;
    using Common.Logging;
    using Microsoft.Owin.Security.Twitter.Messages;
    using Models;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System.Net;
    using System.Text.RegularExpressions;
    using System.Web.Mvc.Html;
    using System.Web.Script.Serialization;
    using System.Web.Script.Services;

    public class PersonController : BaseController
    {
        public ActionResult PersonDownline(string ocode = null)
        {
            if (!String.IsNullOrEmpty(ocode))
            {
                var currentOCode = ocode ?? AuthManager.AuthIdentity.PersonCode;
                TempData["ocode"] = currentOCode;
            }

            return View();
        }

        public ActionResult Downline()
        {
            TempData["ocode"] = AuthManager.AuthIdentity.PersonCode;
            return RedirectToAction("PersonDownline", "Person");
        }

        [HttpGet]
        [AjaxOnlyAttribute]
        public async Task<JsonResult> GetPersonDownline(string ocode = null, bool? isMember = null, int pageIndex = 1)
        {
            PagedList<PersonResp> result = null;
            TempData["ocode"] = ocode;

            var currentOCode = ocode ?? AuthManager.AuthIdentity.PersonCode;
            result = await this.person.GetPersonDownlines(AuthManager.AuthIdentity.Token, currentOCode, isMember, pageIndex, TotalItemPerPages);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AjaxOnlyAttribute]
        public async Task<JsonResult> GetBreadcrumbPerson(string ocode = null)
        {
            ocode = ocode ?? AuthManager.AuthIdentity.PersonCode;
            var breadcrumbPerson = await this.person.BreadcrumbPersonByOCode(AuthManager.AuthIdentity.Token, ocode);

            return Json(breadcrumbPerson, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AjaxOnlyAttribute]
        public async Task<JsonResult> SearchUsername(string username = null)
        {
            var sreachUsername = await this.person.SearchUsername(AuthManager.AuthIdentity.Token, AuthManager.AuthIdentity.PersonCode, username);
            return Json(sreachUsername.Select(c => c.Username), JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> AddPerson(CreateAgencyAndMember type)
        {
            var fixedCurrency = AuthManager.AuthIdentity.FixedCurrency;
            var uplinesCurrencies = AuthManager.AuthIdentity.Currencies;
            var personOCode = AuthManager.AuthIdentity.PersonCode;
            var personType = AuthManager.AuthIdentity.Type; ;
            if (type == CreateAgencyAndMember.CreateMember)
            {
                personType = PersonType.Member;
            }

            var personViewModelData = new PersonViewModel();
            //var defaultCurrency = StringHelper.ToArray(ConfigurationManager.Get<string>(WebConfigurations.Common, WebConfigurations.DefaultCurrency, "USD"));

            var OnlyCurrency = uplinesCurrencies.Select(c => c.Code).FirstOrDefault();
            var getCurrencies = await this.currency.GetAllCurrency(AuthManager.AuthIdentity.Token);

            if (fixedCurrency == false)
            {
                ViewBag.GetAllCurrencies = getCurrencies;
            }
            else
            {
                getCurrencies = getCurrencies.ToList();
                ViewBag.GetAllCurrencies = getCurrencies;
            }

            ViewBag.onlyCurrency = OnlyCurrency;

            var uplinePersonMaxCredits = await this.person.GetPersonMaxCreditByOcode(AuthManager.AuthIdentity.Token, personOCode, getUpline: true);
            List<PersonMaxCreditResp> uplinePersonNoCurrencyMaxCredits = null;

            if (uplinePersonMaxCredits != null)
            {
                uplinePersonNoCurrencyMaxCredits = getCurrencies.Where(c => !uplinePersonMaxCredits.Select(cx => cx.CurrencyCode).Contains(c.Code))
                                   .Select(cc => new PersonMaxCreditResp
                                   {
                                       CurrencyCode = cc.Code,
                                       PersonOCode = uplinePersonMaxCredits.Select(c => c.PersonOCode).FirstOrDefault(),
                                       Type = uplinePersonMaxCredits.Select(c => c.Type).FirstOrDefault(),
                                       Value = uplinePersonMaxCredits.Select(c => c.Value).FirstOrDefault() * ConvertCurrencyRate(OnlyCurrency, cc.Code, getCurrencies),
                                   }).ToList();

                uplinePersonMaxCredits = uplinePersonNoCurrencyMaxCredits.Concat(uplinePersonMaxCredits).ToList();
            }

            ViewData["uplinePersonMaxCredits"] = uplinePersonMaxCredits;

            var uplinePersonBalances = await this.person.GetPersonListBalanceByOcode(AuthManager.AuthIdentity.Token, personOCode, getUpline: true);
            List<PersonBalanceResp> uplinePersonNoCurrencyBalances = null;
            if (uplinePersonBalances != null)
            {
                uplinePersonNoCurrencyBalances = getCurrencies.Where(c => !uplinePersonBalances.Select(cx => cx.CurrencyCode).Contains(c.Code))
                                   .Select(cc => new PersonBalanceResp
                                   {
                                       CurrencyCode = cc.Code,
                                       PersonOCode = uplinePersonBalances.Select(c => c.PersonOCode).FirstOrDefault(),
                                       Type = uplinePersonBalances.Select(c => c.Type).FirstOrDefault(),
                                       Value = uplinePersonBalances.Select(c => c.Value).FirstOrDefault() * ConvertCurrencyRate(OnlyCurrency, cc.Code, getCurrencies),
                                   }).ToList();

                uplinePersonBalances = uplinePersonNoCurrencyBalances.Concat(uplinePersonBalances).ToList();
            }
            ViewData["uplinePersonBalances"] = uplinePersonBalances;

            var uplinePersonBetSetting = await this.person.GetPersonBetSettingByOcode(AuthManager.AuthIdentity.Token, personOCode, getUpline: true);
            PersonBetSettingResp betSettingResp = new PersonBetSettingResp();
            List<PersonBetSettingResp> uplineNoPersonBetSettings = null;

            if (uplinePersonBetSetting != null)
            {
                var _valMaxMinLimit = uplinePersonBetSetting.Where(cc => cc.Type == BetSettingType.MaxWinLimit).Select(cc => cc.Value).FirstOrDefault();
                var _valMinLossLimit = uplinePersonBetSetting.Where(cc => cc.Type == BetSettingType.MinLossLimit).Select(cc => cc.Value).FirstOrDefault();

                var uplineNoPersonBetSettingMaxWinLimits = getCurrencies.Where(c => !uplinePersonBetSetting.Select(cx => cx.Currency).Contains(c.Code)).Select(c => new PersonBetSettingResp
                {
                    Currency = c.Code,
                    Type = BetSettingType.MaxWinLimit,
                    Value = _valMaxMinLimit * ConvertCurrencyRate(OnlyCurrency, c.Code, getCurrencies),
                }).ToList();

                var uplineNoPersonBetSettingMinLossLimits = getCurrencies.Where(c => !uplinePersonBetSetting.Select(cx => cx.Currency).Contains(c.Code)).Select(c => new PersonBetSettingResp
                {
                    Currency = c.Code,
                    Type = BetSettingType.MinLossLimit,
                    Value = _valMinLossLimit * ConvertCurrencyRate(OnlyCurrency, c.Code, getCurrencies),
                }).ToList();

                uplineNoPersonBetSettings = uplineNoPersonBetSettingMaxWinLimits.Concat(uplineNoPersonBetSettingMinLossLimits).ToList();
                uplinePersonBetSetting = uplineNoPersonBetSettings.Concat(uplinePersonBetSetting).ToList();
            }
            ViewData["uplinePersonBetSetting"] = uplinePersonBetSetting;

            var dtPersonMaxCredit = getCurrencies.Select(c => new PersonMaxCredit
            {
                PersonOCode = personOCode,
                CurrencyCode = c.Code,
                PersonType = personType,
                MaxCredit = 0m
            }).ToDictionary(c => c.CurrencyCode);
            personViewModelData.PersonMaxCredits = dtPersonMaxCredit;

            var dtPersonBalance = getCurrencies.Select(c => new PersonBalance
            {
                PersonOCode = personOCode,
                CurrencyCode = c.Code,
                Available = 0m
            }).ToDictionary(c => c.CurrencyCode);

            personViewModelData.PersonBalances = dtPersonBalance;

            var dtBetSetting = getCurrencies.ToDictionary(c => c.Code, c => new List<BetSettingViewModel>
              {
                    new BetSettingViewModel
                   {
                        PersonOCode = personOCode,
                        CurrencyCode= c.Code,
                        BetTypeItemOcode = BetTypeItemOCodes.SlotGame,
                        SettingType= BetSettingType.MaxWinLimit,
                        Value = 0m,
                        ValueString=string.Empty
                    } ,
                     new BetSettingViewModel
                     {
                        PersonOCode = personOCode,
                        CurrencyCode= c.Code,
                        BetTypeItemOcode = BetTypeItemOCodes.SlotGame,
                        SettingType= BetSettingType.MinLossLimit,
                        Value = 0m,
                        ValueString=string.Empty
                    }
              });
            personViewModelData.BetSettings = dtBetSetting;

            var dtAddPersonPositionTaking = new Dictionary<string, List<PositionTakingViewModel>>();
            var dtUplinePersonPositionTaking = new Dictionary<string, List<PositionTakingViewModel>>();

            if (AuthManager.AuthIdentity.Type == PersonType.Admin)
            {
                dtUplinePersonPositionTaking = getCurrencies.ToDictionary(c => c.Code, c => new List<PositionTakingViewModel> {
                    new PositionTakingViewModel
                    {
                        CurrencyCode = c.Code,
                        Type =  PostitionTakingType.Upline,
                        Value = 0
                    },
                    new PositionTakingViewModel
                    {
                        CurrencyCode = c.Code,
                        Type =  PostitionTakingType.CurrentMax,
                        Value = 1
                    }
                });

                dtAddPersonPositionTaking = getCurrencies.ToDictionary(c => c.Code, c => new List<PositionTakingViewModel> {
                    new PositionTakingViewModel
                    {
                        CurrencyCode = c.Code,
                        Type =  PostitionTakingType.Upline,
                        Value = 0
                    },
                    new PositionTakingViewModel
                    {
                        CurrencyCode = c.Code,
                        Type =  PostitionTakingType.CurrentMax,
                        Value = 1
                    }
                });
            }
            else
            {
                var getPositionTakingUplines = await this.person.GetPositionTaking(AuthManager.AuthIdentity.Token, personOCode, getUpline: false);
                dtUplinePersonPositionTaking = getPositionTakingUplines.Select(c => new PositionTakingViewModel
                {
                    CurrencyCode = c.Currency,
                    Type = EnumHelper<PostitionTakingType>.Parse(c.Type),
                    Value = c.Value
                })
                .GroupBy(c => c.CurrencyCode)
                .ToDictionary(c => c.Key, c => c.Select(cc => new PositionTakingViewModel
                {
                    CurrencyCode = cc.CurrencyCode,
                    Type = cc.Type,
                    Value = cc.Value
                }).ToList());

                dtAddPersonPositionTaking = getPositionTakingUplines
                    .Select(c => new PositionTakingViewModel
                    {
                        CurrencyCode = c.Currency,
                        Type = EnumHelper<PostitionTakingType>.Parse(c.Type),
                        Value = c.Type == PostitionTakingTypeString.Upline ? 0m : c.Value
                    })
                .GroupBy(c => c.CurrencyCode)
                .ToDictionary(c => c.Key, c => c.Select(cc => new PositionTakingViewModel
                {
                    CurrencyCode = cc.CurrencyCode,
                    Type = cc.Type,
                    Value = cc.Value
                }).ToList());

                var dtAddPersonNoCurrencyCodePositionTaking = getCurrencies.Where(cc => !dtAddPersonPositionTaking.Select(b => b.Key)
                                                                  .Contains(cc.Code)).ToDictionary(c => c.Code, c => new List<PositionTakingViewModel> {
                                                                    new PositionTakingViewModel
                                                                    {
                                                                        CurrencyCode = c.Code,
                                                                        Type =  PostitionTakingType.Upline,
                                                                        Value = dtAddPersonPositionTaking[OnlyCurrency].Where(l=>l.Type ==PostitionTakingType.Upline).Select(cc=>cc.Value).FirstOrDefault()
                                                                    },
                                                                    new PositionTakingViewModel
                                                                    {
                                                                        CurrencyCode = c.Code,
                                                                        Type =  PostitionTakingType.CurrentMax,
                                                                        Value = dtAddPersonPositionTaking[OnlyCurrency].Where(l=>l.Type ==PostitionTakingType.CurrentMax).Select(cc=>cc.Value).FirstOrDefault()
                                                                    }
                                                                    });

                dtAddPersonPositionTaking = dtAddPersonNoCurrencyCodePositionTaking.Concat(dtAddPersonPositionTaking).ToDictionary(c => c.Key, c => c.Value);
            }
            personViewModelData.PositionTakings = dtAddPersonPositionTaking;
            ViewData["dtUplinePersonPositionTaking"] = dtAddPersonPositionTaking;

            personViewModelData.DOB = DateTime.Now;
            personViewModelData.PersonType = personType;
            personViewModelData.IsUpdate = false;
            personViewModelData.FixedCurrency = fixedCurrency;
            personViewModelData.UsernameUpper = AuthManager.AuthIdentity.Username;

            return View("AddPerson", personViewModelData);
        }

        [HttpPost]
        public async Task<ActionResult> AddPerson(PersonViewModel personVM)
        {
            UpdatePersonReq createPerson = new UpdatePersonReq();
            var personOCode = AuthManager.AuthIdentity.PersonCode;
            personVM.IsUpdate = false;

            var OnlyCurrency = AuthManager.AuthIdentity.Currencies.Select(cc=>cc.Code).FirstOrDefault(); 

            personVM.Currencies = StringHelper.ToArray(personVM.Currencies.FirstOrDefault());

            var getCurrencies = await this.currency.GetAllCurrency(AuthManager.AuthIdentity.Token);

            if (personVM.FixedCurrency == false)
            {
                ViewBag.GetAllCurrencies = getCurrencies;
            }
            else
            {
                getCurrencies = getCurrencies.ToList();
                ViewBag.GetAllCurrencies = getCurrencies;
            }

            ViewBag.onlyCurrency = personVM.Currencies.FirstOrDefault();

            var uplinePersonMaxCredits = await this.person.GetPersonMaxCreditByOcode(AuthManager.AuthIdentity.Token, personOCode, getUpline: true);
            List<PersonMaxCreditResp> uplinePersonNoCurrencyMaxCredits = null;

            if (uplinePersonMaxCredits != null)
            {
                uplinePersonNoCurrencyMaxCredits = getCurrencies.Where(c => !uplinePersonMaxCredits.Select(cx => cx.CurrencyCode).Contains(c.Code))
                                   .Select(cc => new PersonMaxCreditResp
                                   {
                                       CurrencyCode = cc.Code,
                                       PersonOCode = uplinePersonMaxCredits.Select(c => c.PersonOCode).FirstOrDefault(),
                                       Type = uplinePersonMaxCredits.Select(c => c.Type).FirstOrDefault(),
                                       Value = uplinePersonMaxCredits.Select(c => c.Value).FirstOrDefault() * ConvertCurrencyRate(OnlyCurrency, cc.Code, getCurrencies),
                                   }).ToList();

                uplinePersonMaxCredits = uplinePersonNoCurrencyMaxCredits.Concat(uplinePersonMaxCredits).ToList();
            }

            ViewData["uplinePersonMaxCredits"] = uplinePersonMaxCredits;

            var uplinePersonBalances = await this.person.GetPersonListBalanceByOcode(AuthManager.AuthIdentity.Token, personOCode, getUpline: true);
            List<PersonBalanceResp> uplinePersonNoCurrencyBalances = null;
            if (uplinePersonBalances != null)
            {
                uplinePersonNoCurrencyBalances = getCurrencies.Where(c => !uplinePersonBalances.Select(cx => cx.CurrencyCode).Contains(c.Code))
                                   .Select(cc => new PersonBalanceResp
                                   {
                                       CurrencyCode = cc.Code,
                                       PersonOCode = uplinePersonBalances.Select(c => c.PersonOCode).FirstOrDefault(),
                                       Type = uplinePersonBalances.Select(c => c.Type).FirstOrDefault(),
                                       Value = uplinePersonBalances.Select(c => c.Value).FirstOrDefault() * ConvertCurrencyRate(OnlyCurrency, cc.Code, getCurrencies),
                                   }).ToList();

                uplinePersonBalances = uplinePersonNoCurrencyBalances.Concat(uplinePersonBalances).ToList();
            }
            ViewData["uplinePersonBalances"] = uplinePersonBalances;

            var uplinePersonBetSetting = await this.person.GetPersonBetSettingByOcode(AuthManager.AuthIdentity.Token, personOCode, getUpline: true);
            PersonBetSettingResp betSettingResp = new PersonBetSettingResp();
            List<PersonBetSettingResp> uplineNoPersonBetSettings = null;

            if (uplinePersonBetSetting != null)
            {
                var _valMaxMinLimit = uplinePersonBetSetting.Where(cc => cc.Type == BetSettingType.MaxWinLimit).Select(cc => cc.Value).FirstOrDefault();
                var _valMinLossLimit = uplinePersonBetSetting.Where(cc => cc.Type == BetSettingType.MinLossLimit).Select(cc => cc.Value).FirstOrDefault();

                var uplineNoPersonBetSettingMaxWinLimits = getCurrencies.Where(c => !uplinePersonBetSetting.Select(cx => cx.Currency).Contains(c.Code)).Select(c => new PersonBetSettingResp
                {
                    Currency = c.Code,
                    Type = BetSettingType.MaxWinLimit,
                    Value = _valMaxMinLimit * ConvertCurrencyRate(OnlyCurrency, c.Code, getCurrencies),
                }).ToList();

                var uplineNoPersonBetSettingMinLossLimits = getCurrencies.Where(c => !uplinePersonBetSetting.Select(cx => cx.Currency).Contains(c.Code)).Select(c => new PersonBetSettingResp
                {
                    Currency = c.Code,
                    Type = BetSettingType.MinLossLimit,
                    Value = _valMinLossLimit * ConvertCurrencyRate(OnlyCurrency, c.Code, getCurrencies),
                }).ToList();

                uplineNoPersonBetSettings = uplineNoPersonBetSettingMaxWinLimits.Concat(uplineNoPersonBetSettingMinLossLimits).ToList();
                uplinePersonBetSetting = uplineNoPersonBetSettings.Concat(uplinePersonBetSetting).ToList();
            }

            ViewData["uplinePersonBetSetting"] = uplinePersonBetSetting;

           

            var getPositionTakingUplines = await this.person.GetPositionTaking(AuthManager.AuthIdentity.Token, personOCode, false);
            var dtAddPersonPositionTaking = new Dictionary<string, List<PositionTakingViewModel>>();
            var dtUplinePersonPositionTaking = new Dictionary<string, List<PositionTakingViewModel>>();
            if (AuthManager.AuthIdentity.Type == PersonType.Admin)
            {
                dtAddPersonPositionTaking = getCurrencies.ToDictionary(c => c.Code, c => new List<PositionTakingViewModel> {
                            new PositionTakingViewModel
                            {
                                CurrencyCode = c.Code,
                                Type =  PostitionTakingType.Upline,
                                Value = 1
                            },
                            new PositionTakingViewModel
                            {
                                CurrencyCode = c.Code,
                                Type =  PostitionTakingType.CurrentMax,
                                Value = 1
                            }
                        });

                dtUplinePersonPositionTaking = dtAddPersonPositionTaking;
            }
            else
            {
                dtUplinePersonPositionTaking = getPositionTakingUplines.Select(c => new PositionTakingViewModel
                {
                    CurrencyCode = c.Currency,
                    Type = EnumHelper<PostitionTakingType>.Parse(c.Type),
                    Value = c.Value
                })
                       .GroupBy(c => c.CurrencyCode)
                       .ToDictionary(c => c.Key, c => c.Select(cc => new PositionTakingViewModel
                       {
                           CurrencyCode = cc.CurrencyCode,
                           Type = cc.Type,
                           Value = cc.Value
                       }).ToList());

                var currencyOCode = dtUplinePersonPositionTaking.Select(cc => cc.Key).FirstOrDefault();

                var dtAddPersonNoCurrencyCodePositionTaking = getCurrencies.Where(cc => !dtUplinePersonPositionTaking.Select(b => b.Key)
                                                                 .Contains(cc.Code)).ToDictionary(c => c.Code, c => new List<PositionTakingViewModel> {
                                                                    new PositionTakingViewModel
                                                                    {
                                                                        CurrencyCode = c.Code,
                                                                        Type =  PostitionTakingType.Upline,
                                                                        Value = dtUplinePersonPositionTaking[currencyOCode].Where(l=>l.Type ==PostitionTakingType.Upline).Select(cc=>cc.Value).FirstOrDefault()
                                                                    },
                                                                    new PositionTakingViewModel
                                                                    {
                                                                        CurrencyCode = c.Code,
                                                                        Type =  PostitionTakingType.CurrentMax,
                                                                        Value = dtUplinePersonPositionTaking[currencyOCode].Where(l=>l.Type ==PostitionTakingType.CurrentMax).Select(cc=>cc.Value).FirstOrDefault()
                                                                    }
                                                                   });

                dtUplinePersonPositionTaking = dtAddPersonNoCurrencyCodePositionTaking.Concat(dtUplinePersonPositionTaking).ToDictionary(c => c.Key, c => c.Value);
            }
            ViewData["dtUplinePersonPositionTaking"] = dtUplinePersonPositionTaking;

            personVM.IsAdd = true;

            if (ModelState.IsValid)
            {
                createPerson.Username = personVM.Username;
                createPerson.Firstname = personVM.Firstname;
                createPerson.Lastname = personVM.Lastname;
                createPerson.DisplayName = personVM.DisplayName;
                createPerson.Password = personVM.Password;
                createPerson.DOB = personVM.DOB ?? DateTime.Now;
                createPerson.Address = personVM.Address;
                createPerson.Type = personVM.PersonType;
                createPerson.Status = personVM.Status;
                createPerson.CountryCode = personVM.CountryCode;
                createPerson.Currencies = personVM.Currencies;
                createPerson.Phone = personVM.Phone;
                createPerson.Email = personVM.Email;
                createPerson.External = personVM.External;
                createPerson.AutoTransfer = personVM.AutoTransfer;
                createPerson.LimitIP = personVM.LimitIP;
                createPerson.UplineOcode = AuthManager.AuthIdentity.PersonCode;
                createPerson.FixedCurrency = personVM.FixedCurrency;
                createPerson.CreateAgency = personVM.CreateAgency;
                createPerson.CreateMember = personVM.CreateMember;

                var personMaxCredits = personVM.PersonMaxCredits.Select(c => c.Value)
                                                                 .Where(c => personVM.Currencies.Contains(c.CurrencyCode))
                                                                 .Select(cc => new PersonMaxCreditReq
                                                                 {
                                                                     PersonOCode = personOCode,
                                                                     CurrencyCode = cc.CurrencyCode,
                                                                     PersonType = cc.PersonType,
                                                                     MaxCredit = cc.MaxCredit ?? 0m
                                                                 }).ToList();
                createPerson.PersonMaxCredits = personMaxCredits;

                var personBalances = personVM.PersonBalances.Select(c => c.Value)
                                                                .Where(c => personVM.Currencies.Contains(c.CurrencyCode))
                                                                .Select(cc => new PersonBalanceReq
                                                                {
                                                                    PersonOCode = personOCode,
                                                                    CurrencyCode = cc.CurrencyCode,
                                                                    Available = cc.Available ?? 0m
                                                                }).ToList();
                createPerson.PersonBalances = personBalances;

                var personBetSettings = personVM.BetSettings
                                                .Where(c => personVM.Currencies.Contains(c.Key))
                                                .SelectMany(c => c.Value)
                                                .Where(c => personVM.Currencies.Contains(c.CurrencyCode)).ToList();
                var personBetSettingReq = personBetSettings.Select(c => new PersonBetSettingReq
                {
                    CurrencyCode = c.CurrencyCode,
                    BetTypeItemOcode = BetTypeItemOCodes.SlotGame,
                    PersonOCode = c.PersonOCode,
                    SettingType = c.SettingType,
                    Value = c.Value ?? 0m,
                    ValueString = c.ValueString
                }).ToList();
                createPerson.BetSettings = personBetSettingReq;

                var personPositionTakings = personVM.PositionTakings
                                                    .Where(c => personVM.Currencies.Contains(c.Key))
                                                    .SelectMany(c => c.Value)
                                                    .Where(c => personVM.Currencies.Contains(c.CurrencyCode)).ToList();
                var personPositionTakingReq = personPositionTakings.Select(c => new PositionTakingReq
                {
                    PersonOCode = c.PersonOCode,
                    Type = c.Type,
                    BetTypeItemOcode = BetTypeItemOCodes.SlotGame,
                    CurrencyCode = c.CurrencyCode,
                    Value = c.Value
                }).ToList();
                createPerson.PositionTakings = personPositionTakingReq;
                try
                {
                    var res = await this.person.AddPerson(AuthManager.AuthIdentity.Token, createPerson);

                    if (res)
                    {
                        TempData["UpdatedInfo"] = "" + createPerson.Username + " is added successful";
                        TempData["ocode"] = AuthManager.AuthIdentity.PersonCode;

                        return RedirectToAction("PersonDownline", "Person");
                    }
                    else
                    {
                        ModelState.AddModelError("ErrorMessageID", "Invalid data request.");
                    }
                }
                catch (Exceptions.ALTServiceException ex)
                {                    
                    ModelState.AddModelError("ErrorMessageID", ex.Message);
                }
            }
            else
            {
                ModelState.AddModelError("ErrorMessageID", "Invalid data request.");
            }

            return View(personVM);
        }

        public async Task<ActionResult> EditPerson(string ocode)
        {
            var personViewModelData = new PersonViewModel();
            personViewModelData.IsUpdate = true;

            var personOCode = ocode ?? AuthManager.AuthIdentity.PersonCode;

            var uplinesCurrencies = AuthManager.AuthIdentity.Currencies;
            ViewBag.CurrencyUplines = uplinesCurrencies;

            var getPersonDownline = await this.person.GetPersonByOcode(AuthManager.AuthIdentity.Token, ocode);

            var getCurrencies = await this.currency.GetAllCurrency(AuthManager.AuthIdentity.Token);

            getCurrencies = getCurrencies.Where(c => c.Code == getPersonDownline.Currencies.FirstOrDefault()).ToList();
            ViewBag.GetAllCurrencies = getCurrencies;
            ViewBag.onlyCurrency = getPersonDownline.Currencies.FirstOrDefault();

            var uplinePersonMaxCredits = await this.person.GetPersonMaxCreditByOcode(AuthManager.AuthIdentity.Token, personOCode, getUpline: false);
            ViewData["uplinePersonMaxCredits"] = uplinePersonMaxCredits;

            var uplinePersonBalances = await this.person.GetPersonListBalanceByOcode(AuthManager.AuthIdentity.Token, personOCode, getUpline: false);
            ViewData["uplinePersonBalances"] = uplinePersonBalances;

            var uplinePersonBetSetting = await this.person.GetPersonBetSettingByOcode(AuthManager.AuthIdentity.Token, personOCode, getUpline: false);
            ViewData["uplinePersonBetSetting"] = uplinePersonBetSetting;

            personViewModelData.OCode = getPersonDownline.OCode;
            personViewModelData.Username = getPersonDownline.Username;
            personViewModelData.DOB = getPersonDownline.DOB;
            personViewModelData.DisplayName = getPersonDownline.DisplayName;
            personViewModelData.Lastname = getPersonDownline.Lastname;
            personViewModelData.Firstname = getPersonDownline.Firstname;
            personViewModelData.Email = getPersonDownline.Email;
            personViewModelData.Mobile = getPersonDownline.Mobile;
            personViewModelData.Phone = getPersonDownline.Phone;
            personViewModelData.Status = getPersonDownline.Status;
            personViewModelData.CreateAgency = getPersonDownline.CreateAgency;
            personViewModelData.CreateMember = getPersonDownline.CreateMember;

            if (PersonOneCurrency.Any(c => c == getPersonDownline.Type))
            {
                uplinesCurrencies = uplinesCurrencies.Where(c => getPersonDownline.Currencies.Contains(c.Code)).ToList();
                ViewBag.CurrencyUplines = uplinesCurrencies;
            }
            personViewModelData.Currencies = getPersonDownline.Currencies;

            personViewModelData.PersonType = getPersonDownline.Type;
            personViewModelData.UplineOCode = getPersonDownline.UplineOcode;

            var dtHasCurrencyPersonMaxCredit = new Dictionary<string, PersonMaxCredit>();
            var dtHasCurrencyPersonBalance = new Dictionary<string, PersonBalance>();
            var dtHasCurrencyBetSetting = new Dictionary<string, List<BetSettingViewModel>>();
            var dtHasCurrencyPositionTaking = new Dictionary<string, List<PositionTakingViewModel>>();

            var dtUplineHasCurrencyPersonPositionTaking = new Dictionary<string, List<PositionTakingViewModel>>();
            var getPositionTakingUplines = await this.person.GetPositionTaking(AuthManager.AuthIdentity.Token, personOCode, getUpline: true);
            dtUplineHasCurrencyPersonPositionTaking = getPositionTakingUplines
                                                                                .Select(c => new PositionTakingViewModel
                                                                                {
                                                                                    CurrencyCode = c.Currency,
                                                                                    Type = EnumHelper<PostitionTakingType>.Parse(c.Type),
                                                                                    Value = c.Value
                                                                                })
                                                                                .GroupBy(c => c.CurrencyCode)
                                                                                .ToDictionary(c => c.Key, c => c.Select(cc => new PositionTakingViewModel
                                                                                {
                                                                                    CurrencyCode = cc.CurrencyCode,
                                                                                    Type = cc.Type,
                                                                                    Value = cc.Value
                                                                                }).ToList());

            var dtUplineNoCurrecyPersonPositionTaking = uplinesCurrencies.Where(cc => !dtUplineHasCurrencyPersonPositionTaking.Select(b => b.Key)
                                                                  .Contains(cc.Code)).ToDictionary(c => c.Code, c => new List<PositionTakingViewModel> {
                                                                    new PositionTakingViewModel
                                                                    {
                                                                        CurrencyCode = c.Code,
                                                                        Type =  PostitionTakingType.Upline,
                                                                        Value = 0
                                                                    },
                                                                    new PositionTakingViewModel
                                                                    {
                                                                        CurrencyCode = c.Code,
                                                                        Type =  PostitionTakingType.CurrentMax,
                                                                        Value = 1
                                                                    }
                                                                    });

            var positionTalkingByAllCurrecies = dtUplineNoCurrecyPersonPositionTaking.Concat(dtUplineHasCurrencyPersonPositionTaking)
                                                                                     .ToDictionary(c => c.Key, c => c.Value);
            ViewData["dtUplinePersonPositionTaking"] = positionTalkingByAllCurrecies;

            dtHasCurrencyPersonMaxCredit = getPersonDownline.PersonMaxCredits
                                                            .Select(c => new PersonMaxCredit
                                                            {
                                                                PersonOCode = c.PersonOCode,
                                                                CurrencyCode = c.CurrencyCode,
                                                                PersonType = c.PersonType,
                                                                MaxCredit = c.MaxCredit
                                                            })
                                                            .ToDictionary(c => c.CurrencyCode, c => new PersonMaxCredit
                                                            {
                                                                PersonOCode = c.PersonOCode,
                                                                CurrencyCode = c.CurrencyCode,
                                                                PersonType = c.PersonType,
                                                                MaxCredit = c.MaxCredit
                                                            });

            var dtNoCurrencyPersonMaxCredit = uplinesCurrencies.Where(c => !dtHasCurrencyPersonMaxCredit
                                                               .Select(cc => cc.Key).Contains(c.Code))
                                                               .ToDictionary(c => c.Code, c => new PersonMaxCredit
                                                               {
                                                                   PersonOCode = string.Empty,
                                                                   CurrencyCode = c.Code,
                                                                   PersonType = null,
                                                                   MaxCredit = 0M
                                                               });

            var personMaxCreditAllCurrencies = dtNoCurrencyPersonMaxCredit.Concat(dtHasCurrencyPersonMaxCredit)
                                                                          .ToDictionary(c => c.Key, c => c.Value);
            personViewModelData.PersonMaxCredits = personMaxCreditAllCurrencies;

            dtHasCurrencyPersonBalance = getPersonDownline.PersonBalances
                                                           .Select(c => new PersonBalance
                                                           {
                                                               PersonOCode = c.PersonOCode,
                                                               CurrencyCode = c.CurrencyCode,
                                                               Available = c.Available
                                                           })
                                                           .ToDictionary(c => c.CurrencyCode, c => new PersonBalance
                                                           {
                                                               PersonOCode = c.PersonOCode,
                                                               CurrencyCode = c.CurrencyCode,
                                                               Available = c.Available
                                                           });
            var dtNoCurrencyPersonBalance = uplinesCurrencies.Where(c => !dtHasCurrencyPersonBalance.Select(cc => cc.Key).Contains(c.Code))
                                                              .ToDictionary(c => c.Code, c => new PersonBalance
                                                              {
                                                                  PersonOCode = string.Empty,
                                                                  CurrencyCode = c.Code,
                                                                  Available = 0M
                                                              });

            var personBalancesAllCurrencies = dtNoCurrencyPersonBalance.Concat(dtHasCurrencyPersonBalance)
                                                                       .ToDictionary(c => c.Key, c => c.Value);
            personViewModelData.PersonBalances = personBalancesAllCurrencies;

            dtHasCurrencyBetSetting = getPersonDownline.BetSettings
                                                        .Select(c => new BetSettingViewModel
                                                        {
                                                            PersonOCode = c.PersonOCode,
                                                            CurrencyCode = c.CurrencyCode,
                                                            BetTypeItemOcode = c.BetTypeItemOcode,
                                                            SettingType = c.SettingType,
                                                            Value = c.Value,
                                                            ValueString = c.ValueString
                                                        })
                                                        .GroupBy(c => c.CurrencyCode)
                                                        .ToDictionary(c => c.Key, c => c.Select(cc => new BetSettingViewModel
                                                        {
                                                            PersonOCode = cc.PersonOCode,
                                                            CurrencyCode = cc.CurrencyCode,
                                                            BetTypeItemOcode = cc.BetTypeItemOcode,
                                                            SettingType = cc.SettingType,
                                                            Value = cc.Value,
                                                            ValueString = cc.ValueString
                                                        }).ToList());

            var dtNoCurrencyBetSetting = uplinesCurrencies.Where(c => !dtHasCurrencyBetSetting.Select(cc => cc.Key).Contains(c.Code))
                                                           .ToDictionary(c => c.Code, c => new List<BetSettingViewModel>
                                                            {
                                                                new BetSettingViewModel
                                                                {
                                                                    PersonOCode = personOCode,
                                                                    CurrencyCode= c.Code,
                                                                    BetTypeItemOcode = BetTypeItemOCodes.SlotGame,
                                                                    SettingType= BetSettingType.MaxWinLimit,
                                                                    Value = 0m,
                                                                    ValueString=string.Empty
                                                                } ,
                                                                    new BetSettingViewModel
                                                                    {
                                                                    PersonOCode = personOCode,
                                                                    CurrencyCode= c.Code,
                                                                    BetTypeItemOcode = BetTypeItemOCodes.SlotGame,
                                                                    SettingType= BetSettingType.MinLossLimit,
                                                                    Value = 0m,
                                                                    ValueString=string.Empty
                                                                }
                                                            });

            var dtBetSettingAllCurrencies = dtNoCurrencyBetSetting.Concat(dtHasCurrencyBetSetting).ToDictionary(c => c.Key, c => c.Value);
            personViewModelData.BetSettings = dtBetSettingAllCurrencies;

            dtHasCurrencyPositionTaking = getPersonDownline.PositionTakings
                                                            .Select(c => new PositionTakingViewModel
                                                            {
                                                                PersonOCode = c.PersonOCode,
                                                                CurrencyCode = c.CurrencyCode,
                                                                BetTypeItemOcode = c.BetTypeItemOcode,
                                                                Type = c.Type,
                                                                Value = c.Value
                                                            })
                                                            .GroupBy(c => c.CurrencyCode)
                                                            .ToDictionary(c => c.Key, c => c.Select(cc => new PositionTakingViewModel
                                                            {
                                                                PersonOCode = cc.PersonOCode,
                                                                CurrencyCode = cc.CurrencyCode,
                                                                BetTypeItemOcode = cc.BetTypeItemOcode,
                                                                Type = cc.Type,
                                                                Value = cc.Value
                                                            }).ToList());

            var dtNoCurrencyPositionTaking = uplinesCurrencies.Where(c => !dtHasCurrencyPositionTaking.Select(cc => cc.Key).Contains(c.Code))
                                                              .ToDictionary(c => c.Code, c => new List<PositionTakingViewModel> {
                                                                new PositionTakingViewModel
                                                                {
                                                                    CurrencyCode = c.Code,
                                                                    Type =  PostitionTakingType.Upline,
                                                                    Value = 0
                                                                },
                                                                new PositionTakingViewModel
                                                                {
                                                                    CurrencyCode = c.Code,
                                                                    Type =  PostitionTakingType.CurrentMax,
                                                                    Value = 1
                                                                }
                                                              });

            var dtPositionTakingAllCurrencies = dtNoCurrencyPositionTaking.Concat(dtHasCurrencyPositionTaking)
                                                                          .ToDictionary(c => c.Key, c => c.Value);
            personViewModelData.PositionTakings = dtPositionTakingAllCurrencies;

            return View("EditPerson", personViewModelData);
        }

        [HttpPost]
        public async Task<ActionResult> EditPerson(PersonViewModel personVM)
        {
            UpdatePersonReq editPerson = new UpdatePersonReq();
            personVM.IsUpdate = true;
            var personOCode = personVM.OCode;

            var uplinesCurrencies = AuthManager.AuthIdentity.Currencies;

            var OnlyCurrency = uplinesCurrencies.Select(c => c.Code).FirstOrDefault();
            var getCurrencies = await this.currency.GetAllCurrency(AuthManager.AuthIdentity.Token);

            if (personVM.FixedCurrency == false)
            {
                ViewBag.GetAllCurrencies = getCurrencies;
            }
            else
            {
                getCurrencies = getCurrencies.Where(c => c.Code == OnlyCurrency).ToList();
                ViewBag.GetAllCurrencies = getCurrencies;
            }

            ViewBag.onlyCurrency = OnlyCurrency;

            personVM.Currencies = StringHelper.ToArray(personVM.Currencies.FirstOrDefault());

            var uplinePersonMaxCredits = await this.person.GetPersonMaxCreditByOcode(AuthManager.AuthIdentity.Token, personOCode, false);
            ViewData["uplinePersonMaxCredits"] = uplinePersonMaxCredits;

            var uplinePersonBalances = await this.person.GetPersonListBalanceByOcode(AuthManager.AuthIdentity.Token, personOCode, false);
            ViewData["uplinePersonBalances"] = uplinePersonBalances;

            var uplinePersonBetSetting = await this.person.GetPersonBetSettingByOcode(AuthManager.AuthIdentity.Token, personOCode, false);
            ViewData["uplinePersonBetSetting"] = uplinePersonBetSetting;

            var dtUplineHasCurrencyPersonPositionTaking = new Dictionary<string, List<PositionTakingViewModel>>();
            var getPositionTakingUplines = await this.person.GetPositionTaking(AuthManager.AuthIdentity.Token, personOCode, true);

            dtUplineHasCurrencyPersonPositionTaking = getPositionTakingUplines.Select(c => new PositionTakingViewModel
            {
                CurrencyCode = c.Currency,
                Type = EnumHelper<PostitionTakingType>.Parse(c.Type),
                Value = c.Value
            })
                                                                                 .GroupBy(c => c.CurrencyCode)
                                                                                 .ToDictionary(c => c.Key, c => c.Select(cc => new PositionTakingViewModel
                                                                                 {
                                                                                     CurrencyCode = cc.CurrencyCode,
                                                                                     Type = cc.Type,
                                                                                     Value = cc.Value
                                                                                 }).ToList());

            var dtUplineNoCurrecyPersonPositionTaking = uplinesCurrencies.Where(cc => !dtUplineHasCurrencyPersonPositionTaking.Select(b => b.Key)
                                                                  .Contains(cc.Code)).ToDictionary(c => c.Code, c => new List<PositionTakingViewModel> {
                                                                    new PositionTakingViewModel
                                                                    {
                                                                        CurrencyCode = c.Code,
                                                                        Type =  PostitionTakingType.Upline,
                                                                        Value = 0
                                                                    },
                                                                    new PositionTakingViewModel
                                                                    {
                                                                        CurrencyCode = c.Code,
                                                                        Type =  PostitionTakingType.CurrentMax,
                                                                        Value = 1
                                                                    }
                                                                    });

            var positionTalkingByAllCurrecies = dtUplineNoCurrecyPersonPositionTaking.Concat(dtUplineHasCurrencyPersonPositionTaking)
                                                                                     .ToDictionary(c => c.Key, c => c.Value);
            ViewData["dtUplinePersonPositionTaking"] = positionTalkingByAllCurrecies;


            var dtHasCurrencyPositionTaking = new Dictionary<string, List<PositionTakingViewModel>>();
            dtHasCurrencyPositionTaking = personVM.PositionTakings.ToDictionary(c => c.Key, cc => cc.Value);
            var dtNoCurrencyPositionTaking = uplinesCurrencies.Where(c => !dtHasCurrencyPositionTaking.Select(cc => cc.Key).Contains(c.Code))
                                                              .ToDictionary(c => c.Code, c => new List<PositionTakingViewModel> {
                                                                new PositionTakingViewModel
                                                                {
                                                                    CurrencyCode = c.Code,
                                                                    Type =  PostitionTakingType.Upline,
                                                                    Value = 1
                                                                },
                                                                new PositionTakingViewModel
                                                                {
                                                                    CurrencyCode = c.Code,
                                                                    Type =  PostitionTakingType.CurrentMax,
                                                                    Value = 1
                                                                }
                                                              });

            var dtPositionTakingAllCurrencies = dtNoCurrencyPositionTaking.Concat(dtHasCurrencyPositionTaking)
                                                                          .ToDictionary(c => c.Key, c => c.Value);
            personVM.PositionTakings = dtPositionTakingAllCurrencies;



            if (ModelState.IsValid)
            {
                editPerson.Username = personVM.Username;
                editPerson.Firstname = personVM.Firstname;
                editPerson.Lastname = personVM.Lastname;
                editPerson.DisplayName = personVM.DisplayName;
                editPerson.Password = personVM.Password;
                editPerson.DOB = personVM.DOB ?? DateTime.Now;
                editPerson.Address = personVM.Address;
                editPerson.Type = personVM.PersonType;
                editPerson.Status = personVM.Status;
                editPerson.CountryCode = personVM.CountryCode;
                editPerson.Currencies = personVM.Currencies;
                editPerson.Mobile = personVM.Mobile;
                editPerson.Phone = personVM.Phone;
                editPerson.Email = personVM.Email;
                editPerson.External = personVM.External;
                editPerson.AutoTransfer = personVM.AutoTransfer;
                editPerson.LimitIP = personVM.LimitIP;
                editPerson.UplineOcode = personVM.UplineOCode;

                var personMaxCredits = personVM.PersonMaxCredits.Select(c => c.Value)
                                                                 .Where(c => personVM.Currencies.Contains(c.CurrencyCode))
                                                                 .Select(cc => new PersonMaxCreditReq
                                                                 {
                                                                     PersonOCode = personOCode,
                                                                     CurrencyCode = cc.CurrencyCode,
                                                                     PersonType = cc.PersonType,
                                                                     MaxCredit = cc.MaxCredit ?? 0m
                                                                 }).ToList();
                editPerson.PersonMaxCredits = personMaxCredits;

                var personBalances = personVM.PersonBalances.Select(c => c.Value)
                                                                .Where(c => personVM.Currencies.Contains(c.CurrencyCode))
                                                                .Select(cc => new PersonBalanceReq
                                                                {
                                                                    PersonOCode = personOCode,
                                                                    CurrencyCode = cc.CurrencyCode,
                                                                    Available = cc.Available ?? 0m
                                                                }).ToList();
                editPerson.PersonBalances = personBalances;

                var personBetSettings = personVM.BetSettings
                                                .Where(c => personVM.Currencies.Contains(c.Key))
                                                .SelectMany(c => c.Value)
                                                .Where(c => personVM.Currencies.Contains(c.CurrencyCode)).ToList();
                var personBetSettingReq = personBetSettings.Select(c => new PersonBetSettingReq
                {
                    CurrencyCode = c.CurrencyCode,
                    BetTypeItemOcode = BetTypeItemOCodes.SlotGame,
                    PersonOCode = c.PersonOCode,
                    SettingType = c.SettingType,
                    Value = c.Value ?? 0m,
                    ValueString = c.ValueString
                }).ToList();
                editPerson.BetSettings = personBetSettingReq;

                var personPositionTakings = personVM.PositionTakings
                                                    .Where(c => personVM.Currencies.Contains(c.Key))
                                                    .SelectMany(c => c.Value)
                                                    .Where(c => personVM.Currencies.Contains(c.CurrencyCode)).ToList();
                var personPositionTakingReq = personPositionTakings.Select(c => new PositionTakingReq
                {
                    PersonOCode = c.PersonOCode,
                    Type = c.Type,
                    BetTypeItemOcode = BetTypeItemOCodes.SlotGame,
                    CurrencyCode = c.CurrencyCode,
                    Value = c.Value
                }).ToList();

                editPerson.PositionTakings = personPositionTakingReq;
                try
                {
                    var res = await this.person.EditPerson(AuthManager.AuthIdentity.Token, personVM.OCode, editPerson);
                    if (res)
                    {
                        TempData["UpdatedInfo"] = "" + editPerson.Username + " is edited successful";
                        TempData["ocode"] = personVM.UplineOCode;

                        return RedirectToAction("PersonDownline", "Person");
                    }
                    else
                    {
                        ModelState.AddModelError("ErrorMessageID", "Invalid data request.");
                    }
                }
                catch (Exceptions.ALTServiceException ex)
                {

                    var getPersonDownline = await this.person.GetPersonByOcode(AuthManager.AuthIdentity.Token, personVM.OCode);
                    personVM.SelectedCurrencies = personVM.Currencies.Where(cc => !getPersonDownline.Currencies.Contains(cc)).ToArray();
                    ModelState.AddModelError("ErrorMessageID", ex.Message);
                }
            }
            else
            {
                ModelState.AddModelError("ErrorMessageID", "Invalid data request.");
            }
            return View(personVM);
        }

        public async Task<ActionResult> EditMainInfo(string ocode)
        {
            //PersonViewModel personViewModel = new PersonViewModel();

            //var currentOCode = ocode ?? AuthManager.AuthIdentity.PersonCode;
            //var resPerson = await this.person.GetPersonByOcode(AuthManager.AuthIdentity.Token, ocode);
            //ViewBag.OtoZ = new SelectList(Prefix());

            //if (AuthManager.AuthIdentity.Type != PersonType.ShareHolder && AuthManager.AuthIdentity.Type != PersonType.Admin)
            //{
            //    ViewBag.CurrencyUplines = resPerson.Currencies;
            //}

            //var canCreatedDownlines = AuthManager.AuthIdentity.Type.CanCreatedDownlines();
            //ViewBag.CreatedDownlines = canCreatedDownlines;

            //if (resPerson != null)
            //{
            //    personViewModel.Username = resPerson.Username;
            //    var listPrefix = ListPrefix(personViewModel.Username);
            //    for (int i = 0; i < listPrefix.Count; i++)
            //    {
            //        if (i == 0)
            //        {
            //            personViewModel.OtoZ1 = listPrefix[i];
            //        }
            //        if (i == 1)
            //        {
            //            personViewModel.OtoZ2 = listPrefix[i];
            //        }
            //        if (i == 2)
            //        {
            //            personViewModel.OtoZ3 = listPrefix[i];
            //        }
            //    }
            //    personViewModel.OCode = ocode;
            //    personViewModel.DOB = resPerson.DOB;
            //    personViewModel.Email = resPerson.Email;
            //    personViewModel.LimitIP = resPerson.LimitIP;
            //    personViewModel.Firstname = resPerson.Firstname;
            //    personViewModel.Lastname = resPerson.Lastname;
            //    personViewModel.DisplayName = resPerson.DisplayName;
            //    personViewModel.Address = string.Empty;
            //    personViewModel.Type = resPerson.Type;
            //    personViewModel.Status = resPerson.Status;
            //}
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> EditMainInfo(PersonViewModel personViewModel)
        {

            return View();
        }

        public ActionResult Cancel(string ocode = null)
        {
            TempData["ocode"] = ocode;
            return RedirectToAction("PersonDownline", "Person");
        }

        [HttpPost]
        [AjaxOnlyAttribute]
        public async Task<JsonResult> ChangeStatus(string status = null, string ocode = null)
        {
            PersonChangeStatusReq personStatus = new PersonChangeStatusReq();
            personStatus.Status = EnumHelper<PersonStatusType>.Parse(status);

            var changeResult = await this.person.ChangeStatus(AuthManager.AuthIdentity.Token, ocode, personStatus);
            var result = new JsonObjResp
            {
                Status = (int)HttpStatusCode.OK,
                Message = HttpStatusCode.OK.ToString(),
                ResultObj = JsonConvert.SerializeObject(changeResult)

            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AjaxOnlyAttribute]
        public async Task<JsonResult> ChangePassword(string ocode, string oldPassword, string newPassword)
        {
            PersonChangePasswordReq personChangePassword = new PersonChangePasswordReq();
            personChangePassword.OldPassword = oldPassword;
            personChangePassword.NewPassword = newPassword;

            var changeResult = await this.person.ChangePassword(AuthManager.AuthIdentity.Token, ocode, personChangePassword);
            var result = new JsonObjResp
            {
                Status = (int)HttpStatusCode.OK,
                Message = HttpStatusCode.OK.ToString(),
                ResultObj = JsonConvert.SerializeObject(changeResult)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditBetSetting()
        {
            PersonViewModel person = new PersonViewModel();

            return View(person);
        }

        [HttpPost]
        public ActionResult EditBetSetting(PersonViewModel personViewModel)
        {

            PersonViewModel person = new PersonViewModel();
            return View(person);
        }

        public ActionResult EditPositionTaking()
        {

            PersonViewModel person = new PersonViewModel();
            return View(person);
        }

        [HttpPost]
        public ActionResult EditPositionTaking(PersonViewModel personViewModel)
        {
            PersonViewModel person = new PersonViewModel();
            return View(person);
        }

        [HttpPost]
        [AjaxOnlyAttribute]
        public async Task<ActionResult> CheckUsername(string username)
        {
            var isCheckUsrname = await this.person.CheckUsername(AuthManager.AuthIdentity.Token, AuthManager.AuthIdentity.PersonCode, username);
            var res = new CheckUsername
            {
                isCheck = isCheckUsrname.IsCheck,
                Username = isCheckUsrname.Username,
                Message = "Not available"
            };

            var result = new JsonObjResp
            {
                Status = (int)HttpStatusCode.OK,
                Message = HttpStatusCode.OK.ToString(),
                ResultObj = JsonConvert.SerializeObject(res)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public decimal ConvertCurrencyRate(string FromCurrency, string ToCurrency, List<CurrencyResp> currencyResp)
        {
            decimal result;
            var FromCurrencyRate = currencyResp.Where(c => c.Code == FromCurrency).Select(cc => cc.Rate).FirstOrDefault();
            var ToCurrencyRate = currencyResp.Where(c => c.Code == ToCurrency).Select(cc => cc.Rate).FirstOrDefault();
            result = FromCurrencyRate / ToCurrencyRate;
            return result;
        }

        public PersonController(IPerson person, ICurrency currency)
        {
            this.person = person;
            this.currency = currency;
        }
        private readonly IPerson person;
        private readonly ICurrency currency;

        private readonly static int TotalItemPerPages = 25;

        private List<PersonType> PersonOneCurrency = new List<PersonType> {
                                                        PersonType.ShareHolder ,
                                                        PersonType.SuperSenior ,
                                                        PersonType.Senior ,
                                                        PersonType.SuperMaster ,
                                                        PersonType.Master ,
                                                        PersonType.SuperAgent ,
                                                        PersonType.Agent ,
                                                        PersonType.Member
                                                    };

    }
}