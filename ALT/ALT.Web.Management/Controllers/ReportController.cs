﻿using ALT.Common;
using ALT.Common.Helpers;
using ALT.Common.Models;
using ALT.Web.Management.Core;
using ALT.Web.Management.Infrastructer;
using ALT.Web.Management.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ALT.Web.Management.Controllers
{
    public class ReportController : BaseController
    {
        // GET: Peport
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult WinLossReport()
        {
            return View();
        }

        [HttpGet]
        [AjaxOnlyAttribute]
        public async Task<ActionResult> DailyWinLossReport(int? page, DateTime startDate, DateTime endDate,
                                          DateTime startTime, DateTime endTime, string Username)
        {
            DailyWinLossViewModel dailyWinLossReport = new DailyWinLossViewModel();

            startDate = startDate.AddHours(startTime.TimeOfDay.Hours).AddMinutes(startTime.TimeOfDay.Minutes);
            endDate = endDate.AddHours(endTime.TimeOfDay.Hours).AddMinutes(endTime.TimeOfDay.Minutes);
            var currencyCode = AuthManager.AuthIdentity.Currencies.Select(c => c.Code).FirstOrDefault();

            dailyWinLossReport.WinLossResp = await this.report.DailyWinLossPeport(AuthManager.AuthIdentity.Token, AuthManager.AuthIdentity.PersonCode,
                Username, startDate, endDate, (int)page, TotalItemPerPages , currencyCode);

            return Json(dailyWinLossReport, JsonRequestBehavior.AllowGet);
        }


        public async Task<ActionResult> TransactionDetailReport(DateTime? startDate, DateTime? endDate, DateTime? startTime, DateTime? endTime, string username = null, string currencyOcode = null)
        {
            FillerTransactionDetailViewModel filterTransactionDetailViewModel = new FillerTransactionDetailViewModel();

            filterTransactionDetailViewModel.Username = username;
            filterTransactionDetailViewModel.CurrencyOcode = currencyOcode;
            filterTransactionDetailViewModel.StartDate = startDate.HasValue ? startDate.Value : DateTime.Now;
            filterTransactionDetailViewModel.EndDate = endDate.HasValue ? endDate.Value : DateTime.Now;
            filterTransactionDetailViewModel.StartTime = startTime.HasValue ? startTime.Value : filterTransactionDetailViewModel.StartTime.AddHours(00).AddMinutes(00);
            filterTransactionDetailViewModel.EndTime = endTime.HasValue ? endTime.Value : filterTransactionDetailViewModel.StartTime.AddHours(23).AddMinutes(00);

            var getCurrencyOCode = await this.currency.GetAllCurrency(AuthManager.AuthIdentity.Token);
            ViewBag.GetAllCurrency = ListCurrencyViewModel(getCurrencyOCode);

            ViewBag.filterTransactionDetail = filterTransactionDetailViewModel;

            return View();
        }

        [HttpGet]
        [AjaxOnlyAttribute]
        public async Task<ActionResult> TransactionDetailFillterReport(int? page, DateTime startDate, DateTime endDate, DateTime startTime,
                                                                       DateTime endTime, string username, string currencyCode)
        {
            List<CurrencyViewModel> currencyViewModel = new List<CurrencyViewModel>();

            startDate = startDate.AddHours(startTime.TimeOfDay.Hours).AddMinutes(startTime.TimeOfDay.Minutes);
            endDate = endDate.AddHours(endTime.TimeOfDay.Hours).AddMinutes(endTime.TimeOfDay.Minutes);

            var _transactionDetailModel = new TransactionDetailViewModel()
            {
                PersonOCode = AuthManager.AuthIdentity.PersonCode,
                BetTypeItemOCodes = StringHelper.ToArray(BetTypeItemOCodes.SlotGame),
                GameOCode = String.Empty,
                UsernameDownline = username,
                CurrencyCode = currencyCode,
                StartDate = startDate,
                EndDate = endDate,
                TransactionDetails = new Common.Helpers.PagedList<ALT.Common.Models.TransactionDetailResp> { }
            };

            _transactionDetailModel.TransactionDetails = await this.report.GetTransactionDetails(AuthManager.AuthIdentity.Token, _transactionDetailModel.PersonOCode,
                                                    _transactionDetailModel.BetTypeItemOCodes, _transactionDetailModel.GameOCode, _transactionDetailModel.CurrencyCode, _transactionDetailModel.UsernameDownline,
                                                    _transactionDetailModel.StartDate, _transactionDetailModel.EndDate, (int)page, TotalItemPerPages);

            return Json(_transactionDetailModel, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> SummaryReport()
        {
            var getCurrencyOCode = await this.currency.GetAllCurrency(AuthManager.AuthIdentity.Token);
            ViewBag.GetAllCurrency = ListCurrencyViewModel(getCurrencyOCode);

            return View();
        }

        [HttpGet]
        [AjaxOnlyAttribute]
        public async Task<ActionResult> SummaryFillterReport(int? page, DateTime startDate, DateTime endDate,
                                          DateTime startTime, DateTime endTime, string Username, string personOcode = null, string currencyCode = null)
        {
            SummaryViewModel summaryViewModel = new SummaryViewModel();
            if (String.IsNullOrEmpty(personOcode))
            {
                personOcode = AuthManager.AuthIdentity.PersonCode;
            }
            startDate = startDate.AddHours(startTime.TimeOfDay.Hours).AddMinutes(startTime.TimeOfDay.Minutes);
            endDate = endDate.AddHours(endTime.TimeOfDay.Hours).AddMinutes(endTime.TimeOfDay.Minutes);

            summaryViewModel.SummaryResp = await this.report.SummaryReport(AuthManager.AuthIdentity.Token, personOcode,
                                                                           Username, startDate, endDate, (int)page, TotalItemPerPages, currencyCode);

            return Json(summaryViewModel, JsonRequestBehavior.AllowGet);
        }

        public List<CurrencyViewModel> ListCurrencyViewModel(List<CurrencyResp> currencyResp)
        {
            return currencyResp.Select(c => new CurrencyViewModel
            {
                Code = c.Code,
                Rate = c.Rate,
                Enable = c.Enable,
                Name = c.Name
            }).ToList();
        }

        public ReportController(IReport report, IPerson person, ICurrency currency)
        {
            this.report = report;
            this.person = person;
            this.currency = currency;
        }
        private readonly IReport report;
        private readonly IPerson person;
        private readonly ICurrency currency;
        public readonly static int TotalItemPerPages = 25;
    }
}