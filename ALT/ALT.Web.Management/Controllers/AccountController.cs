﻿using ALT.Web.Management.Models.Login;
using System.Web.Mvc;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using ALT.Web.Management.Infrastructer;
using CaptchaMvc.Attributes;
using ALT.Web.Management.Core;
using ALT.Common.Models;
using System.Web.Security;
using System.Threading.Tasks;
using ALT.Common.Models.Models;
using System.Net;
using System;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.Linq;

namespace ALT.Web.Management.Controllers
{
    using ALT.Common.Web;
    public class AccountController : BaseController
    {
        // GET: Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            LoginViewModel loginViewModel = new LoginViewModel();
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("PersonDownline", "Person");
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(loginViewModel);
        }

        [HttpPost, CaptchaVerify("Captcha is not valid")]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginViewModel loginViewModel, string ReturnUrl)
        {
            int idex = Session[ItemKeys.LoginTimesKey] == null ? 0 : (int)Session[ItemKeys.LoginTimesKey];
            AuthenticationReq auth = new AuthenticationReq();
            auth.Username = loginViewModel.Username;
            auth.Password = loginViewModel.Password;
            try
            {
                if (idex < 3)
                {
                    AuthenticationResp authenResp = await this.authentication.SignIn(auth);
                    AuthManager.SetAuth(authenResp);

                    return RedirectToAction("PersonDownline", "Person");
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        AuthenticationResp authenResp = await this.authentication.SignIn(auth);
                        AuthManager.SetAuth(authenResp);

                        return RedirectToAction("PersonDownline", "Person");
                    }
                    else
                    {
                        Session[ItemKeys.LoginTimesKey] = idex + 1;
                        ModelState.AddModelError("ErrorMessageID", "Error: captcha is not valid.");
                    }
                }
            }
            catch (Exceptions.ALTServiceException ex)
            {
                Session[ItemKeys.LoginTimesKey] = idex + 1;
                ViewBag.LoginCount = Session[ItemKeys.LoginTimesKey];
                ModelState.AddModelError("ErrorMessageID", ex.Message);
            }

            return View(loginViewModel);
        }

        [AllowAnonymous]
        public ActionResult Logout()
        {
            AuthManager.SignOff();
            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        [AjaxOnlyAttribute]
        public async Task<JsonResult> ChangePassword(string code, string newPassword = null)
        {
            var result = new JsonObjResp()
            {
                Status = (int)HttpStatusCode.OK,
                Message = HttpStatusCode.OK.ToString()
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public AccountController(IAuthentication authentication)
        {
            this.authentication = authentication;
        }

        private readonly IAuthentication authentication;
    }
}