﻿using ALT.Web.Management.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ALT.Web.Management.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public async Task<JsonResult> GetInfo()
        {
            //var result = await this.person.GetAPIInfo("1");
            // Get Current User login;  ALT.Web.Management.Core.AuthManager.AuthIdentity
            return Json("success", JsonRequestBehavior.AllowGet);
        }

        public HomeController(IPerson person)
        {
            this.person = person;
        }

        private readonly IPerson person;
    }
}