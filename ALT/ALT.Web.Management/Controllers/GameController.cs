﻿using ALT.Common.Helpers;
using ALT.Common.Models;
using ALT.Common.Models.Models;
using ALT.Web.Management.Core;
using ALT.Web.Management.Infrastructer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ALT.Web.Management.Controllers
{
    using ALT.Web.Management.Core;
    using Common;
    using Microsoft.Owin.Security.Twitter.Messages;
    using Models;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System.Net;
    using System.Text.RegularExpressions;
    using System.Web.Mvc.Html;

    public class GameController : BaseController
    {
        public async Task<ActionResult> Index()
        {
            return View();
        }

        [HttpGet]
        [AjaxOnlyAttribute]
        public async Task<JsonResult> GetGameBudget(GameBudgetType type, int pageIndex = 1)
        {
            var result = await this.game.GetGameBudget(AuthManager.AuthIdentity.Token, type, string.Empty);

            var getALlGameBudget = result.Select(c => c.GameCode).ToArray();
            var getAllGame = await this.game.GetAllGame(string.Empty, false, "ALT", "Web");

            getAllGame = getAllGame.Where(c => getALlGameBudget.Contains(c.Code)).ToList();
            ViewBag.GameList = getAllGame;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AjaxOnlyAttribute]
        public async Task<JsonResult> UpdateGameBudget(bool add, GameBudgetType type, string GameCode, long Points)
        {
            if (add)
            {
                var changeResult = await this.game.AddGameBudget(AuthManager.AuthIdentity.Token, new GameBudgetReq
                {
                    GameCode = GameCode,
                    Type = type,
                    Points = Points
                });

                var result = new JsonObjResp
                {
                    Status = (int)HttpStatusCode.OK,
                    Message = HttpStatusCode.OK.ToString(),
                    ResultObj = JsonConvert.SerializeObject(changeResult)

                };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var changeResult = await this.game.UpdateGameBudget(AuthManager.AuthIdentity.Token, GameCode, new GameBudgetReq
                {
                    GameCode = GameCode,
                    Type = type,
                    Points = Points
                });

                var result = new JsonObjResp
                {
                    Status = (int)HttpStatusCode.OK,
                    Message = HttpStatusCode.OK.ToString(),
                    ResultObj = JsonConvert.SerializeObject(changeResult)

                };
                return Json(result, JsonRequestBehavior.AllowGet);
            }

        }


        [HttpGet]
        [AjaxOnlyAttribute]
        public async Task<JsonResult> CheckGetGame(GameBudgetType type)
        {
            var result = await this.game.GetGameBudget(AuthManager.AuthIdentity.Token, type, string.Empty);

            var getALlGameBudget = result.Select(c => c.GameCode).ToArray();
            var getAllGame = await this.game.GetAllGame(string.Empty, false, "ALT", "Web");

            getAllGame = getAllGame.Where(c => !getALlGameBudget.Contains(c.Code)).ToList();
            return Json(getAllGame.ToArray(), JsonRequestBehavior.AllowGet);
        }


        public GameController(IGame person)
        {
            this.game = person;
        }
        private readonly IGame game;
    }
}