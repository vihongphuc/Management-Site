﻿using ALT.Common.Models;
using ALT.Web.Management.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ALT.Web.Management.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllerNanme = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (!ExcepControllers.Contains(controllerNanme.ToUpper()) && !ExcepActionNames.Contains(actionName.ToUpper()))
            {
                if (Core.AuthManager.AuthIdentity == null || string.IsNullOrEmpty(Core.AuthManager.AuthIdentity.Token))
                {
                    Core.AuthManager.SignOff();
                    filterContext.Result = new RedirectToRouteResult(
                        new System.Web.Routing.RouteValueDictionary
                        { {"controller" , "Account"} , {"action","Login"} });
                    return;
                }
            }

            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }


        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
        }

        private string[] ExcepControllers = new string[] { "ACCOUNT" };
        private string[] ExcepActionNames = new string[] { "LOGIN" };
    }
}