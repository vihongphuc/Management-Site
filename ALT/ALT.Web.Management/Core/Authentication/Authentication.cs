﻿using ALT.Common.Models;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace ALT.Web.Management.Core
{
    using ALT.Common.Web;
    public class Authentication : IAuthentication, IStore
    {
        private readonly HttpClient httpClient;
        public Authentication(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }
        public async Task<AuthenticationResp> SignIn(AuthenticationReq authenticationReq)
        {
            var response = await httpClient.PostAsJsonAsync<AuthenticationReq>($"api/authentication", authenticationReq)
                                           .ThrowOnErrors()
                                            .ConfigureAwait(continueOnCapturedContext: false);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsAsync<AuthenticationResp>();
                return result;
            }
            else
            {
                string responseBody = await response.Content.ReadAsStringAsync();
                throw new HttpException((int)response.StatusCode, responseBody);

            }
        }

        public async Task<bool> SignOut(string sessionOcode)
        {
            var resq = await this.httpClient.PostAsJsonAsync($"api/authentication/SignOut", sessionOcode);
            return true;
        }

    }
}