﻿using ALT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Web.Management.Core
{
    public interface IAuthentication
    {
        Task<AuthenticationResp> SignIn(AuthenticationReq authenticationRequest);
        Task<bool> SignOut(string sessionCode);
    }
}
