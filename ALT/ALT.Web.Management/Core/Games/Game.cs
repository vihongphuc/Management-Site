﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ALT.Web.Management.Core
{
    using Common.Helpers;
    using Common.Models;
    using Common.Models.Models;
    using System.Net.Http;
    using System.Web.Http;
    using System.Web.Mvc;

    public class Game : IGame, IStore
    {
        private readonly HttpClient httpClient;
        public Game(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<IList<GameResp>> GetAllGame(string gamecode, bool includedSetting, string publisher, string platforms)
        {
            var response = await httpClient.GetAsync($"api/game-data?gamecode={gamecode}&includedSetting={includedSetting}&publisher={publisher}&platforms={platforms}")
                                           .ConfigureAwait(continueOnCapturedContext: false);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsAsync<IList<GameResp>>();
                return result;
            }
            return null;
        }

        public async Task<GameBudgetResp> AddGameBudget(string token, GameBudgetReq gameBudgetRequset)
        {
            GameBudgetResp res;

            var resp = await this.httpClient.PostAsJsonAsync($"api/game-budget?t={token}", gameBudgetRequset)
                                 .ConfigureAwait(continueOnCapturedContext: false);
            resp.EnsureSuccessStatusCode();
            res = await resp.Content.ReadAsAsync<GameBudgetResp>();

            return res;
        }


        public async Task<bool> UpdateGameBudget(string token, string gameCode, GameBudgetReq gameBudgetRequset)
        {
            bool res;

            var resp = await this.httpClient.PutAsJsonAsync($"api/game-budget/update-by-gamecode?gameCode={gameCode}&t={token}", gameBudgetRequset)
                                 .ConfigureAwait(continueOnCapturedContext: false);
            resp.EnsureSuccessStatusCode();
            res = await resp.Content.ReadAsAsync<bool>();

            return res;
        }

        public async Task<GameBudgetResp[]> GetGameBudget(string token, GameBudgetType butgetType, string gameCode)
        {
            GameBudgetResp[] res;

            var resp = await this.httpClient.GetAsync($"api/game-budget?type={butgetType}&gameCode={gameCode}&t={token}")
                                 .ConfigureAwait(continueOnCapturedContext: false);
            resp.EnsureSuccessStatusCode();
            res = await resp.Content.ReadAsAsync<GameBudgetResp[]>();

            return res;
        }
    }
}