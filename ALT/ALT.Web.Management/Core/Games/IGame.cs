﻿using ALT.Common.Helpers;
using ALT.Common.Models;
using ALT.Common.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Web.Management.Core
{
    public interface IGame
    {
        Task<IList<GameResp>> GetAllGame(string gamecode, bool includedSetting, string publisher, string platforms);
        Task<GameBudgetResp> AddGameBudget(string token, GameBudgetReq requset);
        Task<bool> UpdateGameBudget(string token, string gameCode, GameBudgetReq gameBudgetRequset);
        Task<GameBudgetResp[]> GetGameBudget(string token, GameBudgetType butgetType, string gameCode);
    }
}