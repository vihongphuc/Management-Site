﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ALT.Web.Management.Core
{
    using Common.Helpers;
    using Common.Models;
    using Common.Models.Models;
    using System.Net.Http;
    using System.Web.Http;
    using System.Web.Mvc;

    public class Currency : ICurrency, IStore
    {
        private readonly HttpClient httpClient;
        public Currency(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }
        public async Task<List<CurrencyResp>> GetAllCurrency(string token)
        {
            List<CurrencyResp> getCurrency;

            var resp = await this.httpClient.GetAsync($"api/currencies/get-currency?t={token}")
                                            .ConfigureAwait(continueOnCapturedContext: false);

            getCurrency = await resp.Content.ReadAsAsync<List<CurrencyResp>>();

            return getCurrency;
        }

    }
}