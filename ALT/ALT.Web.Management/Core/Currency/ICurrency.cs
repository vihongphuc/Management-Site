﻿using ALT.Common.Helpers;
using ALT.Common.Models;
using ALT.Common.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Web.Management.Core
{
    public interface ICurrency
    {
        Task<List<CurrencyResp>> GetAllCurrency(string token);
    }
}