﻿using ALT.Common.Helpers;
using ALT.Common.Models;
using ALT.Common.Models.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ALT.Web.Management.Core
{
    public interface IReport
    {
        Task<PagedList<TransactionDetailResp>> GetTransactionDetails(string token, string personOCode, string[] betTypeItemOCode, string gameOCode, string currencyCode, string Username, DateTime startDate, DateTime endDate, int PageIndex, int TotalItemPerPages);
        Task<PagedList<WinLossResp>> DailyWinLossPeport(string token, string personOCode, string Username, DateTime startDate, DateTime endDate, int PageIndex, int TotalItemPerPages, string currencyCode);
        Task<PagedList<SummaryResp>> SummaryReport(string token, string personOCode, string Username, DateTime startDate, DateTime endDate, int PageIndex, int TotalItemPerPages, string currencyCode);
    }
}
