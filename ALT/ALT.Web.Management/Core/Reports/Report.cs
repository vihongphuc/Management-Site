﻿using ALT.Common.Helpers;
using ALT.Common.Models;
using ALT.Common.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace ALT.Web.Management.Core
{
    public class Report : IReport, IStore
    {
        private readonly HttpClient httpClient;
        public Report(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<PagedList<TransactionDetailResp>> GetTransactionDetails(string token, string personOCode, string[] betTypeItemOCode, string gameOCode, string currencyCode, string Username, DateTime startDate, DateTime endDate, int PageIndex, int TotalItemPerPages)
        {
            PagedList<TransactionDetailResp> transactionDetail;
            var resp = await this.httpClient.GetAsync($"api/reports/{personOCode}/transaction-detail?t={token}&betTypeItemOCode={StringHelper.Compile(betTypeItemOCode)}&gameOCode={gameOCode}&currencyCode={currencyCode}&Username={Username}&startDate={startDate}&endDate={endDate}&PageIndex={PageIndex}&ItemPerPage={TotalItemPerPages}")
                                            .ConfigureAwait(continueOnCapturedContext: false);

            transactionDetail = await resp.Content.ReadAsAsync<PagedList<TransactionDetailResp>>();

            return transactionDetail;
        }

        public async Task<PagedList<WinLossResp>> DailyWinLossPeport(string token, string personOCode, string Username, DateTime startDate, DateTime endDate, int PageIndex, int TotalItemPerPages,string currencyCode)
        {
            PagedList<WinLossResp> dailyWinLossReport;
            var resp = await this.httpClient.GetAsync($"api/reports/{personOCode}/daily-win-loss-report?t={token}&Username={Username}&CurrencyCode={currencyCode}&startDate={startDate}&endDate={endDate}&PageIndex={PageIndex}&ItemPerPage={TotalItemPerPages}")
                                            .ConfigureAwait(continueOnCapturedContext: false);

            dailyWinLossReport = await resp.Content.ReadAsAsync<PagedList<WinLossResp>>();

            return dailyWinLossReport;
        }
        public async Task<PagedList<SummaryResp>> SummaryReport(string token, string personOCode, string Username, DateTime startDate, DateTime endDate, int PageIndex, int TotalItemPerPages, string currencyCode)
        {
            PagedList<SummaryResp> dailyWinLossReport;
            var resp = await this.httpClient.GetAsync($"api/reports/{personOCode}/summary-report?t={token}&Username={Username}&CurrencyCode={currencyCode}&startDate={startDate}&endDate={endDate}&PageIndex={PageIndex}&ItemPerPage={TotalItemPerPages}")
                                            .ConfigureAwait(continueOnCapturedContext: false);

            dailyWinLossReport = await resp.Content.ReadAsAsync<PagedList<SummaryResp>>();

            return dailyWinLossReport;
        }
    }
}