﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ALT.Web.Management.Core
{
    using ALT.Common;

    public class SessionManager
    {
        private string currentToken = string.Empty;
        public string CurrentToken
        {
            get
            {
                if (string.IsNullOrEmpty(currentToken))
                {
                    //HttpContext.Current == null 
                    return string.Empty; // get currentSesion
                }
                else return currentToken;
            }
        }

        public SessionManager()
        {
            currentToken = OCode.Get(KeyGeneration.GenerateInt32Id());
            if(HttpContext.Current != null && HttpContext.Current.Session == null)
            {
                HttpContext.Current.Items[ItemKeys.RequestSessionSelectorKey] = currentToken;
                HttpContext.Current.Items[ItemKeys.SessionItemKey] = HttpContext.Current.Session;
            }
        }

        public static string GetCurrentSessionID()
        {
            //var currentTokenValue = SessionManager.
            return "";
        }


    }

    public class UserAuthentication
    {
        public string PersonOCode { get; set; }
        public string PersonType { get; set; }
        public string Username { get; set; }

        public string UplineID { get; set; }
        public string UplineType { get; set; }

    }
}