﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ALT.Web.Management.Core
{
    using Common.Helpers;
    using Common.Models;
    using Common.Models.Models;
    using Common.Web;
    using System.Net.Http;
    using System.Web.Http;
    using System.Web.Mvc;

    public class Person : IPerson, IStore
    {
        private readonly HttpClient httpClient;
        public Person(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }
        public async Task<PagedList<PersonResp>> GetPersonDownlines(string token, string ocode, bool? isMember, int pageIndex, int pageSize)
        {
            PagedList<PersonResp> result;
            var resp = await this.httpClient.GetAsync($"api/person/{ocode}/downlines?t={token}&pageIndex={pageIndex}&pageSize={pageSize}&isMember={isMember}")
                                 .ConfigureAwait(continueOnCapturedContext: false);

            result = await resp.Content.ReadAsAsync<PagedList<PersonResp>>();
            return result;
        }
        public async Task<bool> ChangeStatus(string token, string ocode, PersonChangeStatusReq personChangeStatus)
        {
            var result = false;
            var response = await this.httpClient.PutAsJsonAsync<PersonChangeStatusReq>($"api/person/{ocode}/change-status?t={token}", personChangeStatus)
                                     .ConfigureAwait(continueOnCapturedContext: false);
            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<bool>();
                return result;
            }
            return result;
        }
        public async Task<bool> ChangePassword(string token, string ocode, PersonChangePasswordReq personChangePassword)
        {
            bool result = false;
            var response = await this.httpClient.PutAsJsonAsync<PersonChangePasswordReq>($"api/person/{ocode}/change-password?t={token}", personChangePassword)
                                     .ConfigureAwait(continueOnCapturedContext: false);
            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<bool>();
                return result;
            }
            return result;
        }
        public async Task<PersonTransferResp> Transfer(string token, string ocode, PersonTransferReq personTransfer)
        {
            PersonTransferResp result = new PersonTransferResp();
            var response = await this.httpClient.PutAsJsonAsync<PersonTransferReq>($"api/person/{ocode}/transfer?t={token}", personTransfer)
                                      .ThrowOnErrors()
                                      .ConfigureAwait(continueOnCapturedContext: false);
            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<PersonTransferResp>();
                return result;
            }

            return result;
        }
        public async Task<Dictionary<string, decimal>> GetPersonBalanceByOcode(string token, string ocode)
        {
            var resp = await this.httpClient.GetAsync($"api/person/{ocode}/get-person-balances?t={token}")
                                 .ConfigureAwait(continueOnCapturedContext: false);

            return await resp.Content.ReadAsAsync<Dictionary<string, decimal>>();
        }
        public async Task<PagedList<TransferMemberHistoryResp>> TransferHistory(string token, string ocode, DateTime StartDate, DateTime EndDate, int pageIndex, int pageSize)
        {
            PagedList<TransferMemberHistoryResp> result;
            var resp = await this.httpClient.GetAsync($"api/person/{ocode}/get-transfer-statement?t={token}&startdate={StartDate}&endDate={EndDate}&pageIndex={pageIndex}&pageSize={pageSize}")
                                            .ThrowOnErrors()
                                            .ConfigureAwait(continueOnCapturedContext: false);
            result = await resp.Content.ReadAsAsync<PagedList<TransferMemberHistoryResp>>();
            return result;
        }
        public async Task<ChechUsernameResp> CheckUsername(string token, string personCode, string username)
        {
            ChechUsernameResp res;
            var resp = await this.httpClient.GetAsync($"api/person/check-username?t={token}&personCode={personCode}&username={username}")
                               .ConfigureAwait(continueOnCapturedContext: false);

            res = await resp.Content.ReadAsAsync<ChechUsernameResp>();
            return res;
        }
        public async Task<bool> AddPerson(string token, UpdatePersonReq personInfo)
        {
            bool res = false;

            var resp = await this.httpClient.PostAsJsonAsync($"api/person?t={token}", personInfo)
                                 .ThrowOnErrors()
                                 .ConfigureAwait(continueOnCapturedContext: false);

            if (!resp.IsSuccessStatusCode)
            {
                string responseBody = await resp.Content.ReadAsStringAsync();
                throw new HttpException((int)resp.StatusCode, responseBody);
            }

            res = await resp.Content.ReadAsAsync<bool>();
            return res;
        }
        public async Task<bool> EditPerson(string token, string ocode, UpdatePersonReq personInfo)
        {
            bool res = false;

            var resp = await this.httpClient.PutAsJsonAsync($"api/person/{ocode}/?t={token}", personInfo)
                                 .ThrowOnErrors()
                                 .ConfigureAwait(continueOnCapturedContext: false);

            if (!resp.IsSuccessStatusCode)
            {
                string responseBody = await resp.Content.ReadAsStringAsync();
                throw new HttpException((int)resp.StatusCode, responseBody);
            }

            res = await resp.Content.ReadAsAsync<bool>();
            return res;
        }
        public async Task<EditPersonCurrentResp> GetPersonByOcode(string token, string ocode)
        {
            EditPersonCurrentResp resResp = null;
            var resp = await this.httpClient.GetAsync($"api/person/{ocode}/person-by-ocode?t={token}")
                                             .ConfigureAwait(continueOnCapturedContext: false);
            resResp = await resp.Content.ReadAsAsync<EditPersonCurrentResp>();
            return resResp;
        }
        public async Task<List<PositionTalkingResp>> GetPositionTaking(string token, string ocode, bool getUpline = false)
        {
            List<PositionTalkingResp> positionTalkingUpline;

            var resp = await this.httpClient.GetAsync($"api/person/{ocode}/get-position-talking?t={token}&getUpline={getUpline}")
                                            .ConfigureAwait(continueOnCapturedContext: false);

            positionTalkingUpline = await resp.Content.ReadAsAsync<List<PositionTalkingResp>>();
            return positionTalkingUpline;
        }
        public async Task<List<PersonMaxCreditResp>> GetPersonMaxCreditByOcode(string token, string ocode, bool getUpline = false)
        {
            List<PersonMaxCreditResp> personMaxCreditUpline;

            var resp = await this.httpClient.GetAsync($"api/person/{ocode}/get-person-max-credit-by-ocode?t={token}&getUpline={getUpline}")
                                            .ConfigureAwait(continueOnCapturedContext: false);

            personMaxCreditUpline = await resp.Content.ReadAsAsync<List<PersonMaxCreditResp>>();
            return personMaxCreditUpline;
        }
        public async Task<List<PersonBetSettingResp>> GetPersonBetSettingByOcode(string token, string ocode, bool getUpline = false)
        {
            List<PersonBetSettingResp> personBetSettingUpline;

            var resp = await this.httpClient.GetAsync($"api/person/{ocode}/get-person-bet-setting-by-ocode?t={token}&getUpline={getUpline}")
                                            .ConfigureAwait(continueOnCapturedContext: false);

            personBetSettingUpline = await resp.Content.ReadAsAsync<List<PersonBetSettingResp>>();
            return personBetSettingUpline;
        }
        public async Task<List<PersonBalanceResp>> GetPersonListBalanceByOcode(string token, string ocode, bool getUpline = false)
        {
            List<PersonBalanceResp> personBalanceUpline;

            var resp = await this.httpClient.GetAsync($"api/person/{ocode}/get-person-list-balance-by-code?t={token}&getUpline={getUpline}")
                                            .ConfigureAwait(continueOnCapturedContext: false);

            personBalanceUpline = await resp.Content.ReadAsAsync<List<PersonBalanceResp>>();
            return personBalanceUpline;
        }
        public async Task<Dictionary<string, string>> BreadcrumbPersonByOCode(string token, string ocode)
        {
            Dictionary<string, string> personBalanceUpline;
            var resp = await this.httpClient.GetAsync($"api/person/{ocode}/breadcrumb-person-by-ocode?t={token}")
                                            .ConfigureAwait(continueOnCapturedContext: false);

            personBalanceUpline = await resp.Content.ReadAsAsync<Dictionary<string, string>>();

            return personBalanceUpline;
        }
           
        public async Task<List<PersonResp>> SearchUsername(string token, string ocode, string Username = null)
        {
            List<PersonResp> _lstUsername = null;

            var resp = await this.httpClient.GetAsync($"api/person/{ocode}/search-username?t={token}&username={Username}")
                                                       .ConfigureAwait(continueOnCapturedContext: false);

            _lstUsername = await resp.Content.ReadAsAsync<List<PersonResp>>();

            return _lstUsername;
        }
    }
}