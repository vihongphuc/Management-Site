﻿using ALT.Common.Helpers;
using ALT.Common.Models;
using ALT.Common.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Web.Management.Core
{
    public interface IPerson
    {
        Task<PagedList<PersonResp>> GetPersonDownlines(string token, string ocode, bool? isMember, int pageIndex, int pageSize);
        Task<bool> ChangeStatus(string token, string ocode, PersonChangeStatusReq personChangeStatus);
        Task<bool> ChangePassword(string token, string ocode, PersonChangePasswordReq personChange);
        Task<PersonTransferResp> Transfer(string token, string ocode, PersonTransferReq personTransfer);
        Task<Dictionary<string, decimal>> GetPersonBalanceByOcode(string token, string ocode);
        Task<PagedList<TransferMemberHistoryResp>> TransferHistory(string token, string ocode, DateTime StartDate, DateTime EndDate, int pageIndex, int pageSize);
        Task<ChechUsernameResp> CheckUsername(string token, string personCode, string username);
        Task<bool> AddPerson(string token, UpdatePersonReq personInfo);
        Task<bool> EditPerson(string token, string ocode, UpdatePersonReq personInfo);
        Task<EditPersonCurrentResp> GetPersonByOcode(string token, string ocode);
        Task<List<PositionTalkingResp>> GetPositionTaking(string token, string ocode, bool getUpline = false);
        Task<List<PersonMaxCreditResp>> GetPersonMaxCreditByOcode(string token, string ocode, bool getUpline = false);
        Task<List<PersonBalanceResp>> GetPersonListBalanceByOcode(string token, string ocode, bool getUpline = false);
        Task<List<PersonBetSettingResp>> GetPersonBetSettingByOcode(string token, string ocode, bool getUpline = false);
        Task<Dictionary<string, string>> BreadcrumbPersonByOCode(string token, string ocode);        
        Task<List<PersonResp>> SearchUsername(string token, string ocode, string Username = null);
    }
}