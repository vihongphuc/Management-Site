﻿using ALT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace ALT.Web.Management.Core
{
    public static class AuthManager
    {
        public static AuthenticationResp AuthIdentity { get { return GetCurrentAuth(); } }

        public static void SetAuth(AuthenticationResp authData)
        {
            var currentSesson = HttpContext.Current.Session;
            if (currentSesson != null && currentSesson[ItemKeys.AuthenticationKey] != null)
            {
                SignOff();
            }
            FormsAuthentication.SetAuthCookie(authData.Username, true);
            HttpContext.Current.Session.Add(ItemKeys.AuthenticationKey, authData);
            HttpContext.Current.Session.Timeout = 30;
        }

        public static AuthenticationResp GetCurrentAuth()
        {
            var currentSesson = HttpContext.Current.Session;
            if (currentSesson != null && currentSesson[ItemKeys.AuthenticationKey] != null)
            {
                return (AuthenticationResp)currentSesson[ItemKeys.AuthenticationKey];
            }
            else
            {
                return new AuthenticationResp() { PersonCode = string.Empty };
            }
        }

        public static void SignOff()
        {
            FormsAuthentication.SignOut();
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.Abandon();
        }
    }
}