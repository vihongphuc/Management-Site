﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ALT.Web.Management.Core
{
    public static class ItemKeys
    {
        public static readonly string RequestSessionSelectorKey = "t";
        public static readonly string SessionItemKey = "_SessionItemKey";


        public static string LoginTimesKey = "_LoginTimesKey";
        public static string AuthenticationKey = "_AuthenticationKey";
    }
}