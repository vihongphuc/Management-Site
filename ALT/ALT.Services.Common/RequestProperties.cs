﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Services.Common
{
    using ALT.Common.Helpers;
    using ALT.Common.Models;
    public static class RequestProperties
    {
        public static string UplineType = "UplineType";

        public static readonly string CurrentTokenKey = ".Token";
        public static readonly string CurrentRobotTokenKey = ".RobotToken";
        public static readonly string CurrentPersonOCodeKey = "PersonOCode";
        public static readonly string CurrentPersonTypeKey = "PersonType";
        public static readonly string CurrentPersonUplineOCodeKey = "UplineOCode";
        public static readonly string CurrentPersonGroupOCodeKey = "GroupOCode";
        public static readonly string CurrentSubAccountOCodeKey = "SubAccountOCode";

        public static string GetCurrentToken(this HttpRequestMessage request)
        {
            object currentToken = null;
            if (request.Properties.TryGetValue(CurrentTokenKey, out currentToken))
            {
                return (string)currentToken;
            }
            else
            {
                return null;
            }
        }
        public static string GetCurrentRobotToken(this HttpRequestMessage request)
        {
            object currentRobotToken = null;
            if (request.Properties.TryGetValue(CurrentRobotTokenKey, out currentRobotToken))
            {
                return (string)currentRobotToken;
            }
            else
            {
                return null;
            }
        }
        public static string GetCurrentPersonOCode(this HttpRequestMessage request)
        {
            object currentPersonOCode = null;
            if (request.Properties.TryGetValue(CurrentPersonOCodeKey, out currentPersonOCode))
            {
                return (string)currentPersonOCode;
            }
            else
            {
                return null;
            }
        }
        public static string GetCurrentPersonUplineOCode(this HttpRequestMessage request)
        {
            object currentPersonUplineOCode = null;
            if (request.Properties.TryGetValue(CurrentPersonUplineOCodeKey, out currentPersonUplineOCode))
            {
                return (string)currentPersonUplineOCode;
            }
            else
            {
                return null;
            }
        }


        public static Nullable<PersonType> GetCurrentPersonType(this HttpRequestMessage request)
        {
            object currentPersonType = null;
            if (request.Properties.TryGetValue(CurrentPersonTypeKey, out currentPersonType))
            {
                return EnumHelper<PersonType>.Parse(currentPersonType.ToString());
            }
            else
            {
                return null;
            }
        }


        public static Nullable<PersonType> GetCurrentPersonUplineType(this HttpRequestMessage request)
        {
            object currentPersonUplineType = null;
            if (request.Properties.TryGetValue(UplineType, out currentPersonUplineType))
            {
                return (PersonType)currentPersonUplineType;
            }
            else
            {
                return null;
            }
        }

        public static string GetCurrentPersonGroupOCode(this HttpRequestMessage request)
        {
            object currentPersonGroupOCode = null;
            if (request.Properties.TryGetValue(CurrentPersonGroupOCodeKey, out currentPersonGroupOCode))
            {
                return (string)currentPersonGroupOCode;
            }
            else
            {
                return null;
            }
        }
        public static string GetCurrentSubAccountOCode(this HttpRequestMessage request)
        {
            object currentSubAccountOCode = null;
            if (request.Properties.TryGetValue(CurrentSubAccountOCodeKey, out currentSubAccountOCode))
            {
                return (string)currentSubAccountOCode;
            }
            else
            {
                return null;
            }
        }
    }
}
