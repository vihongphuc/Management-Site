﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Services.Common.Logics
{
    using ALT.Data.Entityframework;
    using System.Data.Entity;
    using ALT.Common.Models;

    public static class PersonLogic
    {
        public static bool IsRobot(string token)
        {
            if (string.IsNullOrEmpty(token))
                return true;
            return false;
        }

        public static async Task<bool> CheckUplineStatus(long personID, ALTEntities altEntities)
        {
            var uplineIDs = await altEntities.GetPersonUplines(personID, true);

            var uplines = await altEntities.People.Where(c => uplineIDs.Contains(c.ID))
                                                    .Where(c => c.Status == PersonStatusTypeString.Disabled || c.Status == PersonStatusTypeString.Suspended)
                                                    .ToDictionaryAsync(c => c.ID, c => c.Status);

            return uplines.Count() <= 0;           
        }

        public static async Task<Dictionary<long, string>> GetUplineStatus(long personID, ALTEntities altEntities)
        {
            var uplineIDs = await altEntities.GetPersonUplines(personID, true);

            var dtUplines = await altEntities.People.Where(c => uplineIDs.Contains(c.ID))
                                                    .Where(c => c.Status == PersonStatusTypeString.Disabled || c.Status == PersonStatusTypeString.Suspended)
                                                    .ToDictionaryAsync(c => c.ID, c => c.Status);

            return dtUplines;
        }
    }
}
