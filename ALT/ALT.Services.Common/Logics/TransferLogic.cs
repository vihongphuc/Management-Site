﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Services.Common.Logics
{
    using ALT.Common;
    using ALT.Data.Entityframework;
    public class TransferLogic
    {
        public static bool AddStatement(ALTEntities altContext, string currencyCode, Person dbPerson, Person dbParentPerson,
                                         string personTransferType, string parentTransferType, decimal amount, DateTime time, string actor)
        {
            var success = true;
            try
            {
                var effectedStatement = new Statement
                {
                    ID = KeyGeneration.GenerateInt64Id(),
                    Amount = amount,
                    Type = personTransferType,
                    PersonID = dbPerson.ID,
                    RelatedPersonID = dbParentPerson.ID,
                    Time = time,
                    Ticks = time.Ticks,
                    Detail = $"{actor}: make request {personTransferType} {amount} from {dbParentPerson.Username} to {dbPerson.Username}",
                    CreatedBy = actor,
                    CreatedDate = time,
                    CurrencyCode = currencyCode
                };
                var relatedStatement = new Statement
                {
                    ID = KeyGeneration.GenerateInt64Id(),
                    Amount = -amount,
                    Type = parentTransferType,
                    PersonID = dbParentPerson.ID,
                    RelatedPersonID = dbPerson.ID,
                    Time = time,
                    Ticks = time.Ticks,
                    Detail = $"{actor}: make request {parentTransferType} {amount} from {dbPerson.Username} to {dbParentPerson.Username}; Refer ID: {effectedStatement.ID}",
                    CreatedBy = actor,
                    CreatedDate = time,
                    CurrencyCode = currencyCode
                };

                altContext.Statements.AddRange(new Statement[] { effectedStatement, relatedStatement });
            }
            catch
            {
                success = false;
            }
            return success;
        }
    }
}
