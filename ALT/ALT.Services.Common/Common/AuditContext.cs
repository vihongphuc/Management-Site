﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ALT.Common.Models;

namespace ALT.Services.Common
{
    using ALT.Common;
    using ALT.Data.Entityframework;
    using System.Data.Sql;

    public class AuditContext
    {
        public AuditContext(AuditType auditType, string subject, string ControllerName, string ActionName, HttpRequestMessage request)
            : this(auditType, subject, ControllerName, ActionName)
        {
            var consumerIP = ApiConsumerHelpers.GetConsumerAddress(request);
            if (!String.IsNullOrWhiteSpace(consumerIP))
            {
                this.ConsumerIPAddress = consumerIP;
            }

            var consumerCountry = ApiConsumerHelpers.GetConsumerCountry(request);
            if (!String.IsNullOrWhiteSpace(consumerCountry))
            {
                this.ConsumerIPLocation = consumerCountry;
            }

            var endUserIP = ApiConsumerHelpers.GetEndUserAddress(request);
            if (!String.IsNullOrWhiteSpace(endUserIP))
            {
                this.IPAddress = endUserIP;
            }
            else
            {
                this.IPAddress = consumerIP;
            }

            var endUserCountry = ApiConsumerHelpers.GetEndUserCountry(request);
            if (!String.IsNullOrWhiteSpace(endUserCountry))
            {
                this.IPLocation = endUserCountry;
            }
            else
            {
                this.IPLocation = consumerCountry;
            }
        }
        public AuditContext(AuditType auditType, string subject, string ControllerName, string ActionName)
        {
            this.ActionTime = DateTime.Now;
            this.Type = auditType;
            this.Subject = subject;
            this.ControllerName = ControllerName;
            this.ActionName = ActionName;
        }

        public static string Serialize<T>(T value)
        {
            return JsonConvert.SerializeObject(value, Formatting.Indented);
        }

        public AuditType Type { get; set; }
        public string Subject { get; set; }
        public long? PersonID { get; set; }
        public string Token { get; set; }
        public long? RelatedID { get; set; }
        public string IPAddress { get; set; }
        public string IPLocation { get; set; }
        public string ConsumerIPAddress { get; set; }
        public string ConsumerIPLocation { get; set; }
        public DateTime? ActionTime { get; set; }
        public string Detail { get; set; }
        public string Before { get; set; }
        public string After { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public DateTime CreatedTime { get; set; }
        public string CreatedBy { get; set; }
        public async Task InsertAudit(ALTEntities altContext)
        {
            if (Inserted)
                return;

            altContext.Audits.Add(new Audit
            {
                ID = KeyGeneration.GenerateInt64Id(),
                PersonID = PersonID ?? 0L,
                Token = Token,
                RelatedID = RelatedID,
                Type = Type.ToString(),
                Subject=Subject,
                Time = ActionTime,
                Details = Detail,
                IPAddress = IPAddress,
                IPLocation = IPLocation,
                ConsumerIPAddress = ConsumerIPAddress,
                ConsumerIPLocation = IPLocation,
                Before = Before,
                After = After,
                ControllerName = ControllerName,
                ActionName = ActionName,
                CreatedBy = CreatedBy,
                CreatedDate = CreatedTime
            });

            Inserted = true;

            // retrieve location
            await Task.FromResult(0);
        }

        protected bool Inserted { get; private set; }
    }

    public class RequiredAuditContext : AuditContext, IDisposable
    {
        public RequiredAuditContext(ALTEntities sbsContext, AuditType auditType, string subject, string ControllerName, string ActionName, HttpRequestMessage request)
            : base(auditType, subject, ControllerName, ActionName, request)
        {
            this.sbsContext = sbsContext;
        }
        public RequiredAuditContext(ALTEntities sbsContext, AuditType auditType, string subject, string ControllerName, string ActionName)
            : base(auditType, subject, ControllerName, ActionName)
        {
            this.sbsContext = sbsContext;
        }

        public void Done()
        {
            this.auditWritten = true;
        }

        public async Task WriteAudit()
        {
            if (auditWritten)
                return;

            auditWritten = true;

            await base.InsertAudit(this.sbsContext)
                      .ConfigureAwait(false);
            await this.sbsContext.SaveChangesAsync()
                      .ConfigureAwait(false);

            if (this.sbsContext.Database.CurrentTransaction != null)
            {
                this.sbsContext.Database.CurrentTransaction.Commit();
            }
        }

        public void Dispose()
        {
            if (auditWritten)
                return;

            WriteAudit().Wait();
        }

        private bool auditWritten;
        private readonly ALTEntities sbsContext;
    }
}
