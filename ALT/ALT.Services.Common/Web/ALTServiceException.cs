﻿using ALT.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;

namespace Sbs.Exceptions
{
    public class ALTServiceException : Exception
    {
        public ALTServiceException(HttpResponseMessage response, HttpError httpError, string message)
            : base(message ?? FormatResponse(response))
        {
            this.Response = response;
            this.HttpError = httpError;
        }

        public static string FormatResponse(HttpResponseMessage response)
        {
            if (response == null)
                return String.Empty;
            return String.Format($"URL: {response.RequestMessage.RequestUri.ToString()} - {response.StatusCode}");
        }

        public HttpResponseMessage Response { get; private set; }
        public HttpError HttpError { get; private set; }

        public void SetModelError(ModelStateDictionary modelState)
        {
            if (Response != null)
            {
                IEnumerable<string> values;
                if (Response.Headers.TryGetValues(ALT.Common.Statics.ErrorMessageHeaderKey, out values))
                {
                    foreach (var err in values.SelectMany(v => StringHelper.ToArray(v)))
                    {
                        modelState.AddModelError(String.Empty, err);
                    }
                }
                else
                {
                    modelState.AddModelError(String.Empty, Message);
                }
            }
        }
    }
}
