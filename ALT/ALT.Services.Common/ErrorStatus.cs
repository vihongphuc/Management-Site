﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Filters;

namespace ALT.Services.Common
{

    [DebuggerStepThrough]
    public static class ErrorStatuses
    {
        public static void ThrowBadRequest(bool when, Action on = null)
        {
            if (when)
            {
                if (on != null) on();
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }
        public static void ThrowBadRequest(HttpRequestMessage request, string message, bool when, Action on = null)
        {
            if (when)
            {
                if (on != null) on();
                throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
        }
        public static void ThrowBadRequest()
        {
            throw new HttpResponseException(HttpStatusCode.BadRequest);
        }
        public static void ThrowBadRequest(HttpRequestMessage request, string message)
        {
            throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
        }

        public static void ThrowUnauthorized(bool when, Action on = null)
        {
            if (when)
            {
                if (on != null) on();
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
        }
        public static void ThrowUnauthorized(HttpRequestMessage request, string message, bool when, Action on = null)
        {
            if (when)
            {
                if (on != null) on();
                throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.Unauthorized, message));
            }
        }
        public static void ThrowUnauthorized()
        {
            throw new HttpResponseException(HttpStatusCode.Unauthorized);
        }
        public static void ThrowUnauthorized(HttpRequestMessage request, string message)
        {
            throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.Unauthorized, message));
        }

        public static void ThrowNotFound(bool when, Action on = null)
        {
            if (when)
            {
                if (on != null) on();
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
        public static void ThrowNotFound(HttpRequestMessage request, string message, bool when, Action on = null)
        {
            if (when)
            {
                if (on != null) on();
                throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.NotFound, message));
            }
        }
        public static void ThrowNotFound()
        {
            throw new HttpResponseException(HttpStatusCode.NotFound);
        }
        public static void ThrowNotFound(HttpRequestMessage request, string message)
        {
            throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.NotFound, message));
        }

        public static void ThrowForbidden(bool when, Action on = null)
        {
            if (when)
            {
                if (on != null) on();
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }
        }
        public static void ThrowForbidden(HttpRequestMessage request, string message, bool when, Action on = null)
        {
            if (when)
            {
                if (on != null) on();
                throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.Forbidden, message));
            }
        }
        public static void ThrowForbidden()
        {
            throw new HttpResponseException(HttpStatusCode.Forbidden);
        }
        public static void ThrowForbidden(HttpRequestMessage request, string message)
        {
            throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.Forbidden, message));
        }

        public static void ThrowServerError(bool when, Action on = null)
        {
            if (when)
            {
                if (on != null) on();
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }
        public static void ThrowServerError(HttpRequestMessage request, string message, bool when, Action on = null)
        {
            if (when)
            {
                if (on != null) on();
                throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.InternalServerError, message));
            }
        }
        public static void ThrowServerError()
        {
            throw new HttpResponseException(HttpStatusCode.InternalServerError);
        }
        public static void ThrowServerError(HttpRequestMessage request, string message)
        {
            throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.InternalServerError, message));
        }
    }
}
