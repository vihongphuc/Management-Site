﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ALT.Web.GamePlayer.Infrastructer
{
    public static class HTMLHelpers
    {
        public static string Label(string target, string text)
        {
            return string.Format("<label for='{0}' >{1}</label>", target, text);
        }
    }
}