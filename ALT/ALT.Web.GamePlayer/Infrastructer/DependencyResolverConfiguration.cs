﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Web.GamePlayer.Infrastructer
{
    using Microsoft.Practices.Unity;
    using System.Web.Mvc;
    public class DependencyResolverConfiguration : IDependencyResolver
    {
        private IUnityContainer containner;
        public DependencyResolverConfiguration(IUnityContainer _container)
        {
            this.containner = _container;
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return this.containner.Resolve(serviceType);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviveType)
        {
            try
            {
                return this.containner.ResolveAll(serviveType);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
