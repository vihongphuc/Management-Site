﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ALT.Web.GamePlayer.Models
{
    public class GameCommonViewModel
    {
        public GameAccessViewModel GameAccess { get; set; }
        public GameSettingViewModel GameSetting { get; set; }
    }
}