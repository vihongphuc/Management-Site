﻿using ALT.Common.Helpers;
using ALT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ALT.Web.GamePlayer.Models
{
    public class GameHistoryViewModel
    {
        public string Username { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int CurrentPage { get; set; }
        public int TotalRecords { get; set; }
        
        public string GameCode { get; set; }
        public PagedList<GameHistoryResp> GameHistory { get; set; }
    }
}