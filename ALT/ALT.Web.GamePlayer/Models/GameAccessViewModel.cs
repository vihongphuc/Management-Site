﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ALT.Web.GamePlayer.Models
{
    public class GameAccessViewModel
    {
        public string SessionCode { get; set; }
        public string Username { get; set; }
        public decimal PointRate { get; set; }
        public long Points { get; set; }
        public string TokenMobile { get; set; }
        public string Secret { get; set; }
        public string IPHashKey { get; set; }
    }
}