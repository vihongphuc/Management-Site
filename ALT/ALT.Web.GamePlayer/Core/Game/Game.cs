﻿using ALT.Common.Helpers;
using ALT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace ALT.Web.GamePlayer.Core
{
    public class Game : IGame, IStore
    {
        private readonly HttpClient httpClient;
        public Game(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }
        public async Task<IList<GameResp>> GetAllGame(string gamecode, bool includedSetting, string publisher, string platforms)
        {
            var response = await httpClient.GetAsync($"api/game-data?gamecode={gamecode}&includedSetting={includedSetting}&publisher={publisher}&platforms={platforms}")
                                           .ConfigureAwait(continueOnCapturedContext: false);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsAsync<IList<GameResp>>();
                return result;
            }
            return null;
        }
        public async Task<IList<GameSettingResp>> GameSetting(string gamecode, string publisher, string platforms)
        {
            var response = await httpClient.GetAsync($"api/game-data/game/setting?gamecode={gamecode}&publisher={publisher}&platforms={platforms}")
                                           .ConfigureAwait(continueOnCapturedContext: false);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsAsync<IList<GameSettingResp>>();
                return result;
            }
            return null;
        }
        public async Task<decimal> GetBalanceOfMemberByOcode(string sessionOcode)
        {
            decimal result;
            var resp = await this.httpClient.GetAsync($"api/game-access/get-member-balances?token={sessionOcode}")
                                 .ConfigureAwait(continueOnCapturedContext: false);

            result = await resp.Content.ReadAsAsync<decimal>();
            return result;
        }

        public async Task<PagedList<GameHistoryResp>> GameTransactionHistory(string Username, DateTime StartDate, DateTime EndDate,
                                                                                string GameOCode, int PageIndex, int TotalItemPerPages, int ItemPerPage)
        {
            var response = await httpClient.GetAsync($"api/game-history?Username={Username}&StartDate={StartDate.ToString(Common.Statics.DateTimeShortFormat)}&EndDate={EndDate.ToString(Common.Statics.DateTimeShortFormat)}&GameOCode={GameOCode}&PageIndex={PageIndex}&ItemPerPage={TotalItemPerPages}&TotalItems={ItemPerPage}")
                                           .ConfigureAwait(continueOnCapturedContext: false);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsAsync<PagedList<GameHistoryResp>>();
                return result;
            }
            return null;
        }
    }
}