﻿using ALT.Common.Helpers;
using ALT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Web.GamePlayer.Core
{
    public interface IGame
    {
        Task<IList<GameResp>> GetAllGame(string gamecode, bool includedSetting, string publisher, string platforms);
        Task<IList<GameSettingResp>> GameSetting(string gamecode, string publisher, string platforms);
        Task<decimal> GetBalanceOfMemberByOcode(string sessionOcode);

        Task<PagedList<GameHistoryResp>> GameTransactionHistory(string Username, DateTime StartDate, DateTime EndDate,
                                                                                string GameOCode, int PageIndex,int TotalItemPerPages, int ItemPerPage);
    }
}
