﻿using ALT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Web.GamePlayer.Core
{
    public interface IAuthentication
    {
        Task<GameAccessResp> SignIn(GameAsscessReq authenticationRequest);
        Task<bool> Logout(string token);
    }
}
