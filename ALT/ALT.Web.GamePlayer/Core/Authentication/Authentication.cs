﻿using ALT.Common.Models;
using ALT.Common.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace ALT.Web.GamePlayer.Core
{
    public class Authentication : IAuthentication, IStore
    {
        private readonly HttpClient httpClient;
        public Authentication(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<GameAccessResp> SignIn(GameAsscessReq authenticationReq)
        {
            var response = await httpClient.PostAsJsonAsync<GameAsscessReq>($"api/game-access/sign-in", authenticationReq)
                                            .ThrowOnErrors()
                                           .ConfigureAwait(continueOnCapturedContext: false);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsAsync<GameAccessResp>();
                return result;
            }
            else
            {
                string responseBody = await response.Content.ReadAsStringAsync();
                throw new HttpException((int)response.StatusCode, responseBody);

            }
        }

        public async Task<bool> Logout(string token)
        {
            var tokencontent = new StringContent(token);
            var response = await httpClient.PostAsync($"api/game-access/sign-out/{token}?webSignOut=true", null)
                                           .ConfigureAwait(continueOnCapturedContext: false);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsAsync<bool>();
                return result;
            }
            return false;
        }
    }
}