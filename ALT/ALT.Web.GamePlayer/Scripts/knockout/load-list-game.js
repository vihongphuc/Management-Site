﻿var tokenCurrent = $('#tokenCurrent').val();

var windows;
var window_push_all = new Array();

var GetAllGame = function (data) {
    var self = this;
    self.OCode = ko.observable(data.OCode);
    self.Code = ko.observable(data.Code);
    self.Name = ko.observable(data.Name);
    self.GroupOCode = ko.observable(data.GroupOCode);
    self.GroupName = ko.observable(data.GroupName);
    self.Type = ko.observable(data.Type);
    self.Enable = ko.observable(data.Enable);
    self.imagePath = ko.observable(pathImages + data.Code + ".jpg");
    self.isItemGameLoading = ko.observable(false);

    self.Settings = mapListGameSettings(data.Settings[0]);

    if (data.Settings[0].Enable == false) {
        self.isComingsoon = ko.observable(false);
    }
    else {
        self.isComingsoon = ko.observable(true);
    }
    return self;
}
var GameSettings = function (_dataGameSetting) {
    var self = this;
    if (_dataGameSetting.Enable == false) {
        self.IsNew = ko.observable(false);
        self.IsHot = ko.observable(false);
        self.Enable = ko.observable(_dataGameSetting.Enable);
    }
    else {
        self.IsNew = ko.observable(_dataGameSetting.IsNew);
        self.IsHot = ko.observable(_dataGameSetting.IsHot);
        self.Enable = ko.observable(_dataGameSetting.Enable);
    }
    return self;
};

var pageIndex = 1;

function mapListGameSettings(_dataGameSetting) {
    var result = ko.observableArray();
    var _getGameSettings = new GameSettings(_dataGameSetting);
    result.push(_getGameSettings);

    return result;
}


function GetAllGameViewModel() {
    var self = this;
    self.getAllGame = ko.observableArray();
    self.imagePath = ko.observable(null);
    self.isItemGameLoading = ko.observable(true);
    self.isLoading = ko.observable(true);
    self.isComingsoon = ko.observable(false);

    GetGame();
    function GetGame() {
        $.getJSON(get_all_game, function (data) {
            self.getAllGame.removeAll();
            ko.utils.arrayMap(data, function (i) {
                var _getAllGame = new GetAllGame(i);
                self.getAllGame.push(_getAllGame);
            });
            self.isLoading(false);
        });
    }

    self.playgame = function (data) {
        windows = window.open('Game?gamecode=' + data.Code() + '', "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=1500, height=1300");
        window_push_all[data.Code()] = windows;
        $('#' + data.Code()).prop("disabled", true);
    }

    $('.btn-signout').click(function () {
        try {
            for (var key in window_push_all) {
                window_push_all[key].close();
            }
        } catch (err) {
        }
        return true;
    });
}

var viewModel = new GetAllGameViewModel();

ko.applyBindings(viewModel, document.getElementById("get-all-game"));

var myBroadcastHub = $.connection.gameHub;

//submit exits game delete Windows.
$('.btn-signout').click(function () {
    try {
        for (var key in window_push_all) {
            window_push_all[key].close();
        }
    } catch (err) {
    }
    return true;
});

$.connection.hub.start().done(function () {
    console.log('hub started.');
}).fail(function (error) {
    console.error(error);
});

myBroadcastHub.client.pointUpdated = function (token, add, popup) {
    if (token == tokenCurrent) {
        WebUpdatePoint(add);

        if (popup == true) {
            var pointRate = $("#point-rate").val();
            var str = 'Your are ';
            str += add > 0 ? 'deposited ' : 'withdrawn ';
            str += Math.abs(add * (numeral(pointRate).value() <= 0 ? 1 : numeral(pointRate).value()))
            str += '.';
            alert(str);
        }
    }
};

window.onunload = function () {
    for (var key in window_push_all) {
        window_push_all[key].close();
    }
}

myBroadcastHub.client.signout = function (token) {
    if (token == tokenCurrent) {
        alert("Your account has been logged in defference place.");
        try {
            for (var key in window_push_all) {
                window_push_all[key].close();
            }
        } catch (err) {

        }
        window.location.href = window.logouturl;
    }
};

myBroadcastHub.client.changestatus = function (token) {
    if (token == tokenCurrent) {
        alert("Your account has been Suspended and Disabled.");
        try {
            for (var key in window_push_all) {
                window_push_all[key].close();
            }
        } catch (err) {
        }
        window.location.href = window.logouturl;
    }
};

if (typeof (tokenCurrent) == 'undefined') {
    var data = sessionStorage.getItem('myArray');
    try {
        //no close popup when session finshed times.
    } catch (err) {

    }
}

function OnDisconnetedForWeb(IsDisconneted, gamecode) {
    if (IsDisconneted == true) {
        for (var key in window_push_all) {
            if (window_push_all[gamecode] != null && window_push_all[gamecode] != undefined && key == gamecode) {
                window_push_all[gamecode].close();
                window_push_all.indexOf(gamecode);
                $('#' + gamecode).prop("disabled", false);
            }
        }
    }
}

function ReceiveUpdatePointForWeb(point, gamecode) {
    var pointRate = $("#point-rate").val();
    var balance = parseFloat(point) * parseFloat(pointRate);
    $("#balance").html(numeral(balance).format('0,0.0000'));

    for (var key in window_push_all) {
        if (key != gamecode && window_push_all[gamecode] != null && window_push_all[gamecode] != undefined) {
            window_push_all[key].HandleUpdateCurrentPoint(point);
        }
    }
}

function WebUpdatePoint(addedPoint) {
    var pointRate = $("#point-rate").val();
    var balance = numeral(addedPoint).value() * numeral(pointRate).value();
    var balanceOld = numeral($("#balance").html()).value();
    var balanceNew = balanceOld + balance;
    $("#balance").html(numeral(balanceNew).format('0,0.0000'));

    try {
        for (var key in window_push_all) {
            window_push_all[key].UpdatePoints(addedPoint);
        }
    } catch (err) {

    }
}