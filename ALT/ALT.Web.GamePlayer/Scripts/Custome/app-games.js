﻿(function () {
    var windows;
    var window_push_all = [];

    var tokenCurrent = $('#tokenCurrent').val();

    $('.btn-signout').click(function () {
        try {
            for (var i = 0; i < window_push_all.length; i++) {
                window_push_all[i].close()
            }
        } catch (err) {
            console.log(err);
        }
        return true;
    });

    $('.play-game').click(function () {
        var code = $(this).data('game-code');
        windows = window.open('Game?gamecode=' + code + '', "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=1500, height=1300");
        window_push_all.push(windows);      
        sessionStorage.getItem('myArray', window_push_all);      
    });

    var myChatHub = $.connection.gameHub;
    myChatHub.client.pointUpdated = function (token, add, popup) {

        console.log(token + ';' + add + ';' + popup);

        // check token == current token
        if (token == tokenCurrent) {
            WebUpdatePoint(add);

            if (popup == true) {
                var pointRate = $("#point-rate").val();
                var str = 'Your are ';
                str += add > 0 ? 'deposited ' : 'withdrawn ';
                str += Math.abs(add * (numeral(pointRate).value() <= 0 ? 1 : numeral(pointRate).value()))
                str += '.';
                alert(str);
            }
        }
    };

    if (tokenCurrent == undefined) {
        var data = sessionStorage.getItem('myArray');
        console.log(data);
        try {
            //no close popup when session finshed times.
        } catch (err) {
            console.log(err);
        }
    }

    window.onunload = function () {
        for (var i = 0; i < window_push_all.length; i++) {
            window_push_all[i].close()
        }
    }

    myChatHub.client.signout = function (token) {
        if (token == tokenCurrent) {
            alert("Your account has been logged in defference place.");
            try {
                for (var i = 0; i < window_push_all.length; i++) {
                    window_push_all[i].close()
                }
            } catch (err) {
                console.log(err);
            }
            window.location.href = '@Url.Action("Logout", "Account")';
        }
    };

    myChatHub.client.changestatus = function (token) {
        if (token == tokenCurrent) {
            alert("Your account has been Suspended and Disabled.");
            try {
                console.log(windows);
                for (var i = 0; i < window_push_all.length; i++) {
                    window_push_all[i].close()
                }
            } catch (err) {
                console.log(err);
            }
            window.location.href = '@Url.Action("Logout", "Account")';
        }
    };

    function OnDisconnetedForWeb(IsDisconneted) {
        if (IsDisconneted == true) {
            window.close();
            //window.location.href = '@Url.Action("Logout","Account")';
            //alert("Your account has been loged in defference place");
        }
    }

    function ReceiveUpdatePointForWeb(point) {
        var pointRate = $("#point-rate").val();
        var balance = parseFloat(point) * parseFloat(pointRate);
        $("#balance").html(numeral(balance).format('0,0.0000'));

        for (var i = 0; i < window_push_all.length; i++) {
            if (window_push_all[i].GameCode != windows.GameCode) {

                console.log(window_push_all[i].GameCode);
                window_push_all[i].HandleUpdateCurrentPoint(point);
            }
        }

    }

    $.connection.hub.start().done(function () {
        console.log('hub started.');
    }).fail(function (error) {
        console.error(error);
    });

    function WebUpdatePoint(addedPoint) {        
        var pointRate = $("#point-rate").val();
        var balance = numeral(addedPoint).value() * numeral(pointRate).value();
        var balanceOld = numeral($("#balance").html()).value();
        var balanceNew = balanceOld + balance;
        $("#balance").html(numeral(balanceNew).format('0,0.0000'));

        try {
            windows.UpdatePoints(addedPoint);
        } catch (err) {

        }
    }
}());
