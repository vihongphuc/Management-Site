﻿$(document).ready(function () {
    var ocode = $("#ocode").val();
    getBalance(ocode);
    function getBalance(ocode) {
        $.getJSON(get_balance_url, { sessionOcode: ocode }, function (balance) {
            $("#balance").html(numeral(balance).format('0,0.0000'));
        })
    }
    setInterval(getBalance(ocode), 30000);
});