﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[assembly: System.Web.PreApplicationStartMethod(typeof(ALT.Web.GamePlayer.IocConfiguration), "LoadingConfig")]
namespace ALT.Web.GamePlayer
{
    using Microsoft.Practices.Unity;
    using System.Web.Mvc;
    using Infrastructer;
    using System.IO;
    using System.Reflection;
    using ALT.Common.Configuration;
    using Core;
    using System.Net.Http;
    using Common;
    using Unity.Mvc5;

    public static class IocConfiguration
    {
        public static void ConfigurationIocContainer()
        {
            IUnityContainer container = new UnityContainer();
            RegisterServices(container);
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        public static void RegisterServices(IUnityContainer container)
        {
            var httpClient = InitilizeHttpClient();
            container.RegisterType<IAuthentication, Authentication>(
                new InjectionConstructor(httpClient)
                );
            container.RegisterType<IGame, Game>(
                new InjectionConstructor(httpClient)
                );
        }

        public static void LoadingConfig()
        {
            InitConfigurations();
        }

        private static void InitConfigurations()
        {
            var configPath = System.Web.Hosting.HostingEnvironment.MapPath(DefaultConfigPath);
            var configFile = Path.Combine(configPath, DefaultConfigFileName);
            if (File.Exists(configFile))
            {
                fileConfigStore = new FileConfigurationStore(configFile);
                ConfigurationManager.ApplyConfiguration(fileConfigStore);
            }
        }

        private static HttpClient InitilizeHttpClient()
        {
            var defaultTimeOut = 30;
            var baseURL = ConfigurationManager.Get<string>(ServiceConfigurations.Group, ServiceConfigurations.BaseUrl, string.Empty);
            var timeOut = ConfigurationManager.Get<int>(ServiceConfigurations.Group, ServiceConfigurations.TimeOut, defaultTimeOut);
            if (string.IsNullOrEmpty(baseURL))
            {
                throw new Exception("Must be set BaseURL.");
            }
            var httpClient = new HttpClient()
            {
                BaseAddress = new Uri(baseURL),
                Timeout = new TimeSpan(0, timeOut, 0)
            };

            return httpClient;
        }


        private static FileConfigurationStore fileConfigStore;

        private static FileSystemWatcher fswDynamicConfig;
        private static FileConfigurationStore fileDynamicConfigStore;

        private static object syncConfigLock = new object();
        private static bool updatingConfiguration;
        private static DateTime lastUpdate;


        private const string DefaultConfigPath = "~/";
        private const string DefaultConfigFileName = "configs.ini";
        private const string DefaultDynamicConfigFileName = "dynamic-configs.ini";
        private const int DefaultIDGeneratorScope = 1;
        private const int DefaultHandleConfigurationUpdatedDelay = 1000;
    }
}