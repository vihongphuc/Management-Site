﻿using ALT.Common.Logging;
using System.Web;
using System.Web.Mvc;

namespace ALT.Web.GamePlayer
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new WebLogGlobalFilterAttribute() { CanTraceResponse = WebLoggers.GetMvcLogAll() });
            filters.Add(new HandleErrorAttribute());
        }
    }
}
