﻿using System.Web;
using System.Web.Optimization;

namespace ALT.Web.GamePlayer
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                        "~/Scripts/appgames.js",
                        "~/Scripts/jquery-2.2.3.min.js",
                        "~/Scripts/numeral/numeral.js",
                        "~/Scripts/toastr.js",
                        "~/Scripts/toastr.min.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/moment.js",
                        "~/Scripts/bootstrap-datetimepicker.js",
                        "~/Scripts/Custome/mainScript.js",
                        "~/Scripts/knockout-3.4.0.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                                "~/Scripts/jquery-{version}.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/get-all-game").Include(
                                         "~/Scripts/Custome/get-balance.js",
                                         "~/Scripts/knockout/load-list-game.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/show-play-game").Include(
                                        "~/Scripts/jquery-2.2.3.min.js",
                                        "~/Scripts/tweenjs-0.6.1.min.js"
                                         ));

            bundles.Add(new ScriptBundle("~/bundles/signalR").Include(
                                     "~/Scripts/jquery.signalR-2.2.1.min.js"
                                      ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));
            bundles.Add(new StyleBundle("~/Content/css").Include(
                                       "~/Content/ionicons.min.css",
                                       "~/Content/font-awesome.min.css",
                                       "~/Content/bootstrap.css",
                                       "~/Content/game/style.min.css",
                                       "~/Content/bootstrap.min.css",
                                       "~/Content/toastr.css",
                                       "~/Content/bootstrap-theme.min.css",
                                       "~/Content/bootstrap-datetimepicker.css",
                                       "~/Content/game/loading-game.min.css",
                                       "~/Content/game/theme-load-game.min.css"
                     ));

            bundles.Add(new StyleBundle("~/Content/show-play-game").Include(
                                       "~/Content/game/loading-game.css"
                     ));
        }
    }
}
