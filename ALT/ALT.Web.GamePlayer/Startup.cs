﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ALT.Web.GamePlayer.Startup))]
namespace ALT.Web.GamePlayer
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
