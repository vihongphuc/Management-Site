﻿using ALT.Common.Models;
using ALT.Web.GamePlayer.Core;
using ALT.Web.GamePlayer.Infrastructer;
using ALT.Web.GamePlayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace ALT.Web.GamePlayer.Controllers
{
    public class HomeController : BaseController
    {
        IList<GameResp> getAllGame;
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        [AjaxOnlyAttribute]
        public async Task<ActionResult> GetAllGamne()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string gamecode = "";
                getAllGame = await this.game.GetAllGame(gamecode, IncludedSetting, Publisher, Platforms);

                if (getAllGame == null)
                {
                    return RedirectToAction("Login", "Account");
                }
            }

            return Json(getAllGame, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Game(string gamecode)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                GameCommonViewModel gameCommonViewModel = new GameCommonViewModel();
                IList<GameSettingResp> getGameSetting = await this.game.GameSetting(gamecode, Publisher, Platforms);
                GameAccessViewModel accessGame = new GameAccessViewModel();

                gameCommonViewModel.GameSetting = getGameSetting.Select(c => new GameSettingViewModel
                {
                    GameCode = c.GameCode,
                    Publisher = c.Publisher,
                    Platform = c.Platform,
                    DowloadURL = c.DowloadURL,
                    GameHash = c.GameHash,
                    GameKey = c.GameKey,
                    GameHeader = c.GameHeader,
                    Enable = c.Enable,
                    IsHot = c.IsHot,
                    IsNew = c.IsNew,
                    Rate = c.Rate,
                    Type = c.Type,
                    Width = c.Width,
                    Domain = c.Domain,
                    Height = c.Height,
                    Port = c.Port
                }).FirstOrDefault();

                accessGame.Username = AuthManager.AuthIdentity.Username;
                accessGame.Points = AuthManager.AuthIdentity.Points;
                accessGame.IPHashKey = AuthManager.AuthIdentity.IPHashKey;
                accessGame.PointRate = AuthManager.AuthIdentity.PointRate;
                accessGame.Secret = AuthManager.AuthIdentity.Secret;
                accessGame.SessionCode = AuthManager.AuthIdentity.SessionCode;
                accessGame.TokenMobile = AuthManager.AuthIdentity.TokenMobile;

                gameCommonViewModel.GameAccess = accessGame;

                return View(gameCommonViewModel);
            }
        }

        [HttpGet]
        [AjaxOnlyAttribute]
        public async Task<JsonResult> GetBalanceByOcode(string sessionOcode)
        {
            var result = await this.game.GetBalanceOfMemberByOcode(sessionOcode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public HomeController(IGame game)
        {
            this.game = game;
        }

        private readonly IGame game;
        private static readonly string Publisher = "ALT";
        private static readonly string Platforms = "Web";
        private static readonly bool IncludedSetting = true;
    }
}