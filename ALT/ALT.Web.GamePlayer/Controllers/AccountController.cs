﻿using ALT.Common.Models;
using ALT.Web.GamePlayer.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ALT.Web.GamePlayer.Controllers
{
    using ALT.Common.Web;
        
    public class AccountController : BaseController
    {
        // GET: Login  
        public ActionResult Login(string returnUrl)
        {

            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        [HttpGet]
        public async Task<ActionResult> Logout(string token)
        {
            bool isLogout = true;
            if (!string.IsNullOrEmpty(token))
            {
                isLogout = await this.authentication.Logout(token);

            }
            if (isLogout)
            {
                AuthManager.SignOff();
                return RedirectToAction("Login", "Account");
            }
            AuthManager.SignOff();
            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public async Task<ActionResult> Login(string username, string password)
        {
            GameAsscessReq loginViewModel = new GameAsscessReq();

            loginViewModel.Username = username;
            loginViewModel.Password = password;
            loginViewModel.NotExpired = true;
            loginViewModel.Platform = "Web";

            loginViewModel.IP = this.Request.ServerVariables["REMOTE_ADDR"];
            loginViewModel.IPHashKey = this.Request.UserHostAddress;
            loginViewModel.Data = "Web";
            loginViewModel.Publisher = "ALT";
            loginViewModel.TokenMobile = "";

            try
            {
                GameAccessResp authenResp = await this.authentication.SignIn(loginViewModel);
                if (authenResp == null)
                {

                    ViewBag.Error = "Please Check Username and password";
                    return View();
                }
                AuthManager.SetAuth(authenResp);
                return RedirectToAction("Index", "Home");
            }
            catch (Exceptions.ALTServiceException ex)
            {   
                ModelState.AddModelError("ErrorMessageID", ex.Message);
            }

            return View();
        }

        public AccountController(IAuthentication authentication)
        {
            this.authentication = authentication;
        }

        private readonly IAuthentication authentication;
    }
}