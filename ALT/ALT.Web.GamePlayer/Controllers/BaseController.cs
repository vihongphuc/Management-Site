﻿using ALT.Web.GamePlayer.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ALT.Web.GamePlayer.Controllers
{
    public class BaseController : Controller
    {

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            string actionName = filterContext.ActionDescriptor.ActionName;

            if (!ExceptControllers.Contains(controllerName.ToUpper()) && !ExceptActionNames.Contains(actionName.ToUpper()))
            {
                if (Core.AuthManager.AuthIdentity == null || string.IsNullOrEmpty(Core.AuthManager.AuthIdentity.SessionCode))
                {
                    Core.AuthManager.SignOff();
                    filterContext.Result = new RedirectToRouteResult(
                    new System.Web.Routing.RouteValueDictionary
                    {{"controller", "Account"}, {"action", "Login"}});
                    return;
                }
            }
            base.OnActionExecuting(filterContext);
        }


        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }


        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
        }

        private string[] ExceptControllers = new string[] { "ACCOUNT" };
        private string[] ExceptActionNames = new string[] { "LOGIN" };
    }
}