﻿using ALT.Web.GamePlayer.Core;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ALT.Web.GamePlayer.Controllers
{
    public class BroadcastController : Controller
    {
        [HttpPost]
        public void PointUpdated(string token, long add = 0, bool popup = false)
        {
            //if (false == IsAllowed())
            //    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            //var result = await this.gameStore.GetGameAccessInfo(token); //***********note*********
            GameHub.PointUpdated(token, add, popup);
            //return new EmptyResult();
        }
        [HttpPost]
        public void ChangeStatus(string token, bool popup = false)
        {
            GameHub.ChangeStatus(token);
        }

        [HttpPost]
        public void SignOut(string token)
        {
            GameHub.SignOut(token);
        }

        private bool IsAllowed()
        {
            var token = this.Request.QueryString[RobotTokenQueryString];
            if (String.IsNullOrWhiteSpace(token))
                return false;

            //return allowedRobots.Contains(token);
            return true;
        }

        private const string RobotTokenQueryString = "t";
    }
}