﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ALT.Web.GamePlayer.Controllers
{
    using Core;
    using ALT.Web.GamePlayer.Models;
    using System.Threading.Tasks;
    using PagedList;

    public class GameHistoryController : BaseController
    {
        // GET: Game
        public async Task<ActionResult> Index(int? InstructorPage, DateTime? startDate, DateTime? endDate, int? totalRecords, string gamecode = null)
        {
            var pageNumber = InstructorPage ?? 1;
            DateTime start = startDate == null ? DateTime.Now : (DateTime)startDate;
            DateTime end = endDate == null ? DateTime.Now : (DateTime)endDate;
            totalRecords = totalRecords == null ? 50 : (int)totalRecords;
            var gameHistories = new GameHistoryViewModel()
            {
                Username = ALT.Web.GamePlayer.Core.AuthManager.AuthIdentity.Username,
                StartDate = start,
                EndDate = end,
                TotalRecords = (int)totalRecords,
                CurrentPage = 1,
                GameCode = gamecode,
                GameHistory = new Common.Helpers.PagedList<Common.Models.GameHistoryResp> { }
            };

            gameHistories.GameHistory = await this.game.GameTransactionHistory(gameHistories.Username, gameHistories.StartDate, gameHistories.EndDate, gameHistories.GameCode, pageNumber, TotalItemPerPages, gameHistories.TotalRecords);

            ViewBag.TotalPage = gameHistories.GameHistory != null ? gameHistories.GameHistory.TotalPages : 0;

            var getGames = await this.game.GetAllGame(null, IncludedSetting, Publisher, Platforms);
            ViewBag.GetAllGames = getGames;
            ViewBag.TotalItemPerPages = TotalItemPerPages;

            return View(gameHistories);
        }

        [HttpPost]
        public async Task<ActionResult> Index(int? InstructorPage, GameHistoryViewModel request, string gamecode = null)
        {
            var getGames = await this.game.GetAllGame(null, IncludedSetting, Publisher, Platforms);
            ViewBag.GetAllGames = getGames;
            request.GameHistory = await this.game.GameTransactionHistory(request.Username, request.StartDate, request.EndDate, request.GameCode, 1, TotalItemPerPages, request.TotalRecords);

            ViewBag.TotalPage = request.GameHistory != null ? request.GameHistory.TotalPages : 0;
            ViewBag.TotalItemPerPages = TotalItemPerPages;

            return View(request);
        }

        public GameHistoryController(IGame game)
        {
            this.game = game;
        }
        private readonly IGame game;

        public readonly static int TotalItemPerPages = 50;
        private static readonly string Publisher = "ALT";
        private static readonly string Platforms = "Web";
        private static readonly bool IncludedSetting = true;

    }
}