﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace ALT.Web.GamePlayer
{
    public class GameHub : Hub
    {
        public Task SetGroup(string type)
        {
            return Groups.Add(Context.ConnectionId, type);
        }

        public override Task OnConnected()
        {
            string token = Context.QueryString["token"];
            if (!String.IsNullOrWhiteSpace(token))
            {
                tokens.AddOrUpdate(token,
                    key => new ConcurrentBag<string>(new[] { Context.ConnectionId }),
                    (key, bag) =>
                    {
                        bag.Add(Context.ConnectionId);
                        return bag;
                    });

                connections.AddOrUpdate(Context.ConnectionId, token, (k, v) => token);
            }

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            string token;
            if (connections.TryRemove(Context.ConnectionId, out token))
            {
                ConcurrentBag<string> conns;
                tokens.TryRemove(token, out conns);
            }

            return base.OnDisconnected(stopCalled);
        }

        public static void PointUpdated(string token, decimal add, bool popup)
        {
            var hub = GlobalHost.ConnectionManager.GetHubContext<GameHub>();
            //hub.Clients.Group(SystemType).pointUpdated(token, add, popup);

            hub.Clients.All.pointUpdated(token, add, popup);

            ConcurrentBag<string> tokenBag;
            if (tokens.TryGetValue(token, out tokenBag))
            {
                hub.Clients.Clients(tokenBag.ToArray()).pointUpdated(token, add, popup);
            }
        }

        public static void SignOut(string token)
        {
            var hub = GlobalHost.ConnectionManager.GetHubContext<GameHub>();

            hub.Clients.All.signout(token);

            ConcurrentBag<string> tokenBag;
            if (tokens.TryGetValue(token, out tokenBag))
            {
                hub.Clients.Clients(tokenBag.ToArray()).tokenUpdated(token);
            }
        }
        public static void ChangeStatus(string token)
        {
            var hub = GlobalHost.ConnectionManager.GetHubContext<GameHub>();

            hub.Clients.All.changestatus(token);

            ConcurrentBag<string> tokenBag;
            if (tokens.TryGetValue(token, out tokenBag))
            {
                hub.Clients.Clients(tokenBag.ToArray()).tokenUpdated(token);
            }
        }


        public static void SignedOff(string token)
        {
            var hub = GlobalHost.ConnectionManager.GetHubContext<GameHub>();
            hub.Clients.Group(SystemType).signedOff(token);

            ConcurrentBag<string> tokenBag;
            if (tokens.TryGetValue(token, out tokenBag))
            {
                hub.Clients.Clients(tokenBag.ToArray()).signedOff(token);
            }
        }

        private static readonly string SystemType = "System";
        private static ConcurrentDictionary<string, ConcurrentBag<string>> tokens = new ConcurrentDictionary<string, ConcurrentBag<string>>();
        private static ConcurrentDictionary<string, string> connections = new ConcurrentDictionary<string, string>();
    }
}