﻿using Newtonsoft.Json;
using global::RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using RabbitMQ.Client.Exceptions;
using System.Threading;
using ALT.Common.Configuration;

namespace ALT.Common.Queue
{   
    public class RabbitMQConnection : IQueueConnection
    {
        private readonly Lazy<IConnection> connection;

        public RabbitMQConnection(RabbitMQConnectionSetup setup)
        {
            var connectionFactory = new ConnectionFactory
            {
                AutomaticRecoveryEnabled = true,
                UseBackgroundThreadsForIO = false
            };

            connectionFactory.HostName = setup.Host;
            connectionFactory.VirtualHost = setup.VirtualHost;
            connectionFactory.UserName = setup.UserName;
            connectionFactory.Password = setup.Password;
            connectionFactory.Port = setup.Port == 0 ? setup.Port : setup.Port;
            if (setup.SslEnabled)
            {
                connectionFactory.Ssl =
                    new SslOption
                    {
                        AcceptablePolicyErrors = setup.SslAcceptablePolicyErrors,
                        CertPassphrase = setup.SslCertPassphrase,
                        CertPath = setup.SslCertPath,
                        Enabled = setup.SslEnabled,
                        Version = setup.SslVersion,
                        ServerName = setup.SslServerName
                    };
            }

            connectionFactory.RequestedHeartbeat = setup.RequestedHeartbeat;
            connectionFactory.ClientProperties = new Dictionary<string, object>();
            setup.FillProperties(connectionFactory.ClientProperties);

            this.connection = new Lazy<IConnection>(
                () =>
                {
                    var conn = connectionFactory.CreateConnection();
                    conn.AutoClose = false;

                    return conn;
                },
                isThreadSafe: true
            );
            this.Config = setup;
        }

        protected IConnection Connection
        {
            get
            {
                return this.connection.Value;
            }
        }
        public RabbitMQConnectionSetup Config { get; private set; }

        public virtual IQueuePublisher<T> CreatePublisher<T>(string queueName, string exchangeName = null)
        {
            return CreatePublisherCore<T>(ExchangeType.Direct, exchangeName, queueName);
        }
        public virtual IQueuePublisher<T> CreateFanoutPublisher<T>(string exchangeName)
        {
            return CreatePublisherCore<T>(ExchangeType.Fanout, exchangeName, null);
        }
        public virtual IQueuePublisher<T> CreateHeaderPublisher<T>(string exchangeName)
        {
            return CreatePublisherCore<T>(ExchangeType.Headers, exchangeName, null);
        }
        public virtual IQueuePublisher<T> CreateTopicPublisher<T>(string exchangeName, string pattern)
        {
            return CreatePublisherCore<T>(ExchangeType.Topic, exchangeName, pattern);
        }
        public virtual IDisposable Subscribe<T>(string queueName, QueueProcessor<T> processor)
        {
            var model = this.Connection.CreateModel();
            return new RabbitMQSubscriber<T>(model, Config, queueName, processor);
        }

        public virtual IConnection QueueConnection() {
            return this.Connection;
        }

        protected virtual IQueuePublisher<T> CreatePublisherCore<T>(string type, string exchange, string routingKey)
        {
            var model = this.Connection.CreateModel();
            var pubAddress = new PublicationAddress(type, exchange ?? String.Empty, routingKey ?? String.Empty);

            return new RabbitMQPublisher<T>(model, pubAddress, this.Config);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                if (this.connection.IsValueCreated)
                {
                    this.connection.Value.Dispose();
                }
            }
        }

        public static RabbitMQConnectionSetup CreateRabbitMQConnectionSetup()
        {
            var config = ConfigurationManager.Get<string>(RabbitMQConfigurations.Group, RabbitMQConfigurations.Connection, string.Empty);
            return CreateRabbitMQConnectionSetup(config);
        }
        public static RabbitMQConnectionSetup CreateRabbitMQConnectionSetup(string config)
        {
            if (!config.StartsWith("{") && !config.EndsWith("}"))
            {
                config = "{" + config + "}";
            }
            return JsonConvert.DeserializeObject<RabbitMQConnectionSetup>(config);
        }
    }
}
