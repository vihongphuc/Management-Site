﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Queue
{
    public class RabbitMQPooledPublisher<T> : IQueuePublisher<T>
    {
        private readonly IQueuePublisher<T> basePublisher;
        private readonly RabbitMQPooledConnection connection;
        private readonly string key;
        private readonly int poolKey;

        private bool isDisposed = false;

        public bool IsOpen
        {
            get
            {
                return this.basePublisher.IsOpen;
            }
        }

        public RabbitMQPooledPublisher(RabbitMQPooledConnection connection, string key, int poolKey, IQueuePublisher<T> basePublisher)
        {
            this.basePublisher = basePublisher;
            this.connection = connection;
            this.key = key;
            this.poolKey = poolKey;
        }

        public Task Publish(T data, IDictionary<string, object> headers)
        {
            if (isDisposed)
                throw new ObjectDisposedException(String.Empty);

            return this.basePublisher.Publish(data, headers);
        }

        public void Dispose()
        {
            if (!isDisposed)
            {
                isDisposed = true;

                this.connection.DisposeResource(key, poolKey);
            }
        }
    }
}
