﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Queue
{
    internal static class RabbitMQHelpers
    {
        private static readonly JsonSerializerSettings serializerSetting;

        static RabbitMQHelpers()
        {
            var fnSettings = JsonConvert.DefaultSettings;
            JsonSerializerSettings settings = null;
            if (fnSettings != null)
            {
                settings = fnSettings();
            }

            if (settings == null)
            {
                settings = new JsonSerializerSettings();
            }

            settings.Converters.Add(new StringEnumConverter
            {
                AllowIntegerValues = true,
                CamelCaseText = false
            });
            settings.DateTimeZoneHandling = DateTimeZoneHandling.Local;

            serializerSetting = settings;
        }

        public static T Deserialize<T>(byte[] bytes)
        {
            var data = Encoding.UTF8.GetString(bytes);
            return JsonConvert.DeserializeObject<T>(data);
        }
        public static byte[] Serialize<T>(T data)
        {
            var dataString = JsonConvert.SerializeObject(data);
            return Encoding.UTF8.GetBytes(dataString);
        }
    }
}
