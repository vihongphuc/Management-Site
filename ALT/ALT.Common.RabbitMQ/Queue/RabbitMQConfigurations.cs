﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Queue
{
    public static class RabbitMQConfigurations
    {
        public static readonly string Group = QueueConfigurations.Group;

        public static readonly string Connection = "Connection";
        public static readonly string MaxPool = "MaxPool";
    }
}
