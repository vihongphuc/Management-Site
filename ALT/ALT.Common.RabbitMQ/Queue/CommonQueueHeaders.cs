﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Queue
{
    public static class CommonQueueHeaders
    {
        public static readonly string EventType = "EventType";

        public static readonly string EventTypeALTGameTransaction = "ALT.GameTransaction";
        public static readonly string EventTypeSlotGameTransaction = "SlotGame.GameTransaction";
    }
}
