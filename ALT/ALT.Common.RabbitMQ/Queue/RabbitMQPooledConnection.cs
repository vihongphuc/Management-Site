﻿using ALT.Common.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ALT.Common.Queue
{
    public class RabbitMQPooledConnection : RabbitMQConnection
    {
        public static readonly int DefaultMaxPooledResourcesCount = 24;

        private Dictionary<string, PooledResources> pooledResources =
            new Dictionary<string, PooledResources>(StringComparer.OrdinalIgnoreCase);
        private readonly int maxPooledResourcesCount;
        private readonly object syncLock = new object();

        private bool isDisposed;

        private int poolCounter;

        public RabbitMQPooledConnection(RabbitMQConnectionSetup setup)
            : base(setup)
        {
            this.maxPooledResourcesCount = ConfigurationManager.Get(RabbitMQConfigurations.Group, RabbitMQConfigurations.MaxPool, setup.PrefetchCount < DefaultMaxPooledResourcesCount ? DefaultMaxPooledResourcesCount : setup.PrefetchCount);
        }
        protected override IQueuePublisher<T> CreatePublisherCore<T>(string type, string exchange, string routingKey)
        {
            PooledResources pr;
            var resourceKey = $"Publisher.{type}.{(exchange ?? String.Empty)}.{routingKey ?? String.Empty}.{typeof(T).FullName}";

            lock (syncLock)
            {
                if (isDisposed)
                    throw new ObjectDisposedException(String.Empty);

                if (!pooledResources.TryGetValue(resourceKey, out pr))
                {
                    pr = new PooledResources
                    {
                        Pool = new Dictionary<int, Pooled>(this.maxPooledResourcesCount),
                        Semaphore = new SemaphoreSlim(this.maxPooledResourcesCount, this.maxPooledResourcesCount)
                    };

                    pooledResources.Add(resourceKey, pr);
                }
            }

            if (!pr.Semaphore.Wait(TimeSpan.FromSeconds(Config.Timeout)))
                throw new TimeoutException();

            KeyValuePair<int, Pooled> kvPool;
            lock (pr)
            {
                kvPool = pr.Pool.FirstOrDefault(d => !d.Value.InUse);
                if (kvPool.Value != null)
                {
                    kvPool.Value.InUse = true;
                    kvPool.Value.LastUse = DateTime.Now;

                    return new RabbitMQPooledPublisher<T>(
                        this,
                        resourceKey,
                        kvPool.Key,
                        (IQueuePublisher<T>)kvPool.Value.Resource
                    );
                }
                else
                {
                    var poolID = Interlocked.Increment(ref poolCounter);

                    kvPool = new KeyValuePair<int, Pooled>(
                        poolID,
                        new Pooled
                        {
                            InUse = true,
                            LastUse = DateTime.Now
                        }
                    );

                    pr.Pool.Add(kvPool.Key, kvPool.Value);
                }
            }

            var result = base.CreatePublisherCore<T>(type, exchange, routingKey);
            kvPool.Value.Resource = result;

            return new RabbitMQPooledPublisher<T>(
                this,
                resourceKey,
                kvPool.Key,
                result
            );
        }
        public void DisposeResource(string key, int poolKey)
        {
            PooledResources pr;
            lock (syncLock)
            {
                if (!this.pooledResources.TryGetValue(key, out pr))
                    throw new InvalidOperationException("Resource key cannot be found in the pool");
            }

            lock (pr)
            {
                Pooled pooled;
                if (!pr.Pool.TryGetValue(poolKey, out pooled))
                    throw new InvalidOperationException("The key cannot be found in the pool");

                pooled.InUse = false;
                pooled.LastUse = DateTime.Now;
            }
            pr.Semaphore.Release();
        }

        protected override void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                IEnumerable<PooledResources> prs;
                lock (syncLock)
                {
                    if (isDisposed)
                        return;

                    isDisposed = true;

                    prs = pooledResources.Values.ToArray();
                }

                foreach (var pr in prs)
                {
                    for (int i = 0; i < this.maxPooledResourcesCount; i++)
                    {
                        pr.Semaphore.Wait();    // wait until pool is all claimed
                    }

                    foreach (var pool in pr.Pool.Values)
                    {
                        pool.InUse = true;
                        pool.Resource.Dispose();
                    }

                    pr.Semaphore.Dispose();
                }
            }

            base.Dispose(isDisposing);
        }

        private class PooledResources
        {
            public Dictionary<int, Pooled> Pool { get; set; }
            public SemaphoreSlim Semaphore { get; set; }
        }

        private class Pooled
        {
            public IDisposable Resource { get; set; }
            public bool InUse { get; set; }
            public DateTime LastUse { get; set; }
        }
    }
}
