﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Reflection;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Queue
{
    public class RabbitMQConnectionSetup
    {
        public static readonly ushort DefaultPort = 5672;
        public static readonly string DefaultVirtualHost = "/";
        public static readonly ushort DefaultTimeout = 10;
        public static readonly ushort DefaultHeartbeatTimeout = 10;
        public static readonly ushort DefaultPrefetchCount = 1;

        public string Host { get; set; }
        public ushort Port { get; set; }

        public SslPolicyErrors SslAcceptablePolicyErrors { get; set; }
        public string SslCertPassphrase { get; set; }
        public string SslCertPath { get; set; }
        public bool SslEnabled { get; set; }
        public string SslServerName { get; set; }
        public SslProtocols SslVersion { get; set; }

        public string VirtualHost { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public ushort PrefetchCount { get; set; }
        public ushort RequestedHeartbeat { get; set; }
        public ushort Timeout { get; set; }
        public bool PublisherConfirms { get; set; }
        public bool PersistentMessages { get; set; }
        public bool CancelOnHaFailover { get; set; }
        public string Product { get; set; }
        public string Platform { get; set; }

        public RabbitMQConnectionSetup()
        {
            Port = DefaultPort;
            VirtualHost = string.IsNullOrEmpty(VirtualHost) ? DefaultVirtualHost : VirtualHost;
            Timeout = DefaultTimeout;
            RequestedHeartbeat = DefaultHeartbeatTimeout;
            PrefetchCount = DefaultPrefetchCount;
            PublisherConfirms = true;
            PersistentMessages = true;
            CancelOnHaFailover = false;
        }

        public void FillProperties(IDictionary<string, object> properties)
        {
            var version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            var applicationNameAndPath = Environment.GetCommandLineArgs()[0];

            var applicationName = "unknown";
            var applicationPath = "unknown";
            if (!string.IsNullOrWhiteSpace(applicationNameAndPath))
            {
                try
                {
                    applicationName = Path.GetFileName(applicationNameAndPath);
                    applicationPath = Path.GetDirectoryName(applicationNameAndPath);
                }
                catch (ArgumentException) { }
                catch (PathTooLongException) { }
            }

            var hostname = Environment.MachineName;
            var product = Product ?? applicationName;
            var platform = Platform ?? hostname;

            properties.Add("client_api", "ALT.Common.Queue.RabbitMQConnection");
            properties.Add("product", product);
            properties.Add("platform", platform);
            properties.Add("version", version);
            properties.Add("projectj_version", version);
            properties.Add("application", applicationName);
            properties.Add("application_location", applicationPath);
            properties.Add("machine_name", hostname);
            properties.Add("user", UserName);
            properties.Add("connected", DateTime.Now.ToString("u")); // UniversalSortableDateTimePattern: yyyy'-'MM'-'dd HH':'mm':'ss'Z'
            properties.Add("requested_heartbeat", RequestedHeartbeat.ToString());
            properties.Add("timeout", Timeout.ToString());
            properties.Add("publisher_confirms", PublisherConfirms.ToString());
            properties.Add("persistent_messages", PersistentMessages.ToString());
        }
    }
}
