﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ALT.Exceptions;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ALT.Common.Queue
{
    public class RabbitMQPublisher<T> : IQueuePublisher<T>
    {
        public bool IsOpen
        {
            get
            {
                return this.Channel.IsOpen;
            }
        }

        protected IModel Channel { get; private set; }
        protected RabbitMQConnectionSetup Config { get; private set; }
        public PublicationAddress QueueAddress { get; private set; }

        private ConcurrentDictionary<ulong, TaskCompletionSource<ConfirmActions>> confirms =
            new ConcurrentDictionary<ulong, TaskCompletionSource<ConfirmActions>>();

        public RabbitMQPublisher(IModel channel, PublicationAddress queueAddress, RabbitMQConnectionSetup config)
        {
            this.Channel = channel;
            this.Config = config;
            this.QueueAddress = queueAddress;

            if (config.PublisherConfirms)
            {
                this.Channel.BasicNacks += Channel_BasicNacks;
                this.Channel.BasicAcks += Channel_BasicAcks;

                this.Channel.ConfirmSelect();
            }
        }

        public virtual async Task Publish(T data, IDictionary<string, object> headers)
        {
            var props = this.Channel.CreateBasicProperties();
            props.Persistent = Config.PersistentMessages;
            if (props.Headers == null)
                props.Headers = new Dictionary<string, object>();

            props.Headers.Add("TypeFullName", typeof(T).FullName);

            if (headers != null)
            {
                foreach (var kv in headers)
                {
                    if (!props.Headers.ContainsKey(kv.Key))
                    {
                        props.Headers.Add(kv.Key, kv.Value);
                    }
                }
            }

            var serData = RabbitMQHelpers.Serialize(data);

            if (Config.PublisherConfirms)
            {
                await WaitConfirm(props, serData);
            }
            else
            {
                Channel.BasicPublish(this.QueueAddress, props, serData);
            }
        }

        private async Task WaitConfirm(IBasicProperties props, byte[] data)
        {
            var seqNo = Channel.NextPublishSeqNo;

            var tcs = new TaskCompletionSource<ConfirmActions>();
            if (!confirms.TryAdd(seqNo, tcs))
                throw new QueueException("Duplicated next publisher sequence no");

            ConfirmActions result;
            try
            {
                Channel.BasicPublish(this.QueueAddress, props, data);

                var ct = new CancellationTokenSource();
                var delayTask = Task.Delay(Config.Timeout * 1000, ct.Token)
                    .ContinueWith(t =>
                    {
                        if (t.IsCanceled)
                            return;

                        t.Wait();
                        HandleConfirm(seqNo, false, ConfirmActions.Timeout);
                    })
                    .ConfigureAwait(false);

                result = await tcs.Task;
                ct.Cancel();
            }
            finally
            {
                TaskCompletionSource<ConfirmActions> tcsDel;
                confirms.TryRemove(seqNo, out tcsDel);
            }

            switch (result)
            {
                case ConfirmActions.OK:
                    break;
                case ConfirmActions.Rejected:
                    throw new QueueException("The message was rejected");
                case ConfirmActions.Timeout:
                    throw new TimeoutException();
                default:
                    throw new IndexOutOfRangeException("Cannot recognize the action result");
            }
        }
        private void Channel_BasicAcks(object sender, BasicAckEventArgs e)
        {
            HandleConfirm(e.DeliveryTag, e.Multiple, ConfirmActions.OK);
        }
        private void Channel_BasicNacks(object sender, BasicNackEventArgs e)
        {
            HandleConfirm(e.DeliveryTag, e.Multiple, ConfirmActions.Rejected);
        }
        private void HandleConfirm(ulong sequenceNumber, bool multiple, ConfirmActions confirmActions)
        {
            if (multiple)
            {
                foreach (var kvMatch in confirms.ToArray().Where(kv => kv.Key <= sequenceNumber))
                {
                    kvMatch.Value.TrySetResult(confirmActions);
                }
            }
            else
            {
                TaskCompletionSource<ConfirmActions> tcs;
                if (confirms.TryGetValue(sequenceNumber, out tcs))
                {
                    tcs.TrySetResult(confirmActions);
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                this.Channel.Dispose();
            }
        }

        private enum ConfirmActions
        {
            OK,
            Rejected,
            Timeout
        }

    }
}
