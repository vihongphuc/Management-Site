﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Queue
{
    public class RabbitMQSubscriber<T> : DefaultBasicConsumer, IDisposable
    {
        public string QueueName { get; private set; }

        protected RabbitMQConnectionSetup Config { get; private set; }
        private readonly QueueProcessor<T> processor;

        private readonly object syncLock = new object();

        public RabbitMQSubscriber(IModel channel, RabbitMQConnectionSetup config, string queueName, QueueProcessor<T> processor)
            : base(channel)
        {
            if (processor == null)
                throw new ArgumentNullException("processor");

            this.Config = config;
            this.processor = processor;
            this.QueueName = queueName;

            IDictionary<string, object> arguments = new Dictionary<string, object>
            {
                {"x-cancel-on-ha-failover", config.CancelOnHaFailover }
            };

            channel.BasicQos(0, Config.PrefetchCount, false);
            channel.BasicConsume(queueName, noAck: false,
                                            consumerTag: String.Format("{0}-{1}.{2}.{3}", config.Platform, config.Product, queueName, Guid.NewGuid()),
                                            arguments: arguments,
                                            consumer: this);
        }

        public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
        {
            var data = RabbitMQHelpers.Deserialize<T>(body);
            this.processor(data, properties.Headers)
                .ContinueWith(t =>
                {
                    try
                    {
                        return t.Result;
                    }
                    catch (Exception)
                    {
                        return QueueProcessResult.Reject;
                    }
                })
                .ContinueWith(t =>
                {
                    var result = t.Result;
                    lock (syncLock)
                    {
                        if (result == QueueProcessResult.Success)
                        {
                            base.Model.BasicAck(deliveryTag, multiple: false);
                        }
                        else
                        {
                            base.Model.BasicNack(deliveryTag, multiple: false, requeue: true);
                        }
                    }
                })
                .ConfigureAwait(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                lock (syncLock)
                {
                    this.Model.Dispose();
                }
            }
        }
    }
}
