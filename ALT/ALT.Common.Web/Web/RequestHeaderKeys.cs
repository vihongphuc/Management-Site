﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Web
{
    public static class RequestHeaderKeys
    {
        public static readonly string UserAgent = "User-Agent";
        public static readonly string IpAddress = "Ip-Address";
        public static readonly string IPCountry = "IP-Country";

        public static readonly string ThirdParty = "Third-Party";
    }
}
