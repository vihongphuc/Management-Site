﻿using ALT.Common.Configuration;
using ALT.Common.Helpers;
using ALT.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Web
{
    public static class CommonDomainRestrictionConfigurations
    {
        public static readonly string RestrictDomainGroup = "RestrictDomain";
        public static readonly string RestrictDomainException = "RestrictDomainException";
        public static readonly string DefaultUser = "Default";

        private static readonly Dictionary<string, string> DtRestrictDomainGroups = ConfigurationManager.GetAll(RestrictDomainGroup).ToDictionary(d => d.Key, d => d.Value);
        private static readonly Dictionary<string, string> DtRestrictDomainExceptions = ConfigurationManager.GetAll(RestrictDomainException).ToDictionary(d => d.Key, d => d.Value);

        public static bool ValidateDomain(string username, string host)
        {
            if (DtRestrictDomainGroups.Count() == 0)
                return true;

            if (UserCanAccessOnDomain(username, host, true))
            {
                return true;
            }
            else
            {
                // Checking default setting
                string defaultDomain = string.Empty;
                if (DtRestrictDomainGroups.TryGetValue(DefaultUser, out defaultDomain))
                {
                    return UserCanAccessOnDomain(DefaultUser, defaultDomain, false);
                }
            }
            return false;
        }

        private static bool UserCanAccessOnDomain(string username, string checkingHost, bool checkingException)
        {
            var result = false;

            var userRestrictDomains = DtRestrictDomainGroups.Where(d => username.StartsWith(d.Key))
                                             .Select(d => StringHelper.ToArray(d.Value)).SelectMany(d => d);

            result = userRestrictDomains.Contains(checkingHost);

            if (false == result && checkingException)
            {
                //Checking excption case
                var exceptDomains = DtRestrictDomainExceptions.Where(d => username.Equals(d.Key))
                                                        .Select(d => StringHelper.ToArray(d.Value)).SelectMany(d => d);
                result = exceptDomains.Contains("*") || exceptDomains.Contains(checkingHost);
            }

            return result;
        }
    }
}
