﻿using ALT.Common.Utilities;
using ALT.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ALT.Common.Configuration;

namespace ALT.Common.Web
{
    public static class ApiConsumerHelpers
    {
        public static string GetConsumerAddress(HttpRequestBase request)
        {
            if (request.RequestContext.HttpContext.Items.Contains(ConsumerAddressItemKey))
            {
                return (string)request.RequestContext.HttpContext.Items[ConsumerAddressItemKey];
            }

            var result = GetConsumerAddressCore(request);
            request.RequestContext.HttpContext.Items[ConsumerAddressItemKey] = result;
            return result;
        }
        private static string GetConsumerAddressCore(HttpRequestBase request)
        {
            foreach (var item in IpHeaderItems)
            {
                var ipString = request.Headers[item.Key];
                if (!String.IsNullOrWhiteSpace(ipString))
                {
                    string privateIP = null;
                    bool isPrivate;
                    if (ValidIP(ipString, out isPrivate))
                    {
                        if (!isPrivate)
                        {
                            return ipString;
                        }
                        else if (String.IsNullOrWhiteSpace(privateIP))
                        {
                            privateIP = ipString;
                        }
                    }

                    if (!String.IsNullOrWhiteSpace(privateIP))
                    {
                        return privateIP;
                    }
                }
            }

            return GetConnectingIP(request);
        }
        public static string GetConsumerAddress(HttpRequestMessage request)
        {
            foreach (var item in IpHeaderItems)
            {
                IEnumerable<string> values;
                if (request.Headers.TryGetValues(item.Key, out values))
                {
                    foreach (var ipString in values.Select(s => s == null ? String.Empty : s.Trim()))
                    {
                        string privateIP = null;
                        bool isPrivate;
                        if (item.Split)
                        {
                            foreach (var ip in ipString.Split(',').Select(s => s.Trim()))
                            {
                                if (ValidIP(ip, out isPrivate))
                                {
                                    if (!isPrivate)
                                    {
                                        return ip;
                                    }
                                    else if (String.IsNullOrWhiteSpace(privateIP))
                                    {
                                        privateIP = ip;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (ValidIP(ipString, out isPrivate))
                            {
                                if (!isPrivate)
                                {
                                    return ipString;
                                }
                                else if (String.IsNullOrWhiteSpace(privateIP))
                                {
                                    privateIP = ipString;
                                }
                            }
                        }

                        if (!String.IsNullOrWhiteSpace(privateIP))
                        {
                            return privateIP;
                        }
                    }
                }
            }

            return GetConnectingIP(request);
        }

        public static string GetConnectingIP(HttpRequestBase request)
        {
            return request.UserHostAddress;
        }
        public static string GetConnectingIP(HttpRequestMessage request)
        {
            var wrapper = (HttpContextWrapper)request.Properties.GetOrDefault("MS_HttpContext");
            if (wrapper != null)
                return wrapper.Request.UserHostAddress;

            var remoteEndPointName = (RemoteEndpointMessageProperty)request.Properties.GetOrDefault(RemoteEndpointMessageProperty.Name);
            if (remoteEndPointName != null)
                return remoteEndPointName.Address;

            return null;    //here the user can return whatever they like
        }

        public static string GetConsumerCountry(HttpRequestBase request)
        {
            if (request.RequestContext.HttpContext.Items.Contains(ConsumerCountryItemKey))
            {
                return (string)request.RequestContext.HttpContext.Items[ConsumerCountryItemKey];
            }

            var result = GetConsumerCountryCore(request);
            request.RequestContext.HttpContext.Items[ConsumerCountryItemKey] = result;
            return result;
        }
        private static string GetConsumerCountryCore(HttpRequestBase request)
        {
            foreach (var key in CountryHeaders)
            {
                var country = request.Headers[key];
                if (!String.IsNullOrWhiteSpace(country))
                {
                    return country;
                }
            }

            return null;
        }
        public static string GetConsumerCountry(HttpRequestMessage request)
        {
            foreach (var key in CountryHeaders)
            {
                IEnumerable<string> values;
                if (request.Headers.TryGetValues(key, out values))
                {
                    foreach (var country in values.Select(s => s == null ? String.Empty : s.Trim()))
                    {
                        if (!String.IsNullOrWhiteSpace(country))
                        {
                            return country;
                        }
                    }
                }
            }

            return null;
        }

        public static string GetEndUserAddress(HttpRequestBase request)
        {
            return request.Headers[ApiConsumerKeys.IPAddress];
        }
        public static string GetEndUserAddress(HttpRequestMessage request)
        {
            IEnumerable<string> values;
            if (request.Headers.TryGetValues(ApiConsumerKeys.IPAddress, out values))
            {
                return values.FirstOrDefault();
            }

            return null;
        }

        public static string GetEndUserCountry(HttpRequestBase request)
        {
            return request.Headers[ApiConsumerKeys.IPCountry];
        }
        public static string GetEndUserCountry(HttpRequestMessage request)
        {
            IEnumerable<string> values;
            if (request.Headers.TryGetValues(ApiConsumerKeys.IPCountry, out values))
            {
                return values.FirstOrDefault();
            }

            return null;
        }

        public static string GetEndUserUserAgent(HttpRequestMessage request)
        {
            IEnumerable<string> values;
            if (request.Headers.TryGetValues(RequestHeaderKeys.UserAgent, out values))
            {
                return values.FirstOrDefault();
            }

            return null;
        }

        public static bool IsAccessAllowed(HttpRequestBase request)
        {
            if (IsAccessAllowed(GetConsumerAddress(request), GetConsumerCountry(request)))
                return true;

            var headers = request.Headers.AllKeys.ToDictionary(key => key, key => request.Headers[key], StringComparer.OrdinalIgnoreCase);
            return IsAccessAllowed(GetConnectingIP(request), headers);
        }
        public static bool IsAccessAllowed(HttpRequestMessage request)
        {
            if (IsAccessAllowed(GetConsumerAddress(request), GetConsumerCountry(request)))
                return true;

            var headers = request.Headers.ToDictionary(d => d.Key, d => d.Value.FirstOrDefault(), StringComparer.OrdinalIgnoreCase);
            return IsAccessAllowed(GetConnectingIP(request), headers);
        }
        public static bool IsAccessAllowed(string ip, string country)
        {
            if (!String.IsNullOrWhiteSpace(ip))
            {
                if (IpAllowList.Contains(ip))
                    return true;
            }

            var hasCountry = !String.IsNullOrWhiteSpace(country);
            if (hasCountry && CountryBlockList.Contains(country))
            {
                return false;
            }

            if (CountryWhiteList.Count > 0)
            {
                if (false == hasCountry)
                    return false;

                return CountryWhiteList.Contains(country);
            }

            return true;
        }
        public static bool IsAccessAllowed(string connectingIP, IDictionary<string, string> headers)
        {
            if (BypassHeaders.Count == 0)
                return false;

            var validIP = BypassHeadersIP.Count == 0 || BypassHeadersIP.Contains(connectingIP);
            if (!validIP)
                return false;

            return BypassHeaders.All(d =>
            {
                string check;
                if (headers.TryGetValue(d.Key, out check))
                {
                    return String.Equals(check, d.Value, StringComparison.OrdinalIgnoreCase);
                }

                return false;
            });
        }

        public static bool ValidateIPAddress(params string[] ipAddresses)
        {
            bool valid = true;
            for (int i = 0; i < ipAddresses.Length; i++)
            {
                IPAddress address;
                if (!IPAddress.TryParse(ipAddresses[i], out address))
                {
                    valid = false;
                }
            }

            return valid;
        }

        #region IP Address
        public static bool IsPrivateIP(string ip)
        {
            if (string.IsNullOrWhiteSpace(ip))
            {
                return true;
            }

            bool isPrivate;
            ValidIP(ip, out isPrivate);

            return isPrivate;
        }

        private static bool ValidIP(string ip, out bool isPrivate)
        {
            IPAddress ipAddr;

            isPrivate = false;
            if (0 == ip.Length
                || false == IPAddress.TryParse(ip, out ipAddr)
                || (ipAddr.AddressFamily != AddressFamily.InterNetwork
                    && ipAddr.AddressFamily != AddressFamily.InterNetworkV6))
                return false;

            var addr = IpRange.AddrToUInt64(ipAddr);
            foreach (var range in IpPrivateRanges)
            {
                if (range.Encompasses(addr))
                {
                    isPrivate = true;
                }
            }

            return true;
        }

        private sealed class IpRange
        {
            private readonly UInt64 _start;
            private readonly UInt64 _end;

            public IpRange(string startStr, string endStr)
            {
                _start = ParseToUInt64(startStr);
                _end = ParseToUInt64(endStr);
            }

            public static UInt64 AddrToUInt64(IPAddress ip)
            {
                var ipBytes = ip.GetAddressBytes();
                UInt64 value = 0;

                foreach (var abyte in ipBytes)
                {
                    value <<= 8;    // shift
                    value += abyte;
                }

                return value;
            }

            public static UInt64 ParseToUInt64(string ipStr)
            {
                var ip = IPAddress.Parse(ipStr);
                return AddrToUInt64(ip);
            }

            public bool Encompasses(UInt64 addrValue)
            {
                return _start <= addrValue && addrValue <= _end;
            }

            public bool Encompasses(IPAddress addr)
            {
                var value = AddrToUInt64(addr);
                return Encompasses(value);
            }
        };

        private static readonly IpRange[] IpPrivateRanges =
            new IpRange[]
            {
                new IpRange("0.0.0.0", "0.255.255.255"),
                new IpRange("10.0.0.0", "10.255.255.255"),
                new IpRange("127.0.0.0", "127.255.255.255"),
                new IpRange("169.254.0.0", "169.254.255.255"),
                new IpRange("172.16.0.0", "172.31.255.255"),
                new IpRange("192.0.2.0", "192.0.2.255"),
                new IpRange("192.168.0.0", "192.168.255.255"),
                new IpRange("255.255.255.0", "255.255.255.255")
            };

        private sealed class HeaderItem
        {
            public readonly string Key;
            public readonly bool Split;

            public HeaderItem(string key, bool split)
            {
                Key = key;
                Split = split;
            }
        }
        private static readonly HeaderItem[] IpHeaderItems =
            new HeaderItem[]
            {
                new HeaderItem("CLIENT-IP", false),
                new HeaderItem("X-FORWARDED-FOR", true),
                new HeaderItem("X-FORWARDED", false),
                new HeaderItem("CF-Connecting-IP", false),
                new HeaderItem("X-CLUSTER-CLIENT-IP", false),
                new HeaderItem("FORWARDED-FOR", false),
                new HeaderItem("FORWARDED", false),
                new HeaderItem("REMOTE-ADDR", false)
            };

        private static readonly string[] CountryHeaders = new[] { "CF-IPCountry", "INCAP-Country-Code" };
        #endregion

        private const string ConsumerAddressItemKey = "_ConsumerAddress";
        private const string ConsumerCountryItemKey = "_ConsumerCountry";

        private static readonly HashSet<string> IpAllowList = new HashSet<string>(
            ConfigurationManager.Get(CommonWebConfigurations.Group, CommonWebConfigurations.IPAllow, String.Empty)
                                .Split(" ,;".ToCharArray())
                                .Where(d => !String.IsNullOrWhiteSpace(d))
        );

        private static readonly HashSet<string> CountryWhiteList = new HashSet<string>(
            ConfigurationManager.Get(CommonWebConfigurations.Group, CommonWebConfigurations.CountryWhiteList, String.Empty)
                                .Split(" ,;".ToCharArray())
                                .Where(d => !String.IsNullOrWhiteSpace(d)),
            StringComparer.OrdinalIgnoreCase
        );

        private static readonly HashSet<string> CountryBlockList = new HashSet<string>(
            ConfigurationManager.Get(CommonWebConfigurations.Group, CommonWebConfigurations.CountryBlockList, String.Empty)
                                .Split(" ,;".ToCharArray())
                                .Where(d => !String.IsNullOrWhiteSpace(d)),
            StringComparer.OrdinalIgnoreCase
        );

        private static readonly IDictionary<string, string> BypassHeaders =
            ConfigurationManager.Get(CommonWebConfigurations.Group, CommonWebConfigurations.BypassHeaders, String.Empty)
                                .Split(" ,;".ToCharArray())
                                .Where(d => !String.IsNullOrWhiteSpace(d))
                                .Select(d =>
                                {
                                    var headerCheck = d.Split(":".ToCharArray(), 2);
                                    return new
                                    {
                                        Key = headerCheck[0],
                                        Value = headerCheck[1]
                                    };
                                })
                                .ToDictionary(d => d.Key, d => d.Value, StringComparer.OrdinalIgnoreCase);

        private static readonly HashSet<string> BypassHeadersIP = new HashSet<string>(
            ConfigurationManager.Get(CommonWebConfigurations.Group, CommonWebConfigurations.BypassHeadersIP, String.Empty)
                                .Split(" ,;".ToCharArray())
                                .Where(d => !String.IsNullOrWhiteSpace(d))
        );
    }
}
