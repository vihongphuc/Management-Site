﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Web
{
    /// <summary>
    /// The keys of the items and headers of the request specifying consumer data
    /// </summary>
    public static class ApiConsumerKeys
    {
        public static readonly string IPAddress = "ALT-IP-Address";
        public static readonly string IPCountry = "ALT-IP-Country";
    }
}
