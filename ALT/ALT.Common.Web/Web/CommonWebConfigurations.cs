﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Web
{
    public static class CommonWebConfigurations
    {
        public static readonly string Group = "Web";

        public static readonly string CdnBaseUrl = "CdnBaseUrl";

        public static readonly string IPAllow = "IPAllow";

        public static readonly string CountryBlockList = "CountryBlockList";
        public static readonly string CountryWhiteList = "CountryWhiteList";

        public static readonly string BypassHeaders = "BypassHeaders";
        public static readonly string BypassHeadersIP = "BypassHeadersIP";

        public static readonly string CurrencyReplacementsGroup = "CurrencyReplacements";
        public static readonly string CurrencyMappingsGroup = "CurrencyMappings";
    }
}
