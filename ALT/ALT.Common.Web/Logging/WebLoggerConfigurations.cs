﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Logging
{
    public static class WebLoggerConfigurations
    {
        public static readonly string Group = "WebLogger";

        public static readonly string Type = "Type";
        public static readonly string LogLevel = "LogLevel";

        public static readonly string MvcLogAll = "MvcLogAll";

        public static readonly string Directory = "Directory";

        public static readonly string WarnProcessTime = "WarnProcessTime";
    }
}
