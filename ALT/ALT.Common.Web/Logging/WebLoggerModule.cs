﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ALT.Common.Logging
{
    public class WebLoggerModule : IHttpModule
    {
        public static readonly string WebLoggerContextItem = ".__WebLogger";

        private readonly IList<IWebLogger> webLoggers;

        public static WebLogContext GetCurrentLogContext()
        {
            return (WebLogContext)HttpContext.Current.Items[WebLoggerContextItem];
        }

        public WebLoggerModule()
        {
            webLoggers = WebLoggers.GetWebLoggers();
        }

        public void Init(HttpApplication context)
        {
            if (webLoggers == null)
                return;

            context.BeginRequest += context_BeginRequest;
            context.Error += context_Error;

            EventHandlerTaskAsyncHelper asyncHelper = new EventHandlerTaskAsyncHelper(context_LogRequest);
            context.AddOnLogRequestAsync(asyncHelper.BeginEventHandler, asyncHelper.EndEventHandler);
        }
        private void context_BeginRequest(object sender, EventArgs e)
        {
            var app = (HttpApplication)sender;
            var logContext = new WebLogContext(app.Request, app.Response);
            app.Context.Items.Add(WebLoggerContextItem, logContext);

            foreach (var webLogger in webLoggers)
            {
                logContext.AddWebLoggerContext(webLogger.Name, webLogger.CreateWebLoggerContext(logContext));
            }
        }
        private void context_Error(object sender, EventArgs e)
        {
            var app = (HttpApplication)sender;
            var logContext = (WebLogContext)app.Context.Items[WebLoggerContextItem];
            if (logContext != null)
            {
                logContext.SetLogLevel(LogLevel.Error);
                logContext.AddException(app.Server.GetLastError());
            }
        }
        private async Task context_LogRequest(object sender, EventArgs e)
        {
            var app = (HttpApplication)sender;
            var logContext = (WebLogContext)app.Context.Items[WebLoggerContextItem];
            if (logContext != null)
            {
                logContext.FinalizeContext();

                if (logContext.LoggingEnabled)
                {
                    var tasks = this.webLoggers.Where(webLogger => webLogger.GetCurrentLogLevel().CanLog(logContext.LogLevel))
                                    .Select(webLogger => webLogger.Log(logContext));

                    await Task.WhenAll(tasks);
                }

                logContext.Dispose();

                app.Context.Items.Remove(WebLoggerContextItem);
            }
        }

        public void Dispose() { }
    }
}
