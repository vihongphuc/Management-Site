﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ALT.Common.Logging.Files
{
    public class FileWebLogContext : IWebLoggerContext
    {
        public FileWebLogContext()
        {
        }

        public void Dispose() { }

        public string GetPath(WebLogContext webLogContext)
        {
            if (!String.IsNullOrWhiteSpace(webLogContext.Controller))
            {
                if (!String.IsNullOrWhiteSpace(webLogContext.Action))
                {
                    return Path.Combine(webLogContext.Controller, webLogContext.Action);
                }
            }

            return null;
        }
    }
}
