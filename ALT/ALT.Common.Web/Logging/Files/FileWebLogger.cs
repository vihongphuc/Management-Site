﻿using ALT.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace ALT.Common.Logging.Files
{
    public class FileWebLogger : IWebLogger
    {
        public static readonly string DefaultMethodPath = "_Default";
        public static readonly string UnknownMethod = "_";
        public static readonly string Extension = "log";

        private readonly string directory;

        public string Name { get { return "File"; } }

        public FileWebLogger(string directory)
        {
            this.directory = directory;
        }
        public LogLevel GetCurrentLogLevel()
        {
            return WebLoggers.GetLogLevel();
        }
        public IWebLoggerContext CreateWebLoggerContext(WebLogContext context)
        {
            return new FileWebLogContext();
        }

        public async Task Log(WebLogContext context)
        {
            if (!context.LoggingEnabled)
                return;

            var fileContext = (FileWebLogContext)context.GetWebLoggerContext(this.Name);
            var path = GetDirectoryName(fileContext, context);

            var targetPath = await Task.Run(delegate
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                return Path.Combine(path, GetFileName(context));
            });

            await WriteToFile(context, targetPath);
        }

        private async Task WriteToFile(WebLogContext context, string filename)
        {
            using (var fileStream = File.OpenWrite(filename))
            {
                using (var sw = new StreamWriter(fileStream, Encoding.UTF8))
                {
                    sw.WriteLine("{0} {1} {2} {3}", (int)context.Status, context.Status, context.Request.HttpMethod, context.Request.RawUrl);
                    sw.WriteLine("Start     : {0:yyyy-MM-dd HH:mm:ss.fffffff}", context.StartTime);
                    sw.WriteLine("End       : {0:yyyy-MM-dd HH:mm:ss.fffffff}", context.EndTime);
                    sw.WriteLine("Duration  : {0}", context.TotalMilliseconds);
                    sw.WriteLine("Remote IP : {0}", context.IP);
                    sw.WriteLine("LogLevel  : {0}", context.LogLevel);
                    sw.WriteLine();

                    if (context.DetailItems.Count > 0)
                    {
                        bool first = true;
                        foreach (var detailItem in context.DetailItems)
                        {
                            if (context.LogLevel.CanLog(detailItem.LogLevel))
                            {
                                if (first)
                                {
                                    first = false;
                                    sw.WriteLine("Logging:", context.TotalMilliseconds);
                                }

                                sw.Write("+{0:#,##0.000} ", detailItem.OffsetMilliseconds);
                                sw.Write("{0:HH:mm:ss.fffffff} ", context.StartTime.AddMilliseconds(detailItem.OffsetMilliseconds));
                                sw.Write(detailItem.Message);
                                sw.WriteLine();

                                if (detailItem.Exception != null)
                                {
                                    sw.WriteLine(detailItem.Exception);
                                }
                            }
                        }

                        if (!first)
                        {
                            sw.WriteLine();
                        }
                    }

                    WriteExceptions(context, fileStream, sw);
                    await WriteRequestPart(context, fileStream, sw);
                    await WriteResponsePart(context, fileStream, sw);
                }
            }
        }

        private void WriteExceptions(WebLogContext context, FileStream fileStream, StreamWriter sw)
        {
            if (context.Exceptions.Count > 0)
            {
                sw.WriteLine("Exceptions:");
                sw.WriteLine();

                var first = true;
                foreach (var ex in context.Exceptions)
                {
                    if (false == first)
                    {
                        WriteSeparator(sw);
                        sw.WriteLine();
                    }
                    else
                    {
                        first = false;
                    }

                    var sbsEx = ex as ALTServiceException;
                    if (sbsEx != null)
                    {
                        if (sbsEx.Response != null)
                        {
                            sw.WriteLine($"Http Request: {ALTServiceException.FormatResponse(sbsEx.Response)}");
                            sw.WriteLine($"{sbsEx.Response}");
                            sw.WriteLine();
                        }

                        if (sbsEx.HttpError != null)
                        {
                            sw.WriteLine("Http Error:");
                            foreach (var kv in sbsEx.HttpError)
                            {
                                sw.WriteLine($"   {kv.Key} = {kv.Value}");
                            }
                            sw.WriteLine();
                        }
                    }

                    sw.WriteLine(ex);
                }
                WriteSeparator(sw);
            }
        }

        private static async Task WriteRequestPart(WebLogContext context, FileStream fileStream, StreamWriter sw)
        {
            if (context.RequestInfo.Count > 0)
            {
                sw.WriteLine("Request Information:");
                foreach (var kv in context.RequestInfo)
                {
                    sw.WriteLine("    {0}: {1}", kv.Key, kv.Value);
                }
                sw.WriteLine();
            }

            if (context.Request.Headers.Count > 0)
            {
                sw.WriteLine("Request Headers:");
                foreach (string header in context.Request.Headers)
                {
                    sw.WriteLine("    {0}: {1}", header, context.Request.Headers[header]);
                }
                sw.WriteLine();
            }

            if (context.RequestContentTracingEnabled)
            {
                sw.WriteLine("Request Content:");
                await sw.FlushAsync();

                await context.WriteRequestContent(fileStream);

                WriteSeparator(sw);
            }
        }
        private static async Task WriteResponsePart(WebLogContext context, FileStream fileStream, StreamWriter sw)
        {
            if (context.ResponseInfo.Count > 0)
            {
                sw.WriteLine("Response Information:");
                foreach (var kv in context.ResponseInfo)
                {
                    sw.WriteLine("    {0}: {1}", kv.Key, kv.Value);
                }
                sw.WriteLine();
            }

            if (context.Response.Headers.Count > 0)
            {
                sw.WriteLine("Response Headers:");
                foreach (string header in context.Response.Headers)
                {
                    sw.WriteLine("    {0}: {1}", header, context.Response.Headers[header]);
                }
                sw.WriteLine();
            }

            if (context.ResponseContentTracingEnabled)
            {
                sw.WriteLine("Response Content:");
                sw.WriteLine();
                await sw.FlushAsync();

                await context.WriteResponseContent(fileStream);

                WriteSeparator(sw);
            }
        }
        private static void WriteSeparator(StreamWriter sw)
        {
            sw.WriteLine();
            sw.WriteLine("===========================================================");
            sw.WriteLine();
        }

        private string GetFileName(WebLogContext context)
        {
            return String.Format("{0}.{1}.{2:HHmmssfffffff}.{3}.{4}", context.Request.HttpMethod, (int)context.Response.StatusCode, context.StartTime, context.ID, Extension);
        }
        private string GetDirectoryName(FileWebLogContext fileContext, WebLogContext context)
        {
            var baseDirPath = Path.Combine(this.directory,
                                           context.StartTime.Year.ToString(),
                                           String.Format("{0:00}", context.StartTime.Month),
                                           String.Format("{0:00}", context.StartTime.Day));

            var methodPath = fileContext.GetPath(context);
            if (String.IsNullOrWhiteSpace(methodPath))
            {
                methodPath = DefaultMethodPath;
                var virtualPath = HostingEnvironment.VirtualPathProvider.GetDirectory(context.Request.Url.LocalPath).VirtualPath;
                if (!String.IsNullOrWhiteSpace(virtualPath))
                {
                    virtualPath = virtualPath.TrimStart('/', '\\')
                                             .Replace("/", "\\");

                    methodPath = Path.Combine(methodPath, virtualPath);
                }
            }

            return Path.Combine(baseDirPath, methodPath);
        }

        public void Dispose() { }
    }
}
