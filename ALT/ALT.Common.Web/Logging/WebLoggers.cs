﻿using ALT.Common.Configuration;
using ALT.Common.Logging.Files;
using ALT.Common.Logging.NLogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace ALT.Common.Logging
{
    public static class WebLoggers
    {
        public static readonly string FileWebLoggerType = "File";
        public static readonly string NLogWebLoggerType = "NLog";

        private static IWebLogger[] webLoggers;

        public static IList<IWebLogger> GetWebLoggers()
        {
            if (webLoggers == null)
            {
                lock (typeof(WebLoggers))
                {
                    if (webLoggers == null)
                    {
                        var webLoggerTypes = ConfigurationManager.Get(WebLoggerConfigurations.Group, WebLoggerConfigurations.Type, String.Empty)
                                                                 .Split(WebLoggerTypeSeparator, StringSplitOptions.RemoveEmptyEntries);

                        webLoggers = webLoggerTypes.Select(webLoggerType =>
                        {
                            IWebLogger webLogger;
                            if (String.Equals(webLoggerType, FileWebLoggerType, StringComparison.OrdinalIgnoreCase))
                            {
                                webLogger = CreateFileWebLogger();
                            }
                            else if (String.Equals(webLoggerType, NLogWebLoggerType, StringComparison.OrdinalIgnoreCase))
                            {
                                webLogger = CreateNLogWebLogger();
                            }
                            else
                            {
                                return null;
                            }

                            var regObject = webLogger as IRegisteredObject;
                            if (regObject != null)
                            {
                                HostingEnvironment.RegisterObject(regObject);
                            }

                            return webLogger;
                        }).Where(d => d != null).ToArray();
                    }
                }
            }

            return webLoggers;
        }
        public static LogLevel GetLogLevel()
        {
            var configValue = ConfigurationManager.Get<string>(WebLoggerConfigurations.Group, WebLoggerConfigurations.LogLevel, string.Empty);
            if (!String.IsNullOrWhiteSpace(configValue))
            {
                LogLevel level;
                if (Enum.TryParse<LogLevel>(configValue, out level))
                {
                    return level;
                }
            }

            return LogLevel.Info;
        }

        public static bool GetMvcLogAll()
        {
            return ConfigurationManager.Get(WebLoggerConfigurations.Group, WebLoggerConfigurations.MvcLogAll, false);
        }

        private static FileWebLogger CreateFileWebLogger()
        {
            var directory = ConfigurationManager.Get(WebLoggerConfigurations.Group, WebLoggerConfigurations.Directory, string.Empty);
            if (String.IsNullOrWhiteSpace(directory))
            {
                directory = HostingEnvironment.MapPath(@"~\");
            }
            else if (directory.StartsWith("~"))
            {
                directory = HostingEnvironment.MapPath(directory);
            }

            return new FileWebLogger(directory);
        }
        private static NLogWebLogger CreateNLogWebLogger()
        {
            return new NLogWebLogger();
        }

        private static readonly char[] WebLoggerTypeSeparator = "|".ToCharArray();
    }
}
