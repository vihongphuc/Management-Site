﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Logging
{
    public enum SetLogLevel
    {
        NotSet,
        Off,
        Trace,
        Debug,
        Info,
        Warn,
        Error,
        Fatal
    }

    public static class SetLogLevelExtensions
    {
        public static LogLevel ToLogLevel(this SetLogLevel logLevel)
        {
            switch (logLevel)
            {
                case SetLogLevel.Off:
                    return LogLevel.Off;
                case SetLogLevel.Trace:
                    return LogLevel.Trace;
                case SetLogLevel.Debug:
                    return LogLevel.Debug;
                case SetLogLevel.Info:
                    return LogLevel.Info;
                case SetLogLevel.Warn:
                    return LogLevel.Warn;
                case SetLogLevel.Error:
                    return LogLevel.Error;
                case SetLogLevel.Fatal:
                    return LogLevel.Fatal;
            }

            throw new ArgumentOutOfRangeException("logLevel");
        }
    }
}
