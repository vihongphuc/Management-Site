﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ALT.Common.Logging
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class WebLogFilterAttribute : FilterAttribute, IAuthorizationFilter
    {
        public SetLogLevel LogLevel { get; set; }
        public bool TraceRequestContent { get; set; }
        public bool TraceResponseContent { get; set; }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var logContext = WebLoggerModule.GetCurrentLogContext();
            if (logContext == null)
                return;

            if (LogLevel != SetLogLevel.NotSet)
            {
                logContext.SetLogLevel(LogLevel.ToLogLevel());
            }
            else
            {
                if (!String.Equals(filterContext.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
                {
                    logContext.SetLogLevel(Logging.LogLevel.Info);
                }
            }
            if (TraceRequestContent)
            {
                logContext.EnableRequestContentTracing();
            }
            if (TraceResponseContent)
            {
                logContext.EnableResponseContentTracing();
            }
        }
    }
}
