﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace ALT.Common.Logging
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class WebLogHttpFilterAttribute : AuthorizationFilterAttribute
    {
        public SetLogLevel LogLevel { get; set; }
        public bool TraceRequestContent { get; set; }
        public bool TraceResponseContent { get; set; }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var logContext = WebLoggerModule.GetCurrentLogContext();
            if (logContext == null)
                return;

            if (LogLevel != SetLogLevel.NotSet)
            {
                logContext.SetLogLevel(LogLevel.ToLogLevel());
            }
            else
            {
                if (actionContext.ControllerContext.Request.Method != System.Net.Http.HttpMethod.Get)
                {
                    logContext.SetLogLevel(Logging.LogLevel.Info);
                }
            }
            if (TraceRequestContent)
            {
                logContext.EnableRequestContentTracing();
            }
            if (TraceResponseContent)
            {
                logContext.EnableResponseContentTracing();
            }
        }
    }
}
