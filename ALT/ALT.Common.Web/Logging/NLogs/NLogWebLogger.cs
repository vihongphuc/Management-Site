﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ALT.Common.Logging;
using ALT.Exceptions;
using System.IO;

namespace ALT.Common.Logging.NLogs
{
    public class NLogWebLogger : IWebLogger
    {
        private NlogLogger logger;

        public NLogWebLogger()
        {
            logger = new NlogLogger(NLogLoggerName);
        }

        public string Name { get { return "NLog"; } }
        public LogLevel GetCurrentLogLevel()
        {
            return WebLoggers.GetLogLevel();
        }

        public void Dispose()
        {

        }

        public IWebLoggerContext CreateWebLoggerContext(WebLogContext context)
        {
            return new NLogWebLogContext();
        }

        public async Task Log(WebLogContext context)
        {
            if (!context.LoggingEnabled) return;

            Func<string> fn = () => $"{context.ID} {(int)context.Status} {context.Status} {context.Request.HttpMethod} {context.Controller} {context.Action} {context.TotalMilliseconds}ms {context.StartTime.ToString("yyyy-MM-dd HH:mm:ss.fffffff")} -> {context.EndTime.ToString("yyyy-MM-dd HH:mm:ss.fffffff")} {context.Request.RawUrl} {context.IP}";

            if (context.Exceptions.Count > 0)
            {
                var aggEx = new AggregateException(context.Exceptions);

                switch (context.LogLevel)
                {
                    case LogLevel.Debug:
                        logger.Debug(fn, aggEx);
                        break;
                    case LogLevel.Error:
                        logger.Error(fn, aggEx);
                        break;
                    case LogLevel.Fatal:
                        logger.Fatal(fn, aggEx);
                        break;
                    case LogLevel.Info:
                        logger.Info(fn, aggEx);
                        break;
                    case LogLevel.Trace:
                        logger.Trace(fn, aggEx);
                        break;
                    case LogLevel.Warn:
                        logger.Warn(fn, aggEx);
                        break;
                }
            }
            else
            {
                switch (context.LogLevel)
                {
                    case LogLevel.Debug:
                        logger.Debug(fn);
                        break;
                    case LogLevel.Error:
                        logger.Error(fn);
                        break;
                    case LogLevel.Fatal:
                        logger.Fatal(fn);
                        break;
                    case LogLevel.Info:
                        logger.Info(fn);
                        break;
                    case LogLevel.Trace:
                        logger.Trace(fn);
                        break;
                    case LogLevel.Warn:
                        logger.Warn(fn);
                        break;
                }
            }

            await Task.FromResult(0);
        }

        private const string NLogLoggerName = "Slotty";
    }
}
