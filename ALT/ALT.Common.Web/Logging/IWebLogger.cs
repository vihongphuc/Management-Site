﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ALT.Common.Logging
{
    public interface IWebLogger : IDisposable
    {
        string Name { get; }
        LogLevel GetCurrentLogLevel();

        Task Log(WebLogContext context);
        IWebLoggerContext CreateWebLoggerContext(WebLogContext context);
    }
}
