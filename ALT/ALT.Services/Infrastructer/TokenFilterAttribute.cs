﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace ALT.Services.Infrastructer
{
    using ATL.Common.Redis;
    using Common.Logics;
    using Common;

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class TokenFilterAttribute : AuthorizationFilterAttribute
    {
        private static readonly string DefaultTokenKey = "t";

        public bool Required { get; set; }
        public bool AllowRobot { get; set; }
        public string Token { get; set; }
        public string TokenKey { get; set; }

        public TokenFilterAttribute()
        {
            TokenKey = DefaultTokenKey;
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            OnAuthorizationAsync(actionContext, CancellationToken.None).Wait();
        }

        public override async Task OnAuthorizationAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            var requetToken = actionContext.Request.GetQueryNameValuePairs()
                .Where(c => String.Equals(c.Key, TokenKey, StringComparison.OrdinalIgnoreCase))
                .Select(c => c.Value)
                .FirstOrDefault();

            if (!Required && string.IsNullOrEmpty(requetToken))
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Invalid Access");
                return;
            }

            var isRobot = PersonLogic.IsRobot(requetToken);

            if (isRobot)
            {
                if (false == AllowRobot)
                {
                    actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Invalid Access");
                    return;
                }
            }
            var tokenServer = RedisManager.GetDataBase(RedisConfigurations.TokenServer);
            var tokenKey = KeyDefine.GetPersonLoginKey(requetToken);
            var tokenValues = await tokenServer.HashGetAllAsync(tokenKey);
            if (tokenValues != null && tokenValues.Length > 0)
            {
                if (true == Required)
                {
                    actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Invalid Access");
                    return;
                }
            }

            actionContext.Request.Properties[RequestProperties.CurrentTokenKey] = requetToken;
            if (isRobot)
            {
                actionContext.Request.Properties[RequestProperties.CurrentRobotTokenKey] = requetToken;
            }

            foreach (var item in tokenValues)
            {
                if (item.Name == RequestProperties.CurrentPersonOCodeKey)
                {
                    actionContext.Request.Properties[RequestProperties.CurrentPersonOCodeKey] = (string)item.Value;
                }
                else if (item.Name == RequestProperties.CurrentPersonUplineOCodeKey)
                {
                    actionContext.Request.Properties[RequestProperties.CurrentPersonUplineOCodeKey] = (string)item.Value;
                }
                else if (item.Name == RequestProperties.CurrentPersonTypeKey)
                {
                    actionContext.Request.Properties[RequestProperties.CurrentPersonTypeKey] = (string)item.Value;
                }
                else if (item.Name == RequestProperties.UplineType)
                {
                    actionContext.Request.Properties[RequestProperties.UplineType] = (string)item.Value;
                }
                else if (item.Name == RequestProperties.CurrentSubAccountOCodeKey)
                {
                    actionContext.Request.Properties[RequestProperties.CurrentSubAccountOCodeKey] = (string)item.Value;
                }
            }

        }
    }
}