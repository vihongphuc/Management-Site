﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Services.Infrastructer
{
    using Microsoft.Practices.Unity;
    public class DependencyResolverConfiguration
    {
        private IUnityContainer containner;
        public DependencyResolverConfiguration(IUnityContainer _container)
        {
            this.containner = _container;
        }

        public object GetSerive(Type serviceType)
        {
            try
            {
                return this.containner.Resolve(serviceType);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviveType)
        {
            try
            {
                return this.containner.ResolveAll(serviveType);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
