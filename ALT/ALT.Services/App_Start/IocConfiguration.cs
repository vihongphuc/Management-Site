﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[assembly: System.Web.PreApplicationStartMethod(typeof(ALT.Services.IocConfiguration), "LoadingConfig")]
namespace ALT.Services
{
    using Microsoft.Practices.Unity;
    using System.Web.Mvc;
    using Infrastructer;
    using ALT.Common.Configuration;
    using System.Net.Http;
    using ALT.Common;
    using Data.Entityframework;
    using System.Data.Entity;

    using Microsoft.Practices.Unity;
    using System.Web.Http;
    using Microsoft.Practices.Unity.WebApi;

    using ATL.Common.Redis;
    using Configurations;
    using ALT.Common.Queue;
    using System.IO;
    using System.Threading.Tasks;
    using ALT.Common.Logging;

    public static class IocConfiguration
    {
        private static readonly int DefaultTokenTimeout = 2 * 60 * 60; // seconds
        private static readonly int DefaultTokenDatabase = -1;
        private static readonly int DefaultCacheTimeout = 24 * 60 * 60; // seconds
        private static readonly int DefaultCacheDatabase = -1;
        private static readonly int DefaultRequestTokenTimeOut = 10 * 60;

        public static void ConfigurationIocContainer()
        {
            IUnityContainer container = new UnityContainer();
            RegisterServices(container);

            RegisterQueue(container);

            RegisRedisActivation(container);

            // register dependency resolver for WebAPI RC
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }

        public static void RegisRedisActivation(IUnityContainer container)
        {
            var tokenConnection = RedisManager.ConnectServer(RedisConfigurations.TokenServer).Result;
            var tokenDataBase = ConfigurationManager.Get<int>(RedisConfigurations.Group, RedisConfigurations.TokenDataBase, DefaultTokenDatabase);
            container.RegisterInstance(typeof(StackExchange.Redis.IDatabase), RedisConfigurations.TokenServer, tokenConnection.GetDatabase(tokenDataBase, null), new HierarchicalLifetimeManager());

            var playerConnection = RedisManager.ConnectServer(RedisConfigurations.PlayerServer).Result;
            var playerDataBase = ConfigurationManager.Get<int>(RedisConfigurations.Group, RedisConfigurations.PlayerDataBase, DefaultTokenDatabase);
            container.RegisterInstance(typeof(StackExchange.Redis.IDatabase), RedisConfigurations.PlayerServer, tokenConnection.GetDatabase(playerDataBase, null), new HierarchicalLifetimeManager());
        }

        public static void RegisterServices(IUnityContainer container)
        {
            ALT.Data.Entityframework.ConnectionStringBuilder.InitConnectionString();
            container.RegisterType<ALTEntities>(
                    //new Microsoft.Practices.Unity.HierarchicalLifetimeManager(),
                    new InjectionConstructor(Data.Entityframework.ConnectionStringBuilder.ConnectionString)
               );
            container.RegisterInstance<ICommonLogger>(new DetailWebLogger(WebLoggerModule.GetCurrentLogContext()));

            //container.RegisterType<IService, Service>();
            //container.RegisterType<IService, Service>(
            //    new InjectionConstructor(
            //        param1,
            //        param2
            //        )
            //    );

        }

        public static void RegisterQueue(IUnityContainer container)
        {
            var rabbitMQConnectionSetup = RabbitMQConnection.CreateRabbitMQConnectionSetup();
            var connection = new RabbitMQPooledConnection(rabbitMQConnectionSetup);
            container.RegisterInstance<IQueueConnection>(connection);

            var slotGameTransactionUpdateQueueName = ConfigurationManager.Get(ApiConfigurations.QueueGroup, ApiConfigurations.SlotGameTransactionQueueName, string.Empty);
            if (String.IsNullOrWhiteSpace(slotGameTransactionUpdateQueueName))
                throw new InvalidOperationException("ALT Game Transaction Update Queue Name is not set");

            var exchangeName = ConfigurationManager.Get(ApiConfigurations.QueueGroup, ApiConfigurations.SlotGameTransactionExchangeName, string.Empty);

            var queuePublisher = connection.CreatePublisher<ALT.Common.Models.GameTransactionReq[]>(slotGameTransactionUpdateQueueName, exchangeName);
            container.RegisterInstance<IQueuePublisher<ALT.Common.Models.GameTransactionReq[]>>(queuePublisher, new HierarchicalLifetimeManager());
        }

        public static void LoadingConfig()
        {
            InitConfigurations();
        }

        private static void InitConfigurations()
        {
            var configPath = System.Web.Hosting.HostingEnvironment.MapPath(DefaultConfigPath);
            var configFile = Path.Combine(configPath, DefaultConfigFileName);
            if (File.Exists(configFile))
            {
                fileConfigStore = new FileConfigurationStore(configFile);
                ConfigurationManager.ApplyConfiguration(fileConfigStore);
            }
        }

        private static HttpClient InitilizeHttpClient()
        {
            var defaultTimeOut = 30;
            var baseURL = ConfigurationManager.Get<string>(ServiceConfigurations.Group, ServiceConfigurations.BaseUrl, string.Empty);
            var timeOut = ConfigurationManager.Get<int>(ServiceConfigurations.Group, ServiceConfigurations.TimeOut, defaultTimeOut);
            if (string.IsNullOrEmpty(baseURL))
            {
                throw new Exception("Must be set BaseURL.");
            }
            var httpClient = new HttpClient()
            {
                BaseAddress = new Uri(baseURL),
                Timeout = new TimeSpan(0, timeOut, 0)
            };

            return httpClient;
        }


        private static FileConfigurationStore fileConfigStore;

        private static FileSystemWatcher fswDynamicConfig;
        private static FileConfigurationStore fileDynamicConfigStore;

        private static object syncConfigLock = new object();
        private static bool updatingConfiguration;
        private static DateTime lastUpdate;


        private const string DefaultConfigPath = "~/";
        private const string DefaultConfigFileName = "configs.ini";
        private const string DefaultDynamicConfigFileName = "dynamic-configs.ini";
        private const int DefaultIDGeneratorScope = 1;
        private const int DefaultHandleConfigurationUpdatedDelay = 1000;
    }
}