﻿using ALT.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ALT.Services.Configurations
{
    public class ApiConfigurations
    {
        public static string Group = ServiceConfigurations.Group;

        public const string WebGamePlayerBroadcastURL = "WebGamePlayerBroadcastURL";
        public const string IsUsedQueueTransaction = "IsUsedQueueTransaction";

        public static string QueueGroup = ALT.Common.Queue.QueueConfigurations.Group;
        public static readonly string SlotGameTransactionQueueName = ALT.Common.Queue.QueueConfigurations.SlotGameTransactionQueueName;
        public static readonly string SlotGameTransactionExchangeName = ALT.Common.Queue.QueueConfigurations.SlotGameTransactionExchangeName;

        public static readonly string RetryHeaderArgument = "HeaderArgument";
        public static readonly string DefaultRetryHeaderArgumentValue = "EventType";
    }
}