﻿using ALT.Common.Logging;
using ALT.Common.Web;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace ALT.Service.Filters
{
    public class CommonApiExceptionFilterAttribute : CommonExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (HandleDbValidationException(actionExecutedContext))
            {
                return;
            }

            base.OnException(actionExecutedContext);
        }

        private static bool HandleDbValidationException(HttpActionExecutedContext actionExecutedContext)
        {
            var dbValidationException = actionExecutedContext.Exception as DbEntityValidationException;
            if (dbValidationException != null)
            {
                var logContext = WebLoggerModule.GetCurrentLogContext();
                if (logContext != null)
                {
                    var dbEntityValidationError = dbValidationException.EntityValidationErrors.FirstOrDefault();
                    var entityValidationError = dbEntityValidationError.ValidationErrors.FirstOrDefault();

                    logContext.AddDetailItem(LogLevel.Error, String.Format("Entity Validation Error: {0}: {1}", entityValidationError.PropertyName, entityValidationError.ErrorMessage), null);
                }
            }

            return false; // don't handle at all
        }

    }
}