﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ALT.Services.Controllers.Api
{
    using System.Data.Entity;
    using System.Threading.Tasks;
    using ALT.Common.Models;
    using System.Net.Http.Formatting;
    using Data.Entityframework;
    using ALT.Services.Common;
    using ALT.Common.Utilities;
    using ALT.Common;
    using ATL.Common.Redis;
    using StackExchange.Redis;
    using Microsoft.Practices.Unity;
    using ALT.Common.Configuration;
    using Configurations;
    using Infrastructer;

    //[TokenFilterAttribute]
    [RoutePrefix("api/game-budget")]
    public class GameBudgetController : ApiController
    {
        [HttpPost, Route("")]
        public async Task<GameBudgetResp> Add(GameBudgetReq request)
        {
            var gameBudgetResp = new GameBudgetResp()
            {
                GameCode = request.GameCode
            };
            var success = true;

            ErrorStatuses.ThrowBadRequest(Request, $"Invalid request type: {request.Type}.", when: !GameBudgetTypeArray.Values.Any(c => c == request.Type));

            var dbGame = await this.altDBContext.Games.Where(c => c.Code == request.GameCode).FirstOrDefaultAsync();
            success = dbGame != null;
            if (success)
            {
                try
                {
                    var redisKey = KeyDefine.GetBudgetKey(request.Type.ToString());
                    var redisHashKey = KeyDefine.GetBudgetHashKey(request.GameCode);
                    success = await this.playerServer.HashSetAsync(redisKey, redisHashKey, request.Points, when: When.NotExists);
                    if (success)
                    {
                        gameBudgetResp.GameName = dbGame.Name;
                        gameBudgetResp.Type = request.Type;
                        gameBudgetResp.Points = request.Points;
                    }
                }
                catch
                {
                    success = false;
                }
            }

            return gameBudgetResp;
        }

        [HttpPut, Route("update-by-gamecode")]
        public async Task<bool> Update(string gamecode, GameBudgetReq request)
        {
            var success = true;
            ErrorStatuses.ThrowBadRequest(Request, $"GameCode cannot be null.", when: string.IsNullOrEmpty(gamecode));
            ErrorStatuses.ThrowBadRequest(Request, $"Invalid request type: {request.Type}.", when: !GameBudgetTypeArray.Values.Any(c => c == request.Type));
            try
            {
                var redisKey = KeyDefine.GetBudgetKey(request.Type.ToString());
                var redisHashKey = KeyDefine.GetBudgetHashKey(gamecode);
                await this.playerServer.HashSetAsync(redisKey, redisHashKey, request.Points, when: When.Always);
            }
            catch
            {
                success = false;
            }

            return success;
        }

        [HttpGet, Route("")]
        public async Task<GameBudgetResp[]> GetAll(GameBudgetType type, string gameCode)
        {
            ErrorStatuses.ThrowBadRequest(Request, $"Invalid request type: {type}.", when: !GameBudgetTypeArray.Values.Any(c => c == type));
            //ErrorStatuses.ThrowBadRequest(Request, $"GameCode cannot be null.", when: string.IsNullOrEmpty(gameCode));
            GameBudgetResp[] gameBudgetResps = new GameBudgetResp[] { };
            var qGames = this.altDBContext.Games.AsQueryable();
            if (!string.IsNullOrEmpty(gameCode))
            {
                qGames = qGames.Where(c => c.Code == gameCode).AsQueryable();
            }
            var dbGames = await qGames.OrderBy(c => c.Code).Select(c => new { c.Code, c.Name }).OrderBy(c => c.Code).ToArrayAsync();

            var redisKey = KeyDefine.GetBudgetKey(type.ToString());
            var redisHashKeys = dbGames.Select(c => (RedisValue)KeyDefine.GetBudgetHashKey(c.Code)).ToArray();
            var redisGameBudgetRedisVals = await this.playerServer.HashGetAsync(redisKey, redisHashKeys);
            //update lai cho nay nha anh phuc.
            gameBudgetResps = redisGameBudgetRedisVals.Select((c, inx) =>
              {
                  var gameBudget = new GameBudgetResp();
                  if (c.HasValue == true && inx < dbGames.Length)
                  {
                      var gameInfo = dbGames[inx];
                      if (gameInfo != null)
                      {
                          gameBudget.GameCode = gameInfo.Code;
                          gameBudget.GameName = gameInfo.Name;
                          gameBudget.Type = type;
                          gameBudget.Points = (long)(Convert.ToDouble(c));
                      }
                  }
                  return gameBudget;
              }).ToArray();

            return gameBudgetResps.Where(c => !string.IsNullOrEmpty(c.GameCode)).ToArray();
        }

        public GameBudgetController(ALTEntities altDBContext, [Dependency(RedisConfigurations.PlayerServer)] IDatabase playerServer)
        {
            this.altDBContext = altDBContext;
            this.playerServer = playerServer;
        }

        private readonly ALTEntities altDBContext;
        private readonly IDatabase playerServer;
    }
}