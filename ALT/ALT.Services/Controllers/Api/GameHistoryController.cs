﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ALT.Services.Controllers.Api
{
    using System.Data.Entity;
    using System.Threading.Tasks;
    using ALT.Common.Models;
    using System.Net.Http.Formatting;
    using Data.Entityframework;
    using ALT.Services.Common;
    using ALT.Common.Utilities;
    using ALT.Common;
    using ATL.Common.Redis;
    using StackExchange.Redis;
    using Microsoft.Practices.Unity;
    using ALT.Common.Helpers;

    [RoutePrefix("api/game-history")]
    public class GameHistoryController : ApiController
    {
        [HttpGet, Route("")]
        public async Task<PagedList<GameHistoryResp>> GetGameHistory(string Username, DateTime StartDate, DateTime EndDate,
                                                                                string GameOCode, int PageIndex, int ItemPerPage, int TotalItems = 25)
        {
            PagedList<GameHistoryResp> gameTransactionHistories = new PagedList<GameHistoryResp>() { };

            var dbPerson = await this.altDBContext.People.Where(c => c.Username == Username.ToUpper()).FirstOrDefaultAsync();
            ErrorStatuses.ThrowBadRequest(Request, $"Person does not found.", when: dbPerson == null);

            StartDate = StartDate.Date;
            EndDate = EndDate.Date.AddDays(1);

            Dictionary<int, Game> dtGame;
            var qrGame = this.altDBContext.Games;
            if (!string.IsNullOrEmpty(GameOCode))
            {
                var gameID = 0L;
                dtGame = new Dictionary<int, Game> { };

                if (OCode.TryGetInt64(GameOCode, out gameID))
                {
                    dtGame.Add(Convert.ToInt32(gameID), await qrGame.Where(c => c.ID == gameID).FirstOrDefaultAsync());
                }
            }
            else
            {
                dtGame = await qrGame.ToDictionaryAsync(c => c.ID);
            }

            ErrorStatuses.ThrowBadRequest(Request, $"Game does not found.", when: dtGame.Count() <= 0);

            var gameTrans = this.altDBContext.GameTransactions.Where(c => c.PersonID == dbPerson.ID)
                                                              .Where(c => c.Time >= StartDate && c.Time < EndDate)
                                                              .Where(c => dtGame.Keys.Contains(c.GameID));
            var totalGameTrans = await gameTrans.CountAsync();
            totalGameTrans = Math.Min(totalGameTrans, TotalItems);
            gameTrans = gameTrans.OrderByDescending(c => c.Ticks).Skip((PageIndex - 1) * ItemPerPage).Take(ItemPerPage);
            var res = await gameTrans.ToArrayAsync();
            gameTransactionHistories.PageList = res.Select(c => new GameHistoryResp
            {
                GameOCode = OCode.Get(c.GameID),
                GameCode = dtGame[c.GameID].Code,
                GameName = dtGame[c.GameID].Name,

                Type = c.Type,
                Amount = c.Amount,
                Result = c.Result,
                StartBalance = c.StartBalance,
                EndBalance = c.EndBalance,
                PointRate = c.PointRate ?? 0m,
                GameTransactionOCode = OCode.Get(c.ID),
                Description = c.Description,
                Details = c.Detail,
                Time = c.Time.Value,
                Ticks = c.Ticks.Value
            }).ToArray();

            gameTransactionHistories.TotalPages = Convert.ToInt32(Math.Ceiling((decimal)totalGameTrans / ItemPerPage));
            gameTransactionHistories.CurrentPage = PageIndex;
            gameTransactionHistories.TotalItemPerPage = ItemPerPage;

            return gameTransactionHistories;
        }

        public GameHistoryController(ALTEntities altDBContext)
        {
            this.altDBContext = altDBContext;
        }
        private readonly ALTEntities altDBContext;
    }
}