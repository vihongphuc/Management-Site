﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ALT.Services.Controllers.Api
{
    using Infrastructer;
    using Data.Entityframework;
    using System.Threading.Tasks;
    using ALT.Common.Models.Models;
    using ALT.Common.Helpers;
    using ALT.Common;
    using Common;
    using System.Data.Entity;
    using ALT.Common.Models;
    using ALT.Common.Configuration;
    using Configurations;

    [TokenFilterAttribute]
    [RoutePrefix("api/reports")]
    public class ReportController : ApiController
    {
        public ReportController(ALTEntities altDBContext)
        {
            this.altDBContext = altDBContext;
        }

        [HttpGet, Route("{personOCode}/transaction-detail")]
        public async Task<PagedList<TransactionDetailResp>> GetTransactionDetails(string personOCode, string betTypeItemOCode,
                                                                                  string gameOCode, string currencyCode, string Username, DateTime startDate,
                                                                                  DateTime endDate, int pageIndex, int itemPerPage)
        {
            PagedList<TransactionDetailResp> gameTransactionDetails = new PagedList<TransactionDetailResp>() { };

            long personID;
            if (OCode.TryGetInt64(personOCode, out personID))
            {
                var qrPerson = this.altDBContext.People as IQueryable<Person>;

                if (!String.IsNullOrEmpty(Username))
                {
                    var currentUsername = await this.altDBContext.People.Where(c => c.ID == personID).Select(c => new { c.Username, c.Type }).FirstOrDefaultAsync();
                    qrPerson = qrPerson.Where(c => (c.Username.StartsWith(currentUsername.Username) || currentUsername.Type == PersonTypeString.Admin) && c.Username.StartsWith(Username.ToUpper().Trim().ToString()));
                }
                else
                {
                    qrPerson = qrPerson.Where(c => c.UplineID == personID);
                }

                var personDownlines = await qrPerson.ToArrayAsync();
                ErrorStatuses.ThrowBadRequest(Request, $"Person does not found.", when: personDownlines == null);

                Dictionary<int, Game> dtGame;
                var qrGame = this.altDBContext.Games;

                if (!string.IsNullOrEmpty(gameOCode))
                {
                    var gameID = 0L;
                    dtGame = new Dictionary<int, Game> { };

                    if (OCode.TryGetInt64(gameOCode, out gameID))
                    {
                        dtGame.Add(Convert.ToInt32(gameID), await qrGame.Where(c => c.ID == gameID).FirstOrDefaultAsync());
                    }
                }
                else
                {
                    dtGame = await qrGame.ToDictionaryAsync(c => c.ID);
                }

                ErrorStatuses.ThrowBadRequest(Request, $"Game does not found.", when: dtGame.Count() <= 0);

                var username = personDownlines.ToDictionary(c => c.ID, c => c.Username);
                var dtDownlineUsers = personDownlines.Where(c => c.Type == PersonTypeString.Member).Select(c => c.ID).ToArray();

                var gameTrans = this.altDBContext.GameTransactions.Where(c => dtDownlineUsers.Contains(c.PersonID))
                                                                  .Where(c => c.Time >= startDate && c.Time <= endDate)
                                                                  .Where(c => c.CurrencyCode == currencyCode)
                                                                  .Where(c => dtGame.Keys.Contains(c.GameID));

                var totalGameTrans = await gameTrans.CountAsync();
                gameTrans = gameTrans.OrderByDescending(c => c.Ticks)
                                     .Skip((pageIndex - 1) * itemPerPage)
                                     .Take(itemPerPage);

                var res = await gameTrans.ToArrayAsync();

                gameTransactionDetails.PageList = res.Select(c => new TransactionDetailResp
                {
                    PersonOCode = OCode.Get(personID),
                    GameOCode = OCode.Get(c.GameID),
                    GameCode = dtGame[c.GameID].Code,
                    GameName = dtGame[c.GameID].Name,
                    GameCategory = dtGame[c.GameID].GameCategory.Name,
                    CurrencyCode = c.CurrencyCode,
                    Username = username[c.PersonID],
                    Type = c.Type,
                    Amount = c.Amount,
                    Result = c.Result,
                    StartBalance = c.StartBalance,
                    EndBalance = c.EndBalance,
                    PointRate = c.PointRate ?? 0m,
                    GameTransactionOCode = OCode.Get(c.ID),
                    Details = c.Detail,
                    Time = c.Time.Value
                }).ToArray();

                gameTransactionDetails.TotalPages = Convert.ToInt32(Math.Ceiling((decimal)totalGameTrans / itemPerPage));
                gameTransactionDetails.CurrentPage = pageIndex;
                gameTransactionDetails.TotalItemPerPage = itemPerPage;
            }

            return gameTransactionDetails;
        }

        [HttpGet, Route("{personOCode}/daily-win-loss-report")]
        public async Task<PagedList<WinLossResp>> GetDailyWinLossReport(string personOCode, string Username, string CurrencyCode, DateTime startDate,
                                                                                  DateTime endDate, int pageIndex, int itemPerPage)
        {
            PagedList<WinLossResp> getDailyWinLossReports = new PagedList<WinLossResp>() { };

            long personID;

            if (OCode.TryGetInt64(personOCode, out personID))
            {
                var qrPerson = this.altDBContext.People as IQueryable<Person>;

                var uplineCurrencyOCode = qrPerson.Where(c => c.ID == personID).Select(c => c.CurrencyCodes).FirstOrDefault();

                if (!String.IsNullOrEmpty(Username))
                {
                    var currentUsername = await this.altDBContext.People.Where(c => c.ID == personID).Select(c => new { c.Username, c.Type }).FirstOrDefaultAsync();
                    qrPerson = qrPerson.Where(c => (c.Username.StartsWith(currentUsername.Username) || currentUsername.Type == PersonTypeString.Admin) && c.Username.StartsWith(Username.ToUpper().Trim().ToString()));
                }
                else
                {
                    qrPerson = qrPerson.Where(c => c.UplineID == personID);
                    var qrPersonCurrentID = qrPerson.Where(c => c.ID == personID);
                    qrPerson = qrPerson.Concat(qrPersonCurrentID);
                }

                var dtDownlineUsers = await qrPerson.ToArrayAsync();
                ErrorStatuses.ThrowBadRequest(Request, $"Person does not found.", when: dtDownlineUsers == null);

                var totalNewUsername = dtDownlineUsers.Where(c => c.CreatedDate >= startDate && c.CreatedDate <= endDate).Count();
                var dtDownlineUsersByID = dtDownlineUsers.Select(c => c.ID);

                var selectFieldDownline = dtDownlineUsers.ToDictionary(c => c.ID, c => Tuple.Create<long, string, string, string>(c.ID, c.Username, c.Type, c.CurrencyCodes));

                List<WinLossResp> winLossGroupByTime = new List<WinLossResp>();

                foreach (var itemPerson in dtDownlineUsersByID)
                {
                    if (DateTime.Now.Date == startDate.Date && selectFieldDownline[itemPerson].Item3 == PersonTypeString.Member)
                    {
                        var todayGameTransaction = this.altDBContext.GameTransactions.Where(c => c.PersonID == itemPerson)
                                                                                 .Where(c => c.Time >= startDate && c.Time <= endDate)
                                                                                 .GroupBy(c => c.CurrencyCode)
                                                                                 .ToArray();
                        getDailyWinLossReports.PageList = todayGameTransaction.Select(cc => new
                        {
                            TotalUsername = dtDownlineUsers.Count(),
                            TotalNewUserBet = totalNewUsername,
                            Time = startDate.Date,
                            TotalAmount = cc.Sum(ccc => ccc.Amount * ConvertCurrencyRate(cc.Key, CurrencyCode)),
                            TotalResult = cc.Sum(ccc => ccc.Result * ConvertCurrencyRate(cc.Key, CurrencyCode)),
                            TotalWinLoss = cc.Sum(ccc => (ccc.Result - ccc.Amount) * ConvertCurrencyRate(cc.Key, CurrencyCode))
                        }).GroupBy(cc => cc.Time).Select(ccc => new WinLossResp
                        {
                            Time = ccc.Key,
                            TotalUsername = dtDownlineUsers.Count(),
                            TotalNewUserBet = totalNewUsername,
                            TotalAmount = ccc.Sum(cccc => cccc.TotalAmount),
                            TotalResult = ccc.Sum(cccc => cccc.TotalResult),
                            TotalWinLoss = ccc.Sum(cccc => cccc.TotalWinLoss)
                        }).ToArray();
                    }
                    else
                    {
                        var typeNameID = selectFieldDownline[itemPerson].Item3;
                        var olderGameTransactionSummarys = this.altDBContext.GameTransactionSummaries as IQueryable<GameTransactionSummary>;

                        switch (typeNameID)
                        {
                            case PersonTypeString.SuperShareHolder:
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.SuperShareHolderID == itemPerson);
                                break;
                            case PersonTypeString.ShareHolder:
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.ShareHolderID == itemPerson);
                                break;
                            case PersonTypeString.SuperSenior:
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.SuperSeniorID == itemPerson);
                                break;
                            case PersonTypeString.Senior:
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.SeniorID == itemPerson);
                                break;
                            case PersonTypeString.SuperMaster:
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.SuperMasterID == itemPerson);
                                break;
                            case PersonTypeString.Master:
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.MasterID == itemPerson);
                                break;
                            case PersonTypeString.SuperAgent:
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.SuperAgentID == itemPerson);
                                break;
                            case PersonTypeString.Agent:
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.AgentID == itemPerson);
                                break;
                            case PersonTypeString.Member:
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.PersonID == itemPerson);
                                break;
                            default:
                                ErrorStatuses.ThrowBadRequest(Request, $"No Data.");
                                break;
                        }

                        var olderGameTransactionGroupBySummary = olderGameTransactionSummarys
                                                                                            .GroupBy(c => new
                                                                                            {
                                                                                                c.Day,
                                                                                                c.Month,
                                                                                                c.Year,
                                                                                                c.CurrencyCode
                                                                                            }).ToArray();

                        var groupGameTransactionGroupBySummary = olderGameTransactionGroupBySummary.Select(cc => new
                        {
                            Time = new DateTime(cc.Key.Year, cc.Key.Month, cc.Key.Day),
                            TotalAmount = cc.Sum(ccc => ccc.TotalAmount * ConvertCurrencyRate(cc.Key.CurrencyCode, CurrencyCode)),
                            TotalResult = cc.Sum(ccc => ccc.TotalResult * ConvertCurrencyRate(cc.Key.CurrencyCode, CurrencyCode)),
                            TotalWinLoss = cc.Sum(ccc => (ccc.TotalResult - ccc.TotalAmount) * ConvertCurrencyRate(cc.Key.CurrencyCode, CurrencyCode))

                        }).GroupBy(cc => cc.Time).Select(ccc => new WinLossResp
                        {
                            Time = ccc.Key,
                            TotalUsername = dtDownlineUsers.Count(),
                            TotalNewUserBet = totalNewUsername,
                            TotalAmount = ccc.Sum(cccc => cccc.TotalAmount),
                            TotalResult = ccc.Sum(cccc => cccc.TotalResult),
                            TotalWinLoss = ccc.Sum(cccc => cccc.TotalWinLoss),
                        }).Where(c => c.Time >= startDate && c.Time <= endDate).ToList();

                        foreach (var item in groupGameTransactionGroupBySummary)
                        {
                            winLossGroupByTime.Add(item);
                        }
                    }

                }

               var winLossResp = winLossGroupByTime.GroupBy(cc => cc.Time).Select(ccc => new WinLossResp
                {
                    Time = ccc.Key,
                    TotalUsername = dtDownlineUsers.Count(),
                    TotalNewUserBet = totalNewUsername,
                    TotalAmount = ccc.Sum(cccc => cccc.TotalAmount),
                    TotalResult = ccc.Sum(cccc => cccc.TotalResult),
                    TotalWinLoss = ccc.Sum(cccc => cccc.TotalWinLoss),
                }).ToList();

                getDailyWinLossReports.PageList = winLossResp;
                var totalGameTranSums = getDailyWinLossReports.PageList.Count();
                getDailyWinLossReports.TotalPages = Convert.ToInt32(Math.Ceiling((decimal)totalGameTranSums / itemPerPage));
                getDailyWinLossReports.CurrentPage = pageIndex;
                getDailyWinLossReports.TotalItemPerPage = itemPerPage;
            }

            return getDailyWinLossReports;
        }

        [HttpGet, Route("{personOCode}/summary-report")]
        public async Task<PagedList<SummaryResp>> GetSummaryReport(string personOCode, string Username, string CurrencyCode, DateTime startDate,
                                                                                DateTime endDate, int pageIndex, int itemPerPage)
        {
            List<SummaryResp> summary = new List<SummaryResp>();

            PagedList<SummaryResp> getSummaryReports = new PagedList<SummaryResp>() { };
            long personID;

            if (OCode.TryGetInt64(personOCode, out personID))
            {
                var qrPerson = this.altDBContext.People as IQueryable<Person>;
                if (!String.IsNullOrEmpty(Username))
                {
                    var currentUsername = await this.altDBContext.People.Where(c => c.ID == personID).Select(c => new { c.Username, c.Type }).FirstOrDefaultAsync();
                    qrPerson = qrPerson.Where(c => (c.Username.StartsWith(currentUsername.Username.ToUpper()) || currentUsername.Type == PersonTypeString.Admin) && c.Username.StartsWith(Username.ToUpper().Trim()));
                }
                else
                {
                    var qrPersonDownlineID = qrPerson.Where(c => c.UplineID == personID);
                    var qrPersonCurrentID = qrPerson.Where(c => c.ID == personID);
                    qrPerson = qrPersonDownlineID.Concat(qrPersonCurrentID);
                }

                Dictionary<int, Game> dtGame;
                var qrGame = this.altDBContext.Games;

                dtGame = await qrGame.ToDictionaryAsync(c => c.ID);
                ErrorStatuses.ThrowBadRequest(Request, $"Game does not found.", when: dtGame.Count() <= 0);

                var dtDownlineUsers = await qrPerson.ToArrayAsync();

                var dtDownlineUsersByID = dtDownlineUsers.Select(c => c.ID);
                ErrorStatuses.ThrowBadRequest(Request, $"Person does not found.", when: dtDownlineUsers == null);

                var selectFieldDownline = dtDownlineUsers.ToDictionary(c => c.ID, c => Tuple.Create<long, string, string, string>(c.ID, c.Username, c.Type, c.CurrencyCodes));
                GameTransactionSummary[] olderGameTransactionSummary;

                foreach (var itemPerson in dtDownlineUsersByID)
                {
                    if (DateTime.Now.Date == startDate.Date && selectFieldDownline[itemPerson].Item3 == PersonTypeString.Member)
                    {
                        var _selectRateCurrencyOCode = ConvertCurrencyRate(selectFieldDownline[itemPerson].Item4, CurrencyCode);

                        var todayGameTransaction = this.altDBContext.GameTransactions.Where(c => c.PersonID == itemPerson)
                                                                             .Where(c => c.Time >= startDate && c.Time <= endDate)
                                                                             .Where(c => dtGame.Keys.Contains(c.GameID))
                                                                             .GroupBy(c => c.GameID)
                                                                             .ToArray();

                        var summaryResp = todayGameTransaction.Select(cc => new SummaryResp
                        {
                            PersonOCode = OCode.Get(itemPerson),
                            Time = startDate.Date,
                            GameSites = BetTypeItemNames.SlotGame,
                            Type = selectFieldDownline[itemPerson].Item3,
                            Username = selectFieldDownline[itemPerson].Item2,
                            GameName = dtGame[cc.Key].Name,
                            CurrencyCode = selectFieldDownline[itemPerson].Item4,
                            CurrentNamePerson = Member,
                            BetAmount = cc.Sum(ccc => (ccc.Amount)),
                            WinAmount = cc.Sum(ccc => (ccc.Result)),
                            TotalWinLossAmount = cc.Sum(ccc => (ccc.Result - ccc.Amount)),
                            BetAmountRateCurrencyCode = cc.Sum(ccc => ccc.Amount * _selectRateCurrencyOCode),
                            WinAmountRateCurrencyCode = cc.Sum(ccc => ccc.Result * _selectRateCurrencyOCode),
                            TotalWinLossAmountRateCurrencyCode = cc.Sum(ccc => (ccc.Result - ccc.Amount) * _selectRateCurrencyOCode)
                        }).ToList();
                        foreach (var item in summaryResp)
                        {
                            summary.Add(item);
                        }
                    }
                    else
                    {
                        string currentPerson = string.Empty;
                        var typeNameID = selectFieldDownline[itemPerson].Item3;
                        var olderGameTransactionSummarys = this.altDBContext.GameTransactionSummaries as IQueryable<GameTransactionSummary>;

                        switch (typeNameID)
                        {
                            case PersonTypeString.SuperShareHolder:
                                currentPerson = personID == itemPerson ? CurrentPerson : NonMember;
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.SuperShareHolderID == itemPerson);
                                break;
                            case PersonTypeString.ShareHolder:
                                currentPerson = personID == itemPerson ? CurrentPerson : NonMember;
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.ShareHolderID == itemPerson);
                                break;
                            case PersonTypeString.SuperSenior:
                                currentPerson = personID == itemPerson ? CurrentPerson : NonMember;
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.SuperSeniorID == itemPerson);
                                break;
                            case PersonTypeString.Senior:
                                currentPerson = personID == itemPerson ? CurrentPerson : NonMember;
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.SeniorID == itemPerson);
                                break;
                            case PersonTypeString.SuperMaster:
                                currentPerson = personID == itemPerson ? CurrentPerson : NonMember;
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.SuperMasterID == itemPerson);
                                break;
                            case PersonTypeString.Master:
                                currentPerson = personID == itemPerson ? CurrentPerson : NonMember;
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.MasterID == itemPerson);
                                break;
                            case PersonTypeString.SuperAgent:
                                currentPerson = personID == itemPerson ? CurrentPerson : NonMember;
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.SuperAgentID == itemPerson);
                                break;
                            case PersonTypeString.Agent:
                                currentPerson = personID == itemPerson ? CurrentPerson : NonMember;
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.AgentID == itemPerson);
                                break;
                            case PersonTypeString.Member:
                                currentPerson = Member;
                                olderGameTransactionSummarys = olderGameTransactionSummarys.Where(c => c.PersonID == itemPerson);
                                break;
                            default:
                                ErrorStatuses.ThrowBadRequest(Request, $"No Data.");
                                break;
                        }

                        olderGameTransactionSummary = olderGameTransactionSummarys.ToArray();

                        summary = OlderGameTransactionSummary(olderGameTransactionSummary,
                                   itemPerson,
                                   selectFieldDownline,
                                   startDate,
                                   endDate,
                                   summary,
                                   dtGame,
                                   pageIndex,
                                   itemPerPage,
                                   currentPerson,
                                   CurrencyCode);
                    }
                }

                getSummaryReports.PageList = summary;
            }
            return getSummaryReports;
        }

        private List<SummaryResp> OlderGameTransactionSummary(
            GameTransactionSummary[] olderGameTransactionSummary,
            long itemPerson,
            Dictionary<long, Tuple<long, string, string, string>> selectFieldDownline,
            DateTime startDate,
            DateTime endDate,
            List<SummaryResp> summary,
            Dictionary<int, Game> dtGame,
            int pageIndex,
            int itemPerPage,
            string currentPerson,
            string CurrencyCode
            )
        {
            var orderGameTransactionSummary = olderGameTransactionSummary
                                                                        .GroupBy(c => new
                                                                        {
                                                                            c.Year,
                                                                            c.Month,
                                                                            c.Day,
                                                                            c.CurrencyCode
                                                                        })
                                                                        .Select(cc => new SummaryResp
                                                                        {
                                                                            PersonOCode = OCode.Get(itemPerson),
                                                                            Time = new DateTime(cc.Key.Year, cc.Key.Month, cc.Key.Day),
                                                                            GameSites = BetTypeItemNames.SlotGame,
                                                                            CurrencyCode = selectFieldDownline[itemPerson].Item4,
                                                                            BetAmount = cc.Sum(ccc => ccc.TotalAmount),
                                                                            WinAmount = cc.Sum(ccc => ccc.TotalResult),
                                                                            TotalWinLossAmount = cc.Sum(ccc => (ccc.TotalResult - ccc.TotalAmount)),
                                                                            BetAmountRateCurrencyCode = cc.Sum(ccc => ccc.TotalAmount * ConvertCurrencyRate(cc.Key.CurrencyCode, CurrencyCode)),
                                                                            WinAmountRateCurrencyCode = cc.Sum(ccc => ccc.TotalResult * ConvertCurrencyRate(cc.Key.CurrencyCode, CurrencyCode)),
                                                                            TotalWinLossAmountRateCurrencyCode = cc.Sum(ccc => (ccc.TotalResult - ccc.TotalAmount) * ConvertCurrencyRate(cc.Key.CurrencyCode, CurrencyCode))
                                                                        }).Where(c => c.Time >= startDate && c.Time <= endDate)
                                                                          .ToList();

            var orderGameTransactionSummarys = orderGameTransactionSummary.GroupBy(c => c.GameSites)
                                                                        .Select(cc => new SummaryResp
                                                                        {
                                                                            PersonOCode = OCode.Get(itemPerson),
                                                                            Type = selectFieldDownline[itemPerson].Item3,
                                                                            Username = selectFieldDownline[itemPerson].Item2,
                                                                            BetAmount = cc.Sum(ccc => ccc.BetAmount),
                                                                            CurrentNamePerson = currentPerson,
                                                                            GameSites = cc.Key,
                                                                            CurrencyCode = selectFieldDownline[itemPerson].Item4,
                                                                            WinAmount = cc.Sum(ccc => ccc.WinAmount),
                                                                            TotalWinLossAmount = cc.Sum(ccc => ccc.TotalWinLossAmount),
                                                                            BetAmountRateCurrencyCode = cc.Sum(ccc => ccc.BetAmountRateCurrencyCode),
                                                                            WinAmountRateCurrencyCode = cc.Sum(ccc => ccc.WinAmountRateCurrencyCode),
                                                                            TotalWinLossAmountRateCurrencyCode = cc.Sum(ccc => ccc.TotalWinLossAmountRateCurrencyCode)
                                                                        }).OrderByDescending(c => c.Time)
                                                                          .Skip((pageIndex - 1) * itemPerPage)
                                                                          .Take(itemPerPage)
                                                                          .ToList();
            foreach (var item in orderGameTransactionSummarys)
            {
                summary.Add(item);
            }
            return summary;
        }


        public decimal ConvertCurrencyRate(string FromCurrency, string ToCurrency)
        {
            decimal result = 0m;
            var qrCurrencies = this.altDBContext.Currencies as IQueryable<Currency>;

            decimal fromRateCurrency = qrCurrencies.Where(c => c.CurrencyCode == FromCurrency).Select(c => c.Rate).FirstOrDefault();
            decimal toRateCurrency = qrCurrencies.Where(c => c.CurrencyCode == ToCurrency).Select(c => c.Rate).FirstOrDefault();

            result = fromRateCurrency / toRateCurrency;
            return result;
        }

        private static string Member = "Member";
        private static string CurrentPerson = "CurrentPerson";
        private static string NonMember = "Non-Member";

        private readonly ALTEntities altDBContext;

    }
}