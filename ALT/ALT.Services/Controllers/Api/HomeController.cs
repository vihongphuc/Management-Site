﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace ALT.Services.Controllers.Api
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var assembly = Assembly.GetExecutingAssembly();
            Version version = assembly.GetName().Version;

            var respContent = $"Asian Live Tech API ; Name: {assembly.GetName().Name.ToString()}; Verson: {version.ToString()}";

            Response.Headers.Add("link", "rel=\"shortcut icon\" href=\"logo.ico\"  type=\"image/x-icon\" />");
            HttpContext.Response.Headers.Add("link", "rel=\"shortcut icon\" href=\"logo.ico\"  type=\"image/x-icon\" />");

            return Content(respContent);
        }
    }
}
