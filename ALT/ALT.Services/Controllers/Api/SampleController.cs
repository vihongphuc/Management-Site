﻿using ALT.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ALT.Services.Controllers.Api
{

    using System.Data.Entity;
    using System.Threading.Tasks;
    using ALT.Common.Models;
    using System.Net.Http.Formatting;
    using Data.Entityframework;
    using ALT.Services.Common;
    using ALT.Common.Utilities;
    using ALT.Common;
    using ATL.Common.Redis;
    using StackExchange.Redis;
    using Microsoft.Practices.Unity;

    [RoutePrefix("api/users")]
    public class SampleController : ApiController
    {
        [Route("{id:int}"), HttpGet]
        public async Task<string> GetByID(long id)
        {

            using (var trans = this.dbcontext.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
            {
                var person = await this.dbcontext.People.Where(c => c.UplineID.HasValue == false).FirstOrDefaultAsync();
                person.UpdatedBy = "aaa";

                var a = string.Empty;
                if (id == 1)
                {
                    ErrorStatuses.ThrowBadRequest(Request, "Username, Password does not correct.", when: true);
                }
                if (id == 2)
                {
                    var b = "a";
                    var c = int.Parse(b);
                }

                await this.dbcontext.SaveChangesAsync();
                trans.Commit();
            }

            return $"Get User by ID: {id}";
        }

        [Route("{username}"), HttpGet]
        public string GetByUsername(string username)
        {
            return $"Get User by Username: {username}";
        }

        [Route("find/{username}"), HttpGet]
        public string Find(string username)
        {
            return $"Find User by {username}";
        }

        [Route("find-username/{username}/info"), HttpGet]
        public string Search(string username)
        {
            return $"Find User by {username}";
        }

        [Route(""), HttpPost]
        public string AddPerson(string username)
        {
            return "Post";
        }

        [Route(""), HttpPost]
        public string PostPersonByObj(insertOBJ obj)
        {
            return $"Post: {obj.ID}:{obj.username}";
        }

        [Route(""), HttpPut]
        public string UpdateByusername(string username)
        {
            return "Put";
        }

        [Route("{username}/long"), HttpPut]
        public string UpdateByObj(string username, insertOBJ obj)
        {
            return $"Put by username; {username} - obj; {obj.ID}:{obj.username}";
        }

        public SampleController(ALTEntities dbcontext)
        {
            this.dbcontext = dbcontext;
        }

        private readonly ALTEntities dbcontext;
    }

    public class insertOBJ
    {
        public int ID { get; set; }
        public string username { get; set; }
    }
}
