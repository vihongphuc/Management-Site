﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ALT.Services.Controllers.Api
{
    using ALT.Common;
    using ALT.Common.Helpers;
    using ALT.Common.Models;
    using ALT.Common.Models.Models;
    using ALT.Common.Utilities;
    using Common;
    using System.Data.Entity;
    using System.Threading.Tasks;
    using ALT.Data.Entityframework;
    using ATL.Common.Redis;
    using ALT.Common.Configuration;
    using Configurations;
    using Common.Logics;

    public partial class PersonController : ApiController
    {
        [HttpPut, Route("{ocode}")]
        public async Task<bool> UpdatePerson(string ocode, UpdatePersonReq personInfo)
        {
            var result = false;
            ErrorStatuses.ThrowBadRequest(Request, $"Invalid Person info.", when: false == ValidatePersonInfo(personInfo, isUpdate: true));

            var currentOCode = Request.GetCurrentPersonOCode();
            var currentUplineOCode = Request.GetCurrentPersonUplineOCode();
            var currentType = Request.GetCurrentPersonType();
            long currentPersonID = 0L, updatedUplinePersonID;
            var updatedPersonID = OCode.ToInt64(ocode);

            ErrorStatuses.ThrowNotFound(Request, $"Invalid Current Person OCode {currentOCode}.", when: OCode.TryGetInt64(currentOCode, out currentPersonID) == false);
            ErrorStatuses.ThrowNotFound(Request, $"Invalid requested upline Person OCode {personInfo.UplineOcode}.", when: OCode.TryGetInt64(personInfo.UplineOcode, out updatedUplinePersonID) == false && currentType != PersonType.Admin);
            ErrorStatuses.ThrowNotFound(Request, $"Person is Disabled or Suspended.", when: await PersonLogic.CheckUplineStatus(currentPersonID, this.altDBContext) == false);

            var allCheckingPersonIDs = new HashSet<long> { updatedUplinePersonID, updatedPersonID, currentPersonID };
            var dtPeople = await this.altDBContext.People
                                                  .Where(c => allCheckingPersonIDs.Contains(c.ID))
                                                  .ToDictionaryAsync(c => Tuple.Create<long, string>(c.ID, c.CurrencyCodes));

            var dbCurrentPerson = dtPeople.Where(c => c.Key.Item1 == currentPersonID).Select(c => c.Value).FirstOrDefault();
            ErrorStatuses.ThrowNotFound(Request, $"Person cannot be found.", when: dbCurrentPerson == null);

            var updatedPerson = dtPeople.Where(c => c.Key.Item1 == updatedPersonID).Select(c => c.Value).FirstOrDefault();
            ErrorStatuses.ThrowNotFound(Request, $"Person cannot be found.", when: updatedPerson == null);
            ErrorStatuses.ThrowNotFound(Request, $"Invalid username: {personInfo.Username}", when: personInfo.Username != updatedPerson.Username || updatedUplinePersonID != updatedPerson.UplineID);

            var updatedUplinePerson = dtPeople.Where(c => c.Key.Item1 == updatedUplinePersonID).Select(c => c.Value).FirstOrDefault();
            ErrorStatuses.ThrowNotFound(Request, $"Person cannot be found.", when: updatedUplinePerson == null);

            var currentPersonCurrencies = StringHelper.ToArray(updatedUplinePerson.CurrencyCodes);
            ErrorStatuses.ThrowNotFound(Request, $"Currency does not correct.", when: personInfo.Currencies.Any(c => currentPersonCurrencies.Any(cc => cc == c) == false));

            var allCheckingUpdatedCurrencies = new HashSet<string>(personInfo.Currencies);
            ErrorStatuses.ThrowNotFound(Request, $"Person cannot found.", when: allCheckingUpdatedCurrencies.Any(c => updatedUplinePerson.CurrencyCodes.Contains(c)) == false);

            var dbAllCheckingUpdatedCurrencies = await this.altDBContext.Currencies.Where(c => allCheckingUpdatedCurrencies.Contains(c.CurrencyCode))
                                                                 .ToArrayAsync();
            ErrorStatuses.ThrowNotFound(Request, $"Currency does not correct.", when: dbAllCheckingUpdatedCurrencies == null || dbAllCheckingUpdatedCurrencies.Length == 0);

            var dtBalances = await this.altDBContext.PersonBalances.Where(c => allCheckingPersonIDs.Contains(c.PersonID))
                                                                    .ToDictionaryAsync(c => Tuple.Create<long, string>(c.PersonID, c.CurrencyCode));
            ErrorStatuses.ThrowNotFound(Request, $"Person cannot found.", when: dtBalances == null || dtBalances.Count() < dbAllCheckingUpdatedCurrencies.Length);

            var dtMaxCredits = await this.altDBContext.PersonCreditSettings.Where(c => allCheckingPersonIDs.Contains(c.PersonID))
                                                          .ToDictionaryAsync(c => Tuple.Create<long, string>(c.PersonID, c.CurrencyCode), c => c);

            var dtBetSettings = await this.altDBContext.PersonBetSettings.Where(c => allCheckingPersonIDs.Contains(c.PersonID))
                                                      .ToDictionaryAsync(c => Tuple.Create<long, int, string, string>(c.PersonID, c.BetTypeItemID, c.CurrencyCode, c.SettingType), c => c);

            var dtPositionTakings = await this.altDBContext.PositionTakings
                                                            .Where(c => c.PersonID == updatedPersonID || (c.Person.UplineID.HasValue && c.Person.UplineID.Value == updatedPersonID))
                                                            .ToDictionaryAsync(c => Tuple.Create<long, int, string, string>(c.PersonID, c.BetTypeItemID, c.CurrencyCode, c.Type), c => c);

            GameSession activeGameSession = null;
            using (var trans = this.altDBContext.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
            {
                var updatedSuccess = true;

                if (!string.IsNullOrEmpty(personInfo.Password))
                {
                    updatedPerson.Password = Password.CreateHash(personInfo.Password);
                }
                updatedPerson.Firstname = personInfo.Firstname;
                updatedPerson.Lastname = personInfo.Lastname;
                updatedPerson.DisplayName = personInfo.DisplayName;
                updatedPerson.CurrencyCodes = string.Join(Statics.comma.ToString(), personInfo.Currencies);
                updatedPerson.DOB = personInfo.DOB;
                updatedPerson.Email = personInfo.Email;
                updatedPerson.IsExternal = personInfo.External;
                updatedPerson.LimitIP = personInfo.LimitIP;
                updatedPerson.Mobile = personInfo.Mobile;
                updatedPerson.Phone = personInfo.Phone;
                updatedPerson.Status = personInfo.Status.ToString();
                updatedPerson.Type = personInfo.Type.ToString();
                updatedPerson.UpdatedDate = DateTime.Now;
                updatedPerson.UpdatedBy = dbCurrentPerson.Username;
                updatedPerson.CreateAgency = personInfo.CreateAgency;
                updatedPerson.CreateMember = personInfo.CreateMember;
                

                PersonBalance updatedPersonBalance;
                PersonCreditSetting updatedPersonCreditSetting;
                //Remove un-used currency if updated person does not have any downlines.
                if (false == dtPositionTakings.Any(c => c.Key.Item1 != updatedPersonID))
                {
                    foreach (var currency in dbAllCheckingUpdatedCurrencies.Where(c => !personInfo.Currencies.Contains(c.CurrencyCode)))
                    {
                        if (dtBalances.TryGetValue(Tuple.Create<long, string>(updatedPersonID, currency.CurrencyCode), out updatedPersonBalance))
                        {
                            this.altDBContext.Entry(updatedPersonBalance).State = EntityState.Deleted;
                        }

                        if (dtMaxCredits.TryGetValue(Tuple.Create<long, string>(updatedPersonID, currency.CurrencyCode), out updatedPersonCreditSetting))
                        {
                            this.altDBContext.Entry(updatedPersonCreditSetting).State = EntityState.Deleted;
                        }

                        var _unUsedPersonBetSettingByCurrency = dtBetSettings.Where(c => c.Key.Item1 == updatedPersonID)
                                                                        .Where(c => c.Key.Item3 == currency.CurrencyCode)
                                                                        .Select(c => c.Value)
                                                                        .AsEnumerable();
                        if (_unUsedPersonBetSettingByCurrency != null)
                        {
                            this.altDBContext.PersonBetSettings.RemoveRange(_unUsedPersonBetSettingByCurrency);
                        }

                        var _unUsedPersonPTByCurrency = dtPositionTakings.Where(c => c.Key.Item1 == updatedPersonID)
                                                                        .Where(c => c.Key.Item3 == currency.CurrencyCode)
                                                                        .Select(c => c.Value)
                                                                        .AsEnumerable();
                        if (_unUsedPersonPTByCurrency != null)
                        {
                            this.altDBContext.PositionTakings.RemoveRange(_unUsedPersonPTByCurrency);
                        }
                    }
                }

                var _inx = 0;
                PersonBalance updatedUplinePersonBalance;
                foreach (var currency in dbAllCheckingUpdatedCurrencies.Where(c => personInfo.Currencies.Contains(c.CurrencyCode)))
                {
                    if (!updatedSuccess)
                    {
                        break;
                    }

                    if (false == dtBalances.TryGetValue(Tuple.Create<long, string>(updatedPerson.ID, currency.CurrencyCode), out updatedPersonBalance))
                    {
                        updatedPersonBalance = new PersonBalance
                        {
                            PersonID = updatedPerson.ID,
                            CurrencyCode = currency.CurrencyCode,
                            Available = personInfo.PersonBalances.Where(c => c.CurrencyCode == currency.CurrencyCode).Select(c => c.Available ?? 0m).FirstOrDefault(),
                            YesterdayAvailable = 0m,
                            LastUpdatedDate = DateTime.Now,

                            UpdatedBy = dbCurrentPerson.Username,
                            UpdatedDate = DateTime.Now,
                            CreatedBy = dbCurrentPerson.Username,
                            CreatedDate = DateTime.Now
                        };

                        this.altDBContext.PersonBalances.Add(updatedPersonBalance);
                    }

                    ErrorStatuses.ThrowBadRequest(Request, $"Invalid Upline Person Balance.", when: false == dtBalances.TryGetValue(Tuple.Create<long, string>(updatedUplinePerson.ID, currency.CurrencyCode), out updatedUplinePersonBalance) && currentType != PersonType.Admin);

                    AuditContext auParent = null;
                    if (currentType != PersonType.Admin)
                    {
                        auParent = new AuditContext(AuditType.Balance, nameof(UpdatePerson), nameof(PersonController), nameof(UpdatePerson), Request)
                        {
                            PersonID = updatedUplinePerson.ID,
                            RelatedID = updatedUplinePerson.ID,
                            Token = currentOCode,
                            Detail = $"Update balance after adding {updatedUplinePerson.Username}:{updatedUplinePerson.Type}",
                            Before = $"Available: {updatedPersonBalance.Available}",
                            ActionTime = DateTime.Now,

                            CreatedBy = dbCurrentPerson.Username,
                            CreatedTime = DateTime.Now
                        };

                        await auParent.InsertAudit(this.altDBContext);
                        this.altDBContext.Touch(updatedPersonBalance);
                    }

                    var auPerson = new AuditContext(AuditType.Balance, nameof(UpdatePerson), nameof(PersonController), nameof(UpdatePerson), Request)
                    {
                        PersonID = updatedPerson.ID,
                        RelatedID = updatedPerson.ID,
                        Token = currentOCode,
                        ActionTime = DateTime.Now,

                        Detail = $"Update balance after adding {updatedPerson.Username}:{updatedPerson.Type}",
                        CreatedBy = dbCurrentPerson.Username,
                        CreatedTime = DateTime.Now
                    };

                    await auPerson.InsertAudit(this.altDBContext);

                    if (updatedPerson.Type == PersonTypeString.Member)
                    {
                        activeGameSession = await this.altDBContext.GameSessions.Where(c => c.PersonID == updatedPerson.ID)
                                                                                .Where(c => c.FinishedTime.HasValue == false)
                                                                                .FirstOrDefaultAsync();
                    }

                    var transferType = TransferType.Deposit;
                    var _updateAvailableAmount = 0m;
                    if (updatedSuccess)
                    {
                        var _requestUpdateAvailableAmount = personInfo.PersonBalances.Where(c => c.CurrencyCode == currency.CurrencyCode).Select(c => c.Available ?? 0m).FirstOrDefault();
                        var _currentAvailable = 0m;

                        if (updatedPerson.Type == PersonTypeString.Member && activeGameSession != null)
                        {
                            var token = OCode.Get(activeGameSession.ID);
                            var currentPointResult = await RedisScripts.GameState.GetCurrentPoint(token, null);
                            _currentAvailable = currentPointResult.IsNull ? 0m : (long)currentPointResult * (activeGameSession.PointRate ?? 0m);
                        }
                        else
                        {
                            _currentAvailable = updatedPersonBalance.Available;
                        }

                        if (_requestUpdateAvailableAmount >= _currentAvailable)
                        {
                            transferType = TransferType.Deposit;
                            _updateAvailableAmount = _requestUpdateAvailableAmount - _currentAvailable;
                        }
                        else
                        {
                            transferType = TransferType.Withdraw;
                            _updateAvailableAmount = _currentAvailable - _requestUpdateAvailableAmount;
                        }

                        if (_updateAvailableAmount != 0)
                        {
                            ErrorStatuses.ThrowBadRequest(Request, $"Upline does not enough balance.", when: updatedUplinePerson.Type != PersonTypeString.Admin && updatedUplinePersonBalance.Available < _updateAvailableAmount);
                            var lastedAmount = await UpdatePersonBalance(updatedPersonBalance, updatedPerson, auPerson, updatedUplinePersonBalance, updatedUplinePerson, auParent, currency.CurrencyCode, transferType, _updateAvailableAmount, activeGameSession: activeGameSession, isUpdate: true);
                            updatedSuccess = lastedAmount >= 0m;
                            if (updatedSuccess && updatedPersonBalance.Available > 0)
                                updatedSuccess = TransferLogic.AddStatement(this.altDBContext, currency.CurrencyCode, updatedUplinePerson, updatedPerson, transferType.ToString(), transferType == TransferType.Deposit ? TransferTypeString.DepositTo : TransferTypeString.WithdrawFrom, _updateAvailableAmount, DateTime.Now, dbCurrentPerson.Username);
                        }
                    }

                    if (updatedSuccess)
                        updatedSuccess = UpdateMaxCredit(currency.CurrencyCode, updatedPerson, updatedUplinePerson, personInfo.PersonMaxCredits, dtMaxCredits, updatedPersonBalance, dbCurrentPerson.Username, isUpdate: true);

                    if (updatedSuccess)
                        updatedSuccess = UpdateBetSetting(currency.CurrencyCode, updatedPerson, updatedUplinePerson, personInfo.BetSettings, dtBetSettings, updatedUplinePerson.Username, isUpdate: true);

                    if (updatedSuccess)
                        updatedSuccess = UpdatePositionTaking(currency.CurrencyCode, updatedPerson, updatedUplinePerson, personInfo.PositionTakings, dtPositionTakings, updatedUplinePerson.Username, isUpdate: true);

                    if (_inx == personInfo.Currencies.Length - 2)
                    {
                        if (updatedSuccess)
                            updatedSuccess = await UpdatePersonGroups(updatedPerson, dbCurrentPerson, updatedUplinePerson.Username, isUpdate: true);
                    }

                    _inx++;
                }

                if (updatedSuccess)
                {
                    try
                    {
                        await this.altDBContext.SaveChangesAsync();
                        trans.Commit();

                        if (personInfo.Type == PersonType.Member && personInfo.Status != PersonStatusType.Active)
                        {
                            if (activeGameSession != null)
                            {
                                var token = OCode.Get(activeGameSession.ID);
                                try
                                {
                                    using (var httpClient = new HttpClient()
                                    {
                                        BaseAddress = new Uri(this.WebGamePlayerBroadcastURL)
                                    })
                                    {
                                        await httpClient.PostAsync($"broadcast/ChangeStatus?token={token}&popup={true}", null);
                                    }
                                }
                                catch
                                {

                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        var er = ex;
                        result = false;
                    }

                }
                else
                {
                    trans.Rollback();
                }
                result = updatedSuccess;
            }
            return result;
        }

        [HttpPut, Route("{ocode}/change-status")]
        public async Task<bool> ChangeStatus(string ocode, PersonChangeStatusReq personStatus)
        {
            var result = false;

            var auditChangeStatus = new AuditContext(AuditType.Change, nameof(ChangeStatus), nameof(PersonController), nameof(ChangeStatus))
            {
                ActionName = nameof(PersonController),
                ControllerName = nameof(ChangeStatus),
                Detail = "Change person status.",
                ActionTime = DateTime.Now
            };
            long personID;
            if (OCode.TryGetInt64(ocode, out personID))
            {
                var personCurrent = await this.altDBContext.People.Where(c => c.ID == personID).FirstOrDefaultAsync();
                ErrorStatuses.ThrowBadRequest(Request, $"Invalid person of: {ocode}", when: personCurrent == null);
                ErrorStatuses.ThrowBadRequest(Request, "Cannot find the person current", when: personCurrent == null);

                auditChangeStatus.PersonID = personCurrent.ID;
                auditChangeStatus.RelatedID = personCurrent.ID;

                using (var trans = altDBContext.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                {
                    var now = DateTime.Now;
                    personCurrent.Status = personStatus.Status.ToString();
                    personCurrent.UpdatedBy = personCurrent.Username;
                    personCurrent.UpdatedDate = now;

                    auditChangeStatus.CreatedTime = now;
                    auditChangeStatus.CreatedBy = personCurrent.Username;
                    auditChangeStatus.Before = $"old status : { personCurrent.Status}";
                    auditChangeStatus.After = $"new status : { personStatus.Status }";

                    await this.altDBContext.SaveChangesAsync();
                    await auditChangeStatus.InsertAudit(this.altDBContext);
                    trans.Commit();
                    result = true;

                }

                if (result && personCurrent.Type == PersonTypeString.Member && personCurrent.Status != PersonStatusTypeString.Active)
                {
                    var activeGame = await this.altDBContext.GameSessions.Where(c => c.PersonID == personCurrent.ID && c.FinishedTime.HasValue == false).FirstOrDefaultAsync();
                    if (activeGame != null)
                    {
                        var token = OCode.Get(activeGame.ID);
                        try
                        {
                            using (var httpClient = new HttpClient()
                            {
                                BaseAddress = new Uri(this.WebGamePlayerBroadcastURL)
                            })
                            {
                                await httpClient.PostAsync($"broadcast/ChangeStatus?token={token}&popup={true}", null);
                            }
                        }
                        catch
                        {

                        }
                    }
                }
            }
            return result;
        }

        [HttpPut, Route("{ocode}/change-password")]
        public async Task<bool> ResetPassword(string ocode, PersonChangePasswordReq pass)
        {
            bool result = false;
            long personID;
            var auditResetAndChangePassword = new AuditContext(AuditType.Change, nameof(ResetPassword), nameof(PersonController), nameof(ResetPassword))
            {
                ActionTime = DateTime.Now,
                Detail = "Reset password."
            };
            if (OCode.TryGetInt64(ocode, out personID))
            {
                var personCurrent = await this.altDBContext.People.Where(c => c.ID == personID).FirstOrDefaultAsync();

                auditResetAndChangePassword.PersonID = personCurrent.ID;
                auditResetAndChangePassword.RelatedID = personCurrent.ID;

                ErrorStatuses.ThrowNotFound(Request, $"Invalid person of: {ocode}", when: personCurrent == null);
                var oldPassValidated = true;
                if (!string.IsNullOrEmpty(pass.OldPassword))
                {
                    oldPassValidated = Password.ValidatePassword(personCurrent.Password, pass.OldPassword, isHashed: false);
                }
                ErrorStatuses.ThrowNotFound(Request, $"Invalid person of: {ocode}", when: oldPassValidated == false);

                using (var trans = altDBContext.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                {
                    var now = DateTime.Now;
                    personCurrent.Password = Password.CreateHash(pass.NewPassword);
                    personCurrent.UpdatedBy = personCurrent.Username;
                    personCurrent.UpdatedDate = now;

                    auditResetAndChangePassword.CreatedTime = now;
                    auditResetAndChangePassword.CreatedBy = personCurrent.Username;
                    auditResetAndChangePassword.Before = $"old password : { personCurrent.Password}";

                    await this.altDBContext.SaveChangesAsync();
                    await auditResetAndChangePassword.InsertAudit(this.altDBContext);
                    trans.Commit();
                    result = true;
                }
            }

            return result;
        }

        [HttpPut, Route("{ocode}/transfer")]
        public async Task<PersonTransferResp> PersonTransfer(string ocode, PersonTransferReq transfer)
        {
            var currentPersonOCode = Request.GetCurrentPersonOCode();
            PersonTransferResp personTransfer = new PersonTransferResp();
            long personID = 0L;
            ErrorStatuses.ThrowBadRequest(Request, $"Invalid current person ocdode: {currentPersonOCode}", false == OCode.TryGetInt64(currentPersonOCode, out personID));

            Person currentPerson = await this.altDBContext.People.Where(c => c.ID == personID).FirstOrDefaultAsync();
            ErrorStatuses.ThrowBadRequest(Request, $"Invalid current person ocode: {currentPersonOCode}", currentPerson == null);

            var fromPersonID = OCode.ToInt64(ocode);
            var fromPerson = await this.altDBContext.People.Where(c => c.ID == fromPersonID).FirstOrDefaultAsync();
            ErrorStatuses.ThrowBadRequest(Request, $"Invalid transfer person: {ocode}", fromPerson == null);
            ErrorStatuses.ThrowNotFound(Request, $"Person is Disabled or Suspended.", when: await PersonLogic.CheckUplineStatus(fromPersonID, this.altDBContext) == false);

            var toPersonID = fromPerson.UplineID ?? 0L;
            var toPerson = await this.altDBContext.People.Where(c => c.ID == toPersonID).FirstOrDefaultAsync();
            ErrorStatuses.ThrowBadRequest(Request, $"Invalid transfer person: {ocode}", toPerson == null);

            TransferType fromTransferType = TransferType.Deposit;
            TransferType toTransferType = TransferType.DepositTo;
            var fromAmount = 0m;
            var toAmount = 0m;

            if (transfer.Type == TransferType.Deposit)
            {
                fromTransferType = TransferType.Deposit;
                toTransferType = TransferType.DepositTo;
                fromAmount = transfer.Amount;
                toAmount = -transfer.Amount;
            }
            else if (transfer.Type == TransferType.Withdraw)
            {
                fromTransferType = TransferType.Withdraw;
                toTransferType = TransferType.WithdrawFrom;

                fromAmount = -transfer.Amount;
                toAmount = transfer.Amount;
            }
            else
            {
                ErrorStatuses.ThrowBadRequest(Request, $"Transfer Type does not correct.", when: true);
            }

            var auditTransferFrom = new AuditContext(transfer.Type == TransferType.Deposit ? AuditType.Deposit : AuditType.Withdraw, nameof(PersonTransfer), nameof(PersonController), nameof(PersonTransfer))
            {
                PersonID = fromPersonID,
                RelatedID = toPersonID,
                ActionTime = DateTime.Now,
                Detail = "Person transfer amount",

                CreatedBy = currentPerson.Username,
                CreatedTime = DateTime.Now
            };

            var auditTransferTo = new AuditContext(transfer.Type == TransferType.Deposit ? AuditType.DepositTo : AuditType.WithdrawFrom, nameof(PersonTransfer), nameof(PersonController), nameof(PersonTransfer))
            {
                PersonID = toPersonID,
                RelatedID = fromPersonID,
                ActionTime = DateTime.Now,
                Detail = "Person transfer amount",

                CreatedBy = currentPerson.Username,
                CreatedTime = DateTime.Now
            };

            using (var trans = altDBContext.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
            {
                var dbPersonBalances = (await altDBContext.LockPersonBalance(new long[] { fromPersonID, toPersonID }));
                var personBalances = dbPersonBalances.ToDictionary(c => Tuple.Create<long, string>(c.PersonID, c.CurrencyCode));

                var newBalance = 0m;
                var newUplineBalance = 0m;
                PersonBalance fromPersonBalance = new PersonBalance();
                PersonBalance toPersonBalance = new PersonBalance();
                GameSession activeGameSession = null;
                var token = string.Empty;
                var pointRate = 1m;

                if (fromPerson.Type != PersonTypeString.SuperShareHolder)
                {
                    if (!personBalances.TryGetValue(Tuple.Create<long, string>(toPersonID, transfer.CurrencyCode), out toPersonBalance))
                    {
                        ErrorStatuses.ThrowBadRequest(Request, $"Invalid Upline balance.", when: true);
                    }
                }

                if (!personBalances.TryGetValue(Tuple.Create<long, string>(fromPersonID, transfer.CurrencyCode), out fromPersonBalance))
                {
                    ErrorStatuses.ThrowBadRequest(Request, $"Invalid Person balance.", when: true);
                }

                if (fromPerson.Type == PersonTypeString.Member)
                {
                    activeGameSession = await this.altDBContext.LockActiveGameSession(fromPersonID);
                    if (activeGameSession != null)
                    {
                        if (activeGameSession.FinishedTime.HasValue == false)
                        {
                            token = OCode.Get(activeGameSession.ID);
                            pointRate = activeGameSession.PointRate ?? 1m;
                            var currentPointResult = await RedisScripts.GameState.GetCurrentPoint(token, null);
                            if (currentPointResult.IsNull)
                            {
                                ErrorStatuses.ThrowBadRequest(Request, $"Failed to get point of : {token} from redis.", when: true, on: () =>
                                {
                                    if (auditTransferFrom != null)
                                        auditTransferFrom.Detail = $"Failed to get point of : {token} from redis.";
                                    if (auditTransferFrom != null)
                                        auditTransferFrom.Detail = $"Failed to get point of : {token} from redis.";
                                });
                            }

                            var result = await RedisScripts.GameState.ModifyPoint(token, (long)(fromAmount / activeGameSession.PointRate));
                            ErrorStatuses.ThrowBadRequest(Request, "Not enough available balance", when: !result.Success, on: () =>
                            {
                                if (auditTransferFrom != null)
                                    auditTransferFrom.Detail = $"Not enough available balance to cash out, token: {token}";
                                if (auditTransferFrom != null)
                                    auditTransferFrom.Detail = $"Not enough available balance to cash out, token: {token}";
                            });

                            newBalance = result.CurrentPoint * activeGameSession.PointRate.Value;
                            activeGameSession.TransferAmount += fromAmount;

                            auditTransferFrom.Before = $"Available: {(long)currentPointResult * activeGameSession.PointRate}";
                        }
                    }
                    else
                    {
                        ErrorStatuses.ThrowBadRequest(Request, $"Invalid {fromTransferType} amount.", when: ValidateTransferAmount(fromPersonBalance, toPersonBalance, fromTransferType, fromAmount) == false && currentPerson.Type != PersonTypeString.Admin);

                        auditTransferFrom.Before = $"Available: {fromPersonBalance.Available} ";
                        fromPersonBalance.Available += fromAmount;
                        fromPersonBalance.UpdatedBy = currentPerson.Username;
                        fromPersonBalance.UpdatedDate = DateTime.Now;
                        newBalance = fromPersonBalance.Available;
                    }

                    auditTransferTo.Before = $"Available: {toPersonBalance.Available} ";
                    toPersonBalance.Available += toAmount;
                    toPersonBalance.UpdatedBy = currentPerson.Username;
                    toPersonBalance.UpdatedDate = DateTime.Now;
                    newUplineBalance = toPersonBalance.Available;
                }
                else
                {
                    ErrorStatuses.ThrowBadRequest(Request, $"Invalid {fromTransferType} amount.", when: ValidateTransferAmount(fromPersonBalance, toPersonBalance, fromTransferType, fromAmount) == false && currentPerson.Type != PersonTypeString.Admin);

                    auditTransferFrom.Before = $"Available: {fromPersonBalance.Available} ";
                    fromPersonBalance.Available += fromAmount;
                    fromPersonBalance.UpdatedBy = currentPerson.Username;
                    fromPersonBalance.UpdatedDate = DateTime.Now;

                    auditTransferTo.Before = $"Available: {toPersonBalance.Available} ";
                    toPersonBalance.Available += toAmount;
                    toPersonBalance.UpdatedBy = currentPerson.Username;
                    toPersonBalance.UpdatedDate = DateTime.Now;
                    newBalance = fromPersonBalance.Available;
                    newUplineBalance = toPersonBalance.Available;
                }

                personTransfer.CurrentAmount = newBalance;
                personTransfer.Username = fromPerson.Username;
                personTransfer.UplineAmount = newUplineBalance;
                personTransfer.TransferType = fromTransferType;
                personTransfer.CurrencyCode = transfer.CurrencyCode;

                auditTransferFrom.After = $"Available: {newBalance}";
                auditTransferTo.After = $"Available: {newUplineBalance}";

                TransferLogic.AddStatement(this.altDBContext, transfer.CurrencyCode, fromPerson, toPerson, fromTransferType.ToString(), toTransferType.ToString(), fromAmount, DateTime.Now, currentPerson.Username);

                await auditTransferFrom.InsertAudit(this.altDBContext);
                await auditTransferTo.InsertAudit(this.altDBContext);

                await this.altDBContext.SaveChangesAsync();
                trans.Commit();


                if (activeGameSession != null)
                {
                    try
                    {
                        using (var httpClient = new HttpClient()
                        {
                            BaseAddress = new Uri(this.WebGamePlayerBroadcastURL)
                        })
                        {
                            var addPoint = (long)(fromAmount / pointRate);
                            await httpClient.PostAsync($"broadcast/PointUpdated?token={token}&add={addPoint}&popup={true}", null);
                        }
                    }
                    catch
                    {

                    }
                }
            }

            return personTransfer;
        }

        private bool ValidateTransferAmount(PersonBalance effectedBalance, PersonBalance uplineBalance, TransferType type, decimal amount)
        {
            var success = false;
            if (type == TransferType.Deposit)
            {
                success = uplineBalance.Available >= Math.Abs(amount);
            }
            else if (type == TransferType.Withdraw)
            {
                success = effectedBalance.Available >= Math.Abs(amount);
            }

            return success;
        }

        public string WebGamePlayerBroadcastURL = ConfigurationManager.Get<string>(ApiConfigurations.Group, ApiConfigurations.WebGamePlayerBroadcastURL, string.Empty);
    }
}