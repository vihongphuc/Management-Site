﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ALT.Services.Controllers.Api
{
    using System.Data.Entity;
    using System.Threading.Tasks;
    using ALT.Common.Models;
    using System.Net.Http.Formatting;
    using Data.Entityframework;
    using ALT.Services.Common;
    using ALT.Common.Utilities;
    using ALT.Common;
    using ATL.Common.Redis;
    using StackExchange.Redis;
    using Microsoft.Practices.Unity;

    [RoutePrefix("api/game-data")]
    public class GameDataController : ApiController
    {
        [HttpGet, Route("")]
        public async Task<IList<GameResp>> GetGames(string gamecode, bool includedSetting = false, string publisher = null, string platforms = null)
        {
            var qGameLists = altDBContext.Games as IQueryable<Game>;
            if (!string.IsNullOrEmpty(gamecode))
            {
                qGameLists = qGameLists.Where(c => c.Code.ToUpper() == gamecode.ToUpper());
            }
            if (includedSetting)
            {
                if (!string.IsNullOrEmpty(publisher))
                {
                    qGameLists = qGameLists.Where(c => c.GameSettings.Any(cc => cc.Publisher.ToUpper() == publisher.ToUpper()));
                }
                if (!string.IsNullOrEmpty(platforms))
                {
                    qGameLists = qGameLists.Where(c => c.GameSettings.Any(cc => cc.Platform.ToUpper() == platforms.ToUpper()));
                }
            }
            var dtGroupGames = await this.altDBContext.GameCategories.ToDictionaryAsync(c => c.ID, c => c.Name);

            var games = (await qGameLists.ToArrayAsync()).Select(d =>
            {
                var _gameResp = new GameResp
                {
                    OCode = OCode.Get(d.ID),
                    Code = d.Code,
                    Name = d.Name,
                    GroupOCode = OCode.Get(d.GroupID),
                    GroupName = dtGroupGames[d.GroupID],
                    Type = d.Type,
                    Enable = (bool)d.Enable
                };

                var _gameSettingResp = d.GameSettings.ToArray();
                if (includedSetting)
                {
                    if (!string.IsNullOrEmpty(publisher))
                    {
                        _gameSettingResp = _gameSettingResp.Where(cc => cc.Publisher.ToUpper() == publisher.ToUpper()).ToArray();
                    }
                    if (!string.IsNullOrEmpty(platforms))
                    {
                        _gameSettingResp = _gameSettingResp.Where(cc => cc.Platform.ToUpper() == platforms.ToUpper()).ToArray();
                    }
                }

                _gameResp.Settings = _gameSettingResp.Select(cc => new GameSettingResp
                {
                    GameCode = d.Code,
                    Publisher = cc.Publisher,
                    Platform = cc.Platform,
                    DowloadURL = cc.DownloadURL,
                    GameHash = cc.GameHash,
                    GameHeader = cc.GameHearder,
                    Enable = cc.Enable ?? false,
                    IsHot = cc.IsHot ?? false,
                    IsNew = cc.IsNew ?? false,
                    Rate = cc.Rate,
                    Type = cc.Type,
                    Width = cc.Width ?? 0,
                    Height = cc.Height ?? 0,
                    Domain = cc.Domain,
                    Port = cc.Port ?? 0
                }).ToList();

                return _gameResp;

            }).ToArray();


            return games;
        }

        [HttpGet, Route("game/setting")]
        public async Task<IList<GameSettingResp>> GetGameSettings(string gameCode, string publisher, string platforms)
        {
            ErrorStatuses.ThrowBadRequest(Request, "Cannot find the gamecode", when: gameCode == null);
            IList<GameSettingResp> gameSettings;
            var qGameSettingLists = altDBContext.GameSettings as IQueryable<GameSetting>;

            if (!string.IsNullOrEmpty(gameCode))
            {
                qGameSettingLists = qGameSettingLists.Where(c => c.Game.Code == gameCode);
                if (!string.IsNullOrEmpty(publisher))
                {
                    qGameSettingLists = qGameSettingLists.Where(cc => cc.Publisher.ToUpper() == publisher.ToUpper());
                }
                if (!string.IsNullOrEmpty(platforms))
                {
                    qGameSettingLists = qGameSettingLists.Where(cc => cc.Platform.ToUpper() == platforms.ToUpper());
                }
            }

            gameSettings = (await qGameSettingLists.ToArrayAsync()).Select(c => new GameSettingResp
            {
                GameCode = c.Game.Code,
                Publisher = c.Publisher,
                Platform = c.Platform,
                DowloadURL = c.DownloadURL,
                GameHash = c.GameHash,
                GameKey = c.GameKey,
                GameHeader = c.GameHearder,
                Enable = c.Enable ?? false,
                IsHot = c.IsHot ?? false,
                IsNew = c.IsNew ?? false,
                Rate = c.Rate,
                Type = c.Type,
                Width = c.Width ?? 0m,
                Height = c.Height ?? 0m,
                Domain = c.Domain,
                Port = c.Port ?? 0
            }).ToArray();

            return gameSettings;
        }


        public GameDataController(ALTEntities altDBContext)
        {
            this.altDBContext = altDBContext;
        }

        private readonly ALTEntities altDBContext;
    }
}