﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ALT.Services.Controllers.Api
{
    using System.Threading.Tasks;
    using System.Data.Entity;
    using Data.Entityframework;
    using StackExchange.Redis;
    using ALT.Common;
    using ALT.Common.Models.Models;
    using Common;
    using ALT.Common.Helpers;
    using ALT.Common.Models;
    using ALT.Common.Utilities;
    using ALT.Services.Infrastructer;
    using Microsoft.Practices.Unity;
    using ATL.Common.Redis;

    [TokenFilterAttribute]
    [RoutePrefix("api/person")]
    public partial class PersonController : ApiController
    {
        [HttpGet, Route("{ocode}/downlines")]
        public async Task<PagedList<PersonResp>> GetPersonDownline(string ocode, int pageIndex = 0, int pageSize = 1, bool? isMember = null)
        {
            ErrorStatuses.ThrowBadRequest(Request, "Page size is not set", (pageSize < 0));

            PagedList<PersonResp> pageListPersons = new PagedList<PersonResp>();
            IList<PersonResp> persons;

            long personID;
            if (OCode.TryGetInt64(ocode, out personID))
            {
                var totalDownlines = 0;
                var person = await this.altDBContext.People.Where(c => c.ID == personID).FirstOrDefaultAsync();
                ErrorStatuses.ThrowBadRequest(Request, "Invalid person", when: person == null);

                var dbPersonDownlines = this.altDBContext.People.Where(c => c.UplineID == person.ID);
                if (isMember != null)
                {
                    if (isMember.Value)
                        dbPersonDownlines = dbPersonDownlines.Where(c => c.Type == PersonTypeString.Member);
                    else
                        dbPersonDownlines = dbPersonDownlines.Where(c => c.Type != PersonTypeString.Member);
                }

                totalDownlines = dbPersonDownlines.Count();

                dbPersonDownlines = dbPersonDownlines.OrderBy(c => c.ID).Skip((pageIndex - 1) * pageSize).Take(pageSize).AsQueryable();

                var dtDownlinesBalances = await this.altDBContext.PersonBalances
                                                        .Where(c => dbPersonDownlines.Any(cc => c.PersonID == cc.ID && c.CurrencyCode.Contains(c.CurrencyCode)))
                                                        .ToDictionaryAsync(c => Tuple.Create<long, string>(c.PersonID, c.CurrencyCode), c => c.Available);

                var dtDownlinesCreditSettings = await this.altDBContext.PersonCreditSettings
                                                        .Where(c => dbPersonDownlines.Any(cc => c.PersonID == cc.ID && c.CurrencyCode.Contains(c.CurrencyCode)))
                                                        .ToDictionaryAsync(c => Tuple.Create<long, string>(c.PersonID, c.CurrencyCode), c => c.MaxCredit);

                var dtTotalDownlines = await this.altDBContext.People.Where(c => c.UplineID.HasValue)
                                        .Where(c => dbPersonDownlines.Any(cc => cc.ID == c.UplineID))
                                        .GroupBy(c => new { UplineID = c.UplineID.Value, CurrencyCode = c.CurrencyCodes })
                                        .Select(c => new { c.Key, Count = c.Count() })
                                        .ToDictionaryAsync(c => Tuple.Create<long, string>(c.Key.UplineID, c.Key.CurrencyCode), c => c.Count);

                var personDownlines = await dbPersonDownlines.ToArrayAsync();

                var memberIDs = personDownlines.Where(c => c.Type == PersonTypeString.Member).Select(c => c.ID).ToArray();
                var dtActiveSession = (await this.altDBContext.GameSessions.Where(c => memberIDs.Contains(c.PersonID))
                                                                            .Where(c => c.FinishedTime.HasValue == false)
                                                                            .ToArrayAsync())
                                                                            .ToDictionary(c => c.PersonID, c => Tuple.Create<string, decimal>(OCode.Get(c.ID), c.PointRate ?? 0m));
                var serverKey = KeyDefine.ServerPointKey;
                var hashValues = dtActiveSession.Values.Select(c => (RedisValue)c.Item1).ToList();
                var memberPoints = (await this.playerServer.HashGetAsync(serverKey, hashValues.ToArray()));

                persons = personDownlines.Select(c =>
               {
                   int _totalDownlines = 0;
                   var _currencies = StringHelper.ToArray(c.CurrencyCodes).OrderBy(cc => cc).ToArray();
                   var dtAvailables = new Dictionary<string, decimal>();
                   var dtMaxCredits = new Dictionary<string, decimal>();

                   foreach (var _currencyCode in _currencies)
                   {
                       var _getInPersonBalance = true;
                       var _keys = Tuple.Create<long, string>(c.ID, _currencyCode);
                       decimal _available = 0m, _maxCredit = 0m;
                       int _total;
                       Tuple<string, decimal> activeGameSession;

                       if (dtActiveSession.TryGetValue(c.ID, out activeGameSession))
                       {
                           var _idx = hashValues.IndexOf((RedisValue)activeGameSession.Item1);
                           if (_idx >= 0 && _idx < memberPoints.Length)
                           {
                               if (memberPoints[_idx].HasValue)
                               {
                                   _available = (long)memberPoints[_idx] * activeGameSession.Item2;
                                   _getInPersonBalance = false;
                               }
                           }
                       }

                       if (_getInPersonBalance)
                       {
                           if (!dtDownlinesBalances.TryGetValue(_keys, out _available))
                           {
                               _available = 0m;
                           }
                       }
                       dtAvailables.Add(_currencyCode, _available);

                       if (!dtDownlinesCreditSettings.TryGetValue(_keys, out _maxCredit))
                       {
                           _maxCredit = 0;
                       }
                       dtMaxCredits.Add(_currencyCode, _maxCredit);

                       if (!dtTotalDownlines.TryGetValue(_keys, out _total))
                       {
                           _total = 0;
                       }
                       _totalDownlines += _total;
                   }

                   try
                   {
                       var _person = new PersonResp
                       {
                           UplineOCode = c.UplineID.HasValue ? OCode.Get(c.UplineID.Value) : String.Empty,
                           OCode = OCode.Get(c.ID),
                           Username = c.Username,
                           Firstname = c.Firstname,
                           Lastname = c.Lastname,
                           Status = EnumHelper<PersonStatusType>.Parse(c.Status),
                           Type = EnumHelper<PersonType>.Parse(c.Type),
                           Currencies = _currencies,
                           LastLoginIP = c.LastLoginIP,
                           LastLogin = c.UpdatedDate.Value,
                           Availables = dtAvailables,
                           MaxCredits = dtMaxCredits,

                           TotalDownline = _totalDownlines
                       };
                       return _person;
                   }
                   catch (Exception ex)
                   {
                       var ssss = ex;
                       return null;
                   }
               }).OrderByDescending(c => c.Username)
                 .OrderBy(c => c.Status == PersonStatusType.Suspended)
                 .OrderBy(c => c.Status == PersonStatusType.Disabled)
                 .ToArray();

                pageListPersons.PageList = persons;
                pageListPersons.TotalPages = totalDownlines;
                pageListPersons.CurrentPage = pageIndex;
                pageListPersons.TotalItemPerPage = pageSize;// pageListPersons.PageList.Count();

                return pageListPersons;
            }
            ErrorStatuses.ThrowBadRequest(Request, $"Invalid person OCode: {ocode}");
            return null;
        }

        [HttpGet, Route("{ocode}/person-by-ocode")]
        public async Task<EditPersonCurrentResp> GetPersonByOcode(string ocode)
        {
            long personID;
            if (OCode.TryGetInt64(ocode, out personID))
            {
                var person = await this.altDBContext.People.Where(c => c.ID == personID).FirstOrDefaultAsync();
                ErrorStatuses.ThrowBadRequest(Request, "Invalid person", when: person == null);

                var allCurrencies = altDBContext.PersonBalances.Where(c => c.PersonID == person.ID).Select(c => c.CurrencyCode).ToArray();

                var allPersonCreditSettingByPersonIDs = await this.altDBContext.PersonCreditSettings
                                                       .Where(c => c.PersonID == person.ID).ToArrayAsync();

                var allPersonBalances = await this.altDBContext.PersonBalances.Where(c => c.PersonID == person.ID).ToArrayAsync();
                var allPersonBetSettings = await this.altDBContext.PersonBetSettings.Where(c => c.PersonID == person.ID).ToArrayAsync();
                var allPositionTalkings = await this.altDBContext.PositionTakings.Where(c => c.PersonID == person.ID).ToArrayAsync();


                var personCreditSettings = allPersonCreditSettingByPersonIDs
                                                                           .Select(cc => new PersonMaxCreditReq
                                                                           {
                                                                               PersonOCode = OCode.Get(person.ID),
                                                                               CurrencyCode = cc.CurrencyCode,
                                                                               MaxCredit = cc.MaxCredit,
                                                                               PersonType = EnumHelper<PersonType>.Parse(cc.PersonType)
                                                                           }).ToList();

                var personBalances = allPersonBalances
                                                    .Select(ccc => new PersonBalanceReq
                                                    {
                                                        PersonOCode = OCode.Get(ccc.PersonID),
                                                        CurrencyCode = ccc.CurrencyCode,
                                                        Available = ccc.Available,
                                                    }).ToList();

                var personBetSettings = allPersonBetSettings
                                                            .Select(cc => new PersonBetSettingReq
                                                            {
                                                                CurrencyCode = cc.CurrencyCode,
                                                                BetTypeItemOcode = OCode.Get(cc.BetTypeItemID),
                                                                PersonOCode = OCode.Get(cc.PersonID),
                                                                SettingType = EnumHelper<BetSettingType>.Parse(cc.SettingType),
                                                                Value = cc.Value,
                                                                ValueString = cc.ValueString
                                                            }).ToList();


                var positionTakings = allPositionTalkings
                                                        .Select(cc => new PositionTakingReq
                                                        {
                                                            CurrencyCode = cc.CurrencyCode,
                                                            PersonOCode = OCode.Get(cc.PersonID),
                                                            BetTypeItemOcode = OCode.Get(cc.BetTypeItemID),
                                                            Type = EnumHelper<PostitionTakingType>.Parse(cc.Type),
                                                            Value = (decimal)cc.Value
                                                        }).ToList();


                var memberID = await this.altDBContext.People.Where(c => c.ID == personID && c.Type == PersonTypeString.Member).Select(c => c.ID).FirstOrDefaultAsync();

                var dtActiveSession = (await this.altDBContext.GameSessions.Where(c => c.PersonID == memberID)
                                                                            .Where(c => c.FinishedTime.HasValue == false)
                                                                            .ToArrayAsync())
                                                                            .ToDictionary(c => c.PersonID, c => Tuple.Create<string, decimal>(OCode.Get(c.ID), c.PointRate ?? 0m));

                var serverKey = KeyDefine.ServerPointKey;
                var hashValues = dtActiveSession.Values.Select(c => (RedisValue)c.Item1).ToList();
                var memberPoints = (await this.playerServer.HashGetAsync(serverKey, hashValues.ToArray()));

                var _getInPersonBalance = true;
                var _keys = Tuple.Create<long, string>(person.ID, person.CurrencyCodes);
                decimal _available = 0m;

                Tuple<string, decimal> activeGameSession;

                if (dtActiveSession.TryGetValue(person.ID, out activeGameSession))
                {
                    var _idx = hashValues.IndexOf((RedisValue)activeGameSession.Item1);
                    if (_idx >= 0 && _idx < memberPoints.Length)
                    {
                        if (memberPoints[_idx].HasValue)
                        {
                            _available = (long)memberPoints[_idx] * activeGameSession.Item2;
                            _getInPersonBalance = false;
                        }
                    }

                    if (person.Type == PersonType.Member.ToString())
                    {
                        personBalances = allPersonBalances
                                                     .Select(ccc => new PersonBalanceReq
                                                     {
                                                         PersonOCode = OCode.Get(ccc.PersonID),
                                                         CurrencyCode = ccc.CurrencyCode,
                                                         Available = _available,
                                                     }).ToList();
                    }
                }

                var _person = new EditPersonCurrentResp
                {
                    OCode = OCode.Get(person.ID),
                    Username = person.Username,
                    Firstname = person.Firstname,
                    Lastname = person.Lastname,
                    DisplayName = person.DisplayName,
                    Status = EnumHelper<PersonStatusType>.Parse(person.Status),
                    Password = Password.CreateHash(person.Password),
                    Type = EnumHelper<PersonType>.Parse(person.Type),
                    Currencies = allCurrencies,
                    DOB = person.DOB,
                    Email = person.Email,
                    LimitIP = person.LimitIP,
                    UplineOcode = OCode.Get(person.UplineID),
                    PersonMaxCredits = personCreditSettings,
                    PersonBalances = personBalances,
                    PositionTakings = positionTakings,
                    Mobile = person.Mobile,
                    Phone = person.Phone,
                    BetSettings = personBetSettings,
                    CreateAgency = person.CreateAgency,
                    CreateMember = person.CreateMember,
                    FixedCurrency = person.FixedCurrency
                };
                return _person;
            }
            ErrorStatuses.ThrowBadRequest(Request, $"Invalid person OCode: {ocode}");
            return null;
        }

        [HttpGet, Route("check-username")]
        public async Task<ChechUsernameResp> CheckUsername(string personCode, string username)
        {
            var currentType = Request.GetCurrentPersonType();
            ChechUsernameResp checkusername = new ChechUsernameResp();
            bool isCheck = false;
            long personID;
            if (OCode.TryGetInt64(personCode, out personID))
            {             
                isCheck = await this.altDBContext.People.AnyAsync(c => c.Username == username);
                checkusername.Username = username;
                checkusername.IsCheck = isCheck;
            }

            return checkusername;
        }

        [HttpGet, Route("{ocode}/get-person-balances")]
        public async Task<Dictionary<string, decimal>> GetPersonBalanceByOcode(string ocode)
        {
            long personID;
            if (OCode.TryGetInt64(ocode, out personID))
            {
                var person = await this.altDBContext.People.Where(c => c.ID == personID).FirstOrDefaultAsync();
                ErrorStatuses.ThrowBadRequest(Request, $"Person not found.", when: person == null);

                var personBalance = this.altDBContext.PersonBalances.Where(c => c.PersonID == personID);
                ErrorStatuses.ThrowBadRequest(Request, $"Person not found.", when: personBalance == null);

                return await personBalance.ToDictionaryAsync(c => c.CurrencyCode, c => c.Available);
            }
            return new Dictionary<string, decimal>();
        }

        [HttpGet, Route("{ocode}/breadcrumb-person-by-ocode")]
        public async Task<Dictionary<string, string>> BreadcrumbPerson(string ocode)
        {
            Dictionary<string, string> dicPerson = new Dictionary<string, string>();
            var currentType = Request.GetCurrentPersonType();

            long personID;
            if (OCode.TryGetInt64(ocode, out personID))
            {
                var uplineIDs = await this.altDBContext.GetPersonUplines(personID, (PersonType)currentType, true);

                dicPerson = (await this.altDBContext.People.Where(c => uplineIDs.Contains(c.ID))
                                                           .Select(c => new
                                                           {
                                                               c.ID,
                                                               c.Username,
                                                               c.CreatedDate
                                                           })
                                                           .OrderBy(c => c.Username)
                                                           .OrderBy(c => c.CreatedDate)
                                                           .ToDictionaryAsync(cc => OCode.Get(cc.ID), cc => cc.Username));
                return dicPerson;
            }
            return dicPerson;
        }

        [HttpGet, Route("{ocode}/get-transfer-statement")]
        public async Task<PagedList<TransferMemberHistoryResp>> TransferStatement(string ocode, DateTime startDate, DateTime endDate, int pageIndex = 0, int pageSize = 1)
        {
            PagedList<TransferMemberHistoryResp> pageListTransferMemberHistory = new PagedList<TransferMemberHistoryResp>();
            long personID;
            var start_Date = startDate.Date;
            var end_Date = endDate.Date.AddDays(1);
            if (OCode.TryGetInt64(ocode, out personID))
            {
                ErrorStatuses.ThrowBadRequest(Request, "Page size is not set", (pageSize < 0));

                var personCurrent = await this.altDBContext.People.Where(c => c.ID == personID).FirstOrDefaultAsync();
                ErrorStatuses.ThrowBadRequest(Request, "Cannot find the person.", personCurrent == null);

                var transactionDetail = this.altDBContext.Statements
                                                  .Where(c => c.PersonID == personCurrent.ID)
                                                  .Where(c => c.Time >= start_Date && c.Time <= end_Date);
                ErrorStatuses.ThrowBadRequest(Request, "Cannot find the transaction detail.", transactionDetail == null);

                var qrTransactionDetail = transactionDetail.OrderByDescending(c => c.Time)
                                           .Select(c => new
                                           {
                                               ID = c.ID,
                                               Date = c.Time,
                                               Type = c.Type,
                                               Amount = c.Amount,
                                               CurrencyCode = c.CurrencyCode
                                           });

                var statements = (await qrTransactionDetail.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToArrayAsync())
                                 .Select(c => new TransferMemberHistoryResp
                                 {
                                     Transaction = OCode.Get(c.ID),
                                     Date = c.Date,
                                     Type = c.Type,
                                     Amount = c.Amount,
                                     CurrencyCode = c.CurrencyCode
                                 }).ToArray();

                pageListTransferMemberHistory.PageList = statements;
                pageListTransferMemberHistory.TotalPages = qrTransactionDetail.Count();
                pageListTransferMemberHistory.CurrentPage = pageIndex;
                pageListTransferMemberHistory.TotalItemPerPage = pageSize;// pageListTransferMemberHistory.PageList.Count();

                return pageListTransferMemberHistory;
            }

            ErrorStatuses.ThrowBadRequest(Request, $"Invalid person OCode: {ocode}");
            return null;
        }

        [HttpGet, Route("{ocode}/get-position-talking")]
        public async Task<List<PositionTalkingResp>> GetPositionTaking(string ocode, bool getUpline)
        {
            List<PositionTalkingResp> positionTalkingUpline;
            long personID;
            if (OCode.TryGetInt64(ocode, out personID))
            {
                var preson = await this.altDBContext.People.Where(c => c.ID == personID).FirstOrDefaultAsync(); ;
                if (!getUpline)
                {
                    var positionTalking = await this.altDBContext.PositionTakings
                                                    .Where(c => c.PersonID == preson.ID).ToArrayAsync();

                    ErrorStatuses.ThrowBadRequest(Request, $"Person not found.", when: positionTalking == null);

                    positionTalkingUpline = positionTalking.Select(c => new PositionTalkingResp
                    {
                        PersonOCode = OCode.Get(c.PersonID),
                        Currency = c.CurrencyCode,
                        Type = c.Type,
                        Value = (decimal)c.Value
                    }).ToList();
                }
                else
                {
                    var positionTalking = await this.altDBContext.PositionTakings
                                                    .Where(c => c.PersonID == preson.UplineID).ToArrayAsync();

                    ErrorStatuses.ThrowBadRequest(Request, $"Person not found.", when: positionTalking == null);

                    positionTalkingUpline = positionTalking.Select(c => new PositionTalkingResp
                    {
                        PersonOCode = OCode.Get(c.PersonID),
                        Currency = c.CurrencyCode,
                        Type = c.Type,
                        Value = (decimal)c.Value
                    }).ToList();
                }

                return positionTalkingUpline;
            }
            ErrorStatuses.ThrowBadRequest(Request, $"Invalid person OCode: {ocode}");
            return null;
        }

        [HttpGet, Route("{ocode}/get-person-max-credit-by-ocode")]
        public async Task<List<PersonMaxCreditResp>> GetPersonMaxCreditByOcode(string ocode, bool getUpline)
        {
            List<PersonMaxCreditResp> personMaxCreditUpline;
            long personID;
            if (OCode.TryGetInt64(ocode, out personID))
            {
                var person = await this.altDBContext.People.Where(c => c.ID == personID).FirstOrDefaultAsync();
                ErrorStatuses.ThrowBadRequest(Request, $"Person not found.", when: person == null);

                if (PersonType.Admin == EnumHelper<PersonType>.Parse(person.Type))
                {
                    return null;
                }
                else
                {
                    var personUplineID = getUpline == true ? person.ID : person.UplineID;
                    ErrorStatuses.ThrowBadRequest(Request, $"Person not found.", when: personUplineID == null);

                    var personMaxCreditSetting = await this.altDBContext.PersonCreditSettings
                                     .Where(c => c.PersonID == personUplineID).ToArrayAsync();
                    ErrorStatuses.ThrowBadRequest(Request, $"Person not found.", when: personMaxCreditSetting == null);

                    personMaxCreditUpline = personMaxCreditSetting.Select(c => new PersonMaxCreditResp
                    {
                        PersonOCode = OCode.Get(c.PersonID),
                        CurrencyCode = c.CurrencyCode,
                        Type = c.PersonType,
                        Value = (decimal)c.MaxCredit
                    }).ToList();

                    return personMaxCreditUpline;
                }
            }
            ErrorStatuses.ThrowBadRequest(Request, $"Invalid person OCode: {ocode}");
            return null;
        }

        [HttpGet, Route("{ocode}/get-person-bet-setting-by-ocode")]
        public async Task<List<PersonBetSettingResp>> GetPersonBetSettingByOcode(string ocode, bool getUpline)
        {
            List<PersonBetSettingResp> personBetSettingUpline;
            long personID;
            if (OCode.TryGetInt64(ocode, out personID))
            {
                var person = await this.altDBContext.People.Where(c => c.ID == personID).FirstOrDefaultAsync();
                ErrorStatuses.ThrowBadRequest(Request, $"Person not found.", when: person == null);

                if (PersonType.Admin == EnumHelper<PersonType>.Parse(person.Type))
                {
                    return null;
                }
                else
                {
                    var personUplineID = getUpline == true ? person.ID : person.UplineID;
                    ErrorStatuses.ThrowBadRequest(Request, $"Person not found.", when: personUplineID == null);

                    var personBetSetting = await this.altDBContext.PersonBetSettings
                                            .Where(c => c.PersonID == personUplineID).ToArrayAsync();
                    ErrorStatuses.ThrowBadRequest(Request, $"Person not found.", when: personBetSetting == null);

                    personBetSettingUpline = personBetSetting.Select(c => new PersonBetSettingResp
                    {
                        PersonOCode = OCode.Get(c.PersonID),
                        Currency = c.CurrencyCode,
                        Type = EnumHelper<BetSettingType>.Parse(c.SettingType),
                        Value = (decimal)c.Value
                    }).ToList();

                    return personBetSettingUpline;
                }
            }
            ErrorStatuses.ThrowBadRequest(Request, $"Invalid person OCode: {ocode}");
            return null;
        }

        [HttpGet, Route("{ocode}/get-person-list-balance-by-code")]
        public async Task<List<PersonBalanceResp>> GetPersonListBalanceByOcode(string ocode, bool getUpline)
        {
            List<PersonBalanceResp> personBetSettingUpline;
            long personID;
            if (OCode.TryGetInt64(ocode, out personID))
            {

                var person = await this.altDBContext.People.Where(c => c.ID == personID).FirstOrDefaultAsync();
                ErrorStatuses.ThrowBadRequest(Request, $"Person not found.", when: person == null);

                if (PersonType.Admin == EnumHelper<PersonType>.Parse(person.Type))
                {
                    return null;
                }
                else
                {
                    var personUplineID = getUpline == true ? person.ID : person.UplineID;
                    var personBalance = await this.altDBContext.PersonBalances
                               .Where(c => c.PersonID == personUplineID).ToArrayAsync();

                    ErrorStatuses.ThrowBadRequest(Request, $"Person not found.", when: personBalance == null);

                    personBetSettingUpline = personBalance.Select(c => new PersonBalanceResp
                    {
                        PersonOCode = OCode.Get(c.PersonID),
                        CurrencyCode = c.CurrencyCode,
                        Value = (decimal)c.Available
                    }).ToList();

                    return personBetSettingUpline;
                }
            }
            ErrorStatuses.ThrowBadRequest(Request, $"Invalid person OCode: {ocode}");
            return null;
        }

        [HttpGet, Route("{ocode}/search-username")]
        public async Task<PersonResp[]> SearchUsername(string ocode, string username)
        {
            PersonResp[] personUsername = null;
            var qrPerson = this.altDBContext.People as IQueryable<Person>;
            long personID;

            if (OCode.TryGetInt64(ocode, out personID))
            {
                if (!String.IsNullOrEmpty(username))
                {
                    var currentUsername = await this.altDBContext.People.Where(c => c.ID == personID).Select(c => new { c.Username, c.Type }).FirstOrDefaultAsync();
                    var _lstPerson = qrPerson.Where(c => (c.Username.StartsWith(currentUsername.Username) || currentUsername.Type == PersonTypeString.Admin) && c.Username.StartsWith(username.ToUpper().Trim().ToString()));
                    personUsername = _lstPerson.Select(c => new PersonResp
                    {
                        Username = c.Username,
                    }).ToArray();
                }
                else
                {
                    personUsername = qrPerson.Select(c => new PersonResp
                    {
                        Username = c.Username,
                    }).ToArray();
                }
            }

            return personUsername;
        }

        [HttpGet, Route("{ocode}/test-redis-lua")]
        public async Task<double> ModifyPoints(string ocode)
        {
            var currentPointResult = await RedisScripts.GameState.GetCurrentPoint(ocode, null);
            var result = await RedisScripts.GameState.ModifyPoint(ocode, (long)(((long)currentPointResult) / 0.001) + 100);
            return result.CurrentPoint * 0.01;
        }
        public PersonController(ALTEntities altDBContext, [Dependency(RedisConfigurations.PlayerServer)] StackExchange.Redis.IDatabase playerServer)
        {
            this.altDBContext = altDBContext;
            this.playerServer = playerServer;
        }

        private readonly ALTEntities altDBContext;
        private readonly StackExchange.Redis.IDatabase playerServer;
    }
}