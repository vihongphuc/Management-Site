﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ALT.Services.Controllers.Api
{
    using System.Data.Entity;
    using System.Threading.Tasks;
    using ALT.Common.Models;
    using System.Net.Http.Formatting;
    using Data.Entityframework;
    using ALT.Services.Common;
    using ALT.Common.Utilities;
    using ALT.Common;
    using ATL.Common.Redis;
    using StackExchange.Redis;
    using Microsoft.Practices.Unity;
    using ALT.Common.Configuration;
    using Configurations;

    [RoutePrefix("api/game-access")]
    public class GameAccessController : ApiController
    {
        [HttpPost, Route("sign-in")]
        public async Task<GameAccessResp> SignIn(GameAsscessReq request)
        {
            var gameAccess = new GameAccessResp();
            gameAccess = await SignInCore(request);
            return gameAccess;
        }

        [HttpPost, Route("mobile-sign-in")]
        public async Task<GameAccessResp> MobileSignin(FormDataCollection formData)
        {
            var gameAccess = new GameAccessResp();
            var notExpired = false;
            if (!bool.TryParse(formData["NotExpired"] ?? "", out notExpired))
            {
                notExpired = false;
            }
            var request = new GameAsscessReq
            {
                Username = formData["Username"],
                Password = formData["Password"],
                Data = formData["Data"],
                IP = formData["IP"],
                IPHashKey = formData["IPHashKey"],
                NotExpired = notExpired,
                Platform = formData["Platform"],
                Publisher = formData["Publisher"],
                TokenMobile = formData["TokenMobile"]
            };
            gameAccess = await SignInCore(request);

            return gameAccess;
        }

        [HttpPost, Route("get-game-active/{token}")]
        private async Task<GameAccessResp> GetGameActive(string token)
        {
            ErrorStatuses.ThrowBadRequest(Request, "Invalid Token.", when: string.IsNullOrEmpty(token));

            var gameaccess = new GameAccessResp();
            var sessionID = 0L;
            if (OCode.TryGetInt64(token, out sessionID))
            {
                var gameSession = await this.altDBContext.GameSessions.Where(c => c.ID == sessionID).FirstOrDefaultAsync();
                ErrorStatuses.ThrowBadRequest(Request, $"Token: {token} invalid.", when: gameSession == null);

                if (gameSession.FinishedTime == null)
                {
                    var playerKey = KeyDefine.GetPlayerKey(token);
                    var redisPlayers = await this.playerServer.HashGetAsync(playerKey, new RedisValue[] {
                                                                                        KeyDefine.Username,
                                                                                        KeyDefine.Points,
                                                                                        KeyDefine.PointRate,
                                                                                        KeyDefine.SessionOCode,
                                                                                        KeyDefine.TokenMobile,
                                                                                        KeyDefine.Secret,
                                                                                        KeyDefine.IPHashKey,
                                                                                               });

                    if (false == redisPlayers[0].HasValue || false == redisPlayers[1].HasValue || false == redisPlayers[2].HasValue)
                    {
                        ErrorStatuses.ThrowServerError(Request, "Invalid User info.", when: true);
                    }

                    gameaccess.SessionCode = token;
                    gameaccess.Username = redisPlayers[0];
                    gameaccess.Points = (long)redisPlayers[1];
                    gameaccess.PointRate = (long)redisPlayers[2];
                    gameaccess.TokenMobile = redisPlayers[4];
                    gameaccess.Secret = redisPlayers[5];
                    gameaccess.IPHashKey = redisPlayers[6];
                }
            }

            return gameaccess;
        }

        [HttpPost, Route("sign-out/{token}")]
        public async Task<bool> SignOut(string token, bool webSignOut = false)
        {
            var result = false;
            var sessionID = 0L;
            if (OCode.TryGetInt64(token, out sessionID))
            {

                var gameSession = await this.altDBContext.GameSessions.Where(c => c.ID == sessionID).FirstOrDefaultAsync();
                ErrorStatuses.ThrowBadRequest(Request, $"Token: {token} invalid.", when: null == gameSession);

                using (var trans = altDBContext.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                {
                    var personBalance = await this.altDBContext.LockPersonBalance(gameSession.PersonID, gameSession.CurrencyCode);
                    ErrorStatuses.ThrowBadRequest(Request, $"Person does not correct.", when: null == personBalance);

                    var username = gameSession.Person.Username;

                    var lockGameSession = await this.altDBContext.LockGameSession(sessionID);
                    ErrorStatuses.ThrowBadRequest(Request, $"Token: {token} invalid.", when: null == lockGameSession);
                    ErrorStatuses.ThrowBadRequest(Request, $"Token: {token} is out aldready.", when: true == lockGameSession.FinishedTime.HasValue);

                    var now = DateTime.Now;
                    lockGameSession.FinishedTime = now;
                    lockGameSession.FinishedTimeTicks = now.Ticks;
                    lockGameSession.UpdatedBy = username;
                    lockGameSession.UpdatedDate = now;

                    var playerKey = KeyDefine.GetPlayerKey(token);
                    var redisPlayers = await this.playerServer.HashGetAsync(playerKey, new RedisValue[] {
                                                                                        KeyDefine.Points,
                                                                                        KeyDefine.PointRate
                                                                                    });

                    var serverKey = KeyDefine.ServerPointKey;
                    var currentPoints = await this.playerServer.HashGetAsync(serverKey, (RedisValue)token);

                    if (false == redisPlayers[0].HasValue || false == redisPlayers[1].HasValue || currentPoints.HasValue == false)
                    {
                        result = true;
                        //ErrorStatuses.ThrowServerError(Request, "Invalid User info.", when: true);
                        //will be logging.
                    }
                    else
                    {
                        personBalance.Available = Convert.ToDecimal(redisPlayers[1].ToString()) * Convert.ToDecimal(currentPoints.ToString());
                        personBalance.UpdatedBy = username;
                        personBalance.UpdatedDate = now;
                    }
                    await this.playerServer.KeyDeleteAsync(playerKey);
                    await this.playerServer.HashDeleteAsync(serverKey, token);

                    await this.altDBContext.SaveChangesAsync();
                    trans.Commit();
                    result = true;

                    // send broadcast to GamePlayer site
                    if (!webSignOut)
                    {
                        try
                        {
                            using (var httpClient = new HttpClient()
                            {
                                BaseAddress = new Uri(this.WebGamePlayerBroadcastURL)
                            })
                            {
                                await httpClient.PostAsync($"broadcast/SignOut?token={token}", null);
                            }
                        }
                        catch
                        {

                        }
                    }
                }
            }
            else
            {
                ErrorStatuses.ThrowBadRequest(Request, "Invalid Token.", when: string.IsNullOrEmpty(token));
            }
            return result;
        }


        [HttpGet, Route("get-member-balances")]
        public async Task<decimal> GetBalanceOfMemberByOcode(string token)
        {
            long sessionID;
            decimal Balance;

            if (OCode.TryGetInt64(token, out sessionID))
            {
                var gameSession = await this.altDBContext.GameSessions.Where(c => c.ID == sessionID).FirstOrDefaultAsync();

                var lockGameSession = await this.altDBContext.LockGameSession(sessionID);
                ErrorStatuses.ThrowBadRequest(Request, $"Token: {token} invalid.", when: null == lockGameSession);
                ErrorStatuses.ThrowBadRequest(Request, $"Token: {token} is out aldready.", when: true == lockGameSession.FinishedTime.HasValue);

                var playerKey = KeyDefine.GetPlayerKey(token);
                var redisPlayers = await this.playerServer.HashGetAsync(playerKey, new RedisValue[] {
                                                                                        KeyDefine.Points,
                                                                                        KeyDefine.PointRate
                                                                                    });

                var serverKey = KeyDefine.ServerPointKey;
                var currentPoints = await this.playerServer.HashGetAsync(serverKey, (RedisValue)token);
                if (currentPoints.HasValue != false)
                {
                    Balance = Convert.ToDecimal(redisPlayers[1].ToString()) * Convert.ToDecimal(currentPoints.ToString());
                    return Balance;
                }
            }

            ErrorStatuses.ThrowNotFound(Request, $"OCode invalid.");
            return 0m;
        }

        private async Task<GameAccessResp> SignInCore(GameAsscessReq request)
        {
            var gameaccess = new GameAccessResp();
            var auditSignIn = new AuditContext(AuditType.SignIn, nameof(SignInCore), nameof(GameAccessController), nameof(SignInCore))
            {
                ActionTime = DateTime.Now,
                Detail = "Sign in"
            };

            ErrorStatuses.ThrowBadRequest(Request, "Username, Password does not correct.", when: request == null);
            ErrorStatuses.ThrowBadRequest(Request, "Username, Password does not correct.", when: string.IsNullOrEmpty(request.Username) || string.IsNullOrEmpty(request.Password));
            var username = request.Username.ToUpper();

            var person = await this.altDBContext.People.Where(c => c.Username.ToUpper() == username).FirstOrDefaultAsync();
            ErrorStatuses.ThrowBadRequest(Request, "Username, Password does not correct.", when: person == null);
            ErrorStatuses.ThrowBadRequest(Request, "Username, Password does not correct.", when: Password.ValidatePassword(person.Password, request.Password, false) == false);

            ErrorStatuses.ThrowBadRequest(Request, "Account is suspended.", when: person.Status == PersonStatusTypeString.Suspended);
            ErrorStatuses.ThrowBadRequest(Request, "Account is disabled.", when: person.Status == PersonStatusTypeString.Disabled);

            auditSignIn.PersonID = person.ID;
            auditSignIn.RelatedID = person.ID;
            var activeGameSession = await this.altDBContext.LockActiveGameSession(person.ID);

            if (activeGameSession != null)
            {
                // signout this session
                var _oldSessionOCode = OCode.Get(activeGameSession.ID);
                ErrorStatuses.ThrowServerError(Request, $"Cannot signout old session.", when: await SignOut(_oldSessionOCode) == false);
                // send notice to signout in client.

            }

            using (var trans = this.altDBContext.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
            {

                ErrorStatuses.ThrowBadRequest(Request, "Person does not correct.", when: person.Type != PersonTypeString.Member);

                var currency = await this.altDBContext.Currencies.Where(c => c.CurrencyCode == person.CurrencyCodes).FirstOrDefaultAsync();
                ErrorStatuses.ThrowBadRequest(Request, "Currency does not exist.", when: currency == null);

                var personBalance = await this.altDBContext.LockPersonBalance(person.ID, person.CurrencyCodes);
                ErrorStatuses.ThrowBadRequest(Request, "Not enough available balance.", when: personBalance == null);

                var gameSessionID = KeyGeneration.GenerateInt64Id();//
                var sessionOCode = OCode.Get(gameSessionID);
                long points = 0L;
                var dtNow = DateTime.Now;

                auditSignIn.CreatedTime = dtNow;
                auditSignIn.CreatedBy = person.Username;
                auditSignIn.Before = $"Balance: {personBalance.Available}";
                auditSignIn.Before = $"Balance: 0";

                var newActiveGameSession = new GameSession
                {
                    ID = gameSessionID,
                    Ack = dtNow,
                    AckTicks = dtNow.Ticks,
                    CurrencyCode = person.CurrencyCodes,
                    IpAddress = ApiConsumerHelpers.GetConsumerAddress(Request),
                    Person = person,
                    Platform = request.Platform,
                    Publisher = request.Publisher,
                    PointRate = currency.Rate,
                    TransferAmount = personBalance.Available,
                    Data = string.Empty,
                    TotalBet = 0m,
                    TotalResult = 0m,

                    CreatedBy = username,
                    CreatedDate = dtNow,
                    UpdatedBy = username,
                    UpdatedDate = dtNow

                };

                await auditSignIn.InsertAudit(this.altDBContext);
                this.altDBContext.GameSessions.Add(newActiveGameSession);
                this.altDBContext.Touch(personBalance);

                points = (long)(personBalance.Available / currency.Rate);
                personBalance.Available = 0;

                var secretKey = OCode.GetSecretKey();

                var playerKey = KeyDefine.GetPlayerKey(sessionOCode);
                var hashEntries = new HashEntry[] {
                    new HashEntry( KeyDefine.Username, person.Username),
                    new HashEntry( KeyDefine.Currency, person.CurrencyCodes),
                    new HashEntry( KeyDefine.IPHashKey, request.IPHashKey??""),
                    new HashEntry( KeyDefine.Publisher, request.Publisher??""),
                    new HashEntry( KeyDefine.Platform, request.Platform??""),
                    new HashEntry( KeyDefine.UplineOCode, OCode.Get(person.UplineID.Value)),
                    new HashEntry( KeyDefine.Secret, secretKey),
                    new HashEntry( KeyDefine.TokenMobile, request.TokenMobile??""),
                    new HashEntry( KeyDefine.Points, points),
                    new HashEntry( KeyDefine.PointRate, Convert.ToDouble(currency.Rate)),
                    new HashEntry( KeyDefine.SessionOCode, sessionOCode),
                };

                await this.playerServer.HashSetAsync(playerKey, hashEntries);

                var serverKey = KeyDefine.ServerPointKey;
                await this.playerServer.HashSetAsync(serverKey, sessionOCode, points);

                await this.altDBContext.SaveChangesAsync();
                trans.Commit();

                gameaccess.Username = person.Username;
                gameaccess.Points = points;
                gameaccess.PointRate = currency.Rate;
                gameaccess.SessionCode = sessionOCode;
                gameaccess.TokenMobile = string.Empty;
                gameaccess.Secret = secretKey;
                gameaccess.IPHashKey = request.IPHashKey;
                gameaccess.CurrencyCode = person.CurrencyCodes;
            }

            return gameaccess;
        }

        public GameAccessController(ALTEntities altDBContext, [Dependency(RedisConfigurations.PlayerServer)] IDatabase playerServer)
        {
            this.altDBContext = altDBContext;
            this.playerServer = playerServer;
        }

        private readonly ALTEntities altDBContext;
        private readonly IDatabase playerServer;

        public string WebGamePlayerBroadcastURL = ConfigurationManager.Get<string>(ApiConfigurations.Group, ApiConfigurations.WebGamePlayerBroadcastURL, string.Empty);
    }
}