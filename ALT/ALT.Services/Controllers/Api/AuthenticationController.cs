﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ALT.Services.Controllers
{
    using System.Threading.Tasks;
    using ALT.Common.Models;
    using ALT.Data.Entityframework;
    using Common;
    using ALT.Common;
    using StackExchange.Redis;
    using ALT.Common.Utilities;
    using System.Data.Entity;
    using ALT.Common.Models.Models;
    using ALT.Common.Helpers;
    using ATL.Common.Redis;
    using System.Web.Mvc.Html;
    using Microsoft.Practices.Unity;
    using ALT.Common.Queue;

    [RoutePrefix("api/authentication")]
    public class AuthenticationController : ApiController
    {
        [HttpPost, Route("")]
        [AllowAnonymous]
        public async Task<AuthenticationResp> SignIn(AuthenticationReq request)
        {
            var authentication = new AuthenticationResp();
            authentication = await SignInCore(request);
            return authentication;
        }

        [HttpPost, Route("signout")]
        [AllowAnonymous]
        public async Task<bool> SignOut(string token)
        {
            var result = false;

            result = await SignOutCore(token);

            return result;
        }

        private async Task<AuthenticationResp> SignInCore(AuthenticationReq request)
        {
            var authentication = new AuthenticationResp();

            ErrorStatuses.ThrowBadRequest(Request, "Invalid username or password", request == null);
            ErrorStatuses.ThrowBadRequest(Request, "Invalid username", String.IsNullOrEmpty(request.Username));
            ErrorStatuses.ThrowBadRequest(Request, "Invalid password", String.IsNullOrEmpty(request.Password));

            var username = request.Username.ToUpper();
            var notExpired = request.NotExpired;

            var person = await this.altDBContext.People.Where(c => c.Username.ToUpper() == username).FirstOrDefaultAsync();
            ErrorStatuses.ThrowBadRequest(Request, "Username, Password does not correct.", when: person == null);
            ErrorStatuses.ThrowBadRequest(Request, "Username, Password does not correct.", when: Password.ValidatePassword(person.Password, request.Password, false) == false);
            ErrorStatuses.ThrowBadRequest(Request, "Invalid currency.", when: true == string.IsNullOrEmpty(person.CurrencyCodes));

            ErrorStatuses.ThrowBadRequest(Request, "Account is suspended.", when: person.Status == PersonStatusTypeString.Suspended);
            ErrorStatuses.ThrowBadRequest(Request, "Account is disabled.", when: person.Status == PersonStatusTypeString.Disabled);

            ErrorStatuses.ThrowBadRequest(Request, "Your account does not allow login from here.", when: person.Type == PersonTypeString.Member || person.Type == PersonTypeString.Robot);

            var pesonOCode = OCode.Get(person.ID);
            var uplineOCode = person.UplineID.HasValue ? OCode.Get(person.UplineID.Value) : string.Empty;
            var uplineIDs = await this.altDBContext.GetPersonUplines(person.ID, true);

            using (var audit = new RequiredAuditContext(this.altDBContext, AuditType.SignInFail, nameof(SignInCore), nameof(AuthenticationController), nameof(SignInCore), base.Request))
            {
                audit.Detail = "Login";
                using (var trans = this.altDBContext.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                {
                    var token = OCode.Get(KeyGeneration.GenerateInt64Id());
                    var uplineType = string.Empty;
                    if (person.Type != PersonTypeString.Admin)
                    {
                        var upline = await this.altDBContext.People.Where(c => c.ID == person.UplineID).FirstOrDefaultAsync();
                        ErrorStatuses.ThrowBadRequest(Request, "Person does not correct.", when: null == upline);

                        ErrorStatuses.ThrowBadRequest(Request, "Account is suspended.", when: upline.Status == PersonStatusTypeString.Suspended);
                        ErrorStatuses.ThrowBadRequest(Request, "Account is disabled.", when: upline.Status == PersonStatusTypeString.Disabled);

                        uplineType = upline.Type;
                    }

                    var dtPersonBalance = (await this.altDBContext.LockPersonBalance(person.ID)).ToDictionary(c => Tuple.Create<long, string>(c.PersonID, c.CurrencyCode));
                    ErrorStatuses.ThrowBadRequest(Request, "Person does not correct.", when: (null == dtPersonBalance || dtPersonBalance.Count() <= 0) && person.Type != PersonTypeString.Admin);

                    string[] personCurrencys = personCurrencys = StringHelper.ToArray(person.CurrencyCodes);
                    var currencies = this.altDBContext.Currencies.Where(c => personCurrencys.Contains(c.CurrencyCode)).ToList();
                    ErrorStatuses.ThrowBadRequest(Request, "Invalid currency", when: currencies.Count == 0);

                    //save token on resdis
                    var personLoginKey = KeyDefine.GetPersonLoginKey(token);
                    var hashEntries = new HashEntry[] {
                    new HashEntry( KeyDefine.PersonOCode, pesonOCode),
                    new HashEntry( KeyDefine.Username, person.Username),
                    new HashEntry( KeyDefine.Currency, person.CurrencyCodes),
                    new HashEntry( KeyDefine.UplineOCode, uplineOCode),
                    new HashEntry( KeyDefine.PersonType, person.Type),
                    new HashEntry( KeyDefine.IsSubAccount, person.Type == PersonTypeString.SubAccount),
                    new HashEntry( KeyDefine.UplineOCode, person.UplineID.HasValue ? OCode.Get(person.UplineID.Value): string.Empty),
                    new HashEntry( KeyDefine.SubAccountOCode, string.Empty),
                    new HashEntry( KeyDefine.LoginTime, DateTime.Now.ToString(ALT.Common.Statics.DateTimeFulllFormat))
                };

                    await this.tokenServer.HashSetAsync(personLoginKey, hashEntries);

                    trans.Commit();

                    authentication.Token = token;
                    authentication.Username = person.Username;
                    authentication.Firstname = person.Firstname;
                    authentication.Lastname = person.Lastname;
                    authentication.Displayname = person.DisplayName;
                    authentication.PersonCode = pesonOCode;
                    authentication.Type = EnumHelper<PersonType>.Parse(person.Type);
                    authentication.UplineCode = uplineOCode;
                    authentication.UplineType = EnumHelper<PersonType>.ParseOrNull(uplineType);
                    authentication.Availables = dtPersonBalance.ToDictionary(c => c.Key.Item2, c => c.Value.Available);
                    authentication.PersonStatus = EnumHelper<PersonStatusType>.Parse(person.Status);
                    authentication.Currencies = currencies.Select(c => new CurrencyResp
                    {
                        Code = c.CurrencyCode,
                        Name = c.Name,
                        Rate = c.Rate,
                        Enable = c.Enable
                    }).ToList();

                    authentication.IsSubAccount = person.Type == PersonTypeString.SubAccount;
                    authentication.FixedCurrency = person.FixedCurrency;
                    authentication.CreateAgency = person.CreateAgency;
                    authentication.CreateMember = person.CreateMember;
                }
                audit.Token = authentication.Token;
                audit.After = "";
                audit.Detail = "Success Login";
                await audit.InsertAudit(this.altDBContext);
                audit.Done();
            }
            return authentication;
        }

        private async Task<bool> SignOutCore(string token)
        {
            var personLoginKey = KeyDefine.GetPersonLoginKey(token);

            await this.tokenServer.KeyDeleteAsync(personLoginKey);

            return true;
        }

        //LightInject
        //[Inject(RedisConfigurationsConst.CacheServer)]
        public AuthenticationController(ALTEntities context,
            [Dependency(RedisConfigurations.TokenServer)] StackExchange.Redis.IDatabase tokenServer
            //,Lazy<IQueuePublisher<string>> queue
            )
        {
            this.altDBContext = context;
            this.tokenServer = tokenServer;
            //this.queue = queue;
        }

        //private readonly Lazy<IQueuePublisher<string>> queue;
        private readonly ALTEntities altDBContext;
        private readonly StackExchange.Redis.IDatabase tokenServer;
    }
}