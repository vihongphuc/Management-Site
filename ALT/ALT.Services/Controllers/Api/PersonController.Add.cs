﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ALT.Services.Controllers.Api
{
    using ALT.Common.Models;
    using ALT.Common;
    using ALT.Common.Helpers;
    using ALT.Common.Models.Models;
    using ALT.Common.Utilities;
    using Common;
    using System.Data.Entity;
    using System.Threading.Tasks;
    using ALT.Services.Common.Logics;
    using ALT.Data.Entityframework;
    using ATL.Common.Redis;
    using ALT.Common.Models;
    using ALT.Common.Configuration;
    using Configurations;

    public partial class PersonController : ApiController
    {
        [HttpPost, Route("")]
        public async Task<bool> AddPerson(UpdatePersonReq personInfo)
        {
            var result = false;
            ErrorStatuses.ThrowBadRequest(Request, $"Invalid Person info.", when: false == ValidatePersonInfo(personInfo, isUpdate: false));

            var currentOCode = Request.GetCurrentPersonOCode();
            var currentUplineOCode = Request.GetCurrentPersonUplineOCode();
            var currentType = Request.GetCurrentPersonType();
            long currentPersonID, updatedUplinePersonID;

            ErrorStatuses.ThrowNotFound(Request, $"Invalid Current Person OCode {currentOCode}.", when: OCode.TryGetInt64(currentOCode, out currentPersonID) == false);
            ErrorStatuses.ThrowNotFound(Request, $"Invalid requested upline Person OCode {personInfo.UplineOcode}.", when: OCode.TryGetInt64(personInfo.UplineOcode, out updatedUplinePersonID) == false && currentType != PersonType.Admin);
            ErrorStatuses.ThrowNotFound(Request, $"Invalid Upline PersonID {personInfo.UplineOcode}.", when: currentType != PersonType.Admin && !personInfo.UplineOcode.Equals(currentOCode));
            ErrorStatuses.ThrowNotFound(Request, $"Person is Disabled or Suspended.", when: await PersonLogic.CheckUplineStatus(currentPersonID, this.altDBContext) == false);

            var dbPerson = await this.altDBContext.People.Where(c => c.ID == currentPersonID).FirstOrDefaultAsync();
            ErrorStatuses.ThrowNotFound(Request, $"Person cannot found.", when: dbPerson == null);

            var allCheckingPersonIDs = new HashSet<long> { updatedUplinePersonID, currentPersonID };
            var dtPeople = await this.altDBContext.People
                                                  .Where(c => allCheckingPersonIDs.Contains(c.ID))
                                                  .ToDictionaryAsync(c => Tuple.Create<long, string>(c.ID, c.CurrencyCodes));

            //var currentPersonCurrencies = StringHelper.ToArray(dbPerson.CurrencyCodes);
            //ErrorStatuses.ThrowNotFound(Request, $"Currency does not correct.", when: personInfo.Currencies.Any(c => currentPersonCurrencies.Any(cc => cc == c) == false));

            var allCheckingUpdatedCurrencies = new HashSet<string>(personInfo.Currencies);
            var dbCurrencies = await this.altDBContext.Currencies.Where(c => allCheckingUpdatedCurrencies.Contains(c.CurrencyCode))
                                                                 .ToArrayAsync();
            ErrorStatuses.ThrowNotFound(Request, $"Currency does not correct.", when: dbCurrencies == null || dbCurrencies.Length == 0);

            var dtBalances = await this.altDBContext.PersonBalances.Where(c => dbPerson.ID == c.PersonID)
                                                                    //.Where(c => allCheckingUpdatedCurrencies.Contains(c.CurrencyCode))
                                                                    .ToDictionaryAsync(c => Tuple.Create<long, string>(c.PersonID, c.CurrencyCode));
            ErrorStatuses.ThrowNotFound(Request, $"Person cannot found.", when: dtBalances == null || dtBalances.Count() < dbCurrencies.Count());

            var dtMaxCredits = await this.altDBContext.PersonCreditSettings
                                                      //.Where(c => allCheckingPersonIDs.Contains(c.PersonID))
                                                      .ToDictionaryAsync(c => Tuple.Create<long, string>(c.PersonID, c.CurrencyCode), c => c);

            var dtBetSettings = await this.altDBContext.PersonBetSettings
                                                       //.Where(c => allCheckingPersonIDs.Contains(c.PersonID))
                                                       .ToDictionaryAsync(c => Tuple.Create<long, int, string, string>(c.PersonID, c.BetTypeItemID, c.CurrencyCode, c.SettingType), c => c);

            var dtPositionTakings = await this.altDBContext.PositionTakings
                                                            //.Where(c => c.PersonID == currentPersonID || (c.Person.UplineID.HasValue && c.Person.UplineID.Value == currentPersonID))
                                                            .ToDictionaryAsync(c => Tuple.Create<long, int, string, string>(c.PersonID, c.BetTypeItemID, c.CurrencyCode, c.Type), c => c);

            var newPersonID = KeyGeneration.GenerateInt64Id();
            using (var trans = this.altDBContext.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
            {
                var updatedSuccess = true;
                var _inx = 0;

                var newPerson = new Person
                {
                    ID = newPersonID,
                    Username = personInfo.Username.ToUpper(),
                    CurrencyCodes = StringHelper.Compile(personInfo.Currencies, Statics.comma.ToString()),
                    Password = Password.CreateHash(personInfo.Password),
                    DisplayName = personInfo.DisplayName,
                    UplineID = dbPerson.ID,
                    Status = personInfo.Status.ToString(),
                    Type = personInfo.Type.ToString(),

                    Address = personInfo.Address,
                    CountryCode = personInfo.CountryCode,
                    Firstname = personInfo.Firstname,
                    Lastname = personInfo.Lastname,
                    Email = personInfo.Email,
                    Phone = personInfo.Phone,
                    DOB = personInfo.DOB,
                    LimitIP = personInfo.LimitIP,
                    Mobile = personInfo.Mobile,
                    AutoTransfer = personInfo.AutoTransfer,
                    IsExternal = personInfo.External,
                    FixedCurrency = personInfo.FixedCurrency,
                    CreateAgency = personInfo.CreateAgency,
                    CreateMember = personInfo.CreateMember,

                    LastLoginIP = string.Empty,
                    LastLoginTime = null,
                    CreatedBy = dbPerson.Username,
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    UpdatedBy = dbPerson.Username

                };
                this.altDBContext.People.Add(newPerson);

                foreach (var currency in dbCurrencies)
                {
                    if (!updatedSuccess)
                    {
                        break;
                    }

                    PersonBalance personBalance;
                    ErrorStatuses.ThrowBadRequest(Request, $"Invalid Upline Person Balance.", when: false == dtBalances.TryGetValue(Tuple.Create<long, string>(dbPerson.ID, dbPerson.CurrencyCodes), out personBalance) && currentType != PersonType.Admin);

                    AuditContext auParent = null;
                    var dbPersonID = dbPerson.ID;
                    if (currentType != PersonType.Admin)
                    {
                        auParent = new AuditContext(AuditType.Balance, nameof(AddPerson), nameof(PersonController), nameof(AddPerson), Request)
                        {
                            PersonID = dbPerson.ID,
                            RelatedID = dbPerson.ID,
                            Token = currentOCode,
                            Detail = $"Update balance after adding {dbPerson.Username}:{dbPerson.Type}",
                            Before = $"Available: {personBalance.Available}",
                            ActionTime = DateTime.Now,

                            CreatedBy = dbPerson.Username,
                            CreatedTime = DateTime.Now
                        };

                        await auParent.InsertAudit(this.altDBContext);
                        this.altDBContext.Touch(personBalance);
                    }

                    var auPerson = new AuditContext(AuditType.Balance, nameof(AddPerson), nameof(PersonController), nameof(AddPerson), Request)
                    {
                        PersonID = newPerson.ID,
                        RelatedID = newPerson.ID,
                        Token = currentOCode,
                        Detail = $"Update balance after adding {personInfo.Username}:{personInfo.Type}",
                        ActionTime = DateTime.Now,
                        Before = $"Available: 0",

                        CreatedBy = dbPerson.Username,
                        CreatedTime = DateTime.Now
                    };

                    var newPersonBalance = new PersonBalance
                    {
                        PersonID = newPerson.ID,
                        CurrencyCode = currency.CurrencyCode,
                        Available = personInfo.PersonBalances.Where(c => c.CurrencyCode == currency.CurrencyCode).Select(c => c.Available ?? 0m).FirstOrDefault(),
                        YesterdayAvailable = 0m,
                        LastUpdatedDate = DateTime.Now,

                        UpdatedBy = dbPerson.Username,
                        UpdatedDate = DateTime.Now,
                        CreatedBy = dbPerson.Username,
                        CreatedDate = DateTime.Now
                    };
                    this.altDBContext.PersonBalances.Add(newPersonBalance);

                    await auPerson.InsertAudit(this.altDBContext);

                    //  ErrorStatuses.ThrowBadRequest(Request, $"Invalid Deposit amount.", when: ValidateTransferAmount(newPersonBalance, personBalance, TransferType.Deposit, newPersonBalance.Available) == false && currentType != PersonType.Admin);

                    if (updatedSuccess)
                    {
                        var lastedAmount = await UpdatePersonBalance(newPersonBalance, newPerson, auPerson, personBalance, dbPerson, auParent, currency.CurrencyCode, TransferType.Deposit, newPersonBalance.Available, activeGameSession: null);
                        updatedSuccess = lastedAmount >= 0m;

                        if (updatedSuccess && newPersonBalance.Available > 0)
                            updatedSuccess = TransferLogic.AddStatement(this.altDBContext, currency.CurrencyCode, newPerson, dbPerson, TransferTypeString.Deposit, TransferTypeString.DepositTo, newPersonBalance.Available, DateTime.Now, dbPerson.Username);
                    }

                    if (updatedSuccess)
                        updatedSuccess = UpdateMaxCredit(currency.CurrencyCode, newPerson, dbPerson, personInfo.PersonMaxCredits, dtMaxCredits, newPersonBalance, dbPerson.Username);

                    if (updatedSuccess)
                        updatedSuccess = UpdateBetSetting(currency.CurrencyCode, newPerson, dbPerson, personInfo.BetSettings, dtBetSettings, dbPerson.Username);

                    if (updatedSuccess)
                        updatedSuccess = UpdatePositionTaking(currency.CurrencyCode, newPerson, dbPerson, personInfo.PositionTakings, dtPositionTakings, dbPerson.Username);

                    if (_inx == personInfo.Currencies.Length - 1)
                    {
                        if (updatedSuccess)
                            updatedSuccess = await UpdatePersonGroups(newPerson, dbPerson, dbPerson.Username);

                        if (updatedSuccess)
                            updatedSuccess = await CreateUplinePathAsync(dbPerson, newPerson);
                    }

                    _inx++;
                }

                if (updatedSuccess)
                {
                    try
                    {
                        await this.altDBContext.SaveChangesAsync();
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {

                        var exs = ex;
                        result = false;
                    }
                }
                else
                {
                    trans.Rollback();
                }
                result = updatedSuccess;
            }
            return result;
        }

        private bool ValidatePersonInfo(UpdatePersonReq personInfo, bool isUpdate = false)
        {
            var validated = true;
            validated = personInfo != null;
            if (validated && !isUpdate)
            {
                validated = !string.IsNullOrEmpty(personInfo.Password);
            }

            if (validated)
                validated = !string.IsNullOrEmpty(personInfo.Username) &&
                            !string.IsNullOrEmpty(personInfo.Firstname) && !string.IsNullOrEmpty(personInfo.Lastname) && !string.IsNullOrEmpty(personInfo.DisplayName);
            if (validated)
                validated = personInfo.Currencies.Length > 0;

            if (validated)
                validated = personInfo.PersonMaxCredits.Count > 0 && personInfo.PersonBalances.Count > 0 &&
                            personInfo.BetSettings.Count > 0 && personInfo.PositionTakings.Count > 0;

            return validated;
        }

        private async Task<decimal> UpdatePersonBalance(PersonBalance updatedPersonBalance, Person updatedPerson, AuditContext updatedPersonAudit,
                                                        PersonBalance uplineBalance, Person upline, AuditContext uplineAudit, string currencyCode,
                                                        TransferType transferType, decimal amount, GameSession activeGameSession, bool isUpdate = false)
        {
            var lastedAmount = -1m;
            var diff = amount * (transferType == TransferType.Deposit || transferType == TransferType.WithdrawFrom ? 1 : -1); ;

            if (updatedPerson.Type == PersonTypeString.Member &&
                activeGameSession != null && activeGameSession.FinishedTime.HasValue == false)
            {
                var token = OCode.Get(activeGameSession.ID);
                var currentPointResult = await RedisScripts.GameState.GetCurrentPoint(token, null);
                if (currentPointResult.IsNull)
                {
                    ErrorStatuses.ThrowBadRequest(Request, $"Failed to get point of : {token} from redis.", when: true, on: () =>
                    {
                        if (uplineAudit != null)
                            uplineAudit.Detail = $"Failed to get point of : {token} from redis.";
                        if (uplineAudit != null)
                            uplineAudit.Detail = $"Failed to get point of : {token} from redis.";
                    });
                }

                var result = await RedisScripts.GameState.ModifyPoint(token, (long)(diff / (activeGameSession.PointRate ?? 1m)));
                ErrorStatuses.ThrowBadRequest(Request, "Not enough available balance", when: !result.Success, on: () =>
                {
                    if (uplineAudit != null)
                        uplineAudit.Detail = $"Not enough available balance to cash out, token: {token}";
                    if (uplineAudit != null)
                        uplineAudit.Detail = $"Not enough available balance to cash out, token: {token}";
                });

                updatedPersonAudit.Before = $"Available: {(long)currentPointResult * (activeGameSession.PointRate ?? 1m)}";
                lastedAmount = result.CurrentPoint * (activeGameSession.PointRate ?? 0m);
                activeGameSession.TransferAmount += diff;
                updatedPersonAudit.After = $"Available: {lastedAmount}";

                if (activeGameSession != null)
                {
                    try
                    {
                        using (var httpClient = new HttpClient()
                        {
                            BaseAddress = new Uri(this.WebGamePlayerBroadcastURL)
                        })
                        {
                            await httpClient.PostAsync($"broadcast/PointUpdated?token={token}&add={(long)(diff / (activeGameSession.PointRate ?? 1m))}&popup={true}", null);
                        }
                    }
                    catch
                    {

                    }
                }
            }
            else
            {
                if (isUpdate)
                {
                    updatedPersonBalance.Available += diff;
                }
                else
                {
                    updatedPersonBalance.Available = Math.Abs(diff);
                }

                lastedAmount = updatedPersonBalance.Available;
            }

            if (upline.Type != PersonTypeString.Admin)
            {
                updatedPersonAudit.Before = $"Available: {uplineBalance.Available}";

                uplineBalance.Available -= diff * ConvertCurrencyRate(currencyCode, uplineBalance.CurrencyCode);
                uplineBalance.UpdatedDate = DateTime.Now;
                uplineBalance.LastUpdatedDate = DateTime.Now;
                uplineBalance.UpdatedBy = upline.Username;

                updatedPersonAudit.After = $"Available: {uplineBalance.Available}";
            }

            return lastedAmount;
        }

        private bool UpdateMaxCredit(string currencyCode, Person updatedPerson, Person upline, IList<PersonMaxCreditReq> creditSettingReqs, Dictionary<Tuple<long, string>, PersonCreditSetting> dtCredits, PersonBalance newPersonBalance, string actor, bool isUpdate = false)
        {
            var success = true;

            var dbPersonCredits = new PersonCreditSetting();
            foreach (var item in creditSettingReqs.Where(c => c.CurrencyCode == currencyCode))
            {

                if (dtCredits.TryGetValue(Tuple.Create<long, string>(upline.ID, item.CurrencyCode), out dbPersonCredits))
                {
                    if (dbPersonCredits.MaxCredit > 0 && dbPersonCredits.MaxCredit < item.MaxCredit)
                    {
                        ErrorStatuses.ThrowBadRequest(Request, $"Current Max Credit does not correct.", when: true);
                        success = false;
                        break;
                    }

                    if (newPersonBalance.Available < item.MaxCredit)
                    {
                        ErrorStatuses.ThrowBadRequest(Request, $"Invalid max member credit amount.", when: true);
                        success = false;
                        break;
                    }

                    if (newPersonBalance.Available > dbPersonCredits.MaxCredit && updatedPerson.Type == PersonTypeString.Member)
                    {
                        ErrorStatuses.ThrowBadRequest(Request, $"Invalid max member credit amount.", when: true);
                        success = false;
                        break;
                    }
                }

                try
                {
                    if (isUpdate)
                    {
                        if (!dtCredits.TryGetValue(Tuple.Create<long, string>(updatedPerson.ID, currencyCode), out dbPersonCredits))
                        {
                            dbPersonCredits = new PersonCreditSetting
                            {
                                PersonID = updatedPerson.ID,
                                CurrencyCode = item.CurrencyCode,
                                MaxCredit = item.MaxCredit ?? 0m,
                                PersonType = item.PersonType.Value.ToString(),

                                CreatedBy = actor,
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now,
                                UpdatedBy = actor
                            };
                            updatedPerson.PersonCreditSettings.Add(dbPersonCredits);
                        }
                        else
                        {
                            dbPersonCredits.MaxCredit = (decimal)item.MaxCredit;
                            dbPersonCredits.UpdatedDate = DateTime.Now;
                            dbPersonCredits.UpdatedBy = actor;
                            this.altDBContext.Entry(dbPersonCredits).State = EntityState.Modified;
                        }
                    }
                    else
                    {
                        dbPersonCredits = new PersonCreditSetting
                        {
                            PersonID = updatedPerson.ID,
                            CurrencyCode = currencyCode,
                            MaxCredit = (decimal)item.MaxCredit,
                            PersonType = item.PersonType == null ? updatedPerson.Type.ToString() : item.PersonType.ToString(),

                            CreatedBy = actor,
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            UpdatedBy = actor

                        };
                        updatedPerson.PersonCreditSettings.Add(dbPersonCredits);
                    }
                }
                catch (WebException ex)
                {
                    success = false;
                }
            }
            return success;
        }

        private bool UpdateBetSetting(string currencyCode, Person updatedPerson, Person upline, IList<PersonBetSettingReq> betsettingReqs, Dictionary<Tuple<long, int, string, string>, PersonBetSetting> dtBetSettings, string actor, bool isUpdate = false)
        {
            var success = true;

            foreach (var betSetting in betsettingReqs.Where(c => c.CurrencyCode == currencyCode))
            {
                PersonBetSetting uplineBetSetting;

                //ErrorStatuses.ThrowBadRequest(Request, $"Upline doesnot have BetSetting", when: false == dtBetSettings.TryGetValue(Tuple.Create<long, int, string, string>(upline.ID, (int)OCode.ToInt64(betSetting.BetTypeItemOcode), currencyCode, betSetting.SettingType.ToString()), out uplineBetSetting));
                //ErrorStatuses.ThrowBadRequest(Request, $"Upline doesnot have BetSetting", when: (uplineBetSetting.Value ?? 0m) != 0 &&
                //                                                                                (uplineBetSetting.Value ?? 0m) < (betSetting.Value ?? 0m));

                try
                {
                    if (isUpdate)
                    {
                        var _betSettingUpdates = updatedPerson.PersonBetSettings.Where(c => c.BetTypeItemID == OCode.ToInt64(betSetting.BetTypeItemOcode))
                                                                                .Where(c => c.SettingType == betSetting.SettingType.ToString())
                                                                                .Where(c => c.CurrencyCode == currencyCode)
                                                                                .FirstOrDefault();
                        if (true == dtBetSettings.TryGetValue(Tuple.Create<long, int, string, string>(updatedPerson.ID, (int)OCode.ToInt64(betSetting.BetTypeItemOcode), currencyCode, betSetting.SettingType.ToString()), out _betSettingUpdates))
                        {
                            _betSettingUpdates.Value = betSetting.Value;
                            _betSettingUpdates.ValueString = betSetting.ValueString;
                            _betSettingUpdates.UpdatedBy = actor;
                            _betSettingUpdates.UpdatedDate = DateTime.Now;
                            this.altDBContext.Entry(_betSettingUpdates).State = EntityState.Modified;
                        }
                        else
                        {
                            var _betSettings = new PersonBetSetting
                            {
                                PersonID = updatedPerson.ID,
                                CurrencyCode = currencyCode,
                                BetTypeItemID = Convert.ToInt32(OCode.ToInt64(betSetting.BetTypeItemOcode)),
                                SettingType = betSetting.SettingType.ToString(),
                                Value = betSetting.Value,
                                ValueString = betSetting.ValueString,

                                CreatedBy = actor,
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now,
                                UpdatedBy = actor
                            };

                            updatedPerson.PersonBetSettings.Add(_betSettings);
                        }
                    }
                    else
                    {
                        var _betSettings = new PersonBetSetting
                        {
                            PersonID = updatedPerson.ID,
                            CurrencyCode = currencyCode,
                            BetTypeItemID = Convert.ToInt32(OCode.ToInt64(betSetting.BetTypeItemOcode)),
                            SettingType = betSetting.SettingType.ToString(),
                            Value = betSetting.Value,
                            ValueString = betSetting.ValueString,

                            CreatedBy = actor,
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            UpdatedBy = actor
                        };

                        updatedPerson.PersonBetSettings.Add(_betSettings);
                    }
                }
                catch (WebException ex)
                {
                    success = false;
                }
            }

            return success;
        }

        private bool UpdatePositionTaking(string currencyCode, Person updatedPerson, Person upline, IList<PositionTakingReq> positionTakingReqs, Dictionary<Tuple<long, int, string, string>, PositionTaking> dtPositionTakings, string actor, bool isUpdate = false)
        {
            var success = true;
            if (isUpdate)
            {

                foreach (var betTypeItemOCode in new string[] { BetTypeItemOCodes.SlotGame })
                {
                    var betTypeItemID = (int)OCode.ToInt64(betTypeItemOCode);
                    var updatedCurrentMaxValue = positionTakingReqs.Where(c => c.BetTypeItemOcode == betTypeItemOCode)
                                                                    .Where(c => c.CurrencyCode == currencyCode)
                                                                    .Where(c => c.Type == PostitionTakingType.CurrentMax)
                                                                    .Select(c => c.Value)
                                                                    .FirstOrDefault();

                    var downlinePTAcc = 0m;
                    foreach (var item in dtPositionTakings.Where(c => c.Key.Item1 != updatedPerson.ID)
                                                            .Where(c => c.Key.Item2 == betTypeItemID)
                                                            .Where(c => c.Key.Item3 == currencyCode)
                                                            .GroupBy(c => c.Key.Item1))
                    {
                        downlinePTAcc = item.Sum(c => c.Value.Value ?? 0m);
                        ErrorStatuses.ThrowBadRequest(Request, $"Downlines is aldready used all Upline PTs.", when: updatedCurrentMaxValue < downlinePTAcc);
                    }

                    if (success == false)
                        return success;
                }
            }

            try
            {
                foreach (var positionTaking in positionTakingReqs.Where(c => c.CurrencyCode == currencyCode))
                {
                    var betTypeItemID = Convert.ToInt32(OCode.ToInt64(positionTaking.BetTypeItemOcode));
                    if (isUpdate)
                    {
                        PositionTaking _ptUpdates;
                        if (false == dtPositionTakings.TryGetValue(Tuple.Create<long, int, string, string>(updatedPerson.ID, (int)OCode.ToInt64(positionTaking.BetTypeItemOcode), currencyCode, positionTaking.Type.ToString()), out _ptUpdates))
                        {
                            _ptUpdates = new PositionTaking
                            {
                                PersonID = updatedPerson.ID,
                                CurrencyCode = currencyCode,
                                BetTypeItemID = Convert.ToInt32(OCode.ToInt64(positionTaking.BetTypeItemOcode)),
                                Type = positionTaking.Type.ToString(),
                                Value = positionTaking.Value,

                                CreatedBy = actor,
                                CreatedDate = DateTime.Now,
                                UpdatedDate = DateTime.Now,
                                UpdatedBy = actor
                            };
                            this.altDBContext.PositionTakings.Add(_ptUpdates);
                        }
                        else
                        {
                            _ptUpdates.Value = positionTaking.Value;
                            _ptUpdates.UpdatedBy = actor;
                            _ptUpdates.UpdatedDate = DateTime.Now;
                            this.altDBContext.Entry(_ptUpdates).State = EntityState.Modified;
                        }
                    }
                    else
                    {
                        var _ptPositionTakings = new PositionTaking
                        {
                            PersonID = updatedPerson.ID,
                            CurrencyCode = currencyCode,
                            BetTypeItemID = betTypeItemID,
                            Type = positionTaking.Type.ToString(),
                            Value = positionTaking.Value,

                            CreatedBy = actor,
                            CreatedDate = DateTime.Now,
                            UpdatedDate = DateTime.Now,
                            UpdatedBy = actor
                        };
                        this.altDBContext.PositionTakings.Add(_ptPositionTakings);
                    }
                }
            }
            catch (WebException ex)
            {
                success = false;
            }
            return success;
        }

        private async Task<bool> UpdatePersonGroups(Person updatedPerson, Person upline, string actor, bool isUpdate = false)
        {
            var success = true;
            try
            {
                var uplineGroupPermissionIDs = await this.altDBContext.PersonGroups.Where(c => upline.ID == c.PersonID)
                                                          .Select(c => c.GroupPermissionID).Distinct().ToArrayAsync();

                if (isUpdate)
                {
                    ;// implement later.
                }
                else
                {
                    var groupPermissions = uplineGroupPermissionIDs.Select(c => new PersonGroup
                    {
                        PersonID = updatedPerson.ID,
                        GroupPermissionID = c,
                        CreatedBy = upline.Username,
                        CreatedDate = DateTime.Now
                    }).ToArray();

                    foreach (var item in groupPermissions)
                    {
                        updatedPerson.PersonGroups.Add(item);
                    }
                }
            }
            catch
            {
                success = false;
            }
            return success;
        }

        private async Task<bool> CreateUplinePathAsync(Person parent, Person dbPerson)
        {
            var success = true;
            try
            {
                var currentUserUplines = await this.altDBContext.PersonUplines
                                                              .Where(c => c.PersonID == parent.ID)
                                                              .FirstOrDefaultAsync();

                var allUplines = new PersonUpline();
                if (currentUserUplines != null)
                {
                    allUplines.CopyFrom(currentUserUplines);
                }
                allUplines.Set(parent.Type, parent.ID);
                allUplines.PersonID = dbPerson.ID;
                this.altDBContext.PersonUplines.Add(allUplines);
            }
            catch
            {
                success = false;
            }
            return success;
        }

        public decimal ConvertCurrencyRate(string FromCurrency, string ToCurrency)
        {
            decimal result = 0m;
            var qrCurrencies = this.altDBContext.Currencies as IQueryable<Currency>;

            decimal fromRateCurrency = qrCurrencies.Where(c => c.CurrencyCode == FromCurrency).Select(c => c.Rate).FirstOrDefault();
            decimal toRateCurrency = qrCurrencies.Where(c => c.CurrencyCode == ToCurrency).Select(c => c.Rate).FirstOrDefault();

            result = fromRateCurrency / toRateCurrency;
            return result;
        }

    }
}