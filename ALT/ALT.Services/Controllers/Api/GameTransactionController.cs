﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ALT.Services.Controllers.Api
{
    using System.Data.Entity;
    using System.Threading.Tasks;
    using ALT.Common.Models;
    using System.Net.Http.Formatting;
    using Data.Entityframework;
    using ALT.Services.Common;
    using ALT.Common.Utilities;
    using ALT.Common;
    using ATL.Common.Redis;
    using StackExchange.Redis;
    using Microsoft.Practices.Unity;
    using ALT.Common.Configuration;
    using Configurations;
    using System.Threading;
    using System.IO;
    using Newtonsoft.Json;
    using ALT.Common.Queue;


    //using RabbitMQ.Client;
    using System.Text;
    using ALT.Common.Logging;

    [RoutePrefix("api/game-transaction")]
    public class GameTransactionController : ApiController
    {
        [HttpPost, Route("test-queue")]
        public async Task<bool> PublishToQueue(string data)
        {
            var result = false;
            data = $"{data}:{DateTime.Now}";

            //await SlotGameTransactionQueue.Value.Publish<string>(data).ContinueWith(c =>
            //{
            //    result = c.IsCompleted;
            //});

            var persons = new ALT.Common.Models.Models.ChechUsernameResp[]
            {
                new ALT.Common.Models.Models.ChechUsernameResp {
                    IsCheck = true,
                    Username = data
                },
                new ALT.Common.Models.Models.ChechUsernameResp {
                    IsCheck = true,
                    Username = data
                }
            };

            var _channel3 = queueConnection.CreatePublisher<ALT.Common.Models.Models.ChechUsernameResp[]>("SLOT.GameTransaction");
            await _channel3.Publish<ALT.Common.Models.Models.ChechUsernameResp[]>(persons).ContinueWith(c =>
            {
                result = c.IsCompleted;
            });

            return result;
        }


        [HttpPost, Route("test")]
        public async Task<bool> AddGameTransactionTest(GameTransactionReq request)
        {
            var requests = new GameTransactionReq[] { request };
            var result = false;

            if (IsUsedQueueTransaction)
            {
                await SlotGameTransactionQueue.Value.Publish<ALT.Common.Models.GameTransactionReq[]>(requests).ContinueWith(c =>
                {
                    result = c.IsCompleted;
                });

                //var _channel3 = queueConnection.CreatePublisher<ALT.Common.Models.GameTransactionReq[]>("SLOT.GameTransaction");
                //await _channel3.Publish<GameTransactionReq[]>(requests).ContinueWith(c =>
                //{
                //    result = c.IsCompleted;
                //});

                //using (var publisher = queueConnection.CreatePublisher<GameTransactionReq[]>(".Retry"))
                //{
                //    await publisher.Publish(requests, new Dictionary<string, object>
                //    {
                //        ["EventType"] = "SLOT.GameTransaction"
                //    });
                //}

            }
            else
            {
                result = await AddGameTransaction(requests);
            }

            return result;
        }
        [HttpPost, Route("")]
        public async Task<bool> AddGameTransaction(GameTransactionReq[] requests)
        {
            var success = false;
            WriteLogFile(requests);
            // need to seperate to: AckOnly && non-Ackonly
            var dtRequestData = requests.Where(c => string.IsNullOrEmpty(c.SessionOCode) == false)
                                        .GroupBy(c => c.SessionOCode)
                                        .ToDictionary(c => c.Key, c => c.Select(cc => cc).ToArray());

            var rejectLists = new List<GameTransactionReq>();
            if (dtRequestData.Count > 0)
            {
                await semaphore.WaitAsync();
                try
                {
                    var gameCodes = requests.Select(c => c.GameCode).Distinct().ToArray();
                    var dtGames = await this.altDBContext.Games.Where(c => gameCodes.Contains(c.Code)).ToDictionaryAsync(c => c.Code);
                    var betTypeItemIDs = dtGames.Values.Select(c => c.BetTypeItemID).Distinct().ToArray();
                    foreach (var item in dtRequestData)
                    {
                        var _reject = await ALT.Common.Worker.GameTransaction.UpdaterProcess.UpdateBySession(this.altDBContext, item.Key, item.Value, dtGames, betTypeItemIDs);
                        if (_reject.Length > 0)
                        {
                            rejectLists.AddRange(_reject);
                        }
                    }
                }
                catch (Exception ex)
                {
                    WriteExceptionLogFile(ex);// Retry reject List.

                    success = false;

                }
                finally
                {
                    semaphore.Release();
                }
            }

            if (rejectLists.Count > 0)
            {
                WriteLogFile(requests);// Retry reject List.
            }

            return success;
        }
               
        private bool WriteExceptionLogFile(Exception ex)
        {
            var result = true;

            var name = $"EX: GameTransactionRequest-Exception.{DateTime.Now.ToString("yyyyMMddHHmmss")}.trn";
            try
            {
                this.logger.Error(() => name);
            }
            catch (Exception exx)
            {
                result = false;
            }

            return result;
        }
        private bool WriteLogFile(GameTransactionReq[] request)
        {
            var result = true;

            var path = $"GameTransactionRequest.{string.Join(";", request.Select(c => $"{c.GameTransactionOCode}_{c.SessionOCode}"))}.trn";
            var json = JsonConvert.SerializeObject(request);
            try
            {
                this.logger.Debug($"{path} is exsts");
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        private bool DeleteLogFile(string path)
        {
            var result = true;

            try
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }
            catch (Exception ex)
            {
                //this.logger.Debug($"Path: {path}, Error in deleting AllBetSettleRequest to log: {ex}");
                result = false;
            }

            return result;
        }

        public GameTransactionController(ALTEntities altDBContext, [Dependency(RedisConfigurations.TokenServer)] IDatabase tokenServer,
                                                                    [Dependency(RedisConfigurations.PlayerServer)] IDatabase playerServer,
                                                                    Lazy<IQueuePublisher<ALT.Common.Models.GameTransactionReq[]>> SlotGameTransactionQueue,
                                                                    IQueueConnection queueConnection,
                                                                    ICommonLogger logger)
        {
            this.altDBContext = altDBContext;
            this.tokenServer = tokenServer;
            this.playerServer = playerServer;

            this.SlotGameTransactionQueue = SlotGameTransactionQueue;
            this.queueConnection = queueConnection;
            this.logger = logger;
        }

        private readonly ALTEntities altDBContext;
        private readonly IDatabase tokenServer;
        private readonly IDatabase playerServer;

        private const int DefaultConcurrentUpdateRun = 5;
        private static readonly SemaphoreSlim semaphore = new SemaphoreSlim(DefaultConcurrentUpdateRun, DefaultConcurrentUpdateRun);
        public string WebGamePlayerBroadcastURL = ConfigurationManager.Get<string>(ApiConfigurations.Group, ApiConfigurations.WebGamePlayerBroadcastURL, string.Empty);

        public bool IsUsedQueueTransaction = ConfigurationManager.Get<bool>(ApiConfigurations.Group, ApiConfigurations.IsUsedQueueTransaction, false);

        private readonly Lazy<IQueuePublisher<ALT.Common.Models.GameTransactionReq[]>> SlotGameTransactionQueue;
        private IQueueConnection queueConnection;
        private ICommonLogger logger;
    }
}