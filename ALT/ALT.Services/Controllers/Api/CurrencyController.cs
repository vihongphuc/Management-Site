﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ALT.Services.Controllers.Api
{
    using System.Threading.Tasks;
    using System.Data.Entity;
    using Data.Entityframework;
    using StackExchange.Redis;
    using ALT.Common;
    using ALT.Common.Models.Models;
    using Common;
    using ALT.Common.Helpers;
    using ALT.Common.Models;
    using ALT.Common.Utilities;
    using ALT.Services.Infrastructer;
    using Microsoft.Practices.Unity;
    using ATL.Common.Redis;

    [TokenFilterAttribute]
    [RoutePrefix("api/currencies")]
    public class CurrencyController : ApiController
    {
        [HttpGet, Route("get-currency")]
        public async Task<CurrencyResp[]> GetAllCurrency()
        {
            var currencies = await this.altDBContext.Currencies.ToListAsync();
            var _getCurrencies = currencies.Select(c => new CurrencyResp
            {
                Code = c.CurrencyCode,
                Name = c.DisplayName,
                Rate = c.Rate,
                Enable = c.Enable
            }).ToArray();

            return _getCurrencies;
        }

        public CurrencyController(ALTEntities altDBContext)
        {
            this.altDBContext = altDBContext;
        }

        private readonly ALTEntities altDBContext;
    }
}