﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Worker
{
    public class PersonData
    {
        public string CurrencyCode { get; set; }
        public string Username { get; set; }
        public long PersonID { get; set; }
        public long? UplineID { get; set; }
        public Models.PersonType PersonType { get; set; }
        public Models.PersonType? UplineType { get; set; }

        /// <summary>
        /// int: BetTypeItemID
        /// </summary>
        public Dictionary<int, decimal> UplinePTs { get; set; }
    }
}
