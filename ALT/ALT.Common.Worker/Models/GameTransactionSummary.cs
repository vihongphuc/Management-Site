﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Worker
{
    [Serializable]
    public class GameTransactionSummary
    {
        public long PersonID { get; set; }
        public int  GameID { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }

        public decimal? TotalAmount { get; set; }
        public decimal? TotalResult { get; set; }
        public decimal? TotalWin { get; set; }
        public decimal? TotalLoss { get; set; }
        public decimal? TotalWinLoss { get; set; }

        public decimal? TotalAdminWinLoss { get; set; }
        public decimal? TotalShareHolderWinLoss { get; set; }
        public decimal? TotalSuperSeniorWinLoss { get; set; }
        public decimal? TotalSeniorWinLoss { get; set; }
        public decimal? TotalMasterWinLoss { get; set; }
        public decimal? TotalAgentWinLoss { get; set; }

        public decimal PointRate { get; set; }
    }
}
