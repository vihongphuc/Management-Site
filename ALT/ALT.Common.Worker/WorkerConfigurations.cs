﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Worker
{
    public static class WorkerConfigurations
    {
        public static readonly string Group = "Worker";
        public static readonly string RetryHeaderArgument = "RetryHeaderArgument";
        public static readonly string DefaultRetryHeaderArgumentValue = "EventType";
    }
}
