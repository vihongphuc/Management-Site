﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Worker.GameTransaction
{
    using ALT.Data.Entityframework;
    using ALT.Common;
    using Models;
    using System.Data.Entity;

    public static class UpdaterProcess
    {
        public static async Task<GameTransactionReq[]> UpdateBySession(ALTEntities altEntities, string gameSessionOCode, Models.GameTransactionReq[] dataRequests, Dictionary<string, Game> dtGame, int[] betTypeItemIDs)
        {
            var rejectRequestData = new List<GameTransactionReq>();
            long gameSessionID = 0L;
            if (false == OCode.TryGetInt64(gameSessionOCode, out gameSessionID))
            {
                return rejectRequestData.ToArray();
            }

            var processingGameTransationRequests = dataRequests.Select(c => new
            {
                ID = OCode.ToInt64(c.GameTransactionOCode),
                OnlyUpdateAck = c.OnlyUpdateAck,
                Request = c
            }).ToArray();

            var dtPersonBalanceUpdateReqs = new Dictionary<Tuple<long, string>, PersonBalanceUpdateRequest>();
            var updateGameTransactionSummaryReqs = new List<GameTransactionSummary>();

            using (var trans = altEntities.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
            {
                var gameSession = await altEntities.LockGameSession(gameSessionID);
                if (gameSession == null)
                {
                    return rejectRequestData.ToArray();
                }

                var person = gameSession.Person;
                string actor = $"{person.Username}-SesionID:{gameSessionID}:Game Transaction";
                var dtNow = DateTime.Now;

                var isAckOnly = processingGameTransationRequests.All(c => c.OnlyUpdateAck);
                var commit = false;
                if (isAckOnly)
                {
                    commit = true;
                    gameSession.Ack = dtNow;
                    gameSession.AckTicks = dtNow.Ticks;
                    gameSession.UpdatedBy = actor;
                    gameSession.UpdatedDate = dtNow;
                }
                else
                {
                    var personUplineIDs = await altEntities.GetPersonUplines(person.ID, includedSelf: true);
                    var personDatas = await DataHelper.GetAllUplinesPT(altEntities, person.ID, personUplineIDs.ToArray(), betTypeItemIDs);

                    var transactions = (await altEntities.Transactions
                                                        .Where(d => d.GameSessionID == gameSessionID)
                                                        .Where(d => d.CurrencyCode == gameSession.CurrencyCode)
                                                        .Where(d => betTypeItemIDs.Contains(d.BetTypeItemID))
                                                        .Where(d => d.Type == Models.TransactionTypeString.Game)
                                                        .OrderBy(c => c.Time)
                                                        .ToArrayAsync());

                    var checkRequestIDs = processingGameTransationRequests.Select(c => c.ID)
                                                                           .Distinct()
                                                                           .ToArray();
                    var dbExistingGameTransactionIDs = new HashSet<long>(altEntities.GameTransactions.Where(d => checkRequestIDs.Contains(d.GameTransactionID)).Select(d => d.GameTransactionID));
                    var dbProcessedGameTransactionIDs = new HashSet<long>();
                    var updatedGameTransactions = new List<GameTransaction>();
                    var allGameIDs = dtGame.Values.Select(c => c.ID).ToArray();
                    var dbtGameTransactionSummary = await altEntities.GameTransactionSummaries.Where(c => c.PersonID == person.ID)
                                                                    .Where(c => c.CurrencyCode == person.CurrencyCodes)
                                                                    .Where(c => allGameIDs.Contains(c.GameID))
                                                                    .ToDictionaryAsync(c => Tuple.Create<long, string, int, int, int, int>(c.PersonID, c.CurrencyCode, c.GameID, c.Year, c.Month, c.Day));

                    foreach (var gameTransactionRequest in processingGameTransationRequests)
                    {
                        long gameTransactionID = gameTransactionRequest.ID;
                        if (gameTransactionRequest.OnlyUpdateAck)
                            continue;

                        if (dbExistingGameTransactionIDs.Contains(gameTransactionID))
                        {
                            //logger.Warn($"Not processing duplicated game transaction request: {gtr.GameCode} - {sessionOCode}:{gtr.GameTransactionOCode}:{gameTransactionID}");
                            continue;
                        }
                        if (dbProcessedGameTransactionIDs.Contains(gameTransactionID))
                        {
                            //logger.Warn($"Not processing already processed duplicated game transaction request: {gtr.GameCode} - {sessionOCode}:{gtr.GameTransactionOCode}:{gameTransactionID}");
                            continue;
                        }

                        Game game;
                        if (!dtGame.TryGetValue(gameTransactionRequest.Request.GameCode, out game))
                        {
                            rejectRequestData.Add(gameTransactionRequest.Request);
                            continue;
                        }

                        var totalBetAmount = gameTransactionRequest.Request.PointStake * (gameSession.PointRate ?? 0m);
                        var totalResultAmount = gameTransactionRequest.Request.PointResult * (gameSession.PointRate ?? 0m);

                        var updatedResult = true;
                        var transactionSignature = DataHelper.GetSignature(gameSessionID, game.BetTypeItemID, personDatas);
                        var transactionResult = TransactionProcessig(altEntities, transactions, gameTransactionRequest.Request, gameSession,
                                                                    game, dtNow, actor, transactionSignature, totalBetAmount, totalResultAmount);
                        updatedResult = transactionResult.Item1;
                        if (updatedResult)
                        {
                            var gameTransactionResult = GameTransactionProcessing(altEntities, transactionResult.Item3, gameTransactionRequest.Request, gameSession,
                                                                                    game, person, dtNow, actor, totalBetAmount, totalResultAmount);
                            updatedResult = gameTransactionResult.Item1;
                            if (updatedResult)
                            {
                                var gameTransPTResult = await GameTransactionPTProcessig(altEntities, transactionResult.Item3, gameTransactionResult.Item3, gameSession,
                                                                                        game, person, personDatas, dtPersonBalanceUpdateReqs, gameTransactionRequest.Request.SessionOCode,
                                                                                        dtNow, actor, totalBetAmount, totalResultAmount);
                                updatedResult = gameTransPTResult.Item1;
                                updatedGameTransactions.Add(gameTransactionResult.Item3);
                            }
                        }

                        if (updatedResult)
                        {
                            gameSession.TotalBet = (gameSession.TotalBet ?? 0m) + totalBetAmount;
                            gameSession.TotalResult = (gameSession.TotalResult ?? 0m) + totalResultAmount;

                            dbProcessedGameTransactionIDs.Add(gameTransactionID);
                            commit = true;
                        }
                    }

                    if (commit)
                    {
                        var updateGameTransHis = GameTransactionSummaryProcessing(altEntities, updatedGameTransactions.ToArray(), dbtGameTransactionSummary);
                    }
                }

                if (commit)
                {
                    await altEntities.SaveChangesAsync();
                    trans.Commit();
                }
                else
                {
                    trans.Rollback();
                }

                // send to worker checking Win/Loss Limit.
                if (commit && dtPersonBalanceUpdateReqs.Count() > 0)
                {

                }
            }

            return rejectRequestData.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="altEntities"></param>
        /// <param name="transactionsOnSession"></param>
        /// <param name="gameTransReq"></param>
        /// <param name="gameSession"></param>
        /// <param name="game"></param>
        /// <param name="dtNow"></param>
        /// <param name="actor"></param>
        /// <param name="signature"></param>
        /// <returns>
        /// 1. Success or Not
        /// 2. Created (true)|| Updated (False)
        /// 3. Transaction is object
        /// </returns>
        public static Tuple<bool, bool, Transaction> TransactionProcessig(ALTEntities altEntities, Transaction[] transactionsOnSession, GameTransactionReq gameTransReq, GameSession gameSession,
                                                                    Game game, DateTime dtNow, string actor, string signature, decimal totalBetAmount, decimal totalResultAmount)
        {
            var isSuccessed = true;
            var isUpdated = true;
            Transaction transaction;
            try
            {
                transaction = transactionsOnSession.Where(c => c.Time > gameTransReq.Time)
                                                        .OrderByDescending(c => c.Time)
                                                        .FirstOrDefault();
                if (transaction == null)
                {
                    isUpdated = false;
                    transaction = new Transaction
                    {
                        ID = KeyGeneration.GenerateInt64Id(),
                        BetTypeItemID = game.BetTypeItemID,
                        Amount = totalBetAmount,
                        Result = totalResultAmount,

                        GameSessionID = gameSession.ID,
                        IpAddress = gameSession.IpAddress,
                        PersonID = gameSession.PersonID,
                        CurrencyCode = gameSession.CurrencyCode,
                        Status = TransactionStatusTypeString.Active,
                        Type = TransactionTypeString.Game,
                        Time = dtNow,
                        Description = $"{actor}-Create new transaction for session {gameSession.ID} on {dtNow.ToString(Statics.DateTimeLongFormat)}",
                        Signature = signature,
                        PointRate = gameSession.PointRate ?? 0m,

                        CreatedBy = actor,
                        CreatedDate = dtNow,
                        UpdatedBy = actor,
                        UpdatedDate = dtNow
                    };

                    altEntities.Transactions.Add(transaction);
                }
                else
                {
                    transaction.Amount += totalBetAmount;
                    transaction.Result += totalResultAmount;
                }
            }
            catch (Exception ex)
            {
                isSuccessed = false;
                transaction = null;
            }

            return Tuple.Create<bool, bool, Transaction>(isSuccessed, isUpdated, transaction);
        }

        private static Tuple<bool, bool, GameTransaction> GameTransactionProcessing(ALTEntities altEntities, Transaction updatedTransaction, GameTransactionReq gameTransReq, GameSession gameSession,
                                                                            Game game, Person person, DateTime dtNow, string actor, decimal totalBetAmount, decimal totalResultAmount)
        {
            var isSuccessed = true;
            var isUpdated = true;
            GameTransaction newGameTransaction;
            try
            {
                newGameTransaction = new GameTransaction
                {
                    ID = KeyGeneration.GenerateInt64Id(),
                    GameTransactionID = OCode.ToInt64(gameTransReq.GameTransactionOCode),
                    GameSessionID = gameSession.ID,
                    GameSession = gameSession,
                    BetTypeItemID = game.BetTypeItemID,
                    GameID = game.ID,
                    Game = game,
                    PersonID = person.ID,
                    TransactionID = updatedTransaction.ID,
                    Type = gameTransReq.Type,
                    Time = gameTransReq.Time,
                    Ticks = gameTransReq.Time.Ticks,

                    StartBalance = gameTransReq.StartPointBalance * (gameSession.PointRate ?? 0),
                    EndBalance = gameTransReq.EndPointBalance * (gameSession.PointRate ?? 0),
                    Amount = totalBetAmount,
                    Result = totalResultAmount,
                    PointRate = gameSession.PointRate,
                    CurrencyCode = gameSession.CurrencyCode,

                    Description = gameTransReq.Description,
                    Detail = gameTransReq.Details,

                    CreatedBy = actor,
                    CreatedDate = dtNow
                };

                altEntities.GameTransactions.Add(newGameTransaction);
            }
            catch (Exception ex)
            {
                isSuccessed = false;
                newGameTransaction = null;
            }


            return Tuple.Create<bool, bool, GameTransaction>(isSuccessed, isUpdated, newGameTransaction);
        }

        private static async Task<Tuple<bool, bool, GameTransactionPositionTaking[]>> GameTransactionPTProcessig(ALTEntities altEntities, Transaction updatedTransaction, GameTransaction dbGameTransaction, GameSession gameSession,
                                                                                            Game game, Person person, PersonData[] personDatas, IDictionary<Tuple<long, string>, PersonBalanceUpdateRequest> dtPersonBalanceUpdateReqs,
                                                                                            string gameSessionOCode, DateTime dtNow, string actor, decimal totalBetAmount, decimal totalResultAmount)
        {
            var isSuccessed = true;
            var isUpdated = true;
            List<GameTransactionPositionTaking> gameTransPTs = new List<GameTransactionPositionTaking>() { };
            try
            {
                var dtGameTransPTs = await altEntities.GameTransactionPositionTakings
                                                        .Where(c => c.TransactionID == updatedTransaction.ID)
                                                        .ToDictionaryAsync(c => Tuple.Create<long, long, string>(c.TransactionID, c.PersonID, c.CurrencyCode));
                foreach (var personData in personDatas)
                {
                    decimal uplinePTRate = 0m;
                    var uplinePTAmount = 0M;
                    var allowUpdateGameTranPT = false;
                    if (personData.UplinePTs.TryGetValue(game.BetTypeItemID, out uplinePTRate))
                    {
                        allowUpdateGameTranPT = uplinePTRate >= 0;
                    }
                    if (!allowUpdateGameTranPT)
                    {
                        continue;
                    }
                    uplinePTAmount = uplinePTRate * -(totalResultAmount - totalBetAmount);

                    GameTransactionPositionTaking dbGameTransPT;
                    var gameTransPTKey = Tuple.Create<long, long, string>(updatedTransaction.ID, personData.UplineID ?? 0L, personData.CurrencyCode);
                    if (!dtGameTransPTs.TryGetValue(gameTransPTKey, out dbGameTransPT))
                    {
                        allowUpdateGameTranPT = personData.UplineID.HasValue;
                        if (allowUpdateGameTranPT)
                        {
                            dbGameTransPT = new GameTransactionPositionTaking
                            {
                                ID = KeyGeneration.GenerateInt64Id(),
                                PersonID = personData.UplineID.Value,
                                TransactionID = updatedTransaction.ID,
                                CurrencyCode = personData.CurrencyCode,
                                Rate = uplinePTRate,
                                Amount = 0m,
                                PointRate = gameSession.PointRate,
                                CreatedBy = person.Username,
                                CreatedDate = dtNow
                            };
                            altEntities.GameTransactionPositionTakings.Add(dbGameTransPT);
                        }
                    }

                    if (allowUpdateGameTranPT)
                    {
                        dbGameTransPT.Amount = (dbGameTransPT.Amount ?? 0m) + uplinePTAmount;
                        dbGameTransPT.UpdatedBy = person.Username;
                        dbGameTransPT.UpdatedDate = dtNow;

                        gameTransPTs.Add(dbGameTransPT);

                        if (personData.UplineType.HasValue)
                        {
                            switch (personData.UplineType.Value)
                            {
                                case PersonType.Admin:
                                    dbGameTransaction.AdminID = personData.UplineID;
                                    dbGameTransaction.AdminRate = uplinePTRate;
                                    dbGameTransaction.AdminAmount = uplinePTAmount;
                                    break;
                                case PersonType.SuperShareHolder:
                                    dbGameTransaction.SuperShareHolderID = personData.UplineID;
                                    dbGameTransaction.SuperShareHolderRate = uplinePTRate;
                                    dbGameTransaction.SuperShareHolderAmount = uplinePTAmount;
                                    break;
                                case PersonType.ShareHolder:
                                    dbGameTransaction.ShareHolderID = personData.UplineID;
                                    dbGameTransaction.ShareHolderRate = uplinePTRate;
                                    dbGameTransaction.ShareHolderAmount = uplinePTAmount;
                                    break;
                                case PersonType.SuperSenior:
                                    dbGameTransaction.SuperSeniorID = personData.UplineID;
                                    dbGameTransaction.SuperSeniorRate = uplinePTRate;
                                    dbGameTransaction.SuperSeniorAmount = uplinePTAmount;
                                    break;
                                case PersonType.Senior:
                                    dbGameTransaction.SeniorID = personData.UplineID;
                                    dbGameTransaction.SeniorRate = uplinePTRate;
                                    dbGameTransaction.SeniorAmount = uplinePTAmount;
                                    break;
                                case PersonType.SuperMaster:
                                    dbGameTransaction.SuperMasterID = personData.UplineID;
                                    dbGameTransaction.SuperMasterRate = uplinePTRate;
                                    dbGameTransaction.SuperMasterAmount = uplinePTAmount;
                                    break;
                                case PersonType.Master:
                                    dbGameTransaction.MasterID = personData.UplineID;
                                    dbGameTransaction.MasterRate = uplinePTRate;
                                    dbGameTransaction.MasterAmount = uplinePTAmount;
                                    break;
                                case PersonType.SuperAgent:
                                    dbGameTransaction.SuperAgentID = personData.UplineID;
                                    dbGameTransaction.SuperAgentRate = uplinePTRate;
                                    dbGameTransaction.SuperAgentAmount = uplinePTAmount;
                                    break;
                                case PersonType.Agent:
                                    dbGameTransaction.AgentID = personData.UplineID;
                                    dbGameTransaction.AgentRate = uplinePTRate;
                                    dbGameTransaction.AgentAmount = uplinePTAmount;
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                    // add person update balance object.
                    var updatePersonBalance = GetBalanceUpdateRequest(dtPersonBalanceUpdateReqs, personData.PersonID, personData.CurrencyCode);
                    if (personData.PersonType == PersonType.Member)
                    {
                        updatePersonBalance.SessionOCode = gameSessionOCode;
                    }
                    updatePersonBalance.EGameBalance = (updatePersonBalance.EGameBalance ?? 0) + (totalResultAmount - totalBetAmount);
                }
            }
            catch (Exception ex)
            {
                isSuccessed = false;
            }

            return Tuple.Create<bool, bool, GameTransactionPositionTaking[]>(isSuccessed, isUpdated, gameTransPTs.ToArray());
        }

        private static bool GameTransactionSummaryProcessing(ALTEntities altEntities, GameTransaction[] gameTrans, Dictionary<Tuple<long, string, int, int, int, int>, GameTransactionSummary> dtGameTransactionSummary)
        {
            var hasUpdate = false;
            foreach (var gameTran in gameTrans)
            {
                try
                {
                    hasUpdate = true;
                    var gameSummKey = Tuple.Create<long, string, int, int, int, int>(gameTran.PersonID, gameTran.CurrencyCode, gameTran.GameID, gameTran.Time.Value.Year, gameTran.Time.Value.Month, gameTran.Time.Value.Day);
                    GameTransactionSummary gameTransactionSummary;
                    if (!dtGameTransactionSummary.TryGetValue(gameSummKey, out gameTransactionSummary))
                    {
                        gameTransactionSummary = new GameTransactionSummary
                        {
                            PersonID = gameTran.PersonID,
                            CurrencyCode = gameTran.CurrencyCode,
                            GameID = gameTran.GameID,
                            Year = gameTran.Time.Value.Year,
                            Month = gameTran.Time.Value.Month,
                            Day = gameTran.Time.Value.Day,

                            PointRate = gameTran.PointRate,
                            TotalAmount = gameTran.Amount,
                            TotalResult = gameTran.Result,
                            TotalWin = Math.Max(0, gameTran.Result - gameTran.Amount),
                            TotalLoss = Math.Min(0, gameTran.Result - gameTran.Amount),
                            TotalWinLoss = gameTran.Result - gameTran.Amount,

                            TotalAdminWinLoss = gameTran.AdminAmount,
                            TotalSuperShareHolderWinLoss = gameTran.SuperShareHolderAmount,
                            TotalShareHolderWinLoss = gameTran.ShareHolderAmount,
                            TotalSuperSeniorWinLoss = gameTran.SuperSeniorAmount,
                            TotalSeniorWinLoss = gameTran.SeniorAmount,
                            TotalSuperMasterWinLoss = gameTran.SuperMasterAmount,
                            TotalMasterWinLoss = gameTran.MasterAmount,
                            TotalSuperAgentWinLoss = gameTran.SuperAgentAmount,
                            TotalAgentWinLoss = gameTran.AgentAmount,

                            AdminID = gameTran.AdminID,
                            SuperShareHolderID = gameTran.SuperShareHolderID,
                            ShareHolderID = gameTran.ShareHolderID,
                            SuperSeniorID = gameTran.SuperSeniorID,
                            SeniorID = gameTran.SeniorID,
                            SuperMasterID = gameTran.SuperMasterID,
                            MasterID = gameTran.MasterID,
                            SuperAgentID = gameTran.SuperAgentID,
                            AgentID = gameTran.AgentID
                        };
                        altEntities.GameTransactionSummaries.Add(gameTransactionSummary);
                    }
                    else
                    {
                        gameTransactionSummary.TotalAmount += gameTran.Amount;
                        gameTransactionSummary.TotalResult += gameTran.Result;
                        gameTransactionSummary.TotalWin += Math.Max(0, gameTran.Result - gameTran.Amount);
                        gameTransactionSummary.TotalLoss += Math.Min(0, gameTran.Result - gameTran.Amount);
                        gameTransactionSummary.TotalWinLoss += gameTran.Result - gameTran.Amount;

                        gameTransactionSummary.TotalAdminWinLoss = (gameTransactionSummary.TotalAdminWinLoss ?? 0m) + gameTran.AdminAmount;
                        gameTransactionSummary.TotalAdminWinLoss = (gameTransactionSummary.TotalAdminWinLoss ?? 0m) + gameTran.AdminAmount;
                        gameTransactionSummary.TotalSuperShareHolderWinLoss = (gameTransactionSummary.TotalSuperShareHolderWinLoss ?? 0m) + gameTran.SuperShareHolderAmount;
                        gameTransactionSummary.TotalShareHolderWinLoss = (gameTransactionSummary.TotalShareHolderWinLoss ?? 0m) + gameTran.ShareHolderAmount;
                        gameTransactionSummary.TotalSuperSeniorWinLoss = (gameTransactionSummary.TotalSuperSeniorWinLoss ?? 0m) + gameTran.SuperSeniorAmount;
                        gameTransactionSummary.TotalSeniorWinLoss = (gameTransactionSummary.TotalSeniorWinLoss ?? 0m) + gameTran.SeniorAmount;
                        gameTransactionSummary.TotalSuperMasterWinLoss = (gameTransactionSummary.TotalSuperMasterWinLoss ?? 0m) + gameTran.SuperMasterAmount;
                        gameTransactionSummary.TotalMasterWinLoss = (gameTransactionSummary.TotalMasterWinLoss ?? 0m) + gameTran.MasterAmount;
                        gameTransactionSummary.TotalSuperAgentWinLoss = (gameTransactionSummary.TotalSuperAgentWinLoss ?? 0m) + gameTran.SuperAgentAmount;
                        gameTransactionSummary.TotalAgentWinLoss = (gameTransactionSummary.TotalAgentWinLoss ?? 0m) + gameTran.AgentAmount;

                        gameTransactionSummary.AdminID = gameTransactionSummary.AdminID ?? gameTran.AdminID;
                        gameTransactionSummary.SuperShareHolderID = gameTransactionSummary.SuperShareHolderID ?? gameTran.SuperShareHolderID;
                        gameTransactionSummary.ShareHolderID = gameTransactionSummary.ShareHolderID ?? gameTran.ShareHolderID;
                        gameTransactionSummary.SuperSeniorID = gameTransactionSummary.SuperSeniorID ?? gameTran.SuperSeniorID;
                        gameTransactionSummary.SeniorID = gameTransactionSummary.SeniorID ?? gameTran.SeniorID;
                        gameTransactionSummary.SuperMasterID = gameTransactionSummary.SuperMasterID ?? gameTran.SuperMasterID;
                        gameTransactionSummary.MasterID = gameTransactionSummary.MasterID ?? gameTran.MasterID;
                        gameTransactionSummary.SuperAgentID = gameTransactionSummary.SuperAgentID ?? gameTran.SuperAgentID;
                        gameTransactionSummary.AgentID = gameTransactionSummary.AgentID ?? gameTran.AgentID;

                    }
                }
                catch (Exception ex)
                {

                }
            }
            return hasUpdate;
        }


        public static PersonBalanceUpdateRequest GetBalanceUpdateRequest(IDictionary<Tuple<long, string>, PersonBalanceUpdateRequest> balanceUpdates, long personID, string currencyCode)
        {
            Models.PersonBalanceUpdateRequest balanceUpdate;
            var keys = Tuple.Create<long, string>(personID, currencyCode);
            if (!balanceUpdates.TryGetValue(keys, out balanceUpdate))
            {
                balanceUpdate = new Models.PersonBalanceUpdateRequest
                {
                    PersonOCode = OCode.Get(personID)
                };
                balanceUpdates.Add(keys, balanceUpdate);
            }

            return balanceUpdate;
        }

    }
}
