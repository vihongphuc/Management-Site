﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Worker.GameTransaction
{
    using ALT.Common.Models;
    using ALT.Data.Entityframework;
    using System.Data.Entity;
    using System.Security.Cryptography;

    public static class DataHelper
    {
        public static async Task<PersonData[]> GetAllUplinesPT(ALTEntities altEntites, long personID, long[] uplineIDs, int[] betTypeItemIDs)
        {
            var ptTypes = new[] {
                PostitionTakingTypeString.Upline,
                PostitionTakingTypeString.CurrentMax,
            };

            var allPersons = await altEntites.People.Where(c => uplineIDs.Contains(c.ID)).ToArrayAsync();
            var memberCurrencyCode = allPersons.Where(c => c.ID == personID).Select(c => c.CurrencyCodes).FirstOrDefault();
            var dtPeronType = allPersons.ToDictionary(c => c.ID, c => c.Type);

            var dbPersonData = allPersons.Select(c => new
            {
                CurrencyCode = memberCurrencyCode,
                Username = c.Username,
                PersonID = c.ID,
                UplineID = c.UplineID,

                PersonType = Helpers.EnumHelper<PersonType>.Parse(dtPeronType[c.ID]),
                UplineType = c.UplineID.HasValue ? (Models.PersonType?)Helpers.EnumHelper<PersonType>.Parse(dtPeronType[c.UplineID.Value]) : null,

                PTRates = c.PositionTakings.Where(cc => betTypeItemIDs.Contains(cc.BetTypeItemID))
                                            .Where(cc => cc.CurrencyCode == memberCurrencyCode)
                                            .Where(cc => ptTypes.Contains(cc.Type))
                                            .Select(cc => new
                                            {
                                                cc.BetTypeItemID,
                                                cc.Type,
                                                cc.Value
                                            }).ToArray()
            }).ToArray();

            var personData = dbPersonData.Select(c => new PersonData
            {
                CurrencyCode = c.CurrencyCode,
                PersonID = c.PersonID,
                PersonType = c.PersonType,
                UplineID = c.UplineID,
                UplineType = c.UplineType,
                Username = c.Username,
                UplinePTs = c.PTRates.Where(cc => cc.Type == PostitionTakingTypeString.Upline)
                                     .ToDictionary(cc => cc.BetTypeItemID, cc => cc.Value ?? 0m)
            }).OrderByDescending(c => c.PersonType)
            .ToArray();

            foreach (var betTypeItem in betTypeItemIDs)
            {
                var accPT = 0M;
                foreach (var currentPersonData in personData)
                {
                    var _currUplinePT = 0M;
                    if (currentPersonData.PersonType != PersonType.Admin)
                    {
                        if (!currentPersonData.UplinePTs.TryGetValue(betTypeItem, out _currUplinePT))
                        {
                            _currUplinePT = 0M;
                        }
                        accPT += _currUplinePT;

                        if (currentPersonData.UplineType.HasValue && currentPersonData.UplineType.Value == PersonType.Admin)
                        {
                            if (accPT > 1)
                            {
                                ;// the rule is not correct.
                                currentPersonData.UplinePTs[betTypeItem] = 1 - (accPT - _currUplinePT);
                            }
                            accPT = 1;
                        }
                    }
                }
            }

            return personData;
        }

        public static string GetSignature(long gameSessionID, int betTypeItemID, PersonData[] personData)
        {
            var signature = string.Empty;
            var sData = $"GameSessionID:{gameSessionID};BetTypeItemID:{betTypeItemID};PersonTypes:{String.Join("-", personData.Select(d => $"{d.PersonType.ToString()};UplinePTs:{String.Join(",", d.UplinePTs.OrderBy(kv => kv.Key).Select(kv => kv.Value))}"))}";
            sData += $";{DateTime.Now.Ticks}";

            using (var md5 = MD5.Create())
            {
                signature = Convert.ToBase64String(md5.ComputeHash(Encoding.ASCII.GetBytes(sData)));
            }
            return signature;
        }
    }
}
