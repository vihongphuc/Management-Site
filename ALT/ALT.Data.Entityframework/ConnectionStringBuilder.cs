﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ALT.Data.Entityframework
{
    public static class ConnectionStringBuilder
    {
        //public static string ConnectionString = "metadata = res://*/ALTContext.csdl|res://*/ALTContext.ssdl|res://*/ALTContext.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source=localhost;initial catalog=Alt-SlotGame;user id=sa;password=sa;MultipleActiveResultSets=True;App=EntityFramework&quot;";
        public static string ConnectionString = "metadata = res://*/ALTContext.csdl|res://*/ALTContext.ssdl|res://*/ALTContext.msl;provider=System.Data.SqlClient;provider connection string=\"data source={0};initial catalog={1};user id={2};password={3};MultipleActiveResultSets=True;App=EntityFramework\"";

        private static string DataSource = string.Empty;
        private static string DataBaseName = string.Empty;
        private static string Username = string.Empty;
        private static string Password = string.Empty;

        public static void InitConnectionString()
        {
            if (string.IsNullOrEmpty(DataSource))
            {
                DataSource = ALT.Common.Configuration.ConfigurationManager.Get<string>(ConnectionStringConfiguration.Group, ConnectionStringConfiguration.DataSource, string.Empty);
            }
            if (string.IsNullOrEmpty(DataBaseName))
            {
                DataBaseName = ALT.Common.Configuration.ConfigurationManager.Get<string>(ConnectionStringConfiguration.Group, ConnectionStringConfiguration.DataBaseName, string.Empty);
            }
            if (string.IsNullOrEmpty(Username))
            {
                Username = ALT.Common.Configuration.ConfigurationManager.Get<string>(ConnectionStringConfiguration.Group, ConnectionStringConfiguration.Username, string.Empty);
            }
            if (string.IsNullOrEmpty(Password))
            {
                Password = ALT.Common.Configuration.ConfigurationManager.Get<string>(ConnectionStringConfiguration.Group, ConnectionStringConfiguration.Password, string.Empty);
            }

            ConnectionString = string.Format(ConnectionString, DataSource, DataBaseName, Username, Password);
        }
    }
}