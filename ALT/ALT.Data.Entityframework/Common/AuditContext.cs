﻿using ALT.Common.Models.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Threading.Tasks;

namespace ALT.Data.Entityframework.Common
{
    public class AuditContext
    {
        public AuditContext(AuditType auditType, string subject, HttpRequestMessage request)
            : this(auditType, subject)
        {
            var consumerIP = ApiConsumerHelpers.GetConsumerAddress(request);
            if (!String.IsNullOrWhiteSpace(consumerIP))
            {
                this.ConsumerIPAddress = consumerIP;
            }

            var endUserIP = ApiConsumerHelpers.GetEndUserAddress(request);
            if (!String.IsNullOrWhiteSpace(endUserIP))
            {
                this.IPAddress = endUserIP;
            }
        }

        public AuditContext(AuditType auditType, string subject)
        {
            this.ActionTime = DateTime.Now;
            this.Type = auditType;
            this.Subject = subject;
        }

        public static string Serialize<T>(T value)
        {
            return JsonConvert.SerializeObject(value, Formatting.Indented);
        }

        public AuditType Type { get; set; }
        public string Subject { get; set; }

        public long? PersonID { get; set; }
        public string Token { get; set; }
        public long? RelatedPersonID { get; set; }
        public string IPAddress { get; set; }
        public string IPLocation { get; set; }
        public string ConsumerIPAddress { get; set; }
        public string ConsumerIPLocation { get; set; }
        public DateTime? ActionTime { get; set; }
        public string Detail { get; set; }
        public string Before { get; set; }
        public string After { get; set; }
        public string RelatedObjectType { get; set; }
        public long? RelatedObjectID { get; set; }

        public async Task InsertAudit(SbsContext sbsContext)
        {
            if (Inserted)
                return;

            sbsContext.Audits.Add(new Audit
            {
                Time = DateTime.Now,
                PersonID = PersonID,
                Token = Token,
                RelatedPersonID = RelatedPersonID,
                IPAddress = IPAddress,
                IPLocation = IPLocation,
                ConsumerIPAddress = ConsumerIPAddress,
                ConsumerIPLocation = ConsumerIPLocation,
                Type = Type.ToString(),
                ActionTime = ActionTime,
                Subject = Subject,
                Detail = Detail,
                Before = Before,
                After = After,
                RelatedObjectType = RelatedObjectType,
                RelatedObjectID = RelatedObjectID
            });

            Inserted = true;

            // retrieve location
            await Task.FromResult(0);
        }

        protected bool Inserted { get; private set; }
    }
}