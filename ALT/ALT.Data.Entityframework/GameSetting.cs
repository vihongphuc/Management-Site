//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ALT.Data.Entityframework
{
    using System;
    using System.Collections.Generic;
    
    public partial class GameSetting
    {
        public int GameID { get; set; }
        public string Publisher { get; set; }
        public string Platform { get; set; }
        public string DownloadURL { get; set; }
        public string GameHash { get; set; }
        public string GameKey { get; set; }
        public string GameHearder { get; set; }
        public Nullable<bool> Enable { get; set; }
        public Nullable<bool> IsHot { get; set; }
        public Nullable<bool> IsNew { get; set; }
        public string Rate { get; set; }
        public string Type { get; set; }
        public Nullable<decimal> Width { get; set; }
        public Nullable<decimal> Height { get; set; }
        public string ImageURL { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> Port { get; set; }
        public string Domain { get; set; }
    
        public virtual Game Game { get; set; }
    }
}
