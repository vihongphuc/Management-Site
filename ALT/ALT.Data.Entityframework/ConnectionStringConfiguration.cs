﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ALT.Data.Entityframework
{
    public static class ConnectionStringConfiguration
    {
        public static string Group = "Database";
        public static string DataSource = "DataSource";
        public static string DataBaseName = "DataBaseName";
        public static string Username = "Username";
        public static string Password = "Password";
    }
}