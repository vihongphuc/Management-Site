﻿using ALT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Data.Entityframework
{
    public static class EntityExtensions
    {
        public static PersonUpline CopyFrom(this PersonUpline target, PersonUpline source)
        {
            target.AdminID = source.AdminID;
            target.SuperShareHolderID = source.SuperShareHolderID;
            target.ShareHolderID = source.ShareHolderID;
            target.SuperSeniorID = source.SuperSeniorID;
            target.SeniorID = source.SeniorID;
            target.SuperMasterID = source.SuperMasterID;
            target.MasterID = source.MasterID;
            target.SuperAgentID = source.SuperAgentID;
            target.AgentID = source.AgentID;

            return target;
        }

        public static void Set(this PersonUpline target, string type, long uplineID)
        {
            if (String.Equals(type, PersonTypeString.Admin, StringComparison.OrdinalIgnoreCase))
            {
                target.AdminID = uplineID;
            }
            if (String.Equals(type, PersonTypeString.SuperShareHolder, StringComparison.OrdinalIgnoreCase))
            {
                target.SuperShareHolderID = uplineID;
            }
            if (String.Equals(type, PersonTypeString.ShareHolder, StringComparison.OrdinalIgnoreCase))
            {
                target.ShareHolderID = uplineID;
            }
            if (String.Equals(type, PersonTypeString.SuperSenior, StringComparison.OrdinalIgnoreCase))
            {
                target.SuperSeniorID = uplineID;
            }
            if (String.Equals(type, PersonTypeString.Senior, StringComparison.OrdinalIgnoreCase))
            {
                target.SeniorID = uplineID;
            }           
            if (String.Equals(type, PersonTypeString.SuperMaster, StringComparison.OrdinalIgnoreCase))
            {
                target.SuperMasterID = uplineID;
            }
            if (String.Equals(type, PersonTypeString.Master, StringComparison.OrdinalIgnoreCase))
            {
                target.MasterID = uplineID;
            }
            if (String.Equals(type, PersonTypeString.SuperAgent, StringComparison.OrdinalIgnoreCase))
            {
                target.SuperAgentID = uplineID;
            }
            if (String.Equals(type, PersonTypeString.Agent, StringComparison.OrdinalIgnoreCase))
            {
                target.AgentID = uplineID;
            }
        }
        public static long? Get(this PersonUpline target, string type)
        {
            if (String.Equals(type, PersonTypeString.Admin, StringComparison.OrdinalIgnoreCase))
            {
                return target.AdminID;
            }
            if (String.Equals(type, PersonTypeString.SuperShareHolder, StringComparison.OrdinalIgnoreCase))
            {
                return target.SuperShareHolderID;
            }
            if (String.Equals(type, PersonTypeString.ShareHolder, StringComparison.OrdinalIgnoreCase))
            {
                return target.ShareHolderID;
            }
            if (String.Equals(type, PersonTypeString.SuperSenior, StringComparison.OrdinalIgnoreCase))
            {
                return target.SuperSeniorID;
            }
            if (String.Equals(type, PersonTypeString.Senior, StringComparison.OrdinalIgnoreCase))
            {
                return target.SeniorID;
            }
            if (String.Equals(type, PersonTypeString.SuperMaster, StringComparison.OrdinalIgnoreCase))
            {
                return target.SuperMasterID;
            }
            if (String.Equals(type, PersonTypeString.Master, StringComparison.OrdinalIgnoreCase))
            {
                return target.MasterID;
            }
            if (String.Equals(type, PersonTypeString.SuperAgent, StringComparison.OrdinalIgnoreCase))
            {
                return target.SuperAgentID;
            }
            if (String.Equals(type, PersonTypeString.Agent, StringComparison.OrdinalIgnoreCase))
            {
                return target.AgentID;
            }

            return null;
        }
    }
}
