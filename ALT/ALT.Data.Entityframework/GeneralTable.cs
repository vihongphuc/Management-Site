//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ALT.Data.Entityframework
{
    using System;
    using System.Collections.Generic;
    
    public partial class GeneralTable
    {
        public int Id { get; set; }
        public string TableName { get; set; }
        public string FeildName { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Setting { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }
}
