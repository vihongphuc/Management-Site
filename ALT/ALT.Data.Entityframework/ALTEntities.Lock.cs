﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ALT.Data.Entityframework
{
    public partial class ALTEntities
    {
        /// <summary>
        /// always lock PersonBalance before GameSession
        /// </summary>
        /// <param name="personID"></param>
        /// <returns></returns>

        public async Task<IEnumerable<Person>> GetPersons(long[] personIDs)
        {
            var sql = $"select * from Person where ID in ( @personIDs  )";
            sql += " ;";

            return await this.People.SqlQuery(sql, new SqlParameter("@personIDs", string.Join(",", personIDs.Select(c => c.ToString())))).ToArrayAsync().ConfigureAwait(continueOnCapturedContext: false);
        }

        public async Task<IEnumerable<PersonBalance>> LockPersonBalance(long[] personIDs)
        {
            var sql = $"select * from PersonBalance with(rowlock)  where PersonID in (" +
                            String.Join(", ", personIDs.Select((s, i) => $"@p{i}")) +
                            ");";

            var oPersonIDs = personIDs.Select(i => (object)i).ToArray();
            return await this.PersonBalances.SqlQuery(sql, oPersonIDs).ToArrayAsync().ConfigureAwait(continueOnCapturedContext: false);
        }

        public async Task<IEnumerable<PersonBalance>> LockPersonBalance(long personID)
        {
            return await GetPersoBalance(personID, string.Empty);
        }

        public async Task<PersonBalance> LockPersonBalance(long personID, string currencyCode)
        {
            var pb = (await GetPersoBalance(personID, currencyCode)).FirstOrDefault();
            return pb;
        }

        private async Task<PersonBalance[]> GetPersoBalance(long personID, string currencyCode)
        {
            var sql = $"select * from PersonBalance with(rowlock)  where PersonID = @personID ";
            if (!string.IsNullOrEmpty(currencyCode))
            {
                sql += " and CurrencyCode=@currencyCode";
            }
            sql += " ;";

            if (string.IsNullOrEmpty(currencyCode))
                return await this.PersonBalances.SqlQuery(sql, new SqlParameter("@personID", personID)).ToArrayAsync().ConfigureAwait(continueOnCapturedContext: false);
            else
                return await this.PersonBalances.SqlQuery(sql, new SqlParameter("@personID", personID), new SqlParameter("@currencyCode", currencyCode)).ToArrayAsync().ConfigureAwait(continueOnCapturedContext: false);
        }

        public async Task<GameSession> LockActiveGameSession(long personID)
        {
            var sql = $"select * from GameSession with(rowlock) where PersonID=@personID and FinishedTime is null;";
            return await this.GameSessions.SqlQuery(sql, new SqlParameter("@personID", personID)).FirstOrDefaultAsync();
        }

        public async Task<GameSession> LockGameSession(long gameSessionID)
        {
            var sql = $"select * from GameSession with(rowlock) where ID=@gameSessionID;";
            return await this.GameSessions.SqlQuery(sql, new SqlParameter("@gameSessionID", gameSessionID)).FirstOrDefaultAsync();
        }
    }
}