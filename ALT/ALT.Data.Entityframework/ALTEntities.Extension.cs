﻿using ALT.Common.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ALT.Data.Entityframework
{
    public partial class ALTEntities
    {
        public async Task<IList<long>> GetPersonUplines(long personID, bool includedSelf)
        {
            List<long> personUplineIDs = new List<long>();
            var upline = await this.PersonUplines.Where(c => c.PersonID == personID).FirstOrDefaultAsync();

            if (upline != null)
            {
                if (upline.AdminID.HasValue)
                    personUplineIDs.Add(upline.AdminID.Value);
                if (upline.SuperShareHolderID.HasValue)
                    personUplineIDs.Add(upline.SuperShareHolderID.Value);
                if (upline.ShareHolderID.HasValue)
                    personUplineIDs.Add(upline.ShareHolderID.Value);
                if (upline.SuperSeniorID.HasValue)
                    personUplineIDs.Add(upline.SuperSeniorID.Value);
                if (upline.SeniorID.HasValue)
                    personUplineIDs.Add(upline.SeniorID.Value);
                if (upline.MasterID.HasValue)
                    personUplineIDs.Add(upline.MasterID.Value);
                if (upline.SuperMasterID.HasValue)
                    personUplineIDs.Add(upline.SuperMasterID.Value);
                if (upline.AgentID.HasValue)
                    personUplineIDs.Add(upline.AgentID.Value);
                if (upline.SuperAgentID.HasValue)
                    personUplineIDs.Add(upline.SuperAgentID.Value);
            }

            if (includedSelf)
            {
                personUplineIDs.Add(personID);
            }
            return personUplineIDs;
        }

        public async Task<IList<long>> GetPersonUplines(long personID, PersonType fromPersonType, bool includedSelf)
        {
            var personUplineIDs = new HashSet<long>();

            var dbUpline = await this.PersonUplines.Where(c => c.PersonID == personID).FirstOrDefaultAsync();
            if (dbUpline != null)
            {
                foreach (var personType in PersonTypeArray.Values.Where(c => c != PersonType.Robot || c != PersonType.SubAccount))
                {
                    var _personID = personType >= fromPersonType ? (GetPersonIDByType(personType, dbUpline) ?? 0L) : 0L;
                    personUplineIDs.Add(_personID);
                }
            }

            if (includedSelf)
            {
                personUplineIDs.Add(personID);
            }

            return personUplineIDs.Where(c => c != 0L).ToArray();
        }

        private long? GetPersonIDByType(PersonType type, PersonUpline personUpline)
        {
            var personID = (long?)null;
            switch (type)
            {
                case PersonType.Admin:
                    personID = personUpline.AdminID;
                    break;
                case PersonType.SuperShareHolder:
                    personID = personUpline.SuperShareHolderID;
                    break;
                case PersonType.ShareHolder:
                    personID = personUpline.ShareHolderID;
                    break;
                case PersonType.SuperSenior:
                    personID = personUpline.SuperSeniorID;
                    break;
                case PersonType.Senior:
                    personID = personUpline.SeniorID;
                    break;
                case PersonType.SuperMaster:
                    personID = personUpline.SuperMasterID;
                    break;
                case PersonType.Master:
                    personID = personUpline.MasterID;
                    break;
                case PersonType.SuperAgent:
                    personID = personUpline.SuperAgentID;
                    break;
                case PersonType.Agent:
                    personID = personUpline.AgentID;
                    break;
            }

            return personID;
        }

        public async void Touch(PersonBalance personBalance)
        {
            await Touch(DateTime.Now, personBalance);
        }

        private async Task Touch(DateTime now, PersonBalance personBalance)
        {
            if (now.Date.CompareTo(personBalance.LastUpdatedDate.Value.Date) != 0)
            {
                personBalance.YesterdayAvailable = personBalance.Available;
                personBalance.LastUpdatedDate = now;
            }
        }
    }
}