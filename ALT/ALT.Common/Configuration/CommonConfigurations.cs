﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common
{
    public static class CommonConfigurations
    {
        public static string Group = "Common";
        public static readonly string IDGeneratorScope = "IDGeneratorScope";
        public static readonly string RobotToken = "RobotToken";
    }
}
