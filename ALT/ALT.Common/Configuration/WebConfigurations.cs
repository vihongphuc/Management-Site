﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common
{
    public static class WebConfigurations
    {
        public static string Group = "Web";

        public static string IPAllow = "IPAllow";
        public static string CountryWhiteList = "CountryWhiteList";
        public static string CountryBlockList = "CountryBlockList";
        public static string BypassHeaders = "BypassHeaders";
        public static string BypassHeadersIP = "BypassHeadersIP";

        public static string Common = "Common";
        public static string DefaultCurrency = "DefaultCurrency";

    }
}
