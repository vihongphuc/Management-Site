﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ALT.Common.Models;

namespace ALT.Common.Configuration
{
    public interface IConfigurationStore : IDisposable
    {
        IEnumerable<Models.Configuration> GetConfigurations();
    }
}
