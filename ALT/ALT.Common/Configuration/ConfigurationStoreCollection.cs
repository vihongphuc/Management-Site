﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ALT.Common.Models;

namespace ALT.Common.Configuration
{
    using ALT.Common.Models;
    public class ConfigurationStoreCollection : IConfigurationStore
    {
        public static readonly int DefaultOrderNumber = 100;

        public void AddConfigurationStore(string key, IConfigurationStore store)
        {
            AddConfigurationStore(key, store, DefaultOrderNumber);
        }
        public void AddConfigurationStore(string key, IConfigurationStore store, int order)
        {
            lock (syncLock)
            {
                stores.Add(key, new ConfigStore
                {
                    ConfigurationStore = store,
                    SortOrder = order
                });
            }
        }
        public void ResetConfigurationStore(string key)
        {
            lock (syncLock)
            {
                if (!stores.ContainsKey(key))
                    throw new ArgumentException("Cannot find the configuration key");

                configurations.Remove(key);
            }
        }
        public void ResetAllConfigurationStore()
        {
            lock (syncLock)
            {
                configurations.Clear();
            }
        }
        public void RemoveConfigurationStore(string key)
        {
            lock (syncLock)
            {
                stores.Remove(key);
                configurations.Remove(key);
            }
        }

        public IEnumerable<Configuration> GetConfigurations()
        {
            IEnumerable<Configuration> result = Enumerable.Empty<Configuration>();

            lock (syncLock)
            {
                foreach (var kv in stores.OrderBy(d => d.Value.SortOrder))
                {
                    IEnumerable<Configuration> configs;
                    if (!configurations.TryGetValue(kv.Key, out configs))
                    {
                        configs = kv.Value.ConfigurationStore.GetConfigurations();
                        configurations.Add(kv.Key, configs);
                    }

                    result = result.Concat(configs);
                }
            }

            return result;
        }

        public void Dispose()
        {
            IConfigurationStore[] allStores;
            lock (syncLock)
            {
                allStores = stores.Values.Select(d => d.ConfigurationStore).ToArray();
                stores.Clear();
                configurations.Clear();
            }

            foreach (var st in allStores)
            {
                st.Dispose();
            }
        }

        private readonly object syncLock = new object();
        private readonly Dictionary<string, ConfigStore> stores = new Dictionary<string, ConfigStore>(StringComparer.OrdinalIgnoreCase);
        private readonly Dictionary<string, IEnumerable<Configuration>> configurations = new Dictionary<string, IEnumerable<Configuration>>();

        private sealed class ConfigStore
        {
            public IConfigurationStore ConfigurationStore { get; set; }
            public int SortOrder { get; set; }
        }
    }
}
