﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Configuration
{
    using Utilities;
    using IniParser;
    using System.IO;
    using ALT.Common.Models;
    public static class ConfigurationManager
    {
        //private static readonly object syncLock = new object();
        //public static IniParser.Model.IniData configData;

        //public static void LoadConfig(string confiName)
        //{
        //    if (File.Exists(confiName))
        //    {
        //        lock (syncLock)
        //        {
        //            FileIniDataParser fileIniData = new FileIniDataParser();
        //            fileIniData.Parser.Configuration.CommentString = "#";
        //            configData = fileIniData.ReadFile(confiName);
        //        }
        //    }
        //    else
        //    {
        //        configData = null;
        //    }
        //}

        //public static string GetString(string group, string key, string defaultValue)
        //{
        //    if (configData == null)
        //        return defaultValue;

        //    var groupData = configData.Sections[group];
        //    if (groupData == null)
        //        return defaultValue;

        //    var keyData = groupData[key];
        //    if (string.IsNullOrEmpty(keyData))
        //        return defaultValue;

        //    return keyData;
        //}

        //public static T Get<T>(string group, string key, T defaultValue)
        //{
        //    if (configData == null)
        //        return defaultValue;

        //    var groupData = configData.Sections[group];
        //    if (groupData == null)
        //        return defaultValue;

        //    var keyData = groupData[key];
        //    if (string.IsNullOrEmpty(keyData))
        //        return defaultValue;

        //    try
        //    {
        //        return (T)Convert.ChangeType(keyData, typeof(T));
        //    }
        //    catch (InvalidCastException)
        //    {
        //        return default(T);
        //    }
        //}

        //public static Dictionary<string, string> GetGroup(string group)
        //{
        //    if (configData == null)
        //        return null;
        //    var groupData = configData.Sections[group];
        //    if (configData.Sections[group] == null)
        //        return null;
        //    return groupData.ToDictionary(c => c.KeyName, c => c.Value);
        //}

        //public static IEnumerable<KeyValuePair<string, string>> GetAll(string group)
        //{
        //    //Dictionary<string, Configuration> groupConfig;
        //    //if (allConfigs.TryGetValue(group, out groupConfig))
        //    //{
        //    //    return groupConfig.Select(d => new KeyValuePair<string, string>(d.Key, d.Value.Value));
        //    //}
        //    var defaultValue = Empty<KeyValuePair<string, string>>.Array;
        //    if (configData == null)
        //        return defaultValue;

        //    var groupData = configData.Sections[group];
        //    if (groupData == null)
        //        return defaultValue;

        //    try
        //    {
        //        return groupData.Select(c => new KeyValuePair<string, string>(c.KeyName, c.Value)).AsEnumerable();
        //    }
        //    catch (InvalidCastException)
        //    {
        //        return defaultValue;
        //    }
        //}


        private static readonly object syncLock = new object();
        private static Dictionary<string, Dictionary<string, Configuration>> allConfigs = new Dictionary<string, Dictionary<string, Configuration>>(StringComparer.OrdinalIgnoreCase);

        public static void ResetAndApplyConfiguration(IConfigurationStore configurationStore)
        {
            lock (syncLock)
            {
                ApplyConfigurationCore(new Dictionary<string, Dictionary<string, Configuration>>(StringComparer.OrdinalIgnoreCase), configurationStore);
            }
        }
        public static void ApplyConfiguration(IConfigurationStore configurationStore)
        {
            lock (syncLock)
            {
                var newConfigs = allConfigs.ToDictionary(d => d.Key,
                                                         d => d.Value.ToDictionary(e => e.Key,
                                                                                   e => e.Value, StringComparer.OrdinalIgnoreCase),
                                                         StringComparer.OrdinalIgnoreCase);

                ApplyConfigurationCore(newConfigs, configurationStore);
            }
        }

        private static void ApplyConfigurationCore(Dictionary<string, Dictionary<string, Configuration>> existingConfigs, IConfigurationStore configurationStore)
        {
            foreach (var configGroup in configurationStore.GetConfigurations().GroupBy(d => d.Group))
            {
                Dictionary<string, Configuration> group;
                if (!existingConfigs.TryGetValue(configGroup.Key, out group))
                {
                    group = new Dictionary<string, Configuration>(StringComparer.OrdinalIgnoreCase);
                    existingConfigs.Add(configGroup.Key, group);
                }

                foreach (var config in configGroup)
                {
                    group[config.Key] = config;
                }
            }

            allConfigs = existingConfigs;
        }

        public static string Require(string group, string key)
        {
            Dictionary<string, Configuration> groupConfig;
            if (allConfigs.TryGetValue(group, out groupConfig))
            {
                Configuration configResult;
                if (groupConfig.TryGetValue(key, out configResult))
                {
                    return configResult.Value;
                }

                throw new ArgumentException("The key cannot be found", "key");
            }

            throw new ArgumentException("The group cannot be found", "group");
        }

        public static string Get(string group, string key)
        {
            Dictionary<string, Configuration> groupConfig;
            if (allConfigs.TryGetValue(group, out groupConfig))
            {
                Configuration configResult;
                if (groupConfig.TryGetValue(key, out configResult))
                {
                    return configResult.Value;
                }
            }

            return null;
        }
        public static T Get<T>(string group, string key)
        {
            return (T)Convert.ChangeType(Get(group, key), typeof(T));
        }
        public static T Get<T>(string group, string key, T defaultValue)
        {
            return Get<T>(group, key, () => defaultValue);
        }
        public static T Get<T>(string group, string key, Func<T> fnDefaultValue)
        {
            var val = Get(group, key);
            if (String.IsNullOrWhiteSpace(val))
                return fnDefaultValue();
            return (T)Convert.ChangeType(val, typeof(T));
        }

        public static IEnumerable<string> GetAllGroups()
        {
            return allConfigs.Keys;
        }

        public static bool ContainsGroup(string group)
        {
            return allConfigs.ContainsKey(group);
        }

        public static IEnumerable<KeyValuePair<string, string>> GetAll(string group)
        {
            Dictionary<string, Configuration> groupConfig;
            if (allConfigs.TryGetValue(group, out groupConfig))
            {
                return groupConfig.Select(d => new KeyValuePair<string, string>(d.Key, d.Value.Value));
            }

            return Empty<KeyValuePair<string, string>>.Array;
        }
        public static IEnumerable<KeyValuePair<string, T>> GetAll<T>(string group)
        {
            return GetAll(group).Select(d => new KeyValuePair<string, T>(d.Key, (T)Convert.ChangeType(d.Value, typeof(T))));
        }
        public static IEnumerable<KeyValuePair<string, T>> GetAll<T>(string group, T defaultValue)
        {
            return GetAll<T>(group, () => defaultValue);
        }
        public static IEnumerable<KeyValuePair<string, T>> GetAll<T>(string group, Func<T> fnDefaultValue)
        {
            return GetAll(group).Select(d =>
                new KeyValuePair<string, T>(
                    d.Key,
                    (!String.IsNullOrWhiteSpace(d.Value)) ? (T)Convert.ChangeType(d.Value, typeof(T))
                                                          : fnDefaultValue()
                )
            );
        }

        public static bool TryGet(string group, string key, out string value)
        {
            Dictionary<string, Configuration> groupConfig;
            if (allConfigs.TryGetValue(group, out groupConfig))
            {
                Configuration configResult;
                if (groupConfig.TryGetValue(key, out configResult))
                {
                    value = configResult.Value;
                    return true;
                }
            }

            value = null;
            return false;
        }
        public static bool TryGet<T>(string group, string key, out T value)
        {
            string sValue;
            if (TryGet(group, key, out sValue))
            {
                value = (T)Convert.ChangeType(sValue, typeof(T));
                return true;
            }

            value = default(T);
            return false;
        }

        public static bool IsFalse(string group, string key, bool defaultValue = false)
        {
            var val = Get(group, key);
            if (!String.IsNullOrWhiteSpace(val))
            {
                return String.Equals(val, "0") ||
                       String.Equals(val, "false", StringComparison.OrdinalIgnoreCase);
            }

            return !defaultValue;
        }
        public static bool IsTrue(string group, string key, bool defaultValue = false)
        {
            var val = Get(group, key);
            if (!String.IsNullOrWhiteSpace(val))
            {
                return String.Equals(val, "1") ||
                       String.Equals(val, "true", StringComparison.OrdinalIgnoreCase);
            }

            return defaultValue;
        }
    }
}
