﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Queue
{
    public static class QueueConfigurations
    {
        public static readonly string Group = "Queue";

        public static readonly string ALTGameTransactionUpdateQueueName = "ALTGameTransactionUpdateQueueName";
        public static readonly string SlotGameTransactionQueueName = "SlotGameTransactionQueueName";

        public static readonly string ALTGameTransactionUpdateExchangeName = "ALTGameTransactionUpdateExchangeName";
        public static readonly string SlotGameTransactionExchangeName = "SlotGameTransactionExchangeName";
    }
}
