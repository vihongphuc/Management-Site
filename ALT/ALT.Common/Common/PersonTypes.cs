﻿using ALT.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common
{
    public static class PersonTypes
    {
        public static PersonType? GetNaturalDownline(this PersonType currentType)
        {
            if (currentType == PersonType.Admin)
            {
                return PersonType.SuperShareHolder;
            }
            if (currentType == PersonType.SuperShareHolder)
            {
                return PersonType.ShareHolder;
            }
            if (currentType == PersonType.ShareHolder)
            {
                return PersonType.SuperSenior;
            }
            if (currentType == PersonType.SuperSenior)
            {
                return PersonType.Senior;
            }
            if (currentType == PersonType.Senior)
            {
                return PersonType.SuperMaster;
            }
            if (currentType == PersonType.SuperMaster)
            {
                return PersonType.Master;
            }
            if (currentType == PersonType.Master)
            {
                return PersonType.SuperAgent;
            }
            if (currentType == PersonType.SuperAgent)
            {
                return PersonType.Agent;
            }
            if (currentType == PersonType.Agent)
            {
                return PersonType.Member;
            }
            return null;
        }

        public static List<PersonType> CanCreatedDownlines(this PersonType currentType, bool? CreateAgency, bool? CreateMember)
        {
            List<PersonType> personTypeList = new List<PersonType>();
            if (currentType == PersonType.Admin)
            {
                personTypeList.Add(PersonType.SuperShareHolder);
                personTypeList.Add(PersonType.ShareHolder);
                personTypeList.Add(PersonType.SuperSenior);
                personTypeList.Add(PersonType.Senior);
                personTypeList.Add(PersonType.SuperMaster);
                personTypeList.Add(PersonType.Master);
                personTypeList.Add(PersonType.SuperAgent);
                personTypeList.Add(PersonType.Agent);
            }
            if (currentType == PersonType.SuperShareHolder)
            {
                if ((bool)CreateAgency)
                {
                    personTypeList.Add(PersonType.ShareHolder);
                    personTypeList.Add(PersonType.SuperSenior);
                    personTypeList.Add(PersonType.Senior);
                    personTypeList.Add(PersonType.SuperMaster);
                    personTypeList.Add(PersonType.Master);
                    personTypeList.Add(PersonType.SuperAgent);
                    personTypeList.Add(PersonType.Agent);
                }
                if ((bool)CreateMember)
                {
                    personTypeList.Add(PersonType.Member);
                }
            }
            if (currentType == PersonType.ShareHolder)
            {
                if ((bool)CreateAgency)
                {
                    personTypeList.Add(PersonType.SuperSenior);
                    personTypeList.Add(PersonType.Senior);
                    personTypeList.Add(PersonType.SuperMaster);
                    personTypeList.Add(PersonType.Master);
                    personTypeList.Add(PersonType.SuperAgent);
                    personTypeList.Add(PersonType.Agent);
                }
                if ((bool)CreateMember)
                {
                    personTypeList.Add(PersonType.Member);
                }
            }
            if (currentType == PersonType.SuperSenior)
            {
                if ((bool)CreateAgency)
                {
                    personTypeList.Add(PersonType.Senior);
                    personTypeList.Add(PersonType.SuperMaster);
                    personTypeList.Add(PersonType.Master);
                    personTypeList.Add(PersonType.SuperAgent);
                    personTypeList.Add(PersonType.Agent);
                }
                if ((bool)CreateMember)
                {
                    personTypeList.Add(PersonType.Member);
                }
            }
            if (currentType == PersonType.Senior)
            {
                if ((bool)CreateAgency)
                {
                    personTypeList.Add(PersonType.SuperMaster);
                    personTypeList.Add(PersonType.Master);
                    personTypeList.Add(PersonType.SuperAgent);
                    personTypeList.Add(PersonType.Agent);
                }
                if ((bool)CreateMember)
                {
                    personTypeList.Add(PersonType.Member);
                }
            }
            if (currentType == PersonType.SuperMaster)
            {
                if ((bool)CreateAgency)
                {
                    personTypeList.Add(PersonType.Master);
                    personTypeList.Add(PersonType.SuperAgent);
                    personTypeList.Add(PersonType.Agent);
                }
                if ((bool)CreateMember)
                {
                    personTypeList.Add(PersonType.Member);
                }
            }
            if (currentType == PersonType.Master)
            {
                if ((bool)CreateAgency)
                {
                    personTypeList.Add(PersonType.SuperAgent);
                    personTypeList.Add(PersonType.Agent);
                }
                if ((bool)CreateMember)
                {
                    personTypeList.Add(PersonType.Member);
                }
            }
            if (currentType == PersonType.SuperAgent)
            {
                if ((bool)CreateAgency)
                {
                    personTypeList.Add(PersonType.Agent);
                }
                if ((bool)CreateMember)
                {
                    personTypeList.Add(PersonType.Member);
                }
            }
            if (currentType == PersonType.Agent)
            {
                if ((bool)CreateMember)
                {
                    personTypeList.Add(PersonType.Member);
                }
            }
            return personTypeList;
        }
    }
}
