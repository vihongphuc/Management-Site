﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common
{
    public static class BetTypeItemNames
    {
        public static readonly string SlotGame = "Slot Game";
    }

    public static class BetTypeItemOCodes
    {
        public static readonly string SlotGame = OCode.Get(1100010001);
    }

    public static class BetTypeNames
    {
        public static readonly string Casino = "Casino";
    }

    public static class BetTypeOCodes
    {
        public static readonly string SlotGame = OCode.Get(110001);
    }
}
