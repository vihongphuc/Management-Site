﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common
{
    public static class Statics
    {
        public static string DateTimeShortFormat = "yyyy-MM-dd";
        public static string DateTimeLongFormat = "yyyy-MM-dd HH:mm:ss";
        public static string DateTimeFulllFormat = "yyyyMMddHHmmssfff";

        public static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        public static readonly DateTime CommonEpoch = new DateTime(2015, 8, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        public static readonly char[] spliters = new char[] { dot, comma, semecolon };
        public const char comma = ',';
        public const char semecolon = ';';
        public const char dot = '.';

        public static readonly string compileStr = "&";

        public static readonly string ErrorMessageSplitter = "|";
        public static readonly string ErrorMessageHeaderKey = "ErorrMessages";
        public static readonly string BroadCastString = "broadcast";
    }
}
