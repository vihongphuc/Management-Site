﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Utilities
{
    using Cryptography;
    public static class Password
    {
        public static string CreateHash(string password)
        {
            var hashPass = string.Empty;
            if (string.IsNullOrEmpty(password))
                return hashPass;
            hashPass = HashHepper.HashMD5(password);
            return hashPass;
        }

        public static bool ValidatePassword(string hashPass, string comparePass, bool isHashed)
        {
            var equals = false;
            var comparedPassHash = comparePass;
            if (!isHashed)
                comparedPassHash = CreateHash(comparePass);
            equals = string.Compare(hashPass, comparedPassHash) == 0;
            return equals;
        }

    }
}
