﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Utilities
{
    public static class CollectionHelpers
    {
        public static IEnumerable<T[]> Split<T>(this T[] arr, int count)
        {
            if (count == 0)
                throw new ArgumentOutOfRangeException("count");

            var start = 0;
            var left = arr.Length;
            while (left > 0)
            {
                var copyCount = Math.Min(count, left);
                var copy = new T[copyCount];
                Array.Copy(arr, start, copy, 0, copyCount);

                start += copyCount;
                left -= copyCount;

                yield return copy;
            }
        }
        public static IEnumerable<T[]> Split<T>(this IList<T> arr, int count)
        {
            if (count == 0)
                throw new ArgumentOutOfRangeException("count");

            var start = 0;
            var left = arr.Count;
            while (left > 0)
            {
                var copyCount = Math.Min(count, left);
                var copy = new T[copyCount];
                for (int i = 0; i < copyCount; i++)
                {
                    copy[i] = arr[start + i];
                }

                start += copyCount;
                left -= copyCount;

                yield return copy;
            }
        }
    }
}
