﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Helpers
{
    public static class EnumHelper<T>
        where T : struct
    {
        public static T Parse(string enumValue)
        {
            return (T)Enum.Parse(typeof(T), enumValue);
        }
        public static T Parse(string enumValue, bool ignoreCase)
        {
            return (T)Enum.Parse(typeof(T), enumValue, ignoreCase);
        }
        public static T? ParseOrNull(string enumValue)
        {
            if (String.IsNullOrWhiteSpace(enumValue))
                return null;

            T res;
            if (Enum.TryParse<T>(enumValue, out res))
            {
                return res;
            }

            return null;
        }
    }
}
