﻿using ALT.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Helpers
{
    public class PagedList<T>
    {
        public IList<T> PageList { get; set; }        
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
        public int TotalItemPerPage { get; set; }
    }
}
