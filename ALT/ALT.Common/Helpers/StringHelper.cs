﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Helpers
{
    public static class StringHelper
    {
        public static string[] ToArray(string content)
        {
            if (string.IsNullOrEmpty(content))
                return new string[] { };
            return content.Split(Statics.spliters, int.MaxValue);
        }

        public static string[] ToArray(string content, char spliter)
        {
            if (string.IsNullOrEmpty(content))
                return new string[] { };
            return content.Split(spliter);
        }

        public static string Compile(string[] array)
        {
            if (array == null || array.Length == 0)
                return string.Empty;

            return string.Join(Statics.compileStr, array);
        }

        public static string Compile(string[] array, string compileStr)
        {
            if (array == null || array.Length == 0)
                return string.Empty;

            return string.Join(compileStr, array);
        }

        public static string Compile(Dictionary<string, string> dtContent, bool shortByKey)
        {
            if (dtContent == null || dtContent.Count == 0)
                return string.Empty;
            if (shortByKey)
                return string.Join(Statics.compileStr, dtContent.OrderBy(c => c.Key).Select(c => $"{c.Key}={c.Value}").ToArray());
            return string.Join(Statics.compileStr, dtContent.Select(c => $"{c.Key}={c.Value}").ToArray());
        }

    }
}
