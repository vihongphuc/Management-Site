﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ALT.Common.Cryptography
{
    public static class HashHepper
    {
        public static string HashMD5(string content)
        {
            var data = Encoding.ASCII.GetBytes(content);
            var md5 = new MD5CryptoServiceProvider();
            var md5data = md5.ComputeHash(data);
            
            //get hash result after compute it
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();

        }

        public static string HashSHA1(string content)
        {
            var data = Encoding.ASCII.GetBytes(content);            
            var sha1 = new SHA1CryptoServiceProvider();
            var sha1data = sha1.ComputeHash(data);
            //var hashedPassword = ASCIIEncoding.UTF8.GetString(sha1data);
            var hashedPassword = Convert.ToBase64String(sha1data);
            return hashedPassword;
        }
    }
}
