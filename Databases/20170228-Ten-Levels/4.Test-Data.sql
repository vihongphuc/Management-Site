-- select * from Person order by CreatedDate desc;

INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567890, N'ZZ', N'SuperShareHolder', N'SSH', N'e10adc3949ba59abbe56e057f20f883e', getdate(), N'Address', 
		N'SuperShareHolder', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, getdate(), N'1.1.1.1', 0, 0, N'1.1.1.1', N'ShareHolder-1', 2, 
		100, N'Admin', getdate(), N'', getdate());

INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567891, N'ZZ00', N'ShareHolder', N'SHA', N'e10adc3949ba59abbe56e057f20f883e', getdate(), N'Address', 
		N'ShareHolder', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, getdate(), N'1.1.1.1', 0, 0, N'1.1.1.1', N'ShareHolder-1', 2, 
		1234567890, N'Admin', getdate(), N'', getdate());

INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567892, N'ZZ0011', N'SuperSenior', N'SPS', N'e10adc3949ba59abbe56e057f20f883e', getdate(), N'Address', 
		N'SuperSenior', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, getdate(), N'1.1.1.1', 0, 0, N'1.1.1.1', N'ShareHolder-1', 2, 
		1234567891, N'Admin', getdate(), N'', getdate());

INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567893, N'ZZ001122', N'Senior', N'SEN', N'e10adc3949ba59abbe56e057f20f883e', getdate(), N'Address', 
		N'Senior', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, getdate(), N'1.1.1.1', 0, 0, N'1.1.1.1', N'ShareHolder-1', 2, 
		1234567892, N'Admin', getdate(), N'', getdate());


INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567894, N'ZZ00112233', N'SuperMaster', N'SPM', N'e10adc3949ba59abbe56e057f20f883e', getdate(), N'Address', 
		N'SuperMaster', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, getdate(), N'1.1.1.1', 0, 0, N'1.1.1.1', N'ShareHolder-1', 2, 
		1234567893, N'Admin', getdate(), N'', getdate());

		
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567895, N'ZZ0011223344', N'Master', N'MAS', N'e10adc3949ba59abbe56e057f20f883e', getdate(), N'Address', 
		N'Master', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, getdate(), N'1.1.1.1', 0, 0, N'1.1.1.1', N'ShareHolder-1', 2, 
		1234567894, N'Admin', getdate(), N'', getdate());


		
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567896, N'ZZ001122334455', N'SuperAgent', N'SAG', N'e10adc3949ba59abbe56e057f20f883e', getdate(), N'Address', 
		N'SuperAgent', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, getdate(), N'1.1.1.1', 0, 0, N'1.1.1.1', N'ShareHolder-1', 2, 
		1234567895, N'Admin', getdate(), N'', getdate());

	
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567897, N'ZZ00112233445566', N'Agent', N'AGE', N'e10adc3949ba59abbe56e057f20f883e', getdate(), N'Address', 
		N'Agent', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, getdate(), N'1.1.1.1', 0, 0, N'1.1.1.1', N'ShareHolder-1', 2, 
		1234567896, N'Admin', getdate(), N'', getdate());
		
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567898, N'ZZ0011223344556677', N'Member', N'MEM', N'e10adc3949ba59abbe56e057f20f883e', getdate(), N'Address', 
		N'Member', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, getdate(), N'1.1.1.1', 0, 0, N'1.1.1.1', N'ShareHolder-1', 2, 
		1234567897, N'Admin', getdate(), N'', getdate());


-- select * from [dbo].[PersonBalance] order by CreatedDate desc;
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastUpdatedDate], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567890, N'USD', 1000000.0000, 0.0000, N'SQL-Admin', getdate(), getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastUpdatedDate], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567891, N'USD', 900000.0000, 0.0000, N'SQL-Admin', getdate(), getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastUpdatedDate], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567892, N'USD', 800000.0000, 0.0000, N'SQL-Admin', getdate(), getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastUpdatedDate], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567893, N'USD', 700000.0000, 0.0000, N'SQL-Admin', getdate(), getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastUpdatedDate], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567894, N'USD', 600000.0000, 0.0000, N'SQL-Admin', getdate(), getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastUpdatedDate], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567895, N'USD', 500000.0000, 0.0000, N'SQL-Admin', getdate(), getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastUpdatedDate], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567896, N'USD', 400000.0000, 0.0000, N'SQL-Admin', getdate(), getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastUpdatedDate], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567897, N'USD', 300000.0000, 0.0000, N'SQL-Admin', getdate(), getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastUpdatedDate], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567898, N'USD', 100000.0000, 0.0000, N'SQL-Admin', getdate(), getdate(), N'SQL-Admin', getdate());




-- select * from [dbo].[PersonCreditSetting] order by CreatedDate desc;
INSERT [dbo].[PersonCreditSetting] ([PersonID], [CurrencyCode], [PersonType], [MaxCredit], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567890, N'USD', N'SuperShareHolder', 0.0000, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PersonCreditSetting] ([PersonID], [CurrencyCode], [PersonType], [MaxCredit], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567891, N'USD', N'ShareHolder', 0.0000, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PersonCreditSetting] ([PersonID], [CurrencyCode], [PersonType], [MaxCredit], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567892, N'USD', N'SuperSenior', 0.0000, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PersonCreditSetting] ([PersonID], [CurrencyCode], [PersonType], [MaxCredit], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567893, N'USD', N'Senior', 0.0000, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PersonCreditSetting] ([PersonID], [CurrencyCode], [PersonType], [MaxCredit], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567894, N'USD', N'SuperMaster', 0.0000, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PersonCreditSetting] ([PersonID], [CurrencyCode], [PersonType], [MaxCredit], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567895, N'USD', N'Master', 0.0000, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PersonCreditSetting] ([PersonID], [CurrencyCode], [PersonType], [MaxCredit], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567896, N'USD', N'SuperAgent', 0.0000, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PersonCreditSetting] ([PersonID], [CurrencyCode], [PersonType], [MaxCredit], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567897, N'USD', N'Agent', 0.0000, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PersonCreditSetting] ([PersonID], [CurrencyCode], [PersonType], [MaxCredit], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567898, N'USD', N'Member', 0.0000, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());




-- select * from [dbo].[PersonBetSetting] order by CreatedDate desc;
INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567890, 1100010001, N'USD', N'MaxWinLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567890, 1100010001, N'USD', N'MinLossLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());


INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567891, 1100010001, N'USD', N'MaxWinLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567891, 1100010001, N'USD', N'MinLossLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());


INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567892, 1100010001, N'USD', N'MaxWinLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567892, 1100010001, N'USD', N'MinLossLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());


INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567893, 1100010001, N'USD', N'MaxWinLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567893, 1100010001, N'USD', N'MinLossLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());


INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567894, 1100010001, N'USD', N'MaxWinLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567894, 1100010001, N'USD', N'MinLossLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());


INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567895, 1100010001, N'USD', N'MaxWinLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567895, 1100010001, N'USD', N'MinLossLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());


INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567896, 1100010001, N'USD', N'MaxWinLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567896, 1100010001, N'USD', N'MinLossLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());


INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567897, 1100010001, N'USD', N'MaxWinLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567897, 1100010001, N'USD', N'MinLossLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());


INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567898, 1100010001, N'USD', N'MaxWinLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567898, 1100010001, N'USD', N'MinLossLimit', 0.0000, null, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());






-- select * from [dbo].[PositionTaking] order by CreatedDate desc;
INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567890, 1100010001, N'USD', N'Upline', 0.1, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567890, 1100010001, N'USD', N'CurrentMax', 0.9, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());


INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567891, 1100010001, N'USD', N'Upline', 0.1, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567891, 1100010001, N'USD', N'CurrentMax', 0.8, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567892, 1100010001, N'USD', N'Upline', 0.1, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567892, 1100010001, N'USD', N'CurrentMax', 0.7, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());

INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567893, 1100010001, N'USD', N'Upline', 0.1, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567893, 1100010001, N'USD', N'CurrentMax', 0.6, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());


INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567894, 1100010001, N'USD', N'Upline', 0.1, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567894, 1100010001, N'USD', N'CurrentMax', 0.5, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());


INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567895, 1100010001, N'USD', N'Upline', 0.1, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567895, 1100010001, N'USD', N'CurrentMax', 0.4, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());


INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567896, 1100010001, N'USD', N'Upline', 0.1, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567896, 1100010001, N'USD', N'CurrentMax', 0.3, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());


INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567897, 1100010001, N'USD', N'Upline', 0.1, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567897, 1100010001, N'USD', N'CurrentMax', 0.2, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());


INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567898, 1100010001, N'USD', N'Upline', 0.2, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());
INSERT [dbo].[PositionTaking] ([PersonID], [BetTypeItemID], [CurrencyCode], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) 
VALUES (1234567898, 1100010001, N'USD', N'CurrentMax', 0.0, N'SQL-Admin', getdate(), N'SQL-Admin', getdate());





-- select * from [dbo].[PersonUpline] where PersonID = 1234567898;
INSERT [dbo].[PersonUpline] ([PersonID], [AdminID],[SuperShareHolderID], [ShareHolderID], [SuperSeniorID], [SeniorID], [SuperMasterID], [MasterID], [SuperAgentID], [AgentID])
VALUES (1234567898, 100, 1234567890,1234567891,1234567892,1234567893,1234567894,1234567895,1234567896,1234567897);


