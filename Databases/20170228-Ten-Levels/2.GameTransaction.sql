

-- select SuperSeniorRate from GameTransaction;
-- select TotalSuperSeniorWinLoss from GameTransactionSummary;

use ALT_Slot;
Go
EXEC sp_rename N'dbo.GameTransaction.SupperSeniorAmount', N'SuperSeniorAmount', 'COLUMN';  
GO  

EXEC sp_rename 'dbo.GameTransaction.SupperSeniorRate', 'SuperSeniorRate', 'COLUMN';  
Go
EXEC sp_rename 'dbo.PersonUpline.SupperSeniorID', 'SuperSeniorID', 'COLUMN';  
GO
EXEC sp_rename 'dbo.GameTransactionSummary.TotalSupperSeniorWinLoss', 'TotalSuperSeniorWinLoss', 'COLUMN';  
GO

EXEC sp_rename 'dbo.GameTransactionPositionTaking.UpdateBy', 'UpdatedBy', 'COLUMN';  
GO




EXEC sp_rename N'dbo.GameSetting.UpdateBy', N'UpdatedBy', 'COLUMN';  
GO
EXEC sp_rename N'dbo.GameSetting.UpdateDate', N'UpdatedDate', 'COLUMN';  
GO
EXEC sp_rename N'DF_GameSetting_UpdateDate', N'DF_GameSetting_UpdatedDate', 'OBJECT';  
GO

EXEC sp_rename N'dbo.Person.UpdateDate', N'UpdatedDate', 'COLUMN';  
GO
EXEC sp_rename N'DF_Person_UpdateDate', N'DF_Person_UpdatedDate', 'OBJECT';  
GO


EXEC sp_rename N'dbo.PersonCreditSetting.UpdateDate', N'UpdatedDate', 'COLUMN';  
GO
EXEC sp_rename N'DF_PersonCreditSetting_UpdateDate', N'DF_PersonCreditSetting_UpdatedDate', 'OBJECT';  
GO

EXEC sp_rename N'dbo.Transaction.UpdateBy', N'UpdatedBy', 'COLUMN';  
GO

EXEC sp_rename N'dbo.PersonBalance.LastedUpdateDate', N'LastUpdatedDate', 'COLUMN';  
GO
EXEC sp_rename N'DF_PersonBalance_LastedUpdateDate', N'DF_PersonBalance_LastUpdatedDate', 'OBJECT';  
GO


EXEC sp_rename N'dbo.Configuration.CreateDate', N'CreatedDate', 'COLUMN';  
GO
EXEC sp_rename N'DF_Configuration_CreateDate', N'DF_Configuration_CreatedDate', 'OBJECT';  
GO



update Person set Type = 'SuperSenior' where Type= 'SupperSenior';
update PersonCreditSetting set PersonType = 'SuperSenior' where PersonType= 'SupperSenior';





alter table GameTransaction add SuperShareHolderRate [money] NULL;
alter table GameTransaction add SuperShareHolderAmount [money] NULL;

alter table GameTransaction add SuperMasterRate [money] NULL;
alter table GameTransaction add SuperMasterAmount [money] NULL;

alter table GameTransaction add SuperAgentRate [money] NULL;
alter table GameTransaction add SuperAgentAmount [money] NULL;



alter table GameTransaction add AdminID [bigint] NULL;

alter table GameTransaction add SuperShareHolderID [bigint] NULL;
alter table GameTransaction add ShareHolderID [bigint] NULL;

alter table GameTransaction add SuperSeniorID [bigint] NULL;
alter table GameTransaction add SeniorID [bigint] NULL;

alter table GameTransaction add SuperMasterID [bigint] NULL;
alter table GameTransaction add MasterID [bigint] NULL;

alter table GameTransaction add SuperAgentID [bigint] NULL;
alter table GameTransaction add AgentID [bigint] NULL;


--select * from GameTransactionSummary;


alter table GameTransactionSummary add AdminID [bigint] NULL;

alter table GameTransactionSummary add SuperShareHolderID [bigint] NULL;
alter table GameTransactionSummary add ShareHolderID [bigint] NULL;

alter table GameTransactionSummary add SuperSeniorID [bigint] NULL;
alter table GameTransactionSummary add SeniorID [bigint] NULL;

alter table GameTransactionSummary add SuperMasterID [bigint] NULL;
alter table GameTransactionSummary add MasterID [bigint] NULL;

alter table GameTransactionSummary add SuperAgentID [bigint] NULL;
alter table GameTransactionSummary add AgentID [bigint] NULL;


alter table GameTransactionSummary add TotalSuperShareHolderWinLoss [money] NULL;
alter table GameTransactionSummary add TotalSuperMasterWinLoss [money] NULL;
alter table GameTransactionSummary add TotalSuperAgentWinLoss [money] NULL;




-- select * from PersonUpline;

alter table PersonUpline add SuperShareHolderID [bigint] NULL;
alter table PersonUpline add SuperMasterID [bigint] NULL;
alter table PersonUpline add SuperAgentID [bigint] NULL;
