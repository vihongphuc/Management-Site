


USE [ALT_Slot]
GO

/****** Object:  Table [dbo].[CommissionType]    Script Date: 3/1/2017 11:21:55 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CommissionType](
	[ID] [bigint] NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[DefaultRate] [money] NOT NULL DEFAULT ((0)),
	[Enable] [bit] NULL,
	[Description] [varchar](500) NULL,

	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_CommissionType_UpdatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_CommissionType_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_CommissionType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO





/****** Object:  Table [dbo].[Commission]    Script Date: 3/1/2017 11:26:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Commission](
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[BetTypeID] [int] NOT NULL,
	[BetTypeItemID] [int] NOT NULL,
	[GameID] [int] NOT NULL,	
	[CommissionTypeID] [bigint] NOT NULL,
	[Rate] [money] NULL,
	[PointRate] [money] NULL,

	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_Commission_UpdatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Commission_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Commission] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC,
	[CurrencyCode] ASC,
	[CommissionTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Commission]  WITH CHECK ADD  CONSTRAINT [FK_Commission_CommissionType] FOREIGN KEY([CommissionTypeID])
REFERENCES [dbo].[CommissionType] ([ID])
GO

ALTER TABLE [dbo].[Commission] CHECK CONSTRAINT [FK_Commission_CommissionType]
GO

ALTER TABLE [dbo].[Commission]  WITH CHECK ADD  CONSTRAINT [FK_Commission_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO

ALTER TABLE [dbo].[Commission] CHECK CONSTRAINT [FK_Commission_Currency]
GO

ALTER TABLE [dbo].[Commission]  WITH CHECK ADD  CONSTRAINT [FK_Commission_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO

ALTER TABLE [dbo].[Commission] CHECK CONSTRAINT [FK_Commission_Person]
GO


ALTER TABLE [dbo].[Commission]  WITH CHECK ADD  CONSTRAINT [FK_Commission_BetType] FOREIGN KEY([BetTypeID])
REFERENCES [dbo].[BetType] ([ID])
GO

ALTER TABLE [dbo].[Commission] CHECK CONSTRAINT [FK_Commission_BetType]
GO



ALTER TABLE [dbo].[Commission]  WITH CHECK ADD  CONSTRAINT [FK_Commission_BetTypeItem] FOREIGN KEY([BetTypeItemID])
REFERENCES [dbo].[BetTypeItem] ([ID])
GO

ALTER TABLE [dbo].[Commission] CHECK CONSTRAINT [FK_Commission_BetTypeItem]
GO

ALTER TABLE [dbo].[Commission]  WITH CHECK ADD  CONSTRAINT [FK_Commission_Game] FOREIGN KEY([GameID])
REFERENCES [dbo].[Game] ([ID])
GO

ALTER TABLE [dbo].[Commission] CHECK CONSTRAINT [FK_Commission_Game]
GO





/****** Object:  Table [dbo].[GameTransactionCommission]    Script Date: 3/1/2017 11:34:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GameTransactionCommission](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TransactionID] [bigint] NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NULL,
	[Gross] [money] NULL,
	[Rate] [money] NULL,
	[Amount] [money] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_GameTransactionCommission_UpdatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_GameTransactionCommission_CreatedDate]  DEFAULT (getdate()),
	[PointRate] [money] NULL,
 CONSTRAINT [PK_GameTransactionCommission] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[GameTransactionCommission]  WITH CHECK ADD  CONSTRAINT [FK_GameTransactionCommission_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO

ALTER TABLE [dbo].[GameTransactionCommission] CHECK CONSTRAINT [FK_GameTransactionCommission_Currency]
GO

ALTER TABLE [dbo].[GameTransactionCommission]  WITH CHECK ADD  CONSTRAINT [FK_GameTransactionCommission_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO

ALTER TABLE [dbo].[GameTransactionCommission] CHECK CONSTRAINT [FK_GameTransactionCommission_Person]
GO

ALTER TABLE [dbo].[GameTransactionCommission]  WITH CHECK ADD  CONSTRAINT [FK_GameTransactionCommission_Transaction] FOREIGN KEY([TransactionID])
REFERENCES [dbo].[Transaction] ([ID])
GO

ALTER TABLE [dbo].[GameTransactionCommission] CHECK CONSTRAINT [FK_GameTransactionCommission_Transaction]
GO



/*
	select * from GameTransaction;
	update GameTransaction & GameTransactionSummary
*/

alter table GameTransaction add AdminCmmGross [money] NULL;
alter table GameTransaction add AdminCmmRate [money] NULL;
alter table GameTransaction add AdminCmmAmount [money] NULL;

alter table GameTransaction add SuperShareHolderCmmGross [money] NULL;
alter table GameTransaction add SuperShareHolderCmmRate [money] NULL;
alter table GameTransaction add SuperShareHolderCmmAmount [money] NULL;

alter table GameTransaction add ShareHolderCmmGross [money] NULL;
alter table GameTransaction add ShareHolderCmmRate [money] NULL;
alter table GameTransaction add ShareHolderCmmAmount [money] NULL;

alter table GameTransaction add SuperSeniorCmmGross [money] NULL;
alter table GameTransaction add SuperSeniorCmmRate [money] NULL;
alter table GameTransaction add SuperSeniorCmmAmount [money] NULL;

alter table GameTransaction add SeniorCmmGross [money] NULL;
alter table GameTransaction add SeniorCmmRate [money] NULL;
alter table GameTransaction add SeniorCmmAmount [money] NULL;

alter table GameTransaction add SuperMasterCmmGross [money] NULL;
alter table GameTransaction add SuperMasterCmmRate [money] NULL;
alter table GameTransaction add SuperMasterCmmAmount [money] NULL;

alter table GameTransaction add MasterCmmGross [money] NULL;
alter table GameTransaction add MasterCmmRate [money] NULL;
alter table GameTransaction add MasterCmmAmount [money] NULL;

alter table GameTransaction add SuperAgentCmmGross [money] NULL;
alter table GameTransaction add SuperAgentCmmRate [money] NULL;
alter table GameTransaction add SuperAgentCmmAmount [money] NULL;

alter table GameTransaction add AgentCmmGross [money] NULL;
alter table GameTransaction add AgentCmmRate [money] NULL;
alter table GameTransaction add AgentCmmAmount [money] NULL;


/*
	select * from GameTransactionSummary;
*/

alter table GameTransactionSummary add AdminCmmGross [money] NULL;
alter table GameTransactionSummary add AdminCmmAmount [money] NULL;

alter table GameTransactionSummary add SuperShareHolderCmmGross [money] NULL;
alter table GameTransactionSummary add SuperShareHolderCmmAmount [money] NULL;

alter table GameTransactionSummary add ShareHolderCmmGross [money] NULL;
alter table GameTransactionSummary add ShareHolderCmmAmount [money] NULL;

alter table GameTransactionSummary add SuperSeniorCmmGross [money] NULL;
alter table GameTransactionSummary add SuperSeniorCmmAmount [money] NULL;

alter table GameTransactionSummary add SeniorCmmGross [money] NULL;
alter table GameTransactionSummary add SeniorCmmAmount [money] NULL;

alter table GameTransactionSummary add SuperMasterCmmGross [money] NULL;
alter table GameTransactionSummary add SuperMasterCmmAmount [money] NULL;

alter table GameTransactionSummary add MasterCmmGross [money] NULL;
alter table GameTransactionSummary add MasterCmmAmount [money] NULL;

alter table GameTransactionSummary add SuperAgentCmmGross [money] NULL;
alter table GameTransactionSummary add SuperAgentCmmAmount [money] NULL;

alter table GameTransactionSummary add AgentCmmGross [money] NULL;
alter table GameTransactionSummary add AgentCmmAmount [money] NULL;
