-- delete from [Statement];
select * from [Statement];

alter table [Statement] add CurrencyCode varchar(10) not null;

ALTER TABLE [dbo].[Statement]  WITH CHECK ADD  CONSTRAINT [FK_Statement_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode]);

ALTER TABLE [dbo].[Statement] CHECK CONSTRAINT [FK_Statement_Currency];