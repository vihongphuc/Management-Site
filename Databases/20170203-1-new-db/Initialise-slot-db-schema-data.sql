  -- drop database [ALT_Slot];

if db_id('ALT_Slot') is null
 create database [ALT_Slot];

  
USE [ALT_Slot]
GO
/****** Object:  Table [dbo].[Announcement]    Script Date: 2/3/2017 8:58:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Announcement](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NULL,
	[Type] [varchar](50) NULL,
	[Title] [nvarchar](200) NULL,
	[Value] [text] NULL,
	[LanguageCode] [varchar](10) NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Announcement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Audit]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Audit](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Time] [datetime] NULL CONSTRAINT [DF_Audit_Time]  DEFAULT (getdate()),
	[Subject] [varchar](100) NULL,
	[PersonID] [bigint] NOT NULL,
	[RelatedID] [bigint] NULL,
	[Details] [nvarchar](500) NULL,
	[Before] [nvarchar](500) NULL,
	[After] [nvarchar](500) NULL,
	[Type] [varchar](100) NULL,
	[Token] [varchar](100) NULL,
	[IPAddress] [varchar](100) NULL,
	[IPLocation] [varchar](100) NULL,
	[ConsumerIPAddress] [varchar](100) NULL,
	[ConsumerIPLocation] [varchar](100) NULL,
	[ControllerName] [varchar](100) NULL,
	[ActionName] [varchar](100) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Audit_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Audit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BetType]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BetType](
	[ID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_BetType_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_BetType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BetTypeItem]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BetTypeItem](
	[ID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[BetTypeID] [int] NOT NULL,
	[Description] [nvarchar](200) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_BetTypeItem_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_BetTypeItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Configuration]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Configuration](
	[ConfigKey] [varchar](100) NOT NULL,
	[ConfigValue] [text] NULL,
	[Detail] [text] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreateDate] [datetime] NULL CONSTRAINT [DF_Configuration_CreateDate]  DEFAULT (getdate()),
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_Configuration_UpdatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Configuration] PRIMARY KEY CLUSTERED 
(
	[ConfigKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Currency](
	[CurrencyCode] [varchar](10) NOT NULL,
	[Name] [varchar](50) NULL,
	[DisplayName] [varchar](200) NULL,
	[Enable] [bit] NOT NULL CONSTRAINT [DF_Currency_Enable]  DEFAULT ((0)),
	[Rate] [money] NOT NULL CONSTRAINT [DF_Currency_Rate]  DEFAULT ((1)),
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[CurrencyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Game]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Game](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](100) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[GroupID] [int] NOT NULL,
	[Type] [varchar](50) NULL,
	[Enable] [bit] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_Game_UpdatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Game_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Game] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GameCategory]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GameCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[ImageURL] [nvarchar](200) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_GameCategory_CreatedDate]  DEFAULT (getdate()),
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_GameCategory_UpdatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_GameCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GameSession]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GameSession](
	[ID] [bigint] NOT NULL,
	[TransferAmount] [money] NOT NULL,
	[FinishedTime] [datetime] NULL,
	[FinishedTimeTicks] [bigint] NULL,
	[PersonID] [bigint] NOT NULL,
	[TotalBet] [money] NULL,
	[TotalResult] [money] NULL,
	[Ack] [datetime] NULL,
	[AckTicks] [datetime] NULL,
	[Platform] [varchar](100) NULL,
	[IpAddress] [varchar](100) NULL,
	[Data] [nvarchar](100) NULL,
	[Publisher] [varchar](100) NULL,
	[CurrencyCode] [varchar](50) NULL,
	[PointRate] [money] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_GameSession_UpdatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_GameSession_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_GameSession] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GameSetting]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GameSetting](
	[GameID] [int] NOT NULL,
	[Publisher] [varchar](100) NOT NULL,
	[Platform] [varchar](100) NOT NULL,
	[DownloadURL] [varchar](500) NULL,
	[GameHash] [varchar](500) NULL,
	[GameKey] [varchar](500) NULL,
	[GameHearder] [varchar](500) NULL,
	[Enable] [bit] NULL,
	[IsHot] [bit] NULL DEFAULT ((0)),
	[IsNew] [bit] NULL DEFAULT ((1)),
	[Rate] [varchar](50) NULL,
	[Type] [varchar](50) NULL,
	[Width] [money] NULL,
	[Height] [money] NULL,
	[ImageURL] [varchar](200) NULL,
	[UpdateBy] [varchar](100) NULL,
	[UpdateDate] [datetime] NULL CONSTRAINT [DF_GameSetting_UpdateDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_GameSetting_CreatedDate]  DEFAULT (getdate()),
	[Port] [int] NULL,
	[Domain] [nvarchar](200) NULL,
 CONSTRAINT [PK_GameSetting] PRIMARY KEY CLUSTERED 
(
	[GameID] ASC,
	[Publisher] ASC,
	[Platform] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GameTransaction]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GameTransaction](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[GameSessionID] [bigint] NOT NULL,
	[GameID] [int] NOT NULL,
	[TransactionID] [bigint] NOT NULL,
	[Amount] [money] NOT NULL,
	[Result] [money] NOT NULL,
	[StartBalance] [money] NOT NULL,
	[EndBalance] [money] NOT NULL,
	[Time] [datetime] NULL,
	[Ticks] [datetime] NULL,
	[Detail] [text] NULL,
	[Type] [varchar](100) NULL,
	[BetTypeItemID] [int] NULL,
	[Description] [nvarchar](500) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_GameTransaction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GeneralTable]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GeneralTable](
	[Id] [int] NOT NULL,
	[TableName] [varchar](100) NOT NULL,
	[FeildName] [varchar](100) NOT NULL,
	[Key] [varchar](50) NULL,
	[Value] [varchar](50) NULL,
	[Setting] [nvarchar](500) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_GeneralTable] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GroupPermission]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GroupPermission](
	[ID] [int] NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_GroupPermission] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Module]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Module](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[DisplayName] [nvarchar](200) NOT NULL,
	[Enable] [bit] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Module] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Permission]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Permission](
	[GroupPermissionID] [int] NOT NULL,
	[ModuleID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[Checked] [bit] NOT NULL,
	[Description] [nvarchar](200) NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[GroupPermissionID] ASC,
	[ModuleID] ASC,
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Person]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Person](
	[ID] [bigint] NOT NULL,
	[Username] [varchar](200) NOT NULL,
	[Firstname] [nvarchar](200) NULL,
	[Lastname] [nvarchar](200) NULL,
	[Password] [varchar](200) NOT NULL,
	[DOB] [date] NULL,
	[Address] [nvarchar](200) NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Status] [varchar](10) NULL,
	[CountryCode] [varchar](50) NULL,
	[CurrencyCodes] [varchar](200) NULL,
	[Mobile] [varchar](50) NULL,
	[Phone] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[LastLoginTime] [datetime] NULL,
	[LastLoginIP] [varchar](50) NULL,
	[IsExternal] [bit] NULL,
	[AutoTransfer] [bit] NULL,
	[LimitIP] [varchar](100) NULL,
	[DisplayName] [varchar](2000) NULL,
	[GroupID] [int] NULL,
	[UplineID] [bigint] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdateDate] [datetime] NULL CONSTRAINT [DF_Person_UpdateDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_Person_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonBalance]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersonBalance](
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[Available] [money] NOT NULL,
	[YesterdayAvailable] [money] NOT NULL,
	[UpdatedBy] [nvarchar](100) NULL,
	[LastedUpdateDate] [datetime] NULL CONSTRAINT [DF_PersonBalance_LastedUpdateDate]  DEFAULT (getdate()),
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_PersonBalance_UpdatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_PersonBalance_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_PersonBalance] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC,
	[CurrencyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonBetSetting]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersonBetSetting](
	[PersonID] [bigint] NOT NULL,
	[BetTypeItemID] [int] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[SettingType] [varchar](100) NOT NULL,
	[Value] [decimal](18, 0) NULL,
	[ValueString] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](100) NULL,
 CONSTRAINT [PK_PersonBetSetting] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC,
	[BetTypeItemID] ASC,
	[CurrencyCode] ASC,
	[SettingType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonCreditSetting]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersonCreditSetting](
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[PersonType] [varchar](50) NOT NULL,
	[MaxCredit] [money] NOT NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdateDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_PersonCreditSetting] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC,
	[CurrencyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonGroup]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersonGroup](
	[PersonID] [bigint] NOT NULL,
	[GroupPermissionID] [int] NOT NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_PersonGroup] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC,
	[GroupPermissionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonPositionTaking]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersonPositionTaking](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TransactionID] [bigint] NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[Rate] [money] NULL,
	[Amount] [money] NULL,
	[UpdateBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_PersonPositionTaking] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonUpline]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonUpline](
	[PersonID] [bigint] NOT NULL,
	[AdminID] [bigint] NULL,
	[ShareHolderID] [bigint] NULL,
	[SupperSeniorID] [bigint] NULL,
	[SeniorID] [bigint] NULL,
	[MasterID] [bigint] NULL,
	[AgentID] [bigint] NULL,
 CONSTRAINT [PK_PersonUpline] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PositionTaking]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PositionTaking](
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[BetTypeItemID] [int] NOT NULL,
	[Type] [varchar](100) NOT NULL,
	[Value] [money] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_PositionTaking] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC,
	[CurrencyCode] ASC,
	[BetTypeItemID] ASC,
	[Type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Role]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [date] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Statement]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Statement](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar] (10) NOT NULL,
	[RelatedPersonID] [bigint] NOT NULL,
	[Type] [varchar](100) NOT NULL,
	[Time] [datetime] NOT NULL,
	[Ticks] [bigint] NOT NULL,
	[Amount] [money] NOT NULL,
	[Detail] [nvarchar](500) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Statement_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Statement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 2/3/2017 8:58:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transaction](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GameSessionID] [bigint] NOT NULL,
	[Amount] [money] NOT NULL,
	[Result] [money] NOT NULL,
	[Type] [varchar](50) NULL,
	[Description] [nvarchar](500) NULL,
	[BetTypeItemID] [int] NOT NULL,
	
	[PersonID] [bigint] not null,
	[CurrencyCode] varchar(10) not null,
	[Status] [varchar](10) not null,
	[IpAddress] [varchar](100),
	[Signature] [varchar](500) not null,

	[UpdateBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

INSERT [dbo].[BetType] ([ID], [Name], [Description], [CreatedBy], [CreatedDate]) VALUES (111000, N'BetType', NULL, NULL, NULL)
INSERT [dbo].[BetTypeItem] ([ID], [Name], [BetTypeID], [Description], [CreatedBy], [CreatedDate]) VALUES (1110001, N'BetTypeItem', 111000, NULL, NULL, NULL)
INSERT [dbo].[Configuration] ([ConfigKey], [ConfigValue], [Detail], [CreatedBy], [CreateDate], [UpdatedBy], [UpdatedDate]) VALUES (N'Platform', N'[{Desktop:"Desktop"}]', N'Desktop', N'SQL - Admin', CAST(N'2016-11-19 22:35:15.010' AS DateTime), N'SQL - Admin', CAST(N'2016-11-19 22:35:15.010' AS DateTime))
INSERT [dbo].[Configuration] ([ConfigKey], [ConfigValue], [Detail], [CreatedBy], [CreateDate], [UpdatedBy], [UpdatedDate]) VALUES (N'Publishers', N'[{Alt:"Asian Live Tech Company"}]', N'All Publishers', N'SQL - Admin', CAST(N'2016-11-19 22:35:10.863' AS DateTime), N'SQL - Admin', CAST(N'2016-11-19 22:35:10.863' AS DateTime))
INSERT [dbo].[Currency] ([CurrencyCode], [Name], [DisplayName], [Enable], [Rate]) VALUES (N'USD', N'USD', N'USD', 1, 0.0100)
INSERT [dbo].[Currency] ([CurrencyCode], [Name], [DisplayName], [Enable], [Rate]) VALUES (N'VND', N'Vietnam Dong', N'VND', 1, 1.0000)
SET IDENTITY_INSERT [dbo].[Game] ON 

INSERT [dbo].[Game] ([ID], [Code], [Name], [GroupID], [Type], [Enable], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (1, N'ThreeKingdoms', N'Three Kingdoms', 1, NULL, 1, N'SQL - Admin', CAST(N'2016-11-19 22:31:49.707' AS DateTime), N'SQL - Admin', CAST(N'2016-11-19 22:31:49.707' AS DateTime))
SET IDENTITY_INSERT [dbo].[Game] OFF
SET IDENTITY_INSERT [dbo].[GameCategory] ON 

INSERT [dbo].[GameCategory] ([ID], [Name], [Description], [ImageURL], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'Slot', N'Slot game', NULL, N'SQL - Admin', CAST(N'2016-11-19 22:19:39.730' AS DateTime), N'SQL - Admin', CAST(N'2016-11-19 22:19:39.730' AS DateTime))
INSERT [dbo].[GameCategory] ([ID], [Name], [Description], [ImageURL], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, N'Table', N'Table Game', NULL, N'SQL - Admin', CAST(N'2016-11-19 22:20:04.390' AS DateTime), N'SQL - Admin', CAST(N'2016-11-19 22:20:04.390' AS DateTime))
INSERT [dbo].[GameCategory] ([ID], [Name], [Description], [ImageURL], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (3, N'ThreeKingdoms', NULL, N'', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[GameCategory] ([ID], [Name], [Description], [ImageURL], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (4, N'ThreeKingdoms', NULL, N'', N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[GameCategory] OFF

INSERT [dbo].[GameSetting] ([GameID], [Publisher], [Platform], [DownloadURL], [GameHash], [GameKey], [GameHearder], [Enable], [IsHot], [IsNew], [Rate], [Type], [Width], [Height], [ImageURL], [UpdateBy], [UpdateDate], [CreatedBy], [CreatedDate], [Port], [Domain]) VALUES (1, N'ALT', N'Desktop', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'SQL - Admin', CAST(N'2016-11-19 22:32:32.477' AS DateTime), N'SQL - Admin', CAST(N'2016-11-19 22:32:32.477' AS DateTime), 8001, N'192.168.1.4')
INSERT [dbo].[GameSetting] ([GameID], [Publisher], [Platform], [DownloadURL], [GameHash], [GameKey], [GameHearder], [Enable], [IsHot], [IsNew], [Rate], [Type], [Width], [Height], [ImageURL], [UpdateBy], [UpdateDate], [CreatedBy], [CreatedDate], [Port], [Domain]) VALUES (1, N'ALT', N'Moblie', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8001, N'192.168.1.4')
INSERT [dbo].[GameSetting] ([GameID], [Publisher], [Platform], [DownloadURL], [GameHash], [GameKey], [GameHearder], [Enable], [IsHot], [IsNew], [Rate], [Type], [Width], [Height], [ImageURL], [UpdateBy], [UpdateDate], [CreatedBy], [CreatedDate], [Port], [Domain]) VALUES (1, N'ALT', N'Web', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8001, N'ws://asianlive.tech:8001/games')
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (100, N'admin', N'Mrs', N'Ad', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Admin', N'Active', N'CountryCode', N'USD,VND', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Admin', 1, NULL, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (1000, N'0', N'ShareHolder', N'Sh', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'ShareHolder', N'Active', N'CountryCode', N'USD,VND', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'ShareHolder-1', 2, 100, N'Admin', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (10000, N'000', N'SuperSenior', N'Su', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Suppersenior', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'SuperSenior-1', 2, 1000, N'ShareHolder', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (100000, N'00000', N'Senior', N'Se', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Senior', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Senior-1', 2, 10000, N'SuperSenior', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (1000000, N'0000000', N'Master', N'Ma', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Master', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Master-1', 2, 100000, N'Senior', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (10000000, N'000000000', N'Agent', N'Ag', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Agent', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Agent-1', 2, 1000000, N'Master', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (100000000, N'00000000001', N'Member', N'Me', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Member', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Agent-1', 2, 10000000, N'00000000001', CAST(N'2017-01-10 17:24:15.170' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (1000000000, N'00000000002', N'Member', N'Me', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Member', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Agent-1', 2, 10000000, N'Master', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (10000000000, N'00000000003', N'Member', N'Me', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Member', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Agent-1', 2, 10000000, N'Master', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (100000000000, N'00000000004', N'Member', N'Me', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Member', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Agent-1', 2, 10000000, N'Master', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (1000000000000, N'00000000005', N'Member', N'Me', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Member', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Agent-1', 2, 10000000, N'Master', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (10000000000000, N'00000000006', N'Member', N'Me', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Member', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Agent-1', 2, 10000000, N'Master', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (100000000000000, N'00000000007', N'Member', N'Me', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Member', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Agent-1', 2, 10000000, N'Master', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (100000000000001, N'00000000008', N'Member', N'Me', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Member', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Agent-1', 2, 10000000, N'Master', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (100000000000009, N'00000000009', N'Member', N'Me', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Member', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Agent-1', 2, 10000000, N'Master', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (100000000000010, N'00000000010', N'Member', N'Me', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Member', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Agent-1', 2, 10000000, N'Master', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (100000000000011, N'00000000011', N'Member', N'Me', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Member', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Agent-1', 2, 10000000, N'Master', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (100000000000012, N'00000000012', N'Member', N'Me', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Member', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Agent-1', 2, 10000000, N'Master', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (100000000000013, N'00000000013', N'Member', N'Me', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Member', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Agent-1', 2, 10000000, N'Master', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (100000000000014, N'00000000014', N'Member', N'Me', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Member', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Agent-1', 2, 10000000, N'Master', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (100000000000015, N'00000000015', N'Member', N'Me', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Member', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Agent-1', 2, 10000000, N'Master', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCodes], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [UpdatedBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (100000000000016, N'00000000016', N'Member', N'Me', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Member', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Agent-1', 2, 10000000, N'Master', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100, N'USD', 10000.0000, 100.0000, N'', CAST(N'2016-12-31 16:25:10.947' AS DateTime), CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (1000, N'USD', 1000.0000, 100.0000, N'', CAST(N'2016-12-31 16:25:10.947' AS DateTime), CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (10000, N'USD', 100.0000, 100.0000, N'', CAST(N'2016-12-31 16:25:10.947' AS DateTime), CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100000, N'USD', 100.0029, 100.0000, N'00000', CAST(N'2017-01-13 15:08:16.110' AS DateTime), CAST(N'2017-01-13 15:28:03.927' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (1000000, N'USD', 100.0000, 100.0000, N'', CAST(N'2016-12-31 16:25:10.947' AS DateTime), CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (10000000, N'USD', 284907.6111, 508272.6231, N'000000000', CAST(N'2017-01-13 00:18:00.940' AS DateTime), CAST(N'2017-01-30 19:15:50.330' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100000000, N'USD', 0.0000, 3367.3600, N'00000000001', CAST(N'2017-02-02 11:30:39.107' AS DateTime), CAST(N'2017-02-02 16:30:32.810' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (1000000000, N'USD', 102.7800, 100.7859, N'00000000002', CAST(N'2017-01-16 15:27:44.077' AS DateTime), CAST(N'2017-01-16 15:28:48.593' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (10000000000, N'USD', 2996.9700, 2997.0000, N'00000000003', CAST(N'2017-01-15 15:29:31.563' AS DateTime), CAST(N'2017-01-15 15:30:10.063' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100000000000, N'USD', 0.0000, 95.7400, N'00000000004', CAST(N'2017-01-22 18:02:28.860' AS DateTime), CAST(N'2017-01-18 20:17:44.407' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (1000000000000, N'USD', 2.4900, 1.7500, N'00000000005', CAST(N'2017-01-23 10:23:09.180' AS DateTime), CAST(N'2017-01-23 15:15:59.980' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (10000000000000, N'USD', 0.0000, 91.1100, N'00000000006', CAST(N'2017-01-23 08:35:12.740' AS DateTime), CAST(N'2017-01-22 18:14:44.407' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100000000000000, N'USD', 0.0000, 100.0000, N'00000000007', CAST(N'2017-01-18 18:42:05.617' AS DateTime), CAST(N'2017-01-14 19:29:17.583' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100000000000001, N'USD', 10000.0000, 100.0000, N'', CAST(N'2017-01-14 19:35:33.767' AS DateTime), CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100000000000009, N'USD', 0.0000, 1164.9400, N'00000000009', CAST(N'2017-01-22 16:34:20.193' AS DateTime), CAST(N'2017-01-22 17:24:59.193' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100000000000010, N'USD', 1000.0000, 100.0000, N'00000000010', CAST(N'2017-01-14 19:35:33.767' AS DateTime), CAST(N'2017-01-14 22:28:24.217' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100000000000011, N'USD', 100.0000, 100.0000, N'00000000011', CAST(N'2017-01-14 19:35:33.767' AS DateTime), CAST(N'2017-01-14 19:38:51.997' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100000000000012, N'USD', 100.0000, 100.0000, N'00000000012', CAST(N'2017-01-14 19:35:33.767' AS DateTime), CAST(N'2017-01-14 19:38:56.903' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100000000000013, N'USD', 0.0000, 100.0000, N'00000000013', CAST(N'2017-01-22 17:54:24.487' AS DateTime), CAST(N'2017-01-14 19:39:16.670' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100000000000014, N'USD', 100.0000, 100.0000, N'00000000014', CAST(N'2017-01-14 19:35:33.767' AS DateTime), CAST(N'2017-01-14 19:39:16.200' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100000000000015, N'USD', 0.0000, 100.0000, N'00000000015', CAST(N'2017-01-22 17:43:22.757' AS DateTime), CAST(N'2017-01-22 17:55:37.737' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastedUpdateDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100000000000016, N'USD', 0.0000, 100.0000, N'00000000016', CAST(N'2017-01-18 10:10:46.187' AS DateTime), CAST(N'2017-01-16 10:07:38.640' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))


ALTER TABLE [dbo].[Announcement] ADD  CONSTRAINT [DF_Announcement_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Announcement] ADD  CONSTRAINT [DF_Announcement_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GameTransaction] ADD  CONSTRAINT [DF_GameTransaction_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GeneralTable] ADD  CONSTRAINT [DF_GeneralTable_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GroupPermission] ADD  CONSTRAINT [DF_GroupPermission_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[GroupPermission] ADD  CONSTRAINT [DF_GroupPermission_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Module] ADD  CONSTRAINT [DF_Module_Enable]  DEFAULT ((0)) FOR [Enable]
GO
ALTER TABLE [dbo].[Module] ADD  CONSTRAINT [DF_Module_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Permission] ADD  CONSTRAINT [DF_Permission_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Permission] ADD  CONSTRAINT [DF_Permission_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PersonBetSetting] ADD  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[PersonBetSetting] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PersonCreditSetting] ADD  CONSTRAINT [DF_PersonCreditSetting_UpdateDate]  DEFAULT (getdate()) FOR [UpdateDate]
GO
ALTER TABLE [dbo].[PersonCreditSetting] ADD  CONSTRAINT [DF_PersonCreditSetting_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PersonGroup] ADD  CONSTRAINT [DF_PersonGroup_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PersonPositionTaking] ADD  CONSTRAINT [DF_PersonPositionTaking_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[PersonPositionTaking] ADD  CONSTRAINT [DF_PersonPositionTaking_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PositionTaking] ADD  CONSTRAINT [DF_PositionTaking_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[PositionTaking] ADD  CONSTRAINT [DF_PositionTaking_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Role] ADD  CONSTRAINT [DF_Role_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Transaction] ADD  CONSTRAINT [DF_Transaction_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Transaction] ADD  CONSTRAINT [DF_Transaction_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[BetTypeItem]  WITH CHECK ADD  CONSTRAINT [FK_BetTypeItem_BetType] FOREIGN KEY([BetTypeID])
REFERENCES [dbo].[BetType] ([ID])
GO
ALTER TABLE [dbo].[BetTypeItem] CHECK CONSTRAINT [FK_BetTypeItem_BetType]
GO
ALTER TABLE [dbo].[Game]  WITH CHECK ADD  CONSTRAINT [FK_Game_GameCategory] FOREIGN KEY([GroupID])
REFERENCES [dbo].[GameCategory] ([ID])
GO
ALTER TABLE [dbo].[Game] CHECK CONSTRAINT [FK_Game_GameCategory]
GO
ALTER TABLE [dbo].[GameSession]  WITH CHECK ADD  CONSTRAINT [FK_GameSession_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[GameSession] CHECK CONSTRAINT [FK_GameSession_Person]
GO
ALTER TABLE [dbo].[GameSetting]  WITH CHECK ADD  CONSTRAINT [FK_GameSetting_Game] FOREIGN KEY([GameID])
REFERENCES [dbo].[Game] ([ID])
GO
ALTER TABLE [dbo].[GameSetting] CHECK CONSTRAINT [FK_GameSetting_Game]
GO
ALTER TABLE [dbo].[GameTransaction]  WITH CHECK ADD  CONSTRAINT [FK_GameTransaction_Game] FOREIGN KEY([GameID])
REFERENCES [dbo].[Game] ([ID])
GO
ALTER TABLE [dbo].[GameTransaction] CHECK CONSTRAINT [FK_GameTransaction_Game]
GO
ALTER TABLE [dbo].[GameTransaction]  WITH CHECK ADD  CONSTRAINT [FK_GameTransaction_GameSession] FOREIGN KEY([GameSessionID])
REFERENCES [dbo].[GameSession] ([ID])
GO
ALTER TABLE [dbo].[GameTransaction] CHECK CONSTRAINT [FK_GameTransaction_GameSession]
GO
ALTER TABLE [dbo].[Permission]  WITH CHECK ADD  CONSTRAINT [FK_Permission_GroupPermission] FOREIGN KEY([GroupPermissionID])
REFERENCES [dbo].[GroupPermission] ([ID])
GO
ALTER TABLE [dbo].[Permission] CHECK CONSTRAINT [FK_Permission_GroupPermission]
GO
ALTER TABLE [dbo].[Permission]  WITH CHECK ADD  CONSTRAINT [FK_Permission_Module] FOREIGN KEY([ModuleID])
REFERENCES [dbo].[Module] ([ID])
GO
ALTER TABLE [dbo].[Permission] CHECK CONSTRAINT [FK_Permission_Module]
GO
ALTER TABLE [dbo].[Permission]  WITH CHECK ADD  CONSTRAINT [FK_Permission_Role] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Role] ([ID])
GO
ALTER TABLE [dbo].[Permission] CHECK CONSTRAINT [FK_Permission_Role]
GO
ALTER TABLE [dbo].[PersonBetSetting]  WITH CHECK ADD  CONSTRAINT [FK_PersonBetSetting_BetTypeItem] FOREIGN KEY([BetTypeItemID])
REFERENCES [dbo].[BetTypeItem] ([ID])
GO
ALTER TABLE [dbo].[PersonBetSetting] CHECK CONSTRAINT [FK_PersonBetSetting_BetTypeItem]
GO
ALTER TABLE [dbo].[PersonBetSetting]  WITH CHECK ADD  CONSTRAINT [FK_PersonBetSetting_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO
ALTER TABLE [dbo].[PersonBetSetting] CHECK CONSTRAINT [FK_PersonBetSetting_Currency]
GO
ALTER TABLE [dbo].[PersonBetSetting]  WITH CHECK ADD  CONSTRAINT [FK_PersonBetSetting_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[PersonBetSetting] CHECK CONSTRAINT [FK_PersonBetSetting_Person]
GO
ALTER TABLE [dbo].[PersonCreditSetting]  WITH CHECK ADD  CONSTRAINT [FK_PersonCreditSetting_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO
ALTER TABLE [dbo].[PersonCreditSetting] CHECK CONSTRAINT [FK_PersonCreditSetting_Currency]
GO
ALTER TABLE [dbo].[PersonCreditSetting]  WITH CHECK ADD  CONSTRAINT [FK_PersonCreditSetting_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[PersonCreditSetting] CHECK CONSTRAINT [FK_PersonCreditSetting_Person]
GO
ALTER TABLE [dbo].[PersonGroup]  WITH CHECK ADD  CONSTRAINT [FK_PersonGroup_GroupPermission] FOREIGN KEY([GroupPermissionID])
REFERENCES [dbo].[GroupPermission] ([ID])
GO
ALTER TABLE [dbo].[PersonGroup] CHECK CONSTRAINT [FK_PersonGroup_GroupPermission]
GO
ALTER TABLE [dbo].[PersonGroup]  WITH CHECK ADD  CONSTRAINT [FK_PersonGroup_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[PersonGroup] CHECK CONSTRAINT [FK_PersonGroup_Person]
GO
ALTER TABLE [dbo].[PersonPositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_PersonPositionTaking_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[PersonPositionTaking] CHECK CONSTRAINT [FK_PersonPositionTaking_Person]
GO
ALTER TABLE [dbo].[PersonPositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_PersonPositionTaking_Transaction] FOREIGN KEY([TransactionID])
REFERENCES [dbo].[Transaction] ([ID])
GO
ALTER TABLE [dbo].[PersonPositionTaking] CHECK CONSTRAINT [FK_PersonPositionTaking_Transaction]
GO
ALTER TABLE [dbo].[PositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_PositionTaking_BetTypeItem] FOREIGN KEY([BetTypeItemID])
REFERENCES [dbo].[BetTypeItem] ([ID])
GO
ALTER TABLE [dbo].[PositionTaking] CHECK CONSTRAINT [FK_PositionTaking_BetTypeItem]
GO
ALTER TABLE [dbo].[PositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_PositionTaking_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO
ALTER TABLE [dbo].[PositionTaking] CHECK CONSTRAINT [FK_PositionTaking_Currency]
GO
ALTER TABLE [dbo].[PositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_PositionTaking_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[PositionTaking] CHECK CONSTRAINT [FK_PositionTaking_Person]
GO
ALTER TABLE [dbo].[Statement]  WITH CHECK ADD  CONSTRAINT [FK_Statement_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[Statement] CHECK CONSTRAINT [FK_Statement_Person]
GO

ALTER TABLE [dbo].[Statement]  WITH CHECK ADD  CONSTRAINT [FK_Statement_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO
ALTER TABLE [dbo].[Statement] CHECK CONSTRAINT [FK_Statement_Currency]
GO


ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_BetTypeItem] FOREIGN KEY([BetTypeItemID])
REFERENCES [dbo].[BetTypeItem] ([ID])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_BetTypeItem]
GO
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_GameSession] FOREIGN KEY([GameSessionID])
REFERENCES [dbo].[GameSession] ([ID])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_GameSession]
GO

ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO

ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Person]
GO

ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO

ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Currency]
GO

ALTER TABLE [dbo].[Transaction]  ADD  CONSTRAINT [UN_Transaction_Signature] UNIQUE([Signature])
GO



EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Broadcast; Announcement: on each person' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Announcement', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hot; New; CommingSoon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Game', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hot; New; CommingSoon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameSetting', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Admin; ShareHolder; SuperSenior; Senior; Master; Agent; Member; Robot' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Person', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Actived; Suspended; Disabled; Locked' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Person', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VN,CN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Person', @level2type=N'COLUMN',@level2name=N'CountryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DailyAutoReset; MaxWinLimit; MaxLossLimit; BetLimit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PersonBetSetting', @level2type=N'COLUMN',@level2name=N'SettingType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Auto; Upline; CurrentMax; CurrentMin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PositionTaking', @level2type=N'COLUMN',@level2name=N'Type'
GO
