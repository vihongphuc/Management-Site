alter table PersonBetSetting alter column Value decimal null;

alter table PersonBetSetting add UpdatedDate datetime default getdate() null;
alter table PersonBetSetting add UpdatedBy nvarchar(100) null;
alter table PersonBetSetting add CreatedDate datetime default getdate() null;
alter table PersonBetSetting add CreatedBy nvarchar(100) null;
