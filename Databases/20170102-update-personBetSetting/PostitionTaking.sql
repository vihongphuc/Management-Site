USE [ALT_Slot]
GO

drop TABLE [dbo].[PositionTaking];

/****** Object:  Table [dbo].[PositionTaking]    Script Date: 01/02/2017 23:44:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PositionTaking](
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[BetTypeItemID] [int] NOT NULL,
	[Type] [varchar](100) NOT NULL,
	[Value] [money] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_PositionTaking] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC,
	[CurrencyCode] ASC,
	[BetTypeItemID] ASC,
	[Type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Auto; Upline; CurrentMax; CurrentMin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PositionTaking', @level2type=N'COLUMN',@level2name=N'Type'
GO

ALTER TABLE [dbo].[PositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_PositionTaking_BetTypeItem] FOREIGN KEY([BetTypeItemID])
REFERENCES [dbo].[BetTypeItem] ([ID])
GO

ALTER TABLE [dbo].[PositionTaking] CHECK CONSTRAINT [FK_PositionTaking_BetTypeItem]
GO

ALTER TABLE [dbo].[PositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_PositionTaking_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO

ALTER TABLE [dbo].[PositionTaking] CHECK CONSTRAINT [FK_PositionTaking_Currency]
GO

ALTER TABLE [dbo].[PositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_PositionTaking_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO

ALTER TABLE [dbo].[PositionTaking] CHECK CONSTRAINT [FK_PositionTaking_Person]
GO

ALTER TABLE [dbo].[PositionTaking] ADD  CONSTRAINT [DF_PositionTaking_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO

ALTER TABLE [dbo].[PositionTaking] ADD  CONSTRAINT [DF_PositionTaking_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO


