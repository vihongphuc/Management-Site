USE [ALT_Slot]
GO
INSERT [Currency] ([CurrencyCode], [Name], [DisplayName], [Enable], [Rate]) VALUES (N'VND', N'Vietnam Dong', N'VND', 1, CAST(1 AS Decimal(18, 0)))
INSERT [Configuration] ([ConfigKey], [ConfigValue], [Detail], [CreatedBy], [CreateDate], [UpdatedBy], [UpdatedDate]) VALUES (N'Platform', N'[{Desktop:"Desktop"}]', N'Desktop', N'SQL - Admin', CAST(0x0000A6C301743B07 AS DateTime), N'SQL - Admin', CAST(0x0000A6C301743B07 AS DateTime))
INSERT [Configuration] ([ConfigKey], [ConfigValue], [Detail], [CreatedBy], [CreateDate], [UpdatedBy], [UpdatedDate]) VALUES (N'Publishers', N'[{Alt:"Asian Live Tech Company"}]', N'All Publishers', N'SQL - Admin', CAST(0x0000A6C30174362B AS DateTime), N'SQL - Admin', CAST(0x0000A6C30174362B AS DateTime))
SET IDENTITY_INSERT [GameCategory] ON
INSERT [GameCategory] ([ID], [Name], [Description], [ImageURL], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'Slot', N'Slot game', NULL, N'SQL - Admin', CAST(0x0000A6C3016FF2FF AS DateTime), N'SQL - Admin', CAST(0x0000A6C3016FF2FF AS DateTime))
INSERT [GameCategory] ([ID], [Name], [Description], [ImageURL], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, N'Table', N'Table Game', NULL, N'SQL - Admin', CAST(0x0000A6C301700FE5 AS DateTime), N'SQL - Admin', CAST(0x0000A6C301700FE5 AS DateTime))
SET IDENTITY_INSERT [GameCategory] OFF
SET IDENTITY_INSERT [Game] ON
INSERT [Game] ([ID], [Code], [Name], [GroupID], [Type], [Enable], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (1, N'Sanguo', N'Sang Guo', 1, NULL, 1, N'SQL - Admin', CAST(0x0000A6C301734A70 AS DateTime), N'SQL - Admin', CAST(0x0000A6C301734A70 AS DateTime))
SET IDENTITY_INSERT [Game] OFF
INSERT [GameSetting] ([GameID], [Publisher], [Platform], [DownloadURL], [GameHash], [GameKey], [GameHearder], [Enable], [Type], [Width], [Height], [ImageURL], [UpdateBy], [UpdateDate], [CreatedBy], [CreatedDate]) VALUES (1, N'Alt', N'Desktop', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, N'SQL - Admin', CAST(0x0000A6C301737C8F AS DateTime), N'SQL - Admin', CAST(0x0000A6C301737C8F AS DateTime))
