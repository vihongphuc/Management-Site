  -- drop database [ALT_Slot];

if db_id('ALT_Slot') is null
 create database [ALT_Slot];


USE [ALT_Slot]
GO
/****** Object:  ForeignKey [FK_BetTypeItem_BetType]    Script Date: 11/19/2016 22:54:42 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BetTypeItem_BetType]') AND parent_object_id = OBJECT_ID(N'[dbo].[BetTypeItem]'))
ALTER TABLE [dbo].[BetTypeItem] DROP CONSTRAINT [FK_BetTypeItem_BetType]
GO
/****** Object:  ForeignKey [FK_Game_GameCategory]    Script Date: 11/19/2016 22:54:43 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Game_GameCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[Game]'))
ALTER TABLE [dbo].[Game] DROP CONSTRAINT [FK_Game_GameCategory]
GO
/****** Object:  ForeignKey [FK_GameSession_Person]    Script Date: 11/19/2016 22:54:43 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GameSession_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[GameSession]'))
ALTER TABLE [dbo].[GameSession] DROP CONSTRAINT [FK_GameSession_Person]
GO
/****** Object:  ForeignKey [FK_GameSetting_Game]    Script Date: 11/19/2016 22:54:44 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GameSetting_Game]') AND parent_object_id = OBJECT_ID(N'[dbo].[GameSetting]'))
ALTER TABLE [dbo].[GameSetting] DROP CONSTRAINT [FK_GameSetting_Game]
GO
/****** Object:  ForeignKey [FK_GameTransaction_Game]    Script Date: 11/19/2016 22:54:44 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GameTransaction_Game]') AND parent_object_id = OBJECT_ID(N'[dbo].[GameTransaction]'))
ALTER TABLE [dbo].[GameTransaction] DROP CONSTRAINT [FK_GameTransaction_Game]
GO
/****** Object:  ForeignKey [FK_GameTransaction_GameSession]    Script Date: 11/19/2016 22:54:44 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GameTransaction_GameSession]') AND parent_object_id = OBJECT_ID(N'[dbo].[GameTransaction]'))
ALTER TABLE [dbo].[GameTransaction] DROP CONSTRAINT [FK_GameTransaction_GameSession]
GO
/****** Object:  ForeignKey [FK_Permission_GroupPermission]    Script Date: 11/19/2016 22:54:44 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Permission_GroupPermission]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permission]'))
ALTER TABLE [dbo].[Permission] DROP CONSTRAINT [FK_Permission_GroupPermission]
GO
/****** Object:  ForeignKey [FK_Permission_Module]    Script Date: 11/19/2016 22:54:44 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Permission_Module]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permission]'))
ALTER TABLE [dbo].[Permission] DROP CONSTRAINT [FK_Permission_Module]
GO
/****** Object:  ForeignKey [FK_Permission_Role]    Script Date: 11/19/2016 22:54:44 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Permission_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permission]'))
ALTER TABLE [dbo].[Permission] DROP CONSTRAINT [FK_Permission_Role]
GO
/****** Object:  ForeignKey [FK_PersonBetSetting_BetTypeItem]    Script Date: 11/19/2016 22:54:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonBetSetting_BetTypeItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonBetSetting]'))
ALTER TABLE [dbo].[PersonBetSetting] DROP CONSTRAINT [FK_PersonBetSetting_BetTypeItem]
GO
/****** Object:  ForeignKey [FK_PersonBetSetting_Currency]    Script Date: 11/19/2016 22:54:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonBetSetting_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonBetSetting]'))
ALTER TABLE [dbo].[PersonBetSetting] DROP CONSTRAINT [FK_PersonBetSetting_Currency]
GO
/****** Object:  ForeignKey [FK_PersonBetSetting_Person]    Script Date: 11/19/2016 22:54:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonBetSetting_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonBetSetting]'))
ALTER TABLE [dbo].[PersonBetSetting] DROP CONSTRAINT [FK_PersonBetSetting_Person]
GO
/****** Object:  ForeignKey [FK_PersonCreditSetting_Currency]    Script Date: 11/19/2016 22:54:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonCreditSetting_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonCreditSetting]'))
ALTER TABLE [dbo].[PersonCreditSetting] DROP CONSTRAINT [FK_PersonCreditSetting_Currency]
GO
/****** Object:  ForeignKey [FK_PersonCreditSetting_Person]    Script Date: 11/19/2016 22:54:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonCreditSetting_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonCreditSetting]'))
ALTER TABLE [dbo].[PersonCreditSetting] DROP CONSTRAINT [FK_PersonCreditSetting_Person]
GO
/****** Object:  ForeignKey [FK_PersonGroup_GroupPermission]    Script Date: 11/19/2016 22:54:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonGroup_GroupPermission]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonGroup]'))
ALTER TABLE [dbo].[PersonGroup] DROP CONSTRAINT [FK_PersonGroup_GroupPermission]
GO
/****** Object:  ForeignKey [FK_PersonGroup_Person]    Script Date: 11/19/2016 22:54:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonGroup_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonGroup]'))
ALTER TABLE [dbo].[PersonGroup] DROP CONSTRAINT [FK_PersonGroup_Person]
GO
/****** Object:  ForeignKey [FK_PersonPositionTaking_Person]    Script Date: 11/19/2016 22:54:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonPositionTaking_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonPositionTaking]'))
ALTER TABLE [dbo].[PersonPositionTaking] DROP CONSTRAINT [FK_PersonPositionTaking_Person]
GO
/****** Object:  ForeignKey [FK_PersonPositionTaking_Transaction]    Script Date: 11/19/2016 22:54:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonPositionTaking_Transaction]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonPositionTaking]'))
ALTER TABLE [dbo].[PersonPositionTaking] DROP CONSTRAINT [FK_PersonPositionTaking_Transaction]
GO
/****** Object:  ForeignKey [FK_PositionTaking_BetTypeItem]    Script Date: 11/19/2016 22:54:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PositionTaking_BetTypeItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[PositionTaking]'))
ALTER TABLE [dbo].[PositionTaking] DROP CONSTRAINT [FK_PositionTaking_BetTypeItem]
GO
/****** Object:  ForeignKey [FK_PositionTaking_Currency]    Script Date: 11/19/2016 22:54:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PositionTaking_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[PositionTaking]'))
ALTER TABLE [dbo].[PositionTaking] DROP CONSTRAINT [FK_PositionTaking_Currency]
GO
/****** Object:  ForeignKey [FK_PositionTaking_Person]    Script Date: 11/19/2016 22:54:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PositionTaking_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PositionTaking]'))
ALTER TABLE [dbo].[PositionTaking] DROP CONSTRAINT [FK_PositionTaking_Person]
GO
/****** Object:  ForeignKey [FK_Statement_Person]    Script Date: 11/19/2016 22:54:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Statement_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [FK_Statement_Person]
GO
/****** Object:  ForeignKey [FK_Transaction_BetTypeItem]    Script Date: 11/19/2016 22:54:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaction_BetTypeItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaction]'))
ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Transaction_BetTypeItem]
GO
/****** Object:  ForeignKey [FK_Transaction_GameSession]    Script Date: 11/19/2016 22:54:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaction_GameSession]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaction]'))
ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Transaction_GameSession]
GO
/****** Object:  Table [dbo].[PersonPositionTaking]    Script Date: 11/19/2016 22:54:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonPositionTaking_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonPositionTaking]'))
ALTER TABLE [dbo].[PersonPositionTaking] DROP CONSTRAINT [FK_PersonPositionTaking_Person]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonPositionTaking_Transaction]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonPositionTaking]'))
ALTER TABLE [dbo].[PersonPositionTaking] DROP CONSTRAINT [FK_PersonPositionTaking_Transaction]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PersonPositionTaking_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PersonPositionTaking] DROP CONSTRAINT [DF_PersonPositionTaking_UpdatedDate]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PersonPositionTaking_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PersonPositionTaking] DROP CONSTRAINT [DF_PersonPositionTaking_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonPositionTaking]') AND type in (N'U'))
DROP TABLE [dbo].[PersonPositionTaking]
GO
/****** Object:  Table [dbo].[PositionTaking]    Script Date: 11/19/2016 22:54:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PositionTaking_BetTypeItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[PositionTaking]'))
ALTER TABLE [dbo].[PositionTaking] DROP CONSTRAINT [FK_PositionTaking_BetTypeItem]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PositionTaking_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[PositionTaking]'))
ALTER TABLE [dbo].[PositionTaking] DROP CONSTRAINT [FK_PositionTaking_Currency]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PositionTaking_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PositionTaking]'))
ALTER TABLE [dbo].[PositionTaking] DROP CONSTRAINT [FK_PositionTaking_Person]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PositionTaking_UpdateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PositionTaking] DROP CONSTRAINT [DF_PositionTaking_UpdateDate]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PositionTaking_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PositionTaking] DROP CONSTRAINT [DF_PositionTaking_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PositionTaking]') AND type in (N'U'))
DROP TABLE [dbo].[PositionTaking]
GO
/****** Object:  Table [dbo].[PersonBetSetting]    Script Date: 11/19/2016 22:54:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonBetSetting_BetTypeItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonBetSetting]'))
ALTER TABLE [dbo].[PersonBetSetting] DROP CONSTRAINT [FK_PersonBetSetting_BetTypeItem]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonBetSetting_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonBetSetting]'))
ALTER TABLE [dbo].[PersonBetSetting] DROP CONSTRAINT [FK_PersonBetSetting_Currency]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonBetSetting_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonBetSetting]'))
ALTER TABLE [dbo].[PersonBetSetting] DROP CONSTRAINT [FK_PersonBetSetting_Person]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonBetSetting]') AND type in (N'U'))
DROP TABLE [dbo].[PersonBetSetting]
GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 11/19/2016 22:54:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaction_BetTypeItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaction]'))
ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Transaction_BetTypeItem]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaction_GameSession]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaction]'))
ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Transaction_GameSession]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Transaction_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [DF_Transaction_UpdatedDate]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Transaction_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [DF_Transaction_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Transaction]') AND type in (N'U'))
DROP TABLE [dbo].[Transaction]
GO
/****** Object:  Table [dbo].[GameSetting]    Script Date: 11/19/2016 22:54:44 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GameSetting_Game]') AND parent_object_id = OBJECT_ID(N'[dbo].[GameSetting]'))
ALTER TABLE [dbo].[GameSetting] DROP CONSTRAINT [FK_GameSetting_Game]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GameSetting_UpdateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GameSetting] DROP CONSTRAINT [DF_GameSetting_UpdateDate]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GameSetting_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GameSetting] DROP CONSTRAINT [DF_GameSetting_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GameSetting]') AND type in (N'U'))
DROP TABLE [dbo].[GameSetting]
GO
/****** Object:  Table [dbo].[GameTransaction]    Script Date: 11/19/2016 22:54:44 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GameTransaction_Game]') AND parent_object_id = OBJECT_ID(N'[dbo].[GameTransaction]'))
ALTER TABLE [dbo].[GameTransaction] DROP CONSTRAINT [FK_GameTransaction_Game]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GameTransaction_GameSession]') AND parent_object_id = OBJECT_ID(N'[dbo].[GameTransaction]'))
ALTER TABLE [dbo].[GameTransaction] DROP CONSTRAINT [FK_GameTransaction_GameSession]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GameTransaction_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GameTransaction] DROP CONSTRAINT [DF_GameTransaction_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GameTransaction]') AND type in (N'U'))
DROP TABLE [dbo].[GameTransaction]
GO
/****** Object:  Table [dbo].[Permission]    Script Date: 11/19/2016 22:54:44 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Permission_GroupPermission]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permission]'))
ALTER TABLE [dbo].[Permission] DROP CONSTRAINT [FK_Permission_GroupPermission]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Permission_Module]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permission]'))
ALTER TABLE [dbo].[Permission] DROP CONSTRAINT [FK_Permission_Module]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Permission_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permission]'))
ALTER TABLE [dbo].[Permission] DROP CONSTRAINT [FK_Permission_Role]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Permission_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Permission] DROP CONSTRAINT [DF_Permission_UpdatedDate]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Permission_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Permission] DROP CONSTRAINT [DF_Permission_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Permission]') AND type in (N'U'))
DROP TABLE [dbo].[Permission]
GO
/****** Object:  Table [dbo].[BetTypeItem]    Script Date: 11/19/2016 22:54:42 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BetTypeItem_BetType]') AND parent_object_id = OBJECT_ID(N'[dbo].[BetTypeItem]'))
ALTER TABLE [dbo].[BetTypeItem] DROP CONSTRAINT [FK_BetTypeItem_BetType]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BetTypeItem_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BetTypeItem] DROP CONSTRAINT [DF_BetTypeItem_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BetTypeItem]') AND type in (N'U'))
DROP TABLE [dbo].[BetTypeItem]
GO
/****** Object:  Table [dbo].[Game]    Script Date: 11/19/2016 22:54:43 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Game_GameCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[Game]'))
ALTER TABLE [dbo].[Game] DROP CONSTRAINT [FK_Game_GameCategory]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Game_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Game] DROP CONSTRAINT [DF_Game_UpdatedDate]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Game_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Game] DROP CONSTRAINT [DF_Game_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Game]') AND type in (N'U'))
DROP TABLE [dbo].[Game]
GO
/****** Object:  Table [dbo].[Statement]    Script Date: 11/19/2016 22:54:47 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Statement_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [FK_Statement_Person]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Statement_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Statement] DROP CONSTRAINT [DF_Statement_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Statement]') AND type in (N'U'))
DROP TABLE [dbo].[Statement]
GO
/****** Object:  Table [dbo].[PersonCreditSetting]    Script Date: 11/19/2016 22:54:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonCreditSetting_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonCreditSetting]'))
ALTER TABLE [dbo].[PersonCreditSetting] DROP CONSTRAINT [FK_PersonCreditSetting_Currency]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonCreditSetting_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonCreditSetting]'))
ALTER TABLE [dbo].[PersonCreditSetting] DROP CONSTRAINT [FK_PersonCreditSetting_Person]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PersonCreditSetting_UpdateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PersonCreditSetting] DROP CONSTRAINT [DF_PersonCreditSetting_UpdateDate]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PersonCreditSetting_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PersonCreditSetting] DROP CONSTRAINT [DF_PersonCreditSetting_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonCreditSetting]') AND type in (N'U'))
DROP TABLE [dbo].[PersonCreditSetting]
GO
/****** Object:  Table [dbo].[PersonGroup]    Script Date: 11/19/2016 22:54:45 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonGroup_GroupPermission]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonGroup]'))
ALTER TABLE [dbo].[PersonGroup] DROP CONSTRAINT [FK_PersonGroup_GroupPermission]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonGroup_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonGroup]'))
ALTER TABLE [dbo].[PersonGroup] DROP CONSTRAINT [FK_PersonGroup_Person]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PersonGroup_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PersonGroup] DROP CONSTRAINT [DF_PersonGroup_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonGroup]') AND type in (N'U'))
DROP TABLE [dbo].[PersonGroup]
GO
/****** Object:  Table [dbo].[GameSession]    Script Date: 11/19/2016 22:54:43 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GameSession_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[GameSession]'))
ALTER TABLE [dbo].[GameSession] DROP CONSTRAINT [FK_GameSession_Person]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GameSession_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GameSession] DROP CONSTRAINT [DF_GameSession_UpdatedDate]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GameSession_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GameSession] DROP CONSTRAINT [DF_GameSession_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GameSession]') AND type in (N'U'))
DROP TABLE [dbo].[GameSession]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 11/19/2016 22:54:46 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Role_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Role] DROP CONSTRAINT [DF_Role_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Role]') AND type in (N'U'))
DROP TABLE [dbo].[Role]
GO
/****** Object:  Table [dbo].[PersonUpline]    Script Date: 11/19/2016 22:54:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonUpline]') AND type in (N'U'))
DROP TABLE [dbo].[PersonUpline]
GO
/****** Object:  Table [dbo].[GameCategory]    Script Date: 11/19/2016 22:54:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GameCategory_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GameCategory] DROP CONSTRAINT [DF_GameCategory_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GameCategory_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GameCategory] DROP CONSTRAINT [DF_GameCategory_UpdatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GameCategory]') AND type in (N'U'))
DROP TABLE [dbo].[GameCategory]
GO
/****** Object:  Table [dbo].[Announcement]    Script Date: 11/19/2016 22:54:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Announcement_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Announcement] DROP CONSTRAINT [DF_Announcement_UpdatedDate]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Announcement_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Announcement] DROP CONSTRAINT [DF_Announcement_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Announcement]') AND type in (N'U'))
DROP TABLE [dbo].[Announcement]
GO
/****** Object:  Table [dbo].[Audit]    Script Date: 11/19/2016 22:54:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Audit_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Audit] DROP CONSTRAINT [DF_Audit_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Audit]') AND type in (N'U'))
DROP TABLE [dbo].[Audit]
GO
/****** Object:  Table [dbo].[BetType]    Script Date: 11/19/2016 22:54:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BetType_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BetType] DROP CONSTRAINT [DF_BetType_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BetType]') AND type in (N'U'))
DROP TABLE [dbo].[BetType]
GO
/****** Object:  Table [dbo].[Configuration]    Script Date: 11/19/2016 22:54:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuration_CreateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuration] DROP CONSTRAINT [DF_Configuration_CreateDate]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuration_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuration] DROP CONSTRAINT [DF_Configuration_UpdatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Configuration]') AND type in (N'U'))
DROP TABLE [dbo].[Configuration]
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 11/19/2016 22:54:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Currency_Enable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Currency] DROP CONSTRAINT [DF_Currency_Enable]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Currency_Rate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Currency] DROP CONSTRAINT [DF_Currency_Rate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Currency]') AND type in (N'U'))
DROP TABLE [dbo].[Currency]
GO
/****** Object:  Table [dbo].[Person]    Script Date: 11/19/2016 22:54:45 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Person_UpdateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Person] DROP CONSTRAINT [DF_Person_UpdateDate]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Person_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Person] DROP CONSTRAINT [DF_Person_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Person]') AND type in (N'U'))
DROP TABLE [dbo].[Person]
GO
/****** Object:  Table [dbo].[PersonBalance]    Script Date: 11/19/2016 22:54:45 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PersonBalance_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PersonBalance] DROP CONSTRAINT [DF_PersonBalance_UpdatedDate]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PersonBalance_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PersonBalance] DROP CONSTRAINT [DF_PersonBalance_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonBalance]') AND type in (N'U'))
DROP TABLE [dbo].[PersonBalance]
GO
/****** Object:  Table [dbo].[GeneralTable]    Script Date: 11/19/2016 22:54:44 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GeneralTable_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GeneralTable] DROP CONSTRAINT [DF_GeneralTable_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeneralTable]') AND type in (N'U'))
DROP TABLE [dbo].[GeneralTable]
GO
/****** Object:  Table [dbo].[GroupPermission]    Script Date: 11/19/2016 22:54:44 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GroupPermission_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GroupPermission] DROP CONSTRAINT [DF_GroupPermission_UpdatedDate]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GroupPermission_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GroupPermission] DROP CONSTRAINT [DF_GroupPermission_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GroupPermission]') AND type in (N'U'))
DROP TABLE [dbo].[GroupPermission]
GO
/****** Object:  Table [dbo].[Module]    Script Date: 11/19/2016 22:54:44 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Module_Enable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Module] DROP CONSTRAINT [DF_Module_Enable]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Module_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Module] DROP CONSTRAINT [DF_Module_CreatedDate]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Module]') AND type in (N'U'))
DROP TABLE [dbo].[Module]
GO
/****** Object:  Table [dbo].[Module]    Script Date: 11/19/2016 22:54:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Module]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Module](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[DisplayName] [nvarchar](200) NOT NULL,
	[Enable] [bit] NULL CONSTRAINT [DF_Module_Enable]  DEFAULT ((0)),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Module_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Module] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GroupPermission]    Script Date: 11/19/2016 22:54:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GroupPermission]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GroupPermission](
	[ID] [int] NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_GroupPermission_UpdatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_GroupPermission_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_GroupPermission] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GeneralTable]    Script Date: 11/19/2016 22:54:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeneralTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GeneralTable](
	[Id] [int] NOT NULL,
	[TableName] [varchar](100) NOT NULL,
	[FeildName] [varchar](100) NOT NULL,
	[Key] [varchar](50) NULL,
	[Value] [varchar](50) NULL,
	[Setting] [nvarchar](500) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_GeneralTable_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_GeneralTable] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonBalance]    Script Date: 11/19/2016 22:54:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonBalance]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PersonBalance](
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[Available] [money] NOT NULL,
	[YesterdayAvailable] [money] NOT NULL,
	[UpdatedBy] [nvarchar](100) NULL,
	[LastedUpdateDate] [datetime] NULL CONSTRAINT [DF_PersonBalance_LastedUpdateDate]  DEFAULT (getdate()),
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_PersonBalance_UpdatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_PersonBalance_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_PersonBalance] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC,
	[CurrencyCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Person]    Script Date: 11/19/2016 22:54:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Person]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Person](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](200) NOT NULL,
	[Firstname] [nvarchar](200) NULL,
	[Lastname] [nvarchar](200) NULL,
	[Password] [varchar](200) NOT NULL,
	[DOB] [date] NULL,
	[Address] [nvarchar](200) NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Status] [varchar](10) NULL,
	[CountryCode] [varchar](50) NULL,
	[CurrencyCodes] [varchar](200) NULL,
	[Mobile] [varchar](50) NULL,
	[Phone] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[LastLoginTime] [datetime] NULL,
	[LastLoginIP] [varchar](50) NULL,
	[IsExternal] [bit] NULL,
	[AutoTransfer] [bit] NULL,
	[LimitIP] [varchar](100) NULL,
	[DisplayName] [varchar](2000) NULL,
	[GroupID] [int] NULL,
	[UplineID] [bigint] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdateDate] [datetime] NULL CONSTRAINT [DF_Person_UpdateDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_Person_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Person', N'COLUMN',N'Type'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Admin; ShareHolder; SuperSenior; Senior; Master; Agent; Member; Robot' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Person', @level2type=N'COLUMN',@level2name=N'Type'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Person', N'COLUMN',N'Status'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Actived; Suspended; Disabled; Locked' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Person', @level2type=N'COLUMN',@level2name=N'Status'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Person', N'COLUMN',N'CountryCode'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VN,CN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Person', @level2type=N'COLUMN',@level2name=N'CountryCode'
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 11/19/2016 22:54:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Currency]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Currency](
	[CurrencyCode] [varchar](10) NOT NULL,
	[Name] [varchar](50) NULL,
	[DisplayName] [varchar](200) NULL,
	[Enable] [bit] NOT NULL CONSTRAINT [DF_Currency_Enable]  DEFAULT ((0)),
	[Rate] [money] NOT NULL CONSTRAINT [DF_Currency_Rate]  DEFAULT ((1)),
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[CurrencyCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Configuration]    Script Date: 11/19/2016 22:54:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Configuration]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Configuration](
	[ConfigKey] [varchar](100) NOT NULL,
	[ConfigValue] [text] NULL,
	[Detail] [text] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreateDate] [datetime] NULL CONSTRAINT [DF_Configuration_CreateDate]  DEFAULT (getdate()),
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_Configuration_UpdatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Configuration] PRIMARY KEY CLUSTERED 
(
	[ConfigKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BetType]    Script Date: 11/19/2016 22:54:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BetType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[BetType](
	[ID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_BetType_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_BetType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Audit]    Script Date: 11/19/2016 22:54:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Audit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Audit](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Time] [datetime] NULL CONSTRAINT [DF_Audit_Time]  DEFAULT (getdate()),	
	[Subject] [varchar](100) NULL,
	[PersonID] [bigint] NOT NULL,
	[RelatedID] [bigint] NULL,
	[Details] [nvarchar](500) NULL,
	[Before] [nvarchar](500) NULL,
	[After] [nvarchar](500) NULL,
	[Type] [varchar](100) NULL,
	[Token] [varchar](100) NULL,
	[IPAddress] [varchar](100) NULL,
	[IPLocation] [varchar](100) NULL,
	[ConsumerIPAddress] [varchar](100) NULL,
	[ConsumerIPLocation] [varchar](100) NULL,
	[ControllerName] [varchar](100) NULL,
	[ActionName] [varchar](100) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Audit_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Audit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Announcement]    Script Date: 11/19/2016 22:54:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Announcement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Announcement](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NULL,
	[Type] [varchar](50) NULL,
	[Title] [nvarchar](200) NULL,
	[Value] [text] NULL,
	[LanguageCode] [varchar](10) NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_Announcement_UpdatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Announcement_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Announcement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Announcement', N'COLUMN',N'Type'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Broadcast; Announcement: on each person' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Announcement', @level2type=N'COLUMN',@level2name=N'Type'
GO
/****** Object:  Table [dbo].[GameCategory]    Script Date: 11/19/2016 22:54:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GameCategory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GameCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[ImageURL] [nvarchar](200) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_GameCategory_CreatedDate]  DEFAULT (getdate()),
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_GameCategory_UpdatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_GameCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonUpline]    Script Date: 11/19/2016 22:54:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonUpline]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PersonUpline](
	[PersonID] [bigint] NOT NULL,
	[AdminID] [bigint] NULL,
	[ShareHolderID] [bigint] NULL,
	[SupperSeniorID] [bigint] NULL,
	[SeniorID] [bigint] NULL,
	[MasterID] [bigint] NULL,
	[AgentID] [bigint] NULL,
 CONSTRAINT [PK_PersonUpline] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Role]    Script Date: 11/19/2016 22:54:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Role]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Role](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [date] NULL CONSTRAINT [DF_Role_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GameSession]    Script Date: 11/19/2016 22:54:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GameSession]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GameSession](
	[ID] [bigint] NOT NULL,
	[TransferAmount] [money] NOT NULL,
	[FinishedTime] [datetime] NULL,
	[FinishedTimeTicks] [bigint] NULL,
	[PersonID] [bigint] NOT NULL,
	[TotalBet] [money] NULL,
	[TotalResult] [money] NULL,
	[Ack] [datetime] NULL,
	[AckTicks] [datetime] NULL,
	[Platform] [varchar](100) NULL,
	[IpAddress] [varchar](100) NULL,
	[Data] [nvarchar](100) NULL,
	[Publisher] [varchar](100) NULL,
	[CurrencyCode] [varchar](50) NULL,
	[PointRate] [money] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_GameSession_UpdatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_GameSession_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_GameSession] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonGroup]    Script Date: 11/19/2016 22:54:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonGroup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PersonGroup](
	[PersonID] [bigint] NOT NULL,
	[GroupPermissionID] [int] NOT NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_PersonGroup_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_PersonGroup] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC,
	[GroupPermissionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonCreditSetting]    Script Date: 11/19/2016 22:54:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonCreditSetting]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PersonCreditSetting](
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[PersonType] [varchar](50) NOT NULL,
	[MaxCredit] [money] NOT NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdateDate] [datetime] NULL CONSTRAINT [DF_PersonCreditSetting_UpdateDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_PersonCreditSetting_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_PersonCreditSetting] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC,
	[CurrencyCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Statement]    Script Date: 11/19/2016 22:54:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Statement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Statement](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[RelatedPersonID] [bigint] NOT NULL,
	[Type] [varchar](100) NOT NULL,
	[Time] [datetime] NOT NULL,
	[Ticks] bigint not null,
	[Amount] [money] NOT NULL,
	[Detail] [nvarchar](500) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Statement_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Statement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Game]    Script Date: 11/19/2016 22:54:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Game]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Game](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](100) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[GroupID] [int] NOT NULL,
	[Type] [varchar](50) NULL,
	[Enable] [bit] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_Game_UpdatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Game_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Game] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Game', N'COLUMN',N'Type'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hot; New; CommingSoon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Game', @level2type=N'COLUMN',@level2name=N'Type'
GO
/****** Object:  Table [dbo].[BetTypeItem]    Script Date: 11/19/2016 22:54:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BetTypeItem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[BetTypeItem](
	[ID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[BetTypeID] [int] NOT NULL,
	[Description] [nvarchar](200) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_BetTypeItem_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_BetTypeItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Permission]    Script Date: 11/19/2016 22:54:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Permission]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Permission](
	[GroupPermissionID] [int] NOT NULL,
	[ModuleID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[Checked] [bit] NOT NULL,
	[Description] [nvarchar](200) NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_Permission_UpdatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Permission_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[GroupPermissionID] ASC,
	[ModuleID] ASC,
	[RoleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GameTransaction]    Script Date: 11/19/2016 22:54:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GameTransaction]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GameTransaction](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[GameSessionID] [bigint] NOT NULL,
	[GameID] [int] NOT NULL,
	[TransactionID] [bigint] NOT NULL,
	[Amount] [money] NOT NULL,
	[Result] [money] NOT NULL,
	[StartBalance] [money] NOT NULL,
	[EndBalance] [money] NOT NULL,
	[Time] [datetime] NULL,
	[Ticks] [datetime] null,
	[Detail] [text] NULL,
	[Type] [varchar](100) NULL,
	[BetTypeItemID] [int] NULL,
	[Description] [nvarchar](500) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_GameTransaction_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_GameTransaction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GameSetting]    Script Date: 11/19/2016 22:54:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GameSetting]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GameSetting](
	[GameID] [int] NOT NULL,
	[Publisher] [varchar](100) NOT NULL,
	[Platform] [varchar](100) NOT NULL,
	[DownloadURL] [varchar](500) NULL,
	[GameHash] [varchar](500) NULL,
	[GameKey] [varchar](500) NULL,
	[GameHearder] [varchar](500) NULL,
	[Enable] [bit] NULL,
	[IsHot] [bit] NULL,
	[IsNew] [bit] NULL,
	[Rate] [varchar](50) NULL,	
	[Type] [varchar](50) NULL,
	[Width] [money] NULL,
	[Height] [money] NULL,
	[ImageURL] [varchar](200) NULL,
	[UpdateBy] [varchar](100) NULL,
	[UpdateDate] [datetime] NULL CONSTRAINT [DF_GameSetting_UpdateDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_GameSetting_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_GameSetting] PRIMARY KEY CLUSTERED 
(
	[GameID] ASC,
	[Publisher] ASC,
	[Platform] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'GameSetting', N'COLUMN',N'Type'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hot; New; CommingSoon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameSetting', @level2type=N'COLUMN',@level2name=N'Type'
GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 11/19/2016 22:54:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Transaction]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Transaction](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GameSessionID] [bigint] NOT NULL,
	[Amount] [money] NOT NULL,
	[Result] [money] NOT NULL,
	[Type] [varchar](50) NULL,
	[Description] [nvarchar](500) NULL,
	[BetTypeItemID] [int] NOT NULL,
	[UpdateBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_Transaction_UpdatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Transaction_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonBetSetting]    Script Date: 11/19/2016 22:54:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonBetSetting]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PersonBetSetting](
	[PersonID] [bigint] NOT NULL,
	[BetTypeItemID] [int] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[SettingType] [varchar](100) NOT NULL,
	[Value] [varchar](50) NULL,
	[ValueString] [varchar](50) NULL,
 CONSTRAINT [PK_PersonBetSetting] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC,
	[BetTypeItemID] ASC,
	[CurrencyCode] ASC,
	[SettingType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'PersonBetSetting', N'COLUMN',N'SettingType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DailyAutoReset; MaxWinLimit; MaxLossLimit; BetLimit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PersonBetSetting', @level2type=N'COLUMN',@level2name=N'SettingType'
GO
/****** Object:  Table [dbo].[PositionTaking]    Script Date: 11/19/2016 22:54:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PositionTaking]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PositionTaking](
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[BetTypeID] [int] NOT NULL,
	[Type] [varchar](100) NOT NULL,
	[Value] [money] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdateDate] [datetime] NULL CONSTRAINT [DF_PositionTaking_UpdateDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_PositionTaking_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_PositionTaking] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC,
	[CurrencyCode] ASC,
	[BetTypeID] ASC,
	[Type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'PositionTaking', N'COLUMN',N'Type'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Auto; Upline; CurrentMax; CurrentMin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PositionTaking', @level2type=N'COLUMN',@level2name=N'Type'
GO
/****** Object:  Table [dbo].[PersonPositionTaking]    Script Date: 11/19/2016 22:54:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonPositionTaking]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PersonPositionTaking](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TransactionID] [bigint] NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[Rate] [money] NULL,
	[Amount] [money] NULL,
	[UpdateBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL CONSTRAINT [DF_PersonPositionTaking_UpdatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_PersonPositionTaking_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_PersonPositionTaking] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK_BetTypeItem_BetType]    Script Date: 11/19/2016 22:54:42 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BetTypeItem_BetType]') AND parent_object_id = OBJECT_ID(N'[dbo].[BetTypeItem]'))
ALTER TABLE [dbo].[BetTypeItem]  WITH CHECK ADD  CONSTRAINT [FK_BetTypeItem_BetType] FOREIGN KEY([BetTypeID])
REFERENCES [dbo].[BetType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BetTypeItem_BetType]') AND parent_object_id = OBJECT_ID(N'[dbo].[BetTypeItem]'))
ALTER TABLE [dbo].[BetTypeItem] CHECK CONSTRAINT [FK_BetTypeItem_BetType]
GO
/****** Object:  ForeignKey [FK_Game_GameCategory]    Script Date: 11/19/2016 22:54:43 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Game_GameCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[Game]'))
ALTER TABLE [dbo].[Game]  WITH CHECK ADD  CONSTRAINT [FK_Game_GameCategory] FOREIGN KEY([GroupID])
REFERENCES [dbo].[GameCategory] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Game_GameCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[Game]'))
ALTER TABLE [dbo].[Game] CHECK CONSTRAINT [FK_Game_GameCategory]
GO
/****** Object:  ForeignKey [FK_GameSession_Person]    Script Date: 11/19/2016 22:54:43 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GameSession_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[GameSession]'))
ALTER TABLE [dbo].[GameSession]  WITH CHECK ADD  CONSTRAINT [FK_GameSession_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GameSession_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[GameSession]'))
ALTER TABLE [dbo].[GameSession] CHECK CONSTRAINT [FK_GameSession_Person]
GO
/****** Object:  ForeignKey [FK_GameSetting_Game]    Script Date: 11/19/2016 22:54:44 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GameSetting_Game]') AND parent_object_id = OBJECT_ID(N'[dbo].[GameSetting]'))
ALTER TABLE [dbo].[GameSetting]  WITH CHECK ADD  CONSTRAINT [FK_GameSetting_Game] FOREIGN KEY([GameID])
REFERENCES [dbo].[Game] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GameSetting_Game]') AND parent_object_id = OBJECT_ID(N'[dbo].[GameSetting]'))
ALTER TABLE [dbo].[GameSetting] CHECK CONSTRAINT [FK_GameSetting_Game]
GO
/****** Object:  ForeignKey [FK_GameTransaction_Game]    Script Date: 11/19/2016 22:54:44 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GameTransaction_Game]') AND parent_object_id = OBJECT_ID(N'[dbo].[GameTransaction]'))
ALTER TABLE [dbo].[GameTransaction]  WITH CHECK ADD  CONSTRAINT [FK_GameTransaction_Game] FOREIGN KEY([GameID])
REFERENCES [dbo].[Game] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GameTransaction_Game]') AND parent_object_id = OBJECT_ID(N'[dbo].[GameTransaction]'))
ALTER TABLE [dbo].[GameTransaction] CHECK CONSTRAINT [FK_GameTransaction_Game]
GO
/****** Object:  ForeignKey [FK_GameTransaction_GameSession]    Script Date: 11/19/2016 22:54:44 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GameTransaction_GameSession]') AND parent_object_id = OBJECT_ID(N'[dbo].[GameTransaction]'))
ALTER TABLE [dbo].[GameTransaction]  WITH CHECK ADD  CONSTRAINT [FK_GameTransaction_GameSession] FOREIGN KEY([GameSessionID])
REFERENCES [dbo].[GameSession] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GameTransaction_GameSession]') AND parent_object_id = OBJECT_ID(N'[dbo].[GameTransaction]'))
ALTER TABLE [dbo].[GameTransaction] CHECK CONSTRAINT [FK_GameTransaction_GameSession]
GO
/****** Object:  ForeignKey [FK_Permission_GroupPermission]    Script Date: 11/19/2016 22:54:44 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Permission_GroupPermission]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permission]'))
ALTER TABLE [dbo].[Permission]  WITH CHECK ADD  CONSTRAINT [FK_Permission_GroupPermission] FOREIGN KEY([GroupPermissionID])
REFERENCES [dbo].[GroupPermission] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Permission_GroupPermission]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permission]'))
ALTER TABLE [dbo].[Permission] CHECK CONSTRAINT [FK_Permission_GroupPermission]
GO
/****** Object:  ForeignKey [FK_Permission_Module]    Script Date: 11/19/2016 22:54:44 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Permission_Module]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permission]'))
ALTER TABLE [dbo].[Permission]  WITH CHECK ADD  CONSTRAINT [FK_Permission_Module] FOREIGN KEY([ModuleID])
REFERENCES [dbo].[Module] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Permission_Module]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permission]'))
ALTER TABLE [dbo].[Permission] CHECK CONSTRAINT [FK_Permission_Module]
GO
/****** Object:  ForeignKey [FK_Permission_Role]    Script Date: 11/19/2016 22:54:44 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Permission_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permission]'))
ALTER TABLE [dbo].[Permission]  WITH CHECK ADD  CONSTRAINT [FK_Permission_Role] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Role] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Permission_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permission]'))
ALTER TABLE [dbo].[Permission] CHECK CONSTRAINT [FK_Permission_Role]
GO
/****** Object:  ForeignKey [FK_PersonBetSetting_BetTypeItem]    Script Date: 11/19/2016 22:54:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonBetSetting_BetTypeItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonBetSetting]'))
ALTER TABLE [dbo].[PersonBetSetting]  WITH CHECK ADD  CONSTRAINT [FK_PersonBetSetting_BetTypeItem] FOREIGN KEY([BetTypeItemID])
REFERENCES [dbo].[BetTypeItem] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonBetSetting_BetTypeItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonBetSetting]'))
ALTER TABLE [dbo].[PersonBetSetting] CHECK CONSTRAINT [FK_PersonBetSetting_BetTypeItem]
GO
/****** Object:  ForeignKey [FK_PersonBetSetting_Currency]    Script Date: 11/19/2016 22:54:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonBetSetting_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonBetSetting]'))
ALTER TABLE [dbo].[PersonBetSetting]  WITH CHECK ADD  CONSTRAINT [FK_PersonBetSetting_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonBetSetting_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonBetSetting]'))
ALTER TABLE [dbo].[PersonBetSetting] CHECK CONSTRAINT [FK_PersonBetSetting_Currency]
GO
/****** Object:  ForeignKey [FK_PersonBetSetting_Person]    Script Date: 11/19/2016 22:54:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonBetSetting_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonBetSetting]'))
ALTER TABLE [dbo].[PersonBetSetting]  WITH CHECK ADD  CONSTRAINT [FK_PersonBetSetting_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonBetSetting_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonBetSetting]'))
ALTER TABLE [dbo].[PersonBetSetting] CHECK CONSTRAINT [FK_PersonBetSetting_Person]
GO
/****** Object:  ForeignKey [FK_PersonCreditSetting_Currency]    Script Date: 11/19/2016 22:54:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonCreditSetting_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonCreditSetting]'))
ALTER TABLE [dbo].[PersonCreditSetting]  WITH CHECK ADD  CONSTRAINT [FK_PersonCreditSetting_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonCreditSetting_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonCreditSetting]'))
ALTER TABLE [dbo].[PersonCreditSetting] CHECK CONSTRAINT [FK_PersonCreditSetting_Currency]
GO
/****** Object:  ForeignKey [FK_PersonCreditSetting_Person]    Script Date: 11/19/2016 22:54:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonCreditSetting_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonCreditSetting]'))
ALTER TABLE [dbo].[PersonCreditSetting]  WITH CHECK ADD  CONSTRAINT [FK_PersonCreditSetting_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonCreditSetting_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonCreditSetting]'))
ALTER TABLE [dbo].[PersonCreditSetting] CHECK CONSTRAINT [FK_PersonCreditSetting_Person]
GO
/****** Object:  ForeignKey [FK_PersonGroup_GroupPermission]    Script Date: 11/19/2016 22:54:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonGroup_GroupPermission]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonGroup]'))
ALTER TABLE [dbo].[PersonGroup]  WITH CHECK ADD  CONSTRAINT [FK_PersonGroup_GroupPermission] FOREIGN KEY([GroupPermissionID])
REFERENCES [dbo].[GroupPermission] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonGroup_GroupPermission]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonGroup]'))
ALTER TABLE [dbo].[PersonGroup] CHECK CONSTRAINT [FK_PersonGroup_GroupPermission]
GO
/****** Object:  ForeignKey [FK_PersonGroup_Person]    Script Date: 11/19/2016 22:54:45 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonGroup_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonGroup]'))
ALTER TABLE [dbo].[PersonGroup]  WITH CHECK ADD  CONSTRAINT [FK_PersonGroup_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonGroup_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonGroup]'))
ALTER TABLE [dbo].[PersonGroup] CHECK CONSTRAINT [FK_PersonGroup_Person]
GO
/****** Object:  ForeignKey [FK_PersonPositionTaking_Person]    Script Date: 11/19/2016 22:54:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonPositionTaking_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonPositionTaking]'))
ALTER TABLE [dbo].[PersonPositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_PersonPositionTaking_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonPositionTaking_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonPositionTaking]'))
ALTER TABLE [dbo].[PersonPositionTaking] CHECK CONSTRAINT [FK_PersonPositionTaking_Person]
GO
/****** Object:  ForeignKey [FK_PersonPositionTaking_Transaction]    Script Date: 11/19/2016 22:54:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonPositionTaking_Transaction]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonPositionTaking]'))
ALTER TABLE [dbo].[PersonPositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_PersonPositionTaking_Transaction] FOREIGN KEY([TransactionID])
REFERENCES [dbo].[Transaction] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PersonPositionTaking_Transaction]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonPositionTaking]'))
ALTER TABLE [dbo].[PersonPositionTaking] CHECK CONSTRAINT [FK_PersonPositionTaking_Transaction]
GO
/****** Object:  ForeignKey [FK_PositionTaking_BetTypeItem]    Script Date: 11/19/2016 22:54:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PositionTaking_BetTypeItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[PositionTaking]'))
ALTER TABLE [dbo].[PositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_PositionTaking_BetTypeItem] FOREIGN KEY([BetTypeID])
REFERENCES [dbo].[BetTypeItem] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PositionTaking_BetTypeItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[PositionTaking]'))
ALTER TABLE [dbo].[PositionTaking] CHECK CONSTRAINT [FK_PositionTaking_BetTypeItem]
GO
/****** Object:  ForeignKey [FK_PositionTaking_Currency]    Script Date: 11/19/2016 22:54:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PositionTaking_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[PositionTaking]'))
ALTER TABLE [dbo].[PositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_PositionTaking_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PositionTaking_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[PositionTaking]'))
ALTER TABLE [dbo].[PositionTaking] CHECK CONSTRAINT [FK_PositionTaking_Currency]
GO
/****** Object:  ForeignKey [FK_PositionTaking_Person]    Script Date: 11/19/2016 22:54:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PositionTaking_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PositionTaking]'))
ALTER TABLE [dbo].[PositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_PositionTaking_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PositionTaking_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[PositionTaking]'))
ALTER TABLE [dbo].[PositionTaking] CHECK CONSTRAINT [FK_PositionTaking_Person]
GO
/****** Object:  ForeignKey [FK_Statement_Person]    Script Date: 11/19/2016 22:54:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Statement_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
ALTER TABLE [dbo].[Statement]  WITH CHECK ADD  CONSTRAINT [FK_Statement_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Statement_Person]') AND parent_object_id = OBJECT_ID(N'[dbo].[Statement]'))
ALTER TABLE [dbo].[Statement] CHECK CONSTRAINT [FK_Statement_Person]
GO
/****** Object:  ForeignKey [FK_Transaction_BetTypeItem]    Script Date: 11/19/2016 22:54:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaction_BetTypeItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaction]'))
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_BetTypeItem] FOREIGN KEY([BetTypeItemID])
REFERENCES [dbo].[BetTypeItem] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaction_BetTypeItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaction]'))
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_BetTypeItem]
GO
/****** Object:  ForeignKey [FK_Transaction_GameSession]    Script Date: 11/19/2016 22:54:47 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaction_GameSession]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaction]'))
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_GameSession] FOREIGN KEY([GameSessionID])
REFERENCES [dbo].[GameSession] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaction_GameSession]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaction]'))
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_GameSession]
GO
