
-- delete from GameSession;
select * from GameSession;

alter table GameSession drop column AckTicks;
alter table GameSession add AckTicks bigint;


select * from GameTransaction;

alter table GameTransaction drop column Ticks;
alter table GameTransaction add Ticks bigint;


alter table [Transaction] add [Time] DateTime not null;


-- delete from Game;
-- delete from GameSetting;

select * from Game;
select * from GameSetting;


alter table [Game] add BetTypeItemID int not null;
alter table [Game] add PointPerMoney money not null default(0);
alter table [Game] add [Description] varchar(500) ;


ALTER TABLE [dbo].[Game]  WITH CHECK ADD  CONSTRAINT [FK_Game_BetTypeItem] FOREIGN KEY([BetTypeItemID])
REFERENCES [dbo].[BetTypeItem] ([ID]);
ALTER TABLE [dbo].[Game] CHECK CONSTRAINT [FK_Game_BetTypeItem];


/* 

INSERT INTO [ALT_Slot].[dbo].[Game]
           ([Code]
           ,[Name]
           ,[GroupID]
           ,[Type]
           ,[Enable]
           ,[UpdatedBy]
           ,[UpdatedDate]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[BetTypeItemID]
           ,[PointPerMoney]
           ,[Description])
     VALUES
           ('ThreeKingdoms'
           ,'Three Kingdoms'
           ,1
           ,Null
           ,1
           ,'SQL-Admin'
           ,GETDATE()
           ,'SQL-Admin'
           ,GETDATE()
           ,1110001
           ,0
           ,Null);

INSERT [dbo].[GameSetting] ([GameID], [Publisher], [Platform], [DownloadURL], [GameHash], [GameKey], [GameHearder], [Enable], [IsHot], [IsNew], [Rate], [Type], [Width], [Height], [ImageURL], [UpdateBy], [UpdateDate], [CreatedBy], [CreatedDate], [Port], [Domain]) VALUES (3, N'ALT', N'Desktop', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'SQL - Admin', CAST(0x0000A6C301737C8F AS DateTime), N'SQL - Admin', CAST(0x0000A6C301737C8F AS DateTime), 8001, N'192.168.1.4')
INSERT [dbo].[GameSetting] ([GameID], [Publisher], [Platform], [DownloadURL], [GameHash], [GameKey], [GameHearder], [Enable], [IsHot], [IsNew], [Rate], [Type], [Width], [Height], [ImageURL], [UpdateBy], [UpdateDate], [CreatedBy], [CreatedDate], [Port], [Domain]) VALUES (3, N'ALT', N'Moblie', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8001, N'192.168.1.4')
INSERT [dbo].[GameSetting] ([GameID], [Publisher], [Platform], [DownloadURL], [GameHash], [GameKey], [GameHearder], [Enable], [IsHot], [IsNew], [Rate], [Type], [Width], [Height], [ImageURL], [UpdateBy], [UpdateDate], [CreatedBy], [CreatedDate], [Port], [Domain]) VALUES (3, N'ALT', N'Web', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8001, N'ws://asianlive.tech:8001/games')

*/
