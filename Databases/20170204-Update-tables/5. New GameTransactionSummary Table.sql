
/****** Object:  Table [dbo].[GameTransaction]    Script Date: 02/10/2017 07:52:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GameTransactionSummary](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [nvarchar] (10) NOT NULL,
	[GameID] [int] NOT NULL,
	
	[Year] [int] NOT NULL,
	[Month] [int] NOT NULL,
	[Day] [int] NOT NULL,
	
	[TotalAmount] [money] NOT NULL,
	[TotalResult] [money] NOT NULL,
	[TotalWin] [money] NOT NULL,
	[TotalLoss] [money] NOT NULL,
	[TotalWinLoss] [money] NOT NULL,
	
	[TotalAdminWinLoss] [money] NULL,
	[TotalShareHolderWinLoss] [money] NULL,
	[TotalSupperSeniorWinLoss] [money] NULL,
	[TotalSeniorWinLoss] [money] NULL,
	[TotalMasterWinLoss] [money] NULL,
	[TotalAgentWinLoss] [money] NULL,
	
	[PointRate] [money] NULL,
	
 CONSTRAINT [PK_GameTransactionSummary] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE NONCLUSTERED INDEX [FileterIndexOnLey] ON [dbo].[GameTransactionSummary] 
(
	[ID] ASC,
	[PersonID] ASC,
	[CurrencyCode] ASC,
	[GameID] ASC,
	[Year] ASC,
	[Month] ASC,
	[Day] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO




SET ANSI_PADDING OFF
GO





