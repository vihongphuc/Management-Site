


alter table GameTransaction add AdminRate [money] NULL;
alter table GameTransaction add AdminAmount [money] NULL;

alter table GameTransaction add ShareHolderRate [money] NULL;
alter table GameTransaction add ShareHolderAmount [money] NULL;

alter table GameTransaction add SupperSeniorRate [money] NULL;
alter table GameTransaction add SupperSeniorAmount [money] NULL;

alter table GameTransaction add SeniorRate [money] NULL;
alter table GameTransaction add SeniorAmount [money] NULL;

alter table GameTransaction add MasterRate [money] NULL;
alter table GameTransaction add MasterAmount [money] NULL;

alter table GameTransaction add AgentRate [money] NULL;
alter table GameTransaction add AgentAmount [money] NULL;

alter table GameTransaction add [CurrencyCode] [varchar](10) NULL;
alter table GameTransaction add [PointRate] [money] NULL;
alter table GameTransactionPositionTaking add [PointRate] [money] NULL;




