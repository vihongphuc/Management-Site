USE [ALT_Slot]
GO

/****** Object:  ForeignKey [FK_GameTransactionPositionTaking_Transaction]    Script Date: 02/09/2017 07:58:39 ******/
ALTER TABLE [dbo].[GameTransactionPositionTaking]  drop CONSTRAINT [FK_GameTransactionPositionTaking_Transaction] 
GO



drop table [dbo].[Transaction]
Go




/****** Object:  Table [dbo].[Transaction]    Script Date: 02/09/2017 07:55:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transaction](
	[ID] [bigint] NOT NULL,
	[GameSessionID] [bigint] NOT NULL,
	[Amount] [money] NOT NULL,
	[Result] [money] NOT NULL,
	[Type] [varchar](50) NULL,
	[Description] [nvarchar](500) NULL,
	[BetTypeItemID] [int] NOT NULL,
	[UpdateBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[Status] [varchar](10) NOT NULL,
	[IpAddress] [varchar](100) NULL,
	[Signature] [varchar](500) NOT NULL,
	[Time] [datetime] NOT NULL,
	[PointRate] [money] NOT NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UN_Transaction_Signature] UNIQUE NONCLUSTERED 
(
	[Signature] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [DF_Transaction_UpdatedDate]    Script Date: 02/09/2017 07:55:26 ******/
ALTER TABLE [dbo].[Transaction] ADD  CONSTRAINT [DF_Transaction_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
/****** Object:  Default [DF_Transaction_CreatedDate]    Script Date: 02/09/2017 07:55:26 ******/
ALTER TABLE [dbo].[Transaction] ADD  CONSTRAINT [DF_Transaction_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  ForeignKey [FK_Transaction_BetTypeItem]    Script Date: 02/09/2017 07:55:26 ******/
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_BetTypeItem] FOREIGN KEY([BetTypeItemID])
REFERENCES [dbo].[BetTypeItem] ([ID])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_BetTypeItem]
GO
/****** Object:  ForeignKey [FK_Transaction_Currency]    Script Date: 02/09/2017 07:55:26 ******/
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Currency]
GO
/****** Object:  ForeignKey [FK_Transaction_GameSession]    Script Date: 02/09/2017 07:55:26 ******/
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_GameSession] FOREIGN KEY([GameSessionID])
REFERENCES [dbo].[GameSession] ([ID])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_GameSession]
GO
/****** Object:  ForeignKey [FK_Transaction_Person]    Script Date: 02/09/2017 07:55:26 ******/
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Person]
GO





/****** Object:  ForeignKey [FK_GameTransactionPositionTaking_Transaction]    Script Date: 02/09/2017 07:58:39 ******/
ALTER TABLE [dbo].[GameTransactionPositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_GameTransactionPositionTaking_Transaction] FOREIGN KEY([TransactionID])
REFERENCES [dbo].[Transaction] ([ID])
GO
ALTER TABLE [dbo].[GameTransactionPositionTaking] CHECK CONSTRAINT [FK_GameTransactionPositionTaking_Transaction]
GO


/****** Object:  ForeignKey [FK_GameTransactionPositionTaking_Transaction]    Script Date: 02/09/2017 07:58:39 ******/
ALTER TABLE [dbo].[GameTransaction]  WITH CHECK ADD  CONSTRAINT [FK_GameTransaction_Transaction] FOREIGN KEY([TransactionID])
REFERENCES [dbo].[Transaction] ([ID])
GO
ALTER TABLE [dbo].[GameTransaction] CHECK CONSTRAINT [FK_GameTransaction_Transaction]
GO