USE [ALT_Slot]
GO

drop table [PersonPositionTaking];
/****** Object:  Table [dbo].[PersonPositionTaking]    Script Date: 02/05/2017 15:01:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GameTransactionPositionTaking](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TransactionID] [bigint] NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NULL,
	[Rate] [money] NULL,
	[Amount] [money] NULL,
	[UpdateBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_GameTransactionPositionTaking] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[GameTransactionPositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_GameTransactionPositionTaking_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO

ALTER TABLE [dbo].[GameTransactionPositionTaking] CHECK CONSTRAINT [FK_GameTransactionPositionTaking_Person]
GO

ALTER TABLE [dbo].[GameTransactionPositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_GameTransactionPositionTaking_Transaction] FOREIGN KEY([TransactionID])
REFERENCES [dbo].[Transaction] ([ID])
GO

ALTER TABLE [dbo].[GameTransactionPositionTaking] CHECK CONSTRAINT [FK_GameTransactionPositionTaking_Transaction]
GO

ALTER TABLE [dbo].[GameTransactionPositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_GameTransactionPositionTaking_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO

ALTER TABLE [dbo].[GameTransactionPositionTaking] CHECK CONSTRAINT [FK_GameTransactionPositionTaking_Currency]
GO

ALTER TABLE [dbo].[GameTransactionPositionTaking] ADD  CONSTRAINT [DF_GameTransactionPositionTaking_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO

ALTER TABLE [dbo].[GameTransactionPositionTaking] ADD  CONSTRAINT [DF_GameTransactionPositionTaking_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO


