USE [ALT_Slot]
GO

INSERT INTO [dbo].[PersonBetSetting]
           ([PersonID]
           ,[BetTypeItemID]
           ,[CurrencyCode]
           ,[SettingType]
           ,[Value]
           ,[ValueString])
     VALUES
           (100,		1110001,	'USD',	'MaxLossLimit',0.8,null),
		   (100,		1110001,	'USD',	'WinLossLimit',0.2,null),

		   (1000,		1110001,	'USD','MaxLossLimit',0.5,null),
		   (1000,		1110001,	'USD','WinLossLimit',0.5,null),

           (10000,		1110001,	'USD','MaxLossLimit',0.5,null),
		   (10000,		1110001,	'USD','WinLossLimit',0.5,null),

		   (100000,		1110001,	'USD','MaxLossLimit',0.5,null),
		   (100000,		1110001,	'USD','WinLossLimit',0.5,null),

           (1000000,	1110001,	'USD','MaxLossLimit',0.5,null),
		   (1000000,	1110001,	'USD','WinLossLimit',0.5,null),

		   (10000000,	1110001,	'USD','MaxLossLimit',0.5,null),
		   (10000000,	1110001,	'USD','WinLossLimit',0.5,null),
		             
           (100000000,	1110001,	'USD','MaxLossLimit',0.5,null),
		   (1000000000,	1110001,	'USD','WinLossLimit',0.5,null)
GO


