USE [ALT_Slot]
GO

INSERT INTO [dbo].[PersonUpline]
           ([PersonID]
           ,[AdminID]
           ,[ShareHolderID]
           ,[SupperSeniorID]
           ,[SeniorID]
           ,[MasterID]
           ,[AgentID])
     VALUES
           (100,			0,		0,		0,		0,			0,		0),
		   (1000,			100,	0,		0,		0,			0,		0),
		   (10000,			0,		1000,	0,		0,			0,		0),
		   (100000,			0,		0,		10000,	0,			0,		0),
		   (1000000,		0,		0,		0,		100000,		0,		0),
		   (10000000,		0,		0,		0,		0,			1000000,0),
		   (100000000,		0,		0,		0,		0,			0,		10000000),
		   (1000000000,		0,		0,		0,		0,			0,		10000000)		   
GO


