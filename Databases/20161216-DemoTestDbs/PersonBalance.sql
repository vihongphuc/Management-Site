USE [ALT_Slot]
GO

INSERT INTO [dbo].[PersonBalance]
           ([PersonID]
           ,[CurrencyCode]
           ,[Available]
           ,[YesterdayAvailable]
           ,[UpdatedBy]
           ,[UpdatedDate]
           ,[CreatedBy]
           ,[CreatedDate])
     VALUES
           (100,			'USD',10000,	100,'','','',''),
		   (1000,			'USD',1000,		100,'','','',''),
		   (10000,			'USD',100,		100,'','','',''),
		   (100000,			'USD',100,		100,'','','',''),
           (1000000,		'USD',100,		100,'','','',''),
		   (10000000,		'USD',100,		100,'','','',''),
		   (100000000,		'USD',100,		100,'','','',''),
		   (1000000000,		'USD',100,		100,'','','','')		   
GO


