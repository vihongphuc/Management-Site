USE [ALT_Slot]
GO

INSERT INTO [dbo].[PersonCreditSetting]
           ([PersonID]
           ,[CurrencyCode]
           ,[PersonType]
           ,[MaxCredit]
           ,[UpdatedBy]
           ,[UpdateDate]
           ,[CreatedBy]
           ,[CreatedDate])
     VALUES
           (100,		'USD',	'Admin',		12,'','','',''),
		   (1000,		'USD',	'Shareholder',	12,'','','',''),
		   (10000,		'USD',	'SuperSenoir',	12,'','','',''),
		   (100000,		'USD',	'Senoir',		12,'','','',''),
		   (1000000,	'USD',	'Mater',		12,'','','',''),
		   (10000000,	'USD',	'Agent',		12,'','','',''),
		   (100000000,	'USD',	'Member',		12,'','','',''),
		   (1000000000,	'USD',	'Member',		12,'','','','')		 
		   GO


