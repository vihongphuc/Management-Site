USE [ALT_Slot]
GO

INSERT INTO [dbo].[PositionTaking]
           ([PersonID]
           ,[CurrencyCode]
           ,[BetTypeID]
           ,[Type]
           ,[Value]
           ,[UpdatedBy]
           ,[UpdateDate]
           ,[CreatedBy]
           ,[CreatedDate])
     VALUES
		   (100			,'USD',			1110001,	'Upline' ,	1		,null,null,null,null),
           (100			,'USD',			1110001,	'Current' ,	0.9		,null,null,null,null),

		   (1000		,'USD',			1110001,	'Upline' ,	0.9		,null,null,null,null),
           (1000		,'USD',			1110001,	'Current' ,	0.8		,null,null,null,null),

		   (10000		,'USD',			1110001,	'Upline',	0.8		,null,null,null,null),
		   (10000		,'USD',			1110001,	'Current',	0.7		,null,null,null,null),

		   (100000		,'USD',			1110001,	'Upline',	0.7		,null,null,null,null),
		   (100000		,'USD',			1110001,	'Current',	0.6		,null,null,null,null),

		   (1000000		,'USD',			1110001,	'Upline',	0.6		,null,null,null,null),
		   (1000000		,'USD',			1110001,	'Current',	0.5		,null,null,null,null),

		   (10000000	,'USD',			1110001,	'Upline',	0.5		,null,null,null,null),	
		   (10000000	,'USD',			1110001,	'Current',	0.4		,null,null,null,null), 

		   (100000000	,'USD',			1110001,	'Upline',	0.4		,null,null,null,null),
		   (1000000000	,'USD',			1110001,	'Upline',	0.4		,null,null,null,null)			 
GO


