USE [ALT_Slot]
GO
SET IDENTITY_INSERT Person ON
INSERT INTO [dbo].[Person]
           ([ID]
			,[Username]
            ,[Firstname]
            ,[Lastname]
            ,[Password]
            ,[DOB]
            ,[Address]
            ,[Type]
            ,[Status]
            ,[CountryCode]
            ,[CurrencyCodes]
            ,[Mobile]
            ,[Phone]
            ,[Email]
            ,[LastLoginTime]
            ,[LastLoginIP]
            ,[IsExternal]
            ,[AutoTransfer]
            ,[LimitIP]
            ,[DisplayName]
            ,[GroupID]
            ,[UpdatedBy]
            ,[UpdateDate]
            ,[CreatedBy]
            ,[CreatedDate])
     VALUES 
			 (100,			'admin',			'Mrs',			'Ad',  'e10adc3949ba59abbe56e057f20f883e',	'1/1/2016','Address',	'Admin',		'Active','CountryCode',		'USD,VND',  '09999999',	'0999999888',Null ,	'10/10/2016',		'1.1.1.1',0,0,'1.1.1.1',	'Admin',			1,	'',				'','',15/12/2016),				---Admin
			 (1000,			'0',				'Shareholder',	'Sh',  'e10adc3949ba59abbe56e057f20f883e',	'1/1/2016','Address',	'Shareholder',	'Active','CountryCode',		'USD,VND',  '09999999',	'0999999888',Null,	'10/10/2016',		'1.1.1.1',0,0,'1.1.1.1',	'Sharehoder-1',		2,	'Admin',		'','',15/12/2016),				--Sharehoder
			 (10000,		'000',				'SuperSenoir',	'Su',  'e10adc3949ba59abbe56e057f20f883e',	'1/1/2016','Address',	'Suppersenior',	'Active','CountryCode',		'USD',		'09999999',	'0999999888',Null,	'10/10/2016',		'1.1.1.1',0,0,'1.1.1.1',	'SuperSenior-1',	2,	'Sharehoder',	'','',15/12/2016),				--SuperSenior
			 (100000,		'00000',			'Senoir',		'Se',  'e10adc3949ba59abbe56e057f20f883e',	'1/1/2016','Address',	'Senior',		'Active','CountryCode',		'USD',		'09999999',	'0999999888',Null,	'10/10/2016',		'1.1.1.1',0,0,'1.1.1.1',	'Senoir-1',			2,	'SuperSenior',	'','',15/12/2016),				--Senoir
			 (1000000,		'0000000',			'Mater',		'Ma',  'e10adc3949ba59abbe56e057f20f883e',	'1/1/2016','Address',	'Master',		'Active','CountryCode',		'USD',		'09999999',	'0999999888',Null,	'10/10/2016',		'1.1.1.1',0,0,'1.1.1.1',	'Mater-1',			2,	'Senoir',		'','',15/12/2016),				--Mater		  
			 (10000000,		'000000000',		'Agent',		'Ag',  'e10adc3949ba59abbe56e057f20f883e',	'1/1/2016','Address',	'Agent',		'Active','CountryCode',		'USD',		'09999999',	'0999999888',Null,	'10/10/2016',		'1.1.1.1',0,0,'1.1.1.1',	'Agent-1',			2,	'Mater',		'','',15/12/2016),				--Agent
			 (100000000,	'00000000001',		'Member',		'Me',  'e10adc3949ba59abbe56e057f20f883e',	'1/1/2016','Address',	'Member',		'Active','CountryCode',		'USD',		'09999999',	'0999999888',Null,	'10/10/2016',		'1.1.1.1',0,0,'1.1.1.1',	'Agent-1',			2,	'Mater',		'','',15/12/2016),				--Member 01
			 (1000000000,	'00000000002',		'Member',		'Me',  'e10adc3949ba59abbe56e057f20f883e',	'1/1/2016','Address',	'Member',		'Active','CountryCode',		'USD',		'09999999',	'0999999888',Null,	'10/10/2016',		'1.1.1.1',0,0,'1.1.1.1',	'Agent-1',			2,	'Mater',		'','',15/12/2016)				--Member 02
GO


