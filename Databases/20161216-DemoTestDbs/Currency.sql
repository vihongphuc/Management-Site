USE [ALT_Slot]
GO

INSERT INTO [dbo].[Currency]
           ([CurrencyCode]
           ,[Name]
           ,[DisplayName]
           ,[Enable]
           ,[Rate])
     VALUES
           ('USD','USD','USD',1, 0.0001),
		   ('VND','VND','VND',1, 1)
GO


