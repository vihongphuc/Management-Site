select * from [Transaction];


alter table [Transaction] add PersonID bigint not null;
alter table [Transaction] add CurrencyCode varchar(10) not null;
alter table [Transaction] add [Status] varchar(10) not null;
alter table [Transaction] add IpAddress varchar(100) ;
alter table [Transaction] add [Signature] varchar(500) not null;


ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID]);
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Person];

ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode]);
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Currency];


ALTER TABLE [dbo].[Transaction]  ADD  CONSTRAINT [UN_Transaction_Signature] UNIQUE([Signature]);

