USE [ALT_Slot]
GO

INSERT [dbo].[BetType] ([ID], [Name], [Description], [CreatedBy], [CreatedDate]) VALUES (110001, N'SlotGame', N'Slot Game', N'SQL-Admin', CAST(N'2017-02-25 15:34:50.227' AS DateTime))
GO
INSERT [dbo].[BetTypeItem] ([ID], [Name], [BetTypeID], [Description], [CreatedBy], [CreatedDate]) VALUES (1100010001, N'SlotGame', 110001, N'Slot Game', N'SQL-Admin', CAST(N'2017-02-25 15:34:50.237' AS DateTime))
GO
INSERT [dbo].[Currency] ([CurrencyCode], [Name], [DisplayName], [Enable], [Rate]) VALUES (N'MYR', N'MYR', N'MYR', 1, 0.01)
GO
INSERT [dbo].[Currency] ([CurrencyCode], [Name], [DisplayName], [Enable], [Rate]) VALUES (N'USD', N'USD', N'USD', 1, 1)
GO
INSERT [dbo].[Currency] ([CurrencyCode], [Name], [DisplayName], [Enable], [Rate]) VALUES (N'VND', N'Vietnam Dong', N'VND', 1, 0.001)
GO

SET IDENTITY_INSERT [dbo].[GameCategory] ON 
GO
INSERT [dbo].[GameCategory] ([ID], [Name], [Description], [ImageURL], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'Slot', N'Slot game', NULL, N'SQL - Admin', CAST(N'2016-11-19 22:19:39.730' AS DateTime), N'SQL - Admin', CAST(N'2016-11-19 22:19:39.730' AS DateTime))
GO
INSERT [dbo].[GameCategory] ([ID], [Name], [Description], [ImageURL], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, N'Table', N'Table Game', NULL, N'SQL - Admin', CAST(N'2016-11-19 22:20:04.390' AS DateTime), N'SQL - Admin', CAST(N'2016-11-19 22:20:04.390' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[GameCategory] OFF
GO

SET IDENTITY_INSERT [dbo].[Game] ON 
GO
INSERT [dbo].[Game] ([ID], [Code], [Name], [GroupID], [Type], [Enable], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate], [BetTypeItemID], [PointRate], [Description]) VALUES (2, N'ThreeKingdoms', N'Three Kingdoms', 1, NULL, 1, N'SQL-Admin', CAST(N'2017-02-11 15:34:27.200' AS DateTime), N'SQL-Admin', CAST(N'2017-02-11 15:34:27.200' AS DateTime), 1100010001, 0.0000, N'')
GO
INSERT [dbo].[Game] ([ID], [Code], [Name], [GroupID], [Type], [Enable], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate], [BetTypeItemID], [PointRate], [Description]) VALUES (6, N'ThreeKingdoms2', N'ThreeKingdoms2', 1, N'1', 0, N'SQl-Admin', CAST(N'2017-02-18 15:54:48.217' AS DateTime), N'SQl-Admin', CAST(N'2017-02-18 15:54:48.217' AS DateTime), 1100010001, 0.0000, N'')
GO
INSERT [dbo].[Game] ([ID], [Code], [Name], [GroupID], [Type], [Enable], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate], [BetTypeItemID], [PointRate], [Description]) VALUES (7, N'ThreeKingdoms3', N'ThreeKingdoms3', 1, N'1', 0, N'SQl-Admin', CAST(N'2017-02-18 15:54:54.237' AS DateTime), N'SQl-Admin', CAST(N'2017-02-18 15:54:54.237' AS DateTime), 1100010001, 0.0000, N'')
GO
INSERT [dbo].[Game] ([ID], [Code], [Name], [GroupID], [Type], [Enable], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate], [BetTypeItemID], [PointRate], [Description]) VALUES (11, N'LauraQuest', N'LauraQuest', 1, N'1', 1, N'SQl-Admin', CAST(N'2017-03-09 10:52:36.653' AS DateTime), NULL, CAST(N'2017-03-09 10:52:36.653' AS DateTime), 1100010001, 0.0000, NULL)
GO
SET IDENTITY_INSERT [dbo].[Game] OFF
GO

INSERT [dbo].[Person] ([ID], [Username], [Firstname], [Lastname], [Password], [DOB], [Address], [Type], [Status], [CountryCode], [CurrencyCode], [Mobile], [Phone], [Email], [LastLoginTime], [LastLoginIP], [IsExternal], [AutoTransfer], [LimitIP], [DisplayName], [GroupID], [UplineID], [CreateAgency], [CreateMember], [FixedCurrency] , [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100, N'admin', N'Mrs', N'Ad', N'e10adc3949ba59abbe56e057f20f883e', CAST(N'2016-01-01' AS Date), N'Address', N'Admin', N'Active', N'CountryCode', N'USD', N'09999999', N'0999999888', NULL, CAST(N'2016-10-10 00:00:00.000' AS DateTime), N'1.1.1.1', 0, 0, N'1.1.1.1', N'Admin', 1, NULL, 1, 0, 1, N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime), N'', CAST(N'1900-01-01 00:00:00.000' AS DateTime))
GO

INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedDate], [UpdatedBy], [CreatedDate], [CreatedBy]) VALUES (100, 1100010001, N'MYR', N'MaxWinLimit', CAST(0 AS Decimal(18, 0)), N'', CAST(N'2017-02-25 15:58:40.577' AS DateTime), N'SQL-Admin', CAST(N'2017-02-25 15:58:40.577' AS DateTime), N'SQL-Admin')
GO
INSERT [dbo].[PersonBetSetting] ([PersonID], [BetTypeItemID], [CurrencyCode], [SettingType], [Value], [ValueString], [UpdatedDate], [UpdatedBy], [CreatedDate], [CreatedBy]) VALUES (100, 1100010001, N'MYR', N'MinLossLimit', CAST(0 AS Decimal(18, 0)), N'', CAST(N'2017-02-25 15:58:40.577' AS DateTime), N'SQL-Admin', CAST(N'2017-02-25 15:58:40.577' AS DateTime), N'SQL-Admin')
GO


INSERT [dbo].[PositionTaking] ([PersonID], [CurrencyCode], [BetTypeItemID], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100, N'MYR', 1100010001, N'CurrentMax', 1.0000, N'SQL-Admin', CAST(N'2017-02-25 15:58:40.580' AS DateTime), N'SQL-Admin', CAST(N'2017-02-25 15:58:40.580' AS DateTime))
GO
INSERT [dbo].[PositionTaking] ([PersonID], [CurrencyCode], [BetTypeItemID], [Type], [Value], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100, N'MYR', 1100010001, N'Upline', 0.0000, N'SQL-Admin', CAST(N'2017-02-25 15:58:40.580' AS DateTime), N'SQL-Admin', CAST(N'2017-02-25 15:58:40.580' AS DateTime))
GO



SET IDENTITY_INSERT [dbo].[GameTransactionPositionTaking] ON 

SET IDENTITY_INSERT [dbo].[GameTransactionPositionTaking] OFF
GO
INSERT [dbo].[PersonCreditSetting] ([PersonID], [CurrencyCode], [PersonType], [MaxCredit], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100, N'MYR', N'Admin', 0.0000, N'SQL-Admin', CAST(N'2017-02-25 15:58:40.553' AS DateTime), N'SQL-Admin', CAST(N'2017-02-25 15:58:40.550' AS DateTime))
GO

INSERT [dbo].[GameSetting] ([GameID], [Publisher], [Platform], [DownloadURL], [GameHash], [GameKey], [GameHearder], [Enable], [IsHot], [IsNew], [Rate], [Type], [Width], [Height], [ImageURL], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate], [Port], [Domain]) VALUES (2, N'ALT', N'Desktop', NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, N'SQL - Admin', CAST(N'2016-11-19 22:32:32.477' AS DateTime), N'SQL - Admin', CAST(N'2016-11-19 22:32:32.477' AS DateTime), 8001, N'ws://asianlive.tech:8001/games')
GO
INSERT [dbo].[GameSetting] ([GameID], [Publisher], [Platform], [DownloadURL], [GameHash], [GameKey], [GameHearder], [Enable], [IsHot], [IsNew], [Rate], [Type], [Width], [Height], [ImageURL], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate], [Port], [Domain]) VALUES (2, N'ALT', N'Moblie', NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8001, N'ws://asianlive.tech:8001/games')
GO
INSERT [dbo].[GameSetting] ([GameID], [Publisher], [Platform], [DownloadURL], [GameHash], [GameKey], [GameHearder], [Enable], [IsHot], [IsNew], [Rate], [Type], [Width], [Height], [ImageURL], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate], [Port], [Domain]) VALUES (2, N'ALT', N'Web', N'~/Game/ThreeKingdoms/Release', NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8001, N'ws://asianlive.tech:8001/games')
GO
INSERT [dbo].[GameSetting] ([GameID], [Publisher], [Platform], [DownloadURL], [GameHash], [GameKey], [GameHearder], [Enable], [IsHot], [IsNew], [Rate], [Type], [Width], [Height], [ImageURL], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate], [Port], [Domain]) VALUES (6, N'ALT', N'Web', N'~/Game/ThreeKingdoms2/Release', NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8001, N'ws://asianlive.tech:8001/games')
GO
INSERT [dbo].[GameSetting] ([GameID], [Publisher], [Platform], [DownloadURL], [GameHash], [GameKey], [GameHearder], [Enable], [IsHot], [IsNew], [Rate], [Type], [Width], [Height], [ImageURL], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate], [Port], [Domain]) VALUES (7, N'ALT', N'Web', N'~/Game/ThreeKingdoms3/Release', NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8001, N'ws://asianlive.tech:8001/games')
GO
INSERT [dbo].[GameSetting] ([GameID], [Publisher], [Platform], [DownloadURL], [GameHash], [GameKey], [GameHearder], [Enable], [IsHot], [IsNew], [Rate], [Type], [Width], [Height], [ImageURL], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate], [Port], [Domain]) VALUES (11, N'ALT', N'Web', N'~/Game/LauraQuest/Release', NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8001, N'ws://asianlive.tech:8001/games')
GO




INSERT [dbo].[Configuration] ([ConfigKey], [ConfigValue], [Detail], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (N'Platform', N'[{Desktop:"Desktop"}]', N'Desktop', N'SQL - Admin', CAST(N'2016-11-19 22:35:15.010' AS DateTime), N'SQL - Admin', CAST(N'2016-11-19 22:35:15.010' AS DateTime))
GO
INSERT [dbo].[Configuration] ([ConfigKey], [ConfigValue], [Detail], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (N'Publishers', N'[{Alt:"Asian Live Tech Company"}]', N'All Publishers', N'SQL - Admin', CAST(N'2016-11-19 22:35:10.863' AS DateTime), N'SQL - Admin', CAST(N'2016-11-19 22:35:10.863' AS DateTime))
GO



INSERT [dbo].[PersonBalance] ([PersonID], [CurrencyCode], [Available], [YesterdayAvailable], [UpdatedBy], [LastUpdatedDate], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (100, N'MYR', 99999999999999, 0, NULL, CAST(N'2017-02-02 17:12:18.830' AS DateTime), CAST(N'2017-02-02 17:12:18.830' AS DateTime), NULL, CAST(N'2017-02-02 17:12:18.830' AS DateTime))
GO
