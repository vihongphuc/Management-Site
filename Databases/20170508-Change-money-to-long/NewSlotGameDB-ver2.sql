-- create database [ALT_Slot]

USE [ALT_Slot]
GO
/****** Object:  Table [dbo].[Announcement]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Announcement](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NULL,
	[Type] [varchar](50) NULL,
	[Title] [nvarchar](200) NULL,
	[Value] [text] NULL,
	[LanguageCode] [varchar](10) NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Announcement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Audit]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Audit](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Time] [datetime] NULL,
	[Subject] [varchar](100) NULL,
	[PersonID] [bigint] NOT NULL,
	[RelatedID] [bigint] NULL,
	[Details] [nvarchar](500) NULL,
	[Before] [nvarchar](500) NULL,
	[After] [nvarchar](500) NULL,
	[Type] [varchar](100) NULL,
	[Token] [varchar](100) NULL,
	[IPAddress] [varchar](100) NULL,
	[IPLocation] [varchar](100) NULL,
	[ConsumerIPAddress] [varchar](100) NULL,
	[ConsumerIPLocation] [varchar](100) NULL,
	[ControllerName] [varchar](100) NULL,
	[ActionName] [varchar](100) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Audit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BetType]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BetType](
	[ID] [bigint] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_BetType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BetTypeItem]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BetTypeItem](
	[ID] [bigint] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[BetTypeID] [bigint] NOT NULL,
	[Description] [nvarchar](200) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_BetTypeItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Commission]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Commission](
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[BetTypeID] [bigint] NOT NULL,
	[BetTypeItemID] [bigint] NOT NULL,
	[GameID] [bigint] NOT NULL,
	[CommissionTypeID] [bigint] NOT NULL,
	[Rate] [money] NULL,
	--[PointRate] [money] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Commission] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC,
	[CommissionTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CommissionType]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CommissionType](
	[ID] [bigint] NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[DefaultRate] [money] NOT NULL,
	[Enable] [bit] NULL,
	[Description] [varchar](500) NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_CommissionType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Configuration]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Configuration](
	[ConfigKey] [varchar](100) NOT NULL,
	[ConfigValue] [text] NULL,
	[Detail] [text] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Configuration] PRIMARY KEY CLUSTERED 
(
	[ConfigKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Currency](
	[CurrencyCode] [varchar](10) NOT NULL,
	[Name] [varchar](50) NULL,
	[DisplayName] [varchar](200) NULL,
	[Enable] [bit] NOT NULL,
	[PointRate] [money] NOT NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[CurrencyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CurrencyHistory]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CurrencyHistory](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[Name] [varchar](50) NULL,
	[DisplayName] [varchar](200) NULL,
	[Enable] [bit] NOT NULL,
	[PointRate] [money] NOT NULL,
	[FromDate] [datetime] NOT NULL,
	[ToDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_CurrencyHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Game]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Game](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](100) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[GroupID] [bigint] NOT NULL,
	[Type] [varchar](50) NULL,
	[Enable] [bit] NULL,
	[BetTypeItemID] [bigint] NOT NULL,
	[PointRate] [money] NOT NULL,
	[Description] [varchar](500) NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Game] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GameCategory]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GameCategory](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[ImageURL] [nvarchar](200) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_GameCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GameSession]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GameSession](
	[ID] [bigint] NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NULL,
	[PointRate] [money] NULL,
	[TransferPoint] [bigint] NOT NULL,
	[TotalBet] [bigint] NULL,
	[TotalResult] [bigint] NULL,
	[FinishedTime] [datetime] NULL,
	[FinishedTimeTicks] [bigint] NULL,		
	[Ack] [datetime] NULL,
	[AckTicks] [bigint] NULL,
	[Platform] [varchar](100) NULL,
	[IpAddress] [varchar](100) NULL,
	[Data] [nvarchar](100) NULL,
	[Publisher] [varchar](100) NULL,	
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_GameSession] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GameSetting]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GameSetting](
	[GameID] [bigint] NOT NULL,
	[Publisher] [varchar](100) NOT NULL,
	[Platform] [varchar](100) NOT NULL,
	[DownloadURL] [varchar](500) NULL,
	[GameHash] [varchar](500) NULL,
	[GameKey] [varchar](500) NULL,
	[GameHearder] [varchar](500) NULL,
	[Enable] [bit] NULL,
	[IsHot] [bit] NULL,
	[IsNew] [bit] NULL,
	[Rate] [varchar](50) NULL,
	[Type] [varchar](50) NULL,
	[Width] [money] NULL,
	[Height] [money] NULL,
	[ImageURL] [varchar](200) NULL,
	[Port] [int] NULL,
	[Domain] [nvarchar](200) NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_GameSetting] PRIMARY KEY CLUSTERED 
(
	[GameID] ASC,
	[Publisher] ASC,
	[Platform] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GameTransaction]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GameTransaction](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,	
	[TransactionID] [bigint] NOT NULL,
	[GameSessionID] [bigint] NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[GameID] [bigint] NOT NULL,	
	[BetTypeItemID] [bigint] NULL,
	[GameTransactionID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NULL,
	[PointRate] [money] NULL,
	[Amount] [bigint] NOT NULL,
	[Result] [bigint] NOT NULL,
	[StartBalance] [bigint] NOT NULL,
	[EndBalance] [bigint] NOT NULL,
	[GrossCommission] [bigint] NULL,
	[CommissionRate] [money] NULL,
	[CommissionAmount] [bigint] NULL,
	[Time] [datetime] NULL,
	[Ticks] [bigint] NULL,
	[Detail] [text] NULL,
	[Type] [varchar](100) NULL,
	[Description] [nvarchar](500) NULL,
	[AdminID] [bigint] NULL,
	[AdminRate] [money] NULL,
	[AdminAmount] [bigint] NULL,
	[AdminCmmGross] [bigint] NULL,
	[AdminCmmRate] [money] NULL,
	[AdminCmmAmount] [bigint] NULL,
	[SuperShareHolderID] [bigint] NULL,
	[SuperShareHolderRate] [money] NULL,
	[SuperShareHolderAmount] [bigint] NULL,
	[SuperShareHolderCmmGross] [bigint] NULL,
	[SuperShareHolderCmmRate] [money] NULL,
	[SuperShareHolderCmmAmount] [bigint] NULL,
	[ShareHolderID] [bigint] NULL,
	[ShareHolderRate] [money] NULL,
	[ShareHolderAmount] [bigint] NULL,
	[ShareHolderCmmGross] [bigint] NULL,
	[ShareHolderCmmRate] [money] NULL,
	[ShareHolderCmmAmount] [bigint] NULL,
	[SuperSeniorID] [bigint] NULL,
	[SuperSeniorRate] [money] NULL,
	[SuperSeniorAmount] [bigint] NULL,
	[SuperSeniorCmmGross] [bigint] NULL,
	[SuperSeniorCmmRate] [money] NULL,
	[SuperSeniorCmmAmount] [bigint] NULL,
	[SeniorID] [bigint] NULL,
	[SeniorRate] [money] NULL,
	[SeniorAmount] [bigint] NULL,
	[SeniorCmmGross] [bigint] NULL,
	[SeniorCmmRate] [money] NULL,
	[SeniorCmmAmount] [bigint] NULL,
	[SuperMasterID] [bigint] NULL,
	[SuperMasterRate] [money] NULL,
	[SuperMasterAmount] [bigint] NULL,
	[SuperMasterCmmGross] [bigint] NULL,
	[SuperMasterCmmRate] [money] NULL,
	[SuperMasterCmmAmount] [bigint] NULL,
	[MasterID] [bigint] NULL,
	[MasterRate] [money] NULL,
	[MasterCmmAmount] [bigint] NULL,
	[MasterAmount] [bigint] NULL,
	[MasterCmmGross] [bigint] NULL,
	[MasterCmmRate] [money] NULL,
	[SuperAgentID] [bigint] NULL,
	[SuperAgentRate] [money] NULL,
	[SuperAgentAmount] [bigint] NULL,
	[SuperAgentCmmGross] [bigint] NULL,
	[SuperAgentCmmRate] [money] NULL,
	[SuperAgentCmmAmount] [bigint] NULL,
	[AgentID] [bigint] NULL,
	[AgentRate] [money] NULL,
	[AgentAmount] [bigint] NULL,
	[AgentCmmGross] [bigint] NULL,
	[AgentCmmRate] [money] NULL,
	[AgentCmmAmount] [bigint] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_GameTransaction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UNQ_GameTransaction_GameTransactionID] UNIQUE NONCLUSTERED 
(
	[GameTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GameTransactionCommission]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GameTransactionCommission](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TransactionID] [bigint] NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NULL,
	[PointRate] [money] NULL,
	[Gross] [bigint] NULL,
	[Rate] [money] NULL,
	[Amount] [bigint] NULL,	
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_GameTransactionCommission] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GameTransactionPositionTaking]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GameTransactionPositionTaking](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TransactionID] [bigint] NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NULL,
	[PointRate] [money] NULL,
	[Rate] [money] NULL,
	[Amount] [bigint] NULL,	
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_GameTransactionPositionTaking] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GameTransactionSummary]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[GameTransactionSummary](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [nvarchar](10) NOT NULL,
	[GameID] [bigint] NOT NULL,
	[Year] [int] NOT NULL,
	[Month] [int] NOT NULL,
	[Day] [int] NOT NULL,
	[PointRate] [money] NULL,
	[TotalAmount] [bigint] NOT NULL,
	[TotalResult] [bigint] NOT NULL,
	[TotalWin] [bigint] NOT NULL,
	[TotalLoss] [bigint] NOT NULL,
	[TotalWinLoss] [bigint] NOT NULL,
	[AdminID] [bigint] NULL,
	[TotalAdminWinLoss] [bigint] NULL,
	[SuperShareHolderID] [bigint] NULL,
	[TotalSuperShareHolderWinLoss] [bigint] NULL,
	[ShareHolderID] [bigint] NULL,
	[TotalShareHolderWinLoss] [bigint] NULL,
	[SuperSeniorID] [bigint] NULL,
	[TotalSuperSeniorWinLoss] [bigint] NULL,
	[SeniorID] [bigint] NULL,
	[TotalSeniorWinLoss] [bigint] NULL,
	[SuperMasterID] [bigint] NULL,
	[TotalSuperMasterWinLoss] [bigint] NULL,
	[MasterID] [bigint] NULL,
	[TotalMasterWinLoss] [bigint] NULL,
	[SuperAgentID] [bigint] NULL,
	[TotalSuperAgentWinLoss] [bigint] NULL,
	[AgentID] [bigint] NULL,
	[TotalAgentWinLoss] [bigint] NULL,
	[GrossComm] [bigint] NULL,
	[CommAmount] [bigint] NULL,
	[AdminCmmGross] [bigint] NULL,
	[AdminCmmAmount] [bigint] NULL,
	[SuperShareHolderCmmGross] [bigint] NULL,
	[SuperShareHolderCmmAmount] [bigint] NULL,
	[ShareHolderCmmGross] [bigint] NULL,
	[ShareHolderCmmAmount] [bigint] NULL,
	[SuperSeniorCmmGross] [bigint] NULL,
	[SuperSeniorCmmAmount] [bigint] NULL,
	[SeniorCmmGross] [bigint] NULL,
	[SeniorCmmAmount] [bigint] NULL,
	[SuperMasterCmmGross] [bigint] NULL,
	[SuperMasterCmmAmount] [bigint] NULL,
	[MasterCmmGross] [bigint] NULL,
	[MasterCmmAmount] [bigint] NULL,
	[SuperAgentCmmGross] [bigint] NULL,
	[SuperAgentCmmAmount] [bigint] NULL,
	[AgentCmmGross] [bigint] NULL,
	[AgentCmmAmount] [bigint] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_GameTransactionSummary] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GeneralTable]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GeneralTable](
	[ID] [bigint] NOT NULL,
	[TableName] [varchar](100) NOT NULL,
	[FeildName] [varchar](100) NOT NULL,
	[Key] [varchar](50) NULL,
	[Value] [varchar](50) NULL,
	[Setting] [nvarchar](500) NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_GeneralTable] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GroupPermission]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GroupPermission](
	[ID] [bigint] NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_GroupPermission] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Module]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Module](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[DisplayName] [nvarchar](200) NOT NULL,
	[Enable] [bit] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Module] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Permission]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Permission](
	[GroupPermissionID] [bigint] NOT NULL,
	[ModuleID] [bigint] NOT NULL,
	[RoleID] [bigint] NOT NULL,
	[Checked] [bit] NOT NULL,
	[Description] [nvarchar](200) NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[GroupPermissionID] ASC,
	[ModuleID] ASC,
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Person]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Person](
	[ID] [bigint] NOT NULL,
	[Username] [varchar](200) NOT NULL,
	[Firstname] [nvarchar](200) NULL,
	[Lastname] [nvarchar](200) NULL,
	[Password] [varchar](200) NOT NULL,
	[DOB] [date] NULL,
	[Address] [nvarchar](200) NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Status] [varchar](10) NULL,
	[CountryCode] [varchar](50) NULL,
	[CurrencyCode] [varchar](10) NULL,
	[Mobile] [varchar](50) NULL,
	[Phone] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[LastLoginTime] [datetime] NULL,
	[LastLoginIP] [varchar](50) NULL,
	[IsExternal] [bit] NULL,
	[AutoTransfer] [bit] NULL,
	[LimitIP] [varchar](100) NULL,
	[DisplayName] [varchar](2000) NULL,
	[GroupID] [bigint] NULL,
	[UplineID] [bigint] NULL,
	[CreateAgency] [bit] NOT NULL default ((1)),
	[CreateMember]  [bit] NOT NULL default ((1)),
	[FixedCurrency]  [bit] NOT NULL default ((1)),	

	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonBalance]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersonBalance](
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[Available] [bigint] NOT NULL,
	[YesterdayAvailable] [bigint] NOT NULL,
	[LastUpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_PersonBalance] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonBetSetting]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersonBetSetting](
	[PersonID] [bigint] NOT NULL,
	[BetTypeItemID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[SettingType] [varchar](100) NOT NULL,
	[Value] [bigint] NULL,
	[ValueString] [varchar](50) NULL,
	[UpdatedBy] [nvarchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_PersonBetSetting] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC,
	[BetTypeItemID] ASC,
	[SettingType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonCreditSetting]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersonCreditSetting](
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[PersonType] [varchar](50) NOT NULL,
	[MaxCredit] [bigint] NOT NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_PersonCreditSetting] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonData]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonData](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[Type] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[UpdatedBy] [nvarchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_PersonData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersonGroup]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersonGroup](
	[PersonID] [bigint] NOT NULL,
	[GroupPermissionID] [bigint] NOT NULL,
	[UpdatedBy] [nvarchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_PersonGroup] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC,
	[GroupPermissionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonUpline]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[PersonUpline](
	[PersonID] [bigint] NOT NULL,
	[AdminID] [bigint] NULL,
	[ShareHolderID] [bigint] NULL,
	[SuperSeniorID] [bigint] NULL,
	[SeniorID] [bigint] NULL,
	[MasterID] [bigint] NULL,
	[AgentID] [bigint] NULL,
	[SuperShareHolderID] [bigint] NULL,
	[SuperMasterID] [bigint] NULL,
	[SuperAgentID] [bigint] NULL,
	[UpdatedBy] [nvarchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_PersonUpline] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PositionTaking]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PositionTaking](
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[BetTypeItemID] [bigint] NOT NULL,
	[Type] [varchar](100) NOT NULL,
	[Value] [bigint] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_PositionTaking] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC,
	[BetTypeItemID] ASC,
	[Type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Role]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[UpdatedBy] [nvarchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Statement]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Statement](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[RelatedPersonID] [bigint] NOT NULL,
	[Type] [varchar](100) NOT NULL,
	[Time] [datetime] NOT NULL,
	[Ticks] [bigint] NOT NULL,
	[Amount] [bigint] NOT NULL,
	[PointRate] [money] NOT NULL,
	[Detail] [nvarchar](500) NULL,
	[UpdatedBy] [nvarchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Statement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 5/3/2017 6:54:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transaction](
	[ID] [bigint] NOT NULL,
	[GameSessionID] [bigint] NOT NULL,
	[PersonID] [bigint] NOT NULL,
	[CurrencyCode] [varchar](10) NOT NULL,
	[PointRate] [money] NOT NULL,
	[BetTypeItemID] [bigint] NOT NULL,
	[Type] [varchar](50) NULL,
	[Amount] [bigint] NOT NULL,
	[Result] [bigint] NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Status] [varchar](10) NOT NULL,
	[IpAddress] [varchar](100) NULL,
	[Signature] [varchar](500) NOT NULL,
	[Time] [datetime] NOT NULL,	
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UN_Transaction_Signature] UNIQUE NONCLUSTERED 
(
	[Signature] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Announcement] ADD  CONSTRAINT [DF_Announcement_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Announcement] ADD  CONSTRAINT [DF_Announcement_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Audit] ADD  CONSTRAINT [DF_Audit_Time]  DEFAULT (getdate()) FOR [Time]
GO
ALTER TABLE [dbo].[Audit] ADD  CONSTRAINT [DF_Audit_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[BetType] ADD  CONSTRAINT [DF_BetType_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[BetTypeItem] ADD  CONSTRAINT [DF_BetTypeItem_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Commission] ADD  CONSTRAINT [DF_Commission_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Commission] ADD  CONSTRAINT [DF_Commission_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[CommissionType] ADD  CONSTRAINT [DF_CommissionType_DefaultRate]  DEFAULT ((0)) FOR [DefaultRate]
GO
ALTER TABLE [dbo].[CommissionType] ADD  CONSTRAINT [DF_CommissionType_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[CommissionType] ADD  CONSTRAINT [DF_CommissionType_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Configuration] ADD  CONSTRAINT [DF_Configuration_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Configuration] ADD  CONSTRAINT [DF_Configuration_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Currency] ADD  CONSTRAINT [DF_Currency_Enable]  DEFAULT ((0)) FOR [Enable]
GO
ALTER TABLE [dbo].[Currency] ADD  CONSTRAINT [DF_Currency_PointRate]  DEFAULT ((1)) FOR [PointRate]
GO
ALTER TABLE [dbo].[Currency] ADD  CONSTRAINT [DF_Currency_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Currency] ADD  CONSTRAINT [DF_Currency_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[CurrencyHistory] ADD  CONSTRAINT [DF_CurrencyHistory_Enable]  DEFAULT ((0)) FOR [Enable]
GO
ALTER TABLE [dbo].[CurrencyHistory] ADD  CONSTRAINT [DF_CurrencyHistory_PointRate]  DEFAULT ((1)) FOR [PointRate]
GO
ALTER TABLE [dbo].[CurrencyHistory] ADD  CONSTRAINT [DF_CurrencyHistory_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[CurrencyHistory] ADD  CONSTRAINT [DF_CurrencyHistory_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Game] ADD  DEFAULT ((0)) FOR [PointRate]
GO
ALTER TABLE [dbo].[Game] ADD  CONSTRAINT [DF_Game_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Game] ADD  CONSTRAINT [DF_Game_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GameCategory] ADD  CONSTRAINT [DF_GameCategory_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GameCategory] ADD  CONSTRAINT [DF_GameCategory_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[GameSession] ADD  CONSTRAINT [DF_GameSession_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[GameSession] ADD  CONSTRAINT [DF_GameSession_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GameSetting] ADD  DEFAULT ((0)) FOR [IsHot]
GO
ALTER TABLE [dbo].[GameSetting] ADD  DEFAULT ((1)) FOR [IsNew]
GO
ALTER TABLE [dbo].[GameSetting] ADD  CONSTRAINT [DF_GameSetting_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[GameSetting] ADD  CONSTRAINT [DF_GameSetting_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GameTransaction] ADD  DEFAULT ((0)) FOR [GameTransactionID]
GO
ALTER TABLE [dbo].[GameTransaction] ADD  CONSTRAINT [DF_GameTransaction_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GameTransactionCommission] ADD  CONSTRAINT [DF_GameTransactionCommission_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[GameTransactionCommission] ADD  CONSTRAINT [DF_GameTransactionCommission_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GameTransactionPositionTaking] ADD  CONSTRAINT [DF_GameTransactionPositionTaking_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[GameTransactionPositionTaking] ADD  CONSTRAINT [DF_GameTransactionPositionTaking_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GameTransactionSummary] ADD  CONSTRAINT [DF_GameTransactionSummary_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[GameTransactionSummary] ADD  CONSTRAINT [DF_GameTransactionSummary_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GeneralTable] ADD  CONSTRAINT [DF_GeneralTable_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[GeneralTable] ADD  CONSTRAINT [DF_GeneralTable_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[GroupPermission] ADD  CONSTRAINT [DF_GroupPermission_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[GroupPermission] ADD  CONSTRAINT [DF_GroupPermission_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Module] ADD  CONSTRAINT [DF_Module_Enable]  DEFAULT ((0)) FOR [Enable]
GO
ALTER TABLE [dbo].[Module] ADD  CONSTRAINT [DF_Module_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Module] ADD  CONSTRAINT [DF_Module_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Permission] ADD  CONSTRAINT [DF_Permission_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Permission] ADD  CONSTRAINT [DF_Permission_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Person] ADD  CONSTRAINT [DF_Person_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Person] ADD  CONSTRAINT [DF_Person_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PersonBalance] ADD  CONSTRAINT [DF_PersonBalance_LastUpdatedDate]  DEFAULT (getdate()) FOR [LastUpdatedDate]
GO
ALTER TABLE [dbo].[PersonBalance] ADD  CONSTRAINT [DF_PersonBalance_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[PersonBalance] ADD  CONSTRAINT [DF_PersonBalance_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PersonBetSetting] ADD  CONSTRAINT [DF_PersonBetSetting_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[PersonBetSetting] ADD  CONSTRAINT [DF_PersonBetSetting_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PersonCreditSetting] ADD  CONSTRAINT [DF_PersonCreditSetting_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[PersonCreditSetting] ADD  CONSTRAINT [DF_PersonCreditSetting_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PersonData] ADD  CONSTRAINT [DF_PersonData_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[PersonData] ADD  CONSTRAINT [DF_PersonData_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PersonGroup] ADD  CONSTRAINT [DF_PersonGroup_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[PersonGroup] ADD  CONSTRAINT [DF_PersonGroup_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PersonUpline] ADD  CONSTRAINT [DF_PersonUpline_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[PersonUpline] ADD  CONSTRAINT [DF_PersonUpline_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[PositionTaking] ADD  CONSTRAINT [DF_PositionTaking_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[PositionTaking] ADD  CONSTRAINT [DF_PositionTaking_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Role] ADD  CONSTRAINT [DF_Role_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Role] ADD  CONSTRAINT [DF_Role_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Statement] ADD  CONSTRAINT [DF_Statement_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Transaction] ADD  CONSTRAINT [DF_Transaction_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Transaction] ADD  CONSTRAINT [DF_Transaction_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[BetTypeItem]  WITH CHECK ADD  CONSTRAINT [FK_BetTypeItem_BetType] FOREIGN KEY([BetTypeID])
REFERENCES [dbo].[BetType] ([ID])
GO
ALTER TABLE [dbo].[BetTypeItem] CHECK CONSTRAINT [FK_BetTypeItem_BetType]
GO
ALTER TABLE [dbo].[Commission]  WITH CHECK ADD  CONSTRAINT [FK_Commission_BetType] FOREIGN KEY([BetTypeID])
REFERENCES [dbo].[BetType] ([ID])
GO
ALTER TABLE [dbo].[Commission] CHECK CONSTRAINT [FK_Commission_BetType]
GO
ALTER TABLE [dbo].[Commission]  WITH CHECK ADD  CONSTRAINT [FK_Commission_BetTypeItem] FOREIGN KEY([BetTypeItemID])
REFERENCES [dbo].[BetTypeItem] ([ID])
GO
ALTER TABLE [dbo].[Commission] CHECK CONSTRAINT [FK_Commission_BetTypeItem]
GO
ALTER TABLE [dbo].[Commission]  WITH CHECK ADD  CONSTRAINT [FK_Commission_CommissionType] FOREIGN KEY([CommissionTypeID])
REFERENCES [dbo].[CommissionType] ([ID])
GO
ALTER TABLE [dbo].[Commission] CHECK CONSTRAINT [FK_Commission_CommissionType]
GO
ALTER TABLE [dbo].[Commission]  WITH CHECK ADD  CONSTRAINT [FK_Commission_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO
ALTER TABLE [dbo].[Commission] CHECK CONSTRAINT [FK_Commission_Currency]
GO
ALTER TABLE [dbo].[Commission]  WITH CHECK ADD  CONSTRAINT [FK_Commission_Game] FOREIGN KEY([GameID])
REFERENCES [dbo].[Game] ([ID])
GO
ALTER TABLE [dbo].[Commission] CHECK CONSTRAINT [FK_Commission_Game]
GO
ALTER TABLE [dbo].[Commission]  WITH CHECK ADD  CONSTRAINT [FK_Commission_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[Commission] CHECK CONSTRAINT [FK_Commission_Person]
GO
ALTER TABLE [dbo].[Game]  WITH CHECK ADD  CONSTRAINT [FK_Game_BetTypeItem] FOREIGN KEY([BetTypeItemID])
REFERENCES [dbo].[BetTypeItem] ([ID])
GO
ALTER TABLE [dbo].[Game] CHECK CONSTRAINT [FK_Game_BetTypeItem]
GO
ALTER TABLE [dbo].[Game]  WITH CHECK ADD  CONSTRAINT [FK_Game_GameCategory] FOREIGN KEY([GroupID])
REFERENCES [dbo].[GameCategory] ([ID])
GO
ALTER TABLE [dbo].[Game] CHECK CONSTRAINT [FK_Game_GameCategory]
GO
ALTER TABLE [dbo].[GameSession]  WITH CHECK ADD  CONSTRAINT [FK_GameSession_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[GameSession] CHECK CONSTRAINT [FK_GameSession_Person]
GO
ALTER TABLE [dbo].[GameSetting]  WITH CHECK ADD  CONSTRAINT [FK_GameSetting_Game] FOREIGN KEY([GameID])
REFERENCES [dbo].[Game] ([ID])
GO
ALTER TABLE [dbo].[GameSetting] CHECK CONSTRAINT [FK_GameSetting_Game]
GO
ALTER TABLE [dbo].[GameTransaction]  WITH CHECK ADD  CONSTRAINT [FK_GameTransaction_Game] FOREIGN KEY([GameID])
REFERENCES [dbo].[Game] ([ID])
GO
ALTER TABLE [dbo].[GameTransaction] CHECK CONSTRAINT [FK_GameTransaction_Game]
GO
ALTER TABLE [dbo].[GameTransaction]  WITH CHECK ADD  CONSTRAINT [FK_GameTransaction_GameSession] FOREIGN KEY([GameSessionID])
REFERENCES [dbo].[GameSession] ([ID])
GO
ALTER TABLE [dbo].[GameTransaction] CHECK CONSTRAINT [FK_GameTransaction_GameSession]
GO
ALTER TABLE [dbo].[GameTransaction]  WITH CHECK ADD  CONSTRAINT [FK_GameTransaction_Transaction] FOREIGN KEY([TransactionID])
REFERENCES [dbo].[Transaction] ([ID])
GO
ALTER TABLE [dbo].[GameTransaction] CHECK CONSTRAINT [FK_GameTransaction_Transaction]
GO
ALTER TABLE [dbo].[GameTransactionCommission]  WITH CHECK ADD  CONSTRAINT [FK_GameTransactionCommission_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO
ALTER TABLE [dbo].[GameTransactionCommission] CHECK CONSTRAINT [FK_GameTransactionCommission_Currency]
GO
ALTER TABLE [dbo].[GameTransactionCommission]  WITH CHECK ADD  CONSTRAINT [FK_GameTransactionCommission_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[GameTransactionCommission] CHECK CONSTRAINT [FK_GameTransactionCommission_Person]
GO
ALTER TABLE [dbo].[GameTransactionCommission]  WITH CHECK ADD  CONSTRAINT [FK_GameTransactionCommission_Transaction] FOREIGN KEY([TransactionID])
REFERENCES [dbo].[Transaction] ([ID])
GO
ALTER TABLE [dbo].[GameTransactionCommission] CHECK CONSTRAINT [FK_GameTransactionCommission_Transaction]
GO
ALTER TABLE [dbo].[GameTransactionPositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_GameTransactionPositionTaking_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO
ALTER TABLE [dbo].[GameTransactionPositionTaking] CHECK CONSTRAINT [FK_GameTransactionPositionTaking_Currency]
GO
ALTER TABLE [dbo].[GameTransactionPositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_GameTransactionPositionTaking_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[GameTransactionPositionTaking] CHECK CONSTRAINT [FK_GameTransactionPositionTaking_Person]
GO
ALTER TABLE [dbo].[GameTransactionPositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_GameTransactionPositionTaking_Transaction] FOREIGN KEY([TransactionID])
REFERENCES [dbo].[Transaction] ([ID])
GO
ALTER TABLE [dbo].[GameTransactionPositionTaking] CHECK CONSTRAINT [FK_GameTransactionPositionTaking_Transaction]
GO
ALTER TABLE [dbo].[Permission]  WITH CHECK ADD  CONSTRAINT [FK_Permission_GroupPermission] FOREIGN KEY([GroupPermissionID])
REFERENCES [dbo].[GroupPermission] ([ID])
GO
ALTER TABLE [dbo].[Permission] CHECK CONSTRAINT [FK_Permission_GroupPermission]
GO
ALTER TABLE [dbo].[Permission]  WITH CHECK ADD  CONSTRAINT [FK_Permission_Module] FOREIGN KEY([ModuleID])
REFERENCES [dbo].[Module] ([ID])
GO
ALTER TABLE [dbo].[Permission] CHECK CONSTRAINT [FK_Permission_Module]
GO
ALTER TABLE [dbo].[Permission]  WITH CHECK ADD  CONSTRAINT [FK_Permission_Role] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Role] ([ID])
GO
ALTER TABLE [dbo].[Permission] CHECK CONSTRAINT [FK_Permission_Role]
GO
ALTER TABLE [dbo].[PersonBalance]  WITH CHECK ADD  CONSTRAINT [FK_PersonBalance_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[PersonBalance] CHECK CONSTRAINT [FK_PersonBalance_Person]
GO
ALTER TABLE [dbo].[PersonBetSetting]  WITH CHECK ADD  CONSTRAINT [FK_PersonBetSetting_BetTypeItem] FOREIGN KEY([BetTypeItemID])
REFERENCES [dbo].[BetTypeItem] ([ID])
GO
ALTER TABLE [dbo].[PersonBetSetting] CHECK CONSTRAINT [FK_PersonBetSetting_BetTypeItem]
GO
ALTER TABLE [dbo].[PersonBetSetting]  WITH CHECK ADD  CONSTRAINT [FK_PersonBetSetting_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO
ALTER TABLE [dbo].[PersonBetSetting] CHECK CONSTRAINT [FK_PersonBetSetting_Currency]
GO
ALTER TABLE [dbo].[PersonBetSetting]  WITH CHECK ADD  CONSTRAINT [FK_PersonBetSetting_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[PersonBetSetting] CHECK CONSTRAINT [FK_PersonBetSetting_Person]
GO
ALTER TABLE [dbo].[PersonCreditSetting]  WITH CHECK ADD  CONSTRAINT [FK_PersonCreditSetting_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO
ALTER TABLE [dbo].[PersonCreditSetting] CHECK CONSTRAINT [FK_PersonCreditSetting_Currency]
GO
ALTER TABLE [dbo].[PersonCreditSetting]  WITH CHECK ADD  CONSTRAINT [FK_PersonCreditSetting_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[PersonCreditSetting] CHECK CONSTRAINT [FK_PersonCreditSetting_Person]
GO
ALTER TABLE [dbo].[PersonData]  WITH CHECK ADD  CONSTRAINT [FK_PersonData_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[PersonData] CHECK CONSTRAINT [FK_PersonData_Person]
GO
ALTER TABLE [dbo].[PersonGroup]  WITH CHECK ADD  CONSTRAINT [FK_PersonGroup_GroupPermission] FOREIGN KEY([GroupPermissionID])
REFERENCES [dbo].[GroupPermission] ([ID])
GO
ALTER TABLE [dbo].[PersonGroup] CHECK CONSTRAINT [FK_PersonGroup_GroupPermission]
GO
ALTER TABLE [dbo].[PersonGroup]  WITH CHECK ADD  CONSTRAINT [FK_PersonGroup_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[PersonGroup] CHECK CONSTRAINT [FK_PersonGroup_Person]
GO
ALTER TABLE [dbo].[PositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_PositionTaking_BetTypeItem] FOREIGN KEY([BetTypeItemID])
REFERENCES [dbo].[BetTypeItem] ([ID])
GO
ALTER TABLE [dbo].[PositionTaking] CHECK CONSTRAINT [FK_PositionTaking_BetTypeItem]
GO
ALTER TABLE [dbo].[PositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_PositionTaking_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO
ALTER TABLE [dbo].[PositionTaking] CHECK CONSTRAINT [FK_PositionTaking_Currency]
GO
ALTER TABLE [dbo].[PositionTaking]  WITH CHECK ADD  CONSTRAINT [FK_PositionTaking_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[PositionTaking] CHECK CONSTRAINT [FK_PositionTaking_Person]
GO
ALTER TABLE [dbo].[Statement]  WITH CHECK ADD  CONSTRAINT [FK_Statement_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO
ALTER TABLE [dbo].[Statement] CHECK CONSTRAINT [FK_Statement_Currency]
GO
ALTER TABLE [dbo].[Statement]  WITH CHECK ADD  CONSTRAINT [FK_Statement_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[Statement] CHECK CONSTRAINT [FK_Statement_Person]
GO
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_BetTypeItem] FOREIGN KEY([BetTypeItemID])
REFERENCES [dbo].[BetTypeItem] ([ID])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_BetTypeItem]
GO
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Currency] FOREIGN KEY([CurrencyCode])
REFERENCES [dbo].[Currency] ([CurrencyCode])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Currency]
GO
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_GameSession] FOREIGN KEY([GameSessionID])
REFERENCES [dbo].[GameSession] ([ID])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_GameSession]
GO
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Person] FOREIGN KEY([PersonID])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Person]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Broadcast; Announcement: on each person' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Announcement', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hot; New; CommingSoon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Game', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hot; New; CommingSoon' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GameSetting', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Admin; ShareHolder; SuperSenior; Senior; Master; Agent; Member; Robot' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Person', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Actived; Suspended; Disabled; Locked' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Person', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'VN,CN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Person', @level2type=N'COLUMN',@level2name=N'CountryCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DailyAutoReset; MaxWinLimit; MaxLossLimit; BetLimit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PersonBetSetting', @level2type=N'COLUMN',@level2name=N'SettingType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Auto; Upline; CurrentMax; CurrentMin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PositionTaking', @level2type=N'COLUMN',@level2name=N'Type'
GO
